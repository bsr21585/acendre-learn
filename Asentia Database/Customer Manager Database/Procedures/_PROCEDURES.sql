﻿-- =====================================================================
-- PROCEDURE: [Account.DeleteDomain]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Account.DeleteDomain]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Account.DeleteDomain]
GO

/*

Delete the selected domain alias of a specific site

*/

CREATE PROCEDURE [Account.DeleteDomain]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerAccount		INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,
	
	@domainAliass			NVARCHAR(255)
)
AS

	BEGIN
	SET NOCOUNT ON


	/*
	 DELETE the site to domain alias link

	*/

	DELETE FROM tblAccountToDomainAliasLink
	WHERE domain	=	@domainAliass 
	AND (hostname IS NULL OR hostname = '')
	AND idAccount = @idCallerAccount
		
		
		SET @Return_Code = 0
		SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
	
-- =====================================================================
-- PROCEDURE: [Account.Delete]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Account.Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Account.Delete]
GO

CREATE PROCEDURE [Account.Delete]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerAccount		INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT,
	
	@Accounts				IDTable			READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	IF (SELECT COUNT(1) FROM @Accounts) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'AccountDelete_NoRecordsSelected'
		RETURN 1
		END
		
	IF @idCallerAccount <> 1
		BEGIN
		SET @Return_Code = 3 -- caller permission error
		SET @Error_Description_Code = 'AccountDelete_PermissionError'
		RETURN 1
		END
		
	/*
	
	Mark the account(s) as deleted. DO NOT remove the record.
	
	*/
	
	UPDATE tblAccount SET
		isDeleted = 1
	WHERE idAccount IN (
		SELECT id
		FROM @Accounts
	)
	
	SELECT @Return_Code = 0
		
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
-- =====================================================================
-- PROCEDURE: [Account.Details]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[Account.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Account.Details]
GO

/*
Return all the properties for a given object id.
*/
CREATE PROCEDURE [Account.Details]
(
	@Return_Code		INT					OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerAccount	INT					= 0, --default if not specified
	@callerLangString	NVARCHAR (10),
	@idCaller			INT,
	
	@idAccount			INT					OUTPUT, 
	@idDatabaseServer	INT					OUTPUT, 
	@company			NVARCHAR(255)		OUTPUT,
	@contactFirstName	NVARCHAR(255)		OUTPUT,
	@contactLastName	NVARCHAR(255)		OUTPUT,
	@contactDisplayName	NVARCHAR(512)		OUTPUT,
	@contactEmail		NVARCHAR(255)		OUTPUT,
	@username			NVARCHAR(512)		OUTPUT,
	@password			NVARCHAR(512)		OUTPUT,
	@isActive			BIT					OUTPUT,
	@isDeleted			BIT					OUTPUT,
	@databaseName		NVARCHAR(512)		OUTPUT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	/* validate caller permission */
		
	DECLARE @idCallerRole INT
	SELECT @idCallerRole = 0
	SELECT @idCallerRole = idRole FROM tblAccountUser WHERE idAccountUser = @idCaller		
	
	IF @idCallerRole <> 1 AND @idCallerAccount <> @idAccount
		BEGIN
		SELECT @Return_Code = 3 -- caller permission error
		RETURN 1
		END
		
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblAccount
		WHERE idAccount = @idAccount
		) = 0 
		BEGIN
		SET @Return_Code = 1
		RETURN 1
		END
	
	/*
	
	get the data 
	
	*/
	
	SELECT
		@idAccount = A.idAccount, 
		@idDatabaseserver = A.idDatabaseServer, 
		@company = A.company,
		@contactFirstName = A.contactFirstName,
		@contactLastName = A.contactLastName,
		@contactDisplayName = A.contactDisplayName,
		@contactEmail = A.contactEmail,
		@username = A.username,
		@password = A.[password],
		@isActive = A.isActive,
		@isDeleted = A.isDeleted,
		@databaseName = A.databaseName
	FROM tblAccount A
	WHERE A.idAccount = @idAccount
	
	SELECT @Return_Code = 0
		
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
-- =====================================================================
-- PROCEDURE: [Account.DoesHostNameExists]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Account.DoesHostNameExists]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Account.DoesHostNameExists]
GO

/*

Determines if the current session Site alredy has a host name with same name.

*/
CREATE PROCEDURE [Account.DoesHostNameExists]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerAccount		INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,

	@hostName				NVARCHAR (255),
	@isHostNameAlredyExists	BIT				OUTPUT
)
AS

	BEGIN
	SET NOCOUNT ON
	
	/*
	
	Determines if the current session Site alredy has a host name with same name.
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblAccountToDomainAliasLink ATDAL
		WHERE (ATDAL.hostname = @hostName 
		OR ATDAL.domain = @hostName )
		AND @idCallerAccount <> ATDAL.idAccount
		
		) = 0 
		BEGIN
		SET @isHostNameAlredyExists = 0
		END
	ELSE
		BEGIN
		SET @isHostNameAlredyExists = 1
		END
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
-- =====================================================================
-- PROCEDURE: [Account.GetDatabaseList]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Account.GetDatabaseList]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Account.GetDatabaseList]
GO


/*
Return all database ids and names.
*/
CREATE PROCEDURE [Account.GetDatabaseList]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerAccount			INT				= 0, --default if not specified
	@idDatabaseServer			INT,
	@callerLangString			NVARCHAR(10),
	@idCaller					INT
)
AS
	BEGIN
	SET NOCOUNT ON
		
	/*
	
	get the database details
	
	*/
	
	SELECT DISTINCT
		databaseName,
		idAccount
	FROM tblAccount  
	WHERE idDatabaseServer = @idDatabaseServer
	ORDER BY databaseName
	
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

-- =====================================================================
-- PROCEDURE: [Account.GetDomains]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Account.GetDomains]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Account.GetDomains]
GO

/*

Returns a recordset of  ids and names that are domains of a hostname or account.

*/

CREATE PROCEDURE [Account.GetDomains]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerAccount		INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,
	
	@idAccount              INT
)
AS

	BEGIN
	SET NOCOUNT ON

		/*
		Gets the list of domain aaliases 
		*/
		
	
		SELECT
			DISTINCT
			ATDAL.idAccountToDomainAliasLink AS id, 
			ATDAL.domain AS displayName
		FROM tblAccountToDomainAliasLink ATDAL
		WHERE ATDAL.idAccount = @idAccount
		AND (ATDAL.hostname IS NULL OR ATDAL.hostname=' ')
		
		ORDER BY displayName

		
		SET @Return_Code = 0
		SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
	
-- =====================================================================
-- PROCEDURE: [Account.GetGrid]

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[Account.GetGrid]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Account.GetGrid]
GO

CREATE PROCEDURE [Account.GetGrid]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerAccount		INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT,
	
	@searchParam			NVARCHAR(4000),
	@pageNum				INT, 
	@pageSize				INT, 
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT
)
AS
	BEGIN
		SET NOCOUNT ON
		
		/* get caller permission */
		
		DECLARE @idUserRole INT
		SELECT @idUserRole = idRole FROM tblAccountUser WHERE idAccountUser = @idCaller		
		
		IF @searchParam = '' OR @searchParam = '*'
			BEGIN
			SET @searchParam = null
			END

		IF @searchParam IS NULL
			
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblAccount
			WHERE @idUserRole = 1
			AND idAccount <> 1
			AND (isDeleted IS NULL OR isDeleted = 0)
			
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						idAccount,
						ROW_NUMBER() OVER (ORDER BY 
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN company END DESC,
							
							-- FIRST ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN company END
						)
						AS [row_number]
					FROM tblAccount
					WHERE @idUserRole = 1
					AND idAccount <> 1
					AND (isDeleted IS NULL OR isDeleted = 0)

				), 
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idAccount, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				A.idAccount, 
				A.company, 
				A.contactDisplayName,
				A.contactEmail,
				A.isActive,
				CONVERT(BIT, 1) AS isLogonOn,
				CONVERT(BIT, 1) AS isModifyOn,
				SelectedKeys.[row_number]		
			FROM SelectedKeys
			JOIN tblAccount A ON A.idAccount = SelectedKeys.idAccount
			WHERE (A.isDeleted IS NULL OR A.isDeleted = 0)
			ORDER BY SelectedKeys.[row_number]
			
			END
			
		ELSE
			
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblAccount
			INNER JOIN CONTAINSTABLE(tblAccount, *, @searchParam) K ON K.[key] = tblAccount.idAccount
			WHERE @idUserRole = 1
			AND idAccount <> 1
			AND (isDeleted IS NULL OR isDeleted = 0)
			
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						idAccount,
						ROW_NUMBER() OVER (ORDER BY 
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN idAccount END DESC,
							
							-- FIRST ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN idAccount END
						)
						AS [row_number]
					FROM tblAccount
					INNER JOIN CONTAINSTABLE(tblAccount, *, @searchParam) K ON K.[key] = tblAccount.idAccount
					WHERE @idUserRole = 1
					AND idAccount <> 1
				), 
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idAccount, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				A.idAccount, 
				A.company, 
				A.contactDisplayName,
				A.contactEmail,
				A.isActive,
				CONVERT(BIT, 1) AS isLogonOn,
				CONVERT(BIT, 1) AS isModifyOn,
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN tblAccount A ON A.idAccount = SelectedKeys.idAccount
			WHERE (A.isDeleted IS NULL OR A.isDeleted = 0)
			ORDER BY SelectedKeys.[row_number]
			
			END
		
	END
GO



-- =====================================================================
-- PROCEDURE: [Account.SaveDomainAlias]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Account.SaveDomainAlias]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Account.SaveDomainAlias]
GO

/*

Insert account to domain alis link

*/

CREATE PROCEDURE [Account.SaveDomainAlias]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerAccount		INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,

	@domainAlias			NVARCHAR (255),
	@idAccount				INT		
		
)
AS

	BEGIN
	SET NOCOUNT ON
	


	
	/*
	validate uniqueness for domain alias, if specified
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblAccountToDomainAliasLink ATDAL
		WHERE ( ATDAL.domain = @domainAlias
		OR ATDAL.hostname = @domainAlias)
		) > 0
		BEGIN
		SELECT @Return_Code = 2
		SET @Error_Description_Code = 'AccountSaveDomainAlias_DomainAliasNotUnique'		 
		RETURN 1 
		END
	

	/*
	
    Save site to domain alis link
	
	*/
	
		INSERT INTO tblAccountToDomainAliasLink (
			idAccount,
			hostname,
			domain
		)   
		SELECT
			@idAccount,
			NULL,
			@domainAlias
		WHERE NOT EXISTS (
			SELECT 1
			FROM tblAccountToDomainAliasLink ATDAL
			WHERE ATDAL.idAccount = @idAccount
			AND ATDAL.domain = @domainAlias
		)
		
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
-- =====================================================================
-- PROCEDURE: [Account.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Account.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Account.Save]
GO

CREATE PROCEDURE [Account.Save]
(
	@Return_Code				INT					OUTPUT,
	@Error_Description_Code		NVARCHAR(50)		OUTPUT,
	@idCallerAccount			INT					= 0, -- default if not specified
	@callerLangString			NVARCHAR (10),
	@idCaller					INT					= 0, -- will fail if not specified
	
	@idAccount					INT					OUTPUT,
	@idDatabaseServer			INT					OUTPUT, 
	@company					NVARCHAR(255),
	@contactFirstName			NVARCHAR(255),
	@contactLastName			NVARCHAR(255),
	@contactEmail				NVARCHAR(255),
	@username					NVARCHAR(255),
	@password					NVARCHAR(512),
	@isActive					BIT,
	@isDeleted					BIT,
	@databaseName				NVARCHAR(512)
)
AS
	
	BEGIN
	SET NOCOUNT ON

	/* validate caller permission */
		
	DECLARE @idCallerRole INT
	SELECT @idCallerRole = 0
	SELECT @idCallerRole = idRole FROM tblAccountUser WHERE idAccountUser = @idCaller		
	
	IF @idCallerRole <> 1 AND @idCallerAccount <> @idAccount
		BEGIN
		SELECT @Return_Code = 3 -- caller permission error
		RETURN 1
		END
			 
	/*
	
	validate uniqueness (if required)
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblAccount
		WHERE (idAccount <> @idAccount AND username = @username)
		AND (isDeleted IS NULL OR isDeleted = 0)
		
		) > 0
		BEGIN
		SELECT @Return_Code = 2
		SELECT @Error_Description_Code  = 'AccountSave_UserNameNotUnique'
		RETURN 0 
		END
		
	/*
	
	save the data
	
	*/
	
	IF (@idAccount = 0 OR @idAccount is null)
		
		BEGIN

			IF (
					SELECT COUNT(1) 
					FROM tblAccount 
					WHERE (databaseName=@databaseName AND idDatabaseServer=@idDatabaseServer)
					AND (isDeleted IS NULL OR isDeleted = 0)
				) > 0
				BEGIN
			    SELECT @Return_Code = 2
				SELECT @Error_Description_Code  = 'AccountSave_DatabaseNameNotUnique'
				RETURN 0 
				END
		-- insert the new object
		
		INSERT INTO tblAccount (
			idDatabaseServer,
			company,
			contactFirstName,
			contactLastName,
			contactDisplayName,
			contactEmail,
			username,
			[password],
			isActive,
			isDeleted,
			dtCreated,
			databaseName
		)			
		VALUES (
			@idDatabaseServer,
			@company,
			@contactFirstName,
			@contactLastName,
			@contactLastName + ', ' + @contactFirstName,
			@contactEmail,
			@username,
			dbo.GetHashedString('SHA1', @password),
			@isActive,
			@isDeleted,
			GETUTCDATE(),
			@databaseName
		)
		
		-- get the new object id and return successfully
		
		SELECT @idAccount = SCOPE_IDENTITY()
		
		END
		
	ELSE
		
		BEGIN
		
		-- check that the object id exists
		
		IF (SELECT COUNT(1) FROM tblAccount WHERE idAccount = @idAccount) = 0
			
			BEGIN
				SELECT @idAccount = @idAccount
				SELECT @Return_Code = 1
				RETURN 0
			END
			
		-- update existing object's properties
		
		UPDATE tblAccount SET
			company = @company,
			contactFirstName = @contactFirstName,
			contactLastName = @contactLastName,
			contactDisplayName = @contactLastName + ', ' + @contactFirstName,
			contactEmail = @contactEmail,
			username = @username,
			[password] = dbo.GetHashedString('SHA1', @password),
			isActive = @isActive,
			isDeleted = @isDeleted
		WHERE idAccount = @idAccount 
		
		-- get the id and return successfully
		
		SELECT @idAccount = @idAccount
				
		END
	
	SELECT @Return_Code = 0
		
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
-- =====================================================================
-- PROCEDURE: [AccountToDomainAliasLink.Delete]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[AccountToDomainAliasLink.Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [AccountToDomainAliasLink.Delete]
GO

/*

Delete the selected domain alias of a specific site

*/

CREATE PROCEDURE [AccountToDomainAliasLink.Delete]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerAccount		INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,
	
	@domainAliass			NVARCHAR(255)
)
AS

	BEGIN
	SET NOCOUNT ON


	/*
	 DELETE the site to domain alias link

	*/

	DELETE FROM tblAccountToDomainAliasLink
	WHERE (hostname	=	@domainAliass 
	OR  domain =	@domainAliass )
	AND idAccount = @idCallerAccount
		
		
		SET @Return_Code = 0
		SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
	
-- =====================================================================
-- PROCEDURE: [AccountToDomainAliasLink.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[AccountToDomainAliasLink.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [AccountToDomainAliasLink.Save]
GO

/*

Insert account to domain alis link

*/

CREATE PROCEDURE [AccountToDomainAliasLink.Save]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerAccount		INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,

	@hostName				NVARCHAR (255),
	@idAccount				INT		
		
)
AS

	BEGIN
	SET NOCOUNT ON



	

	/*
	
    Save site to domain alis link
	
	*/
	
		INSERT INTO tblAccountToDomainAliasLink (
			idAccount,
			hostname,
			domain
		)   
		SELECT
			@idAccount,
			@hostname,
			NULL
		WHERE NOT EXISTS (
			SELECT 1
			FROM tblAccountToDomainAliasLink ATDAL
			WHERE ATDAL.idAccount = @idAccount
			AND ATDAL.hostname = @hostName
		)

		
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
-- =====================================================================
-- PROCEDURE: [AccountUser.Delete]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[AccountUser.Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [AccountUser.Delete]
GO

CREATE PROCEDURE [AccountUser.Delete]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR (10)	OUTPUT,
	@idCallerAccount			INT				= 0, --default if not specified
	@callerLangString			NVARCHAR (10),
	@idCaller					INT,
	
	@AccountUsers				IDTable			READONLY	
)
AS
	
	BEGIN
	
	/* validate caller permission */
		
	DECLARE @idCallerRole INT
	SELECT @idCallerRole = 0
	SELECT @idCallerRole = idRole FROM tblAccountUser WHERE idAccountUser = @idCaller
	
	DECLARE @isCallerSystemAdmin BIT
	DECLARE @isCallerSystemUser BIT
	DECLARE @isCallerAccountAdmin BIT
	DECLARE @isCallerAccountUser BIT
	
	SELECT @isCallerSystemAdmin =	CASE WHEN @idCallerAccount = 1 
											  AND @idCaller = 1 
											  AND @idCallerRole = 1 
									THEN 1 
									ELSE 0 END
								  
	SELECT @isCallerSystemUser =	CASE WHEN @idCallerAccount = 1 
										      AND @idCaller > 1 
										      AND @idCallerRole = 1 
									THEN 1 
									ELSE 0 END
									
	SELECT @isCallerAccountAdmin =	CASE WHEN @idCallerAccount > 1 
											  AND @idCaller = 1 
											  AND @idCallerRole = 2 
									THEN 1 
									ELSE 0 END
									
	SELECT @isCallerAccountUser =	CASE WHEN @idCallerAccount > 1 
										      AND @idCaller > 1 
										      AND @idCallerRole = 2 
									THEN 1 
									ELSE 0 END
									
	IF -- account users cannot delete any users
	   (@isCallerAccountUser = 1)
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = 'AccountUserDelete_PermissionError'
		RETURN 1
		END
	
	/*
		check to see if there is anything to do
	*/
	
	IF (SELECT COUNT(1) FROM @AccountUsers) = 0
		BEGIN
		SET @Return_Code = 1 --(1 is 'details not found')
		SET @Error_Description_Code = 'AccountUserDelete_NoRecordFound'
		RETURN 1
		END
	
	/* 
	
	validate caller permissions to the objects being deleted 
	if there is any objects that the caller doesn't have permission
	to delete, then no deletes will happen
	
	*/
	
	IF @isCallerSystemAdmin <> 1
	
		BEGIN
		
		IF (
			SELECT COUNT(1)
			FROM @AccountUsers VAU
			LEFT JOIN tblAccountUser AU ON VAU.id = AU.idAccountUser
			WHERE (
					-- system users cannot delete other system users
					(@isCallerSystemUser = 1 AND AU.idAccount = 1)
					OR
					-- account administrators cannot delete users outside of their account
					(@isCallerAccountAdmin = 1 AND AU.idAccount <> @idCallerAccount)
				)
			) > 0 
			
			BEGIN
			SET @Return_Code = 3 -- sort of (caller not member of specified site).
			SET @Error_Description_Code = 'AccountUserDelete_PermissionError'
			RETURN 1
			END
				
		END
		
	/*
	
	Delete the account user(s).
	
	*/
	
	DELETE FROM tblAccountUser
	WHERE EXISTS (
		SELECT 1
		FROM @AccountUsers VAU
		WHERE tblAccountUser.idAccountUser = VAU.id
	)
	
	SELECT @Return_Code = 0
	
	SET NOCOUNT ON
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
-- =====================================================================
-- PROCEDURE: [AccountUser.Details]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[AccountUser.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [AccountUser.Details]
GO

/*
Return all the properties for a given object id.
*/
CREATE PROCEDURE [AccountUser.Details]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		INT				OUTPUT,
	@idCallerAccount			INT				= 0, --default if not specified
	@callerLangString			NVARCHAR (10),
	@idCaller					INT,
	
	@idAccountUser				INT				OUTPUT,
	@idAccount					INT				OUTPUT,
	@firstName					NVARCHAR(255)	OUTPUT,
	@lastName					NVARCHAR(255)	OUTPUT,
	@displayName				NVARCHAR(512)	OUTPUT,
	@email						NVARCHAR(255)	OUTPUT,
	@username					NVARCHAR(512)	OUTPUT,
	@password					NVARCHAR(512)	OUTPUT,
	@idRole						INT				OUTPUT,
	@roleName					NVARCHAR(255)	OUTPUT,
	@isActive					BIT				OUTPUT
)
AS
	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idCallerRole INT
	SELECT @idCallerRole = 0
	SELECT @idCallerRole = idRole FROM tblAccountUser WHERE idAccountUser = @idCaller
	
	DECLARE @isCallerViewingSelf BIT
	DECLARE @isCallerSystemAdmin BIT
	DECLARE @isCallerSystemUser BIT
	DECLARE @isCallerAccountAdmin BIT
	DECLARE @isCallerAccountUser BIT
	
	SELECT @isCallerViewingSelf =	CASE WHEN @idAccountUser = @idCaller THEN 1 ELSE 0 END
	
	SELECT @isCallerSystemAdmin =	CASE WHEN @idCallerAccount = 1 
											  AND @idCaller = 1 
											  AND @idCallerRole = 1 
									THEN 1 
									ELSE 0 END
								  
	SELECT @isCallerSystemUser =	CASE WHEN @idCallerAccount = 1 
										      AND @idCaller > 1 
										      AND @idCallerRole = 1 
									THEN 1 
									ELSE 0 END
									
	SELECT @isCallerAccountAdmin =	CASE WHEN @idCallerAccount > 1 
											  AND @idCaller = 1 
											  AND @idCallerRole = 2 
									THEN 1 
									ELSE 0 END
									
	SELECT @isCallerAccountUser =	CASE WHEN @idCallerAccount > 1 
										      AND @idCaller > 1 
										      AND @idCallerRole = 2 
									THEN 1 
									ELSE 0 END
									
	-- if caller is not viewing self, make sure the
	-- caller is not an account user
	
	IF @isCallerViewingSelf = 0
		BEGIN
		IF -- account users cannot view any other users
		   (@isCallerAccountUser = 1)
			BEGIN
			SET @Return_Code = 3 -- sort of (caller not member of specified site).
			SET @Error_Description_Code = 'AccountUserDetails_PermissionError'
			RETURN 1
			END
		END
		
	/*

	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblAccountUser
		WHERE idAccountUser = @idAccountUser
		) = 0 
		BEGIN
		SET @Return_Code = 1 --(1 is 'details not found')
		SET @Error_Description_Code = 'AccountUserDetails_NoRecordFound'
		RETURN 1
		END
	
	/*
	
	validate caller permission
	
	*/
	
	IF @isCallerSystemAdmin <> 1 OR @isCallerViewingSelf <> 1
	
		BEGIN
		
		IF (
			SELECT COUNT(1)
			FROM tblAccountUser AU 
			WHERE AU.idAccountUser = @idAccountUser
			AND (
					-- system users cannot view other system users
					(@isCallerSystemUser = 1 AND AU.idAccount = 1)
					OR
					-- account administrators cannot view users outside of their account
					(@isCallerAccountAdmin = 1 AND AU.idAccount <> @idCallerAccount)
				)
			) > 0 
			
			BEGIN
			SET @Return_Code = 3 -- sort of (caller not member of specified site).
			SET @Error_Description_Code = 'AccountUserDetails_PermissionError'
			RETURN 1
			END
				
		END
	
	/*
	
	get the data 
	
	*/
	
	SELECT
		@idAccountUser = AU.idAccountUser,
		@idAccount = AU.idAccount,
		@firstName = AU.firstName,
		@lastName = AU.lastName,
		@displayName = AU.displayName,
		@email = AU.email,
		@username = AU.username,
		@password = AU.[password],
		@idRole = AU.idRole,
		@roleName = R.roleName,
		@isActive = AU.isActive
	FROM tblAccountUser AU
	LEFT JOIN tblRole R ON R.idRole = AU.idRole
	WHERE AU.idAccountUser = @idAccountUser
	
	SELECT @Return_Code = 0
		
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
-- =====================================================================
-- PROCEDURE: [AccountUser.GetGrid]

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[AccountUser.GetGrid]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [AccountUser.GetGrid]
GO

CREATE PROCEDURE [AccountUser.GetGrid]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(255)	OUTPUT,
	@idCallerAccount			INT,
	@callerLangString			NVARCHAR (10),
	@idCaller					INT,			-- this is the ID of the user making the grid call.
	@idRole                     INT              =0,
	@searchParam				NVARCHAR(4000),
	@pageNum					INT, 
	@pageSize					INT, 
	@orderColumn				NVARCHAR(255),
	@orderAsc					BIT
)
AS
	BEGIN
		SET NOCOUNT ON

		DECLARE   @IDTable AS IDTable
		IF (@idRole=0)
			BEGIN
		    INSERT INTO @IDTable
		    SELECT idRole FROM tblRole 
			END
		ELSE
			BEGIN
			INSERT INTO @IDTable
			SELECT idRole FROM tblRole WHERE idRole=@idRole 
			END
		IF @searchParam = '' OR @searchParam = '*'
			BEGIN
			SET @searchParam = NULL
			END

		IF @searchParam IS NULL
			
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblAccountUser AU
			
			WHERE 
				(
				((@idCallerAccount IS NULL OR @idCallerAccount = 1) AND @idCaller = 1) 
				OR
				((@idCallerAccount IS NULL OR @idCallerAccount = 1) AND @idCaller > 1 AND AU.idAccount > 1) 
				OR
				(@idCallerAccount > 1 AND @idCaller = 1 AND AU.idAccount = @idCallerAccount)
				)
			AND AU.idAccountUser <> 1 -- not the system admin user
			AND AU.idRole IN (SELECT id FROM @IDTable)
			
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						AU.idAccountUser,
						ROW_NUMBER() OVER (ORDER BY 
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'username' THEN AU.username END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'email' THEN AU.email END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'accountName' THEN A.username END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'roleName' THEN R.roleName END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName', 'roleName') THEN AU.lastname END) END DESC,
							-- SECOND ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('username', 'email', 'accountName', 'roleName') THEN AU.lastname END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName', 'roleName') THEN AU.firstname END) END DESC,
							-- THIRD ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('username', 'email', 'accountName', 'roleName') THEN AU.firstname END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName', 'roleName') THEN A.username END) END DESC,
							-- FOURTH ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('username', 'email') THEN A.username END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email') THEN AU.username END) END DESC,
							-- FIFTH ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('email') THEN AU.username END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('email') THEN AU.email END) END DESC,
							-- SIXTH ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName') THEN R.roleName END) END DESC,
							
							-- FIRST ORDER ASC
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'username' THEN AU.username END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'email' THEN AU.email END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'accountName' THEN A.username END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'roleName' THEN R.roleName END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName', 'roleName') THEN AU.lastname END) END,
							-- SECOND ORDER ASC
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('username', 'email', 'accountName', 'roleName') THEN AU.lastname END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName', 'roleName') THEN AU.firstname END) END,
							-- THIRD ORDER ASC
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('username', 'email', 'accountName', 'roleName') THEN AU.firstname END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName', 'roleName') THEN A.username END) END,
							-- FOURTH ORDER ASC
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('username', 'email') THEN A.username END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email') THEN AU.username END) END,
							-- FIFTH ORDER ASC
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('email') THEN AU.username END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('email') THEN AU.email END) END,
							-- SIXTH ORDER ASC
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName') THEN R.roleName END) END
						)
						AS [row_number]
					FROM tblAccountUser AU
					JOIN tblRole R ON R.idRole = AU.idRole
					JOIN tblAccount A ON A.idAccount = AU.idAccount
					WHERE 
						(
						((@idCallerAccount IS NULL OR @idCallerAccount = 1) AND @idCaller = 1) 
						OR 
						((@idCallerAccount IS NULL OR @idCallerAccount = 1) AND @idCaller > 1 AND AU.idAccount > 1) 
						OR
						(@idCallerAccount > 1 AND @idCaller = 1 AND AU.idAccount = @idCallerAccount)
						)
					AND AU.idAccountUser <> 1 -- not the system admin user
				), 
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idAccountUser, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				AU.idAccountUser, 
				AU.displayName,
				AU.username, 
				AU.email,
				CASE WHEN A.username = '__SYSTEM__' THEN '' ELSE A.username END AS accountName,
				R.roleName,
				AU.isActive,
				SelectedKeys.[row_number],
				'True' AS isModifyOn
			FROM SelectedKeys
			JOIN tblAccountUser AU ON AU.idAccountUser = SelectedKeys.idAccountUser
			JOIN tblRole R ON R.idRole = AU.idRole
			JOIN tblAccount A ON A.idAccount = AU.idAccount  WHERE  AU.idRole IN (SELECT id FROM @IDTable)
			ORDER BY SelectedKeys.[row_number]
			
			END
			
		ELSE
			
			BEGIN
			
			-- return the rowcount
			SELECT COUNT(1) AS row_count FROM
			(
				SELECT idAccountUser
				FROM tblAccountUser AU
				JOIN tblRole R ON R.idRole = AU.idRole
				INNER JOIN CONTAINSTABLE(tblAccountUser, *, @searchParam) K ON K.[key] = AU.idAccountUser
				WHERE 
					(
					((@idCallerAccount IS NULL OR @idCallerAccount = 1) AND @idCaller = 1) 
					OR 
					((@idCallerAccount IS NULL OR @idCallerAccount = 1) AND @idCaller > 1 AND AU.idAccount > 1) 
					OR
					(@idCallerAccount > 1 AND @idCaller = 1 AND AU.idAccount = @idCallerAccount)
					)
				AND AU.idAccountUser <> 1 -- not the system admin user
				AND AU.idRole IN (SELECT id FROM @IDTable)
				UNION
				
				SELECT idAccountUser
				FROM tblAccountUser AU
				INNER JOIN CONTAINSTABLE(tblAccount, *, @searchParam) K ON K.[key] = AU.idAccount
				WHERE 
					(
					((@idCallerAccount IS NULL OR @idCallerAccount = 1) AND @idCaller = 1) 
					OR 
					((@idCallerAccount IS NULL OR @idCallerAccount = 1) AND @idCaller > 1 AND AU.idAccount > 1) 
					OR
					(@idCallerAccount > 1 AND @idCaller = 1 AND AU.idAccount = @idCallerAccount)
					)
				AND AU.idAccountUser <> 1 -- not the system admin user
				
				UNION
			
				SELECT idAccountUser
				FROM tblAccountUser AU
				INNER JOIN CONTAINSTABLE(tblRole, *, @searchParam) K ON K.[key] = AU.idRole
				WHERE 
					(
					((@idCallerAccount IS NULL OR @idCallerAccount = 1) AND @idCaller = 1) 
					OR 
					((@idCallerAccount IS NULL OR @idCallerAccount = 1) AND @idCaller > 1 AND AU.idAccount > 1) 
					OR
					(@idCallerAccount > 1 AND @idCaller = 1 AND AU.idAccount = @idCallerAccount)
					)
				AND AU.idAccountUser <> 1 -- not the system admin user
			) 
			AS rowCountRS
			
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						AU.idAccountUser,
						ROW_NUMBER() OVER (ORDER BY 
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'username' THEN AU.username END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'email' THEN AU.email END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'accountName' THEN A.username END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'roleName' THEN R.roleName END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName', 'roleName') THEN AU.lastname END) END DESC,
							-- SECOND ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('username', 'email', 'accountName', 'roleName') THEN AU.lastname END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName', 'roleName') THEN AU.firstname END) END DESC,
							-- THIRD ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('username', 'email', 'accountName', 'roleName') THEN AU.firstname END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName', 'roleName') THEN A.username END) END DESC,
							-- FOURTH ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('username', 'email') THEN A.username END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email') THEN AU.username END) END DESC,
							-- FIFTH ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('email') THEN AU.username END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('email') THEN AU.email END) END DESC,
							-- SIXTH ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName') THEN R.roleName END) END DESC,
							
							-- FIRST ORDER ASC
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'username' THEN AU.username END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'email' THEN AU.email END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'accountName' THEN A.username END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'roleName' THEN R.roleName END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName', 'roleName') THEN AU.lastname END) END,
							-- SECOND ORDER ASC
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('username', 'email', 'accountName', 'roleName') THEN AU.lastname END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName', 'roleName') THEN AU.firstname END) END,
							-- THIRD ORDER ASC
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('username', 'email', 'accountName', 'roleName') THEN AU.firstname END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName', 'roleName') THEN A.username END) END,
							-- FOURTH ORDER ASC
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('username', 'email') THEN A.username END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email') THEN AU.username END) END,
							-- FIFTH ORDER ASC
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('email') THEN AU.username END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('email') THEN AU.email END) END,
							-- SIXTH ORDER ASC
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName') THEN R.roleName END) END
						)
						AS [row_number]
					FROM tblAccountUser AU
					JOIN tblRole R ON R.idRole = AU.idRole
					JOIN tblAccount A ON A.idAccount = AU.idAccount
					INNER JOIN CONTAINSTABLE(tblAccountUser, *, @searchParam) K ON K.[key] = AU.idAccountUser
					WHERE 
						(
						((@idCallerAccount IS NULL OR @idCallerAccount = 1) AND @idCaller = 1) 
						OR 
						((@idCallerAccount IS NULL OR @idCallerAccount = 1) AND @idCaller > 1 AND AU.idAccount > 1) 
						OR
						(@idCallerAccount > 1 AND @idCaller = 1 AND AU.idAccount = @idCallerAccount)
						)
					AND AU.idAccountUser <> 1 -- not the system admin user
					
					UNION
					
					SELECT TOP (@pageNum * @pageSize) 
						AU.idAccountUser,
						ROW_NUMBER() OVER (ORDER BY 
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'username' THEN AU.username END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'email' THEN AU.email END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'accountName' THEN A.username END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'roleName' THEN R.roleName END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName', 'roleName') THEN AU.lastname END) END DESC,
							-- SECOND ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('username', 'email', 'accountName', 'roleName') THEN AU.lastname END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName', 'roleName') THEN AU.firstname END) END DESC,
							-- THIRD ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('username', 'email', 'accountName', 'roleName') THEN AU.firstname END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName', 'roleName') THEN A.username END) END DESC,
							-- FOURTH ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('username', 'email') THEN A.username END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email') THEN AU.username END) END DESC,
							-- FIFTH ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('email') THEN AU.username END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('email') THEN AU.email END) END DESC,
							-- SIXTH ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName') THEN R.roleName END) END DESC,
							
							-- FIRST ORDER ASC
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'username' THEN AU.username END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'email' THEN AU.email END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'accountName' THEN A.username END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'roleName' THEN R.roleName END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName', 'roleName') THEN AU.lastname END) END,
							-- SECOND ORDER ASC
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('username', 'email', 'accountName', 'roleName') THEN AU.lastname END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName', 'roleName') THEN AU.firstname END) END,
							-- THIRD ORDER ASC
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('username', 'email', 'accountName', 'roleName') THEN AU.firstname END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName', 'roleName') THEN A.username END) END,
							-- FOURTH ORDER ASC
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('username', 'email') THEN A.username END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email') THEN AU.username END) END,
							-- FIFTH ORDER ASC
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('email') THEN AU.username END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('email') THEN AU.email END) END,
							-- SIXTH ORDER ASC
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName') THEN R.roleName END) END
						)
						AS [row_number]
					FROM tblAccountUser AU
					JOIN tblRole R ON R.idRole = AU.idRole
					JOIN tblAccount A ON A.idAccount = AU.idAccount
					INNER JOIN CONTAINSTABLE(tblAccount, *, @searchParam) K ON K.[key] = AU.idAccount
					WHERE 
						(
						((@idCallerAccount IS NULL OR @idCallerAccount = 1) AND @idCaller = 1) 
						OR 
						((@idCallerAccount IS NULL OR @idCallerAccount = 1) AND @idCaller > 1 AND AU.idAccount > 1) 
						OR
						(@idCallerAccount > 1 AND @idCaller = 1 AND AU.idAccount = @idCallerAccount)
						)
					AND AU.idAccountUser <> 1 -- not the system admin user
					
					UNION
					
					SELECT TOP (@pageNum * @pageSize) 
						AU.idAccountUser,
						ROW_NUMBER() OVER (ORDER BY 
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'username' THEN AU.username END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'email' THEN AU.email END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'accountName' THEN A.username END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'roleName' THEN R.roleName END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName', 'roleName') THEN AU.lastname END) END DESC,
							-- SECOND ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('username', 'email', 'accountName', 'roleName') THEN AU.lastname END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName', 'roleName') THEN AU.firstname END) END DESC,
							-- THIRD ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('username', 'email', 'accountName', 'roleName') THEN AU.firstname END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName', 'roleName') THEN A.username END) END DESC,
							-- FOURTH ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('username', 'email') THEN A.username END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email') THEN AU.username END) END DESC,
							-- FIFTH ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('email') THEN AU.username END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('email') THEN AU.email END) END DESC,
							-- SIXTH ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName') THEN R.roleName END) END DESC,
							
							-- FIRST ORDER ASC
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'username' THEN AU.username END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'email' THEN AU.email END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'accountName' THEN A.username END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'roleName' THEN R.roleName END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName', 'roleName') THEN AU.lastname END) END,
							-- SECOND ORDER ASC
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('username', 'email', 'accountName', 'roleName') THEN AU.lastname END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName', 'roleName') THEN AU.firstname END) END,
							-- THIRD ORDER ASC
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('username', 'email', 'accountName', 'roleName') THEN AU.firstname END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName', 'roleName') THEN A.username END) END,
							-- FOURTH ORDER ASC
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('username', 'email') THEN A.username END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email') THEN AU.username END) END,
							-- FIFTH ORDER ASC
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('email') THEN AU.username END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('email') THEN AU.email END) END,
							-- SIXTH ORDER ASC
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName') THEN R.roleName END) END
						)
						AS [row_number]
					FROM tblAccountUser AU
					JOIN tblRole R ON R.idRole = AU.idRole
					JOIN tblAccount A ON A.idAccount = AU.idAccount
					INNER JOIN CONTAINSTABLE(tblRole, *, @searchParam) K ON K.[key] = AU.idRole
					WHERE 
						(
						((@idCallerAccount IS NULL OR @idCallerAccount = 1) AND @idCaller = 1) 
						OR 
						((@idCallerAccount IS NULL OR @idCallerAccount = 1) AND @idCaller > 1 AND AU.idAccount > 1) 
						OR
						(@idCallerAccount > 1 AND @idCaller = 1 AND AU.idAccount = @idCallerAccount)
						)
					AND AU.idAccountUser <> 1 -- not the system admin user
				), 
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idAccountUser, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				AU.idAccountUser, 
				AU.displayName,
				AU.username, 
				AU.email,
				CASE WHEN A.username = '__SYSTEM__' THEN '' ELSE A.username END AS accountName,
				R.roleName,
				AU.isActive,
				SelectedKeys.[row_number],
				'True' as isModifyOn
			FROM SelectedKeys
			JOIN tblAccountUser AU ON AU.idAccountUser = SelectedKeys.idAccountUser
			JOIN tblRole R ON R.idRole = AU.idRole
			JOIN tblAccount A ON A.idAccount = AU.idAccount WHERE AU.idRole IN (SELECT id FROM @IDTable)
			ORDER BY SelectedKeys.[row_number]
			
			END
		
	END
GO



-- =====================================================================
-- PROCEDURE: [AccountUser.ResetPassword]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[AccountUser.ResetPassword]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [AccountUser.ResetPassword]
GO

/*

Resets the user's password.

*/
CREATE PROCEDURE [AccountUser.ResetPassword]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@userFirstName			NVARCHAR(255)	OUTPUT,
	@userLastName			NVARCHAR(255)	OUTPUT,
	@userEmail				NVARCHAR(255)	OUTPUT,
	@userCulture			NVARCHAR(20)	OUTPUT,
	
	@username				NVARCHAR(512),
	@newPassword			NVARCHAR(512)
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @count INT
	SET @count = 0
	
	DECLARE @idAccountUser INT
	DECLARE @idAccount INT
	
	/*
	
	administrator account cannot be reset
	
	*/
	
	IF @username = 'admin' OR @username = 'administrator'
		BEGIN
		SET @Return_Code = 4 -- failed login
		SET @Error_Description_Code = 'AccountUserResetPassword_CannotResetAdmin'   
		RETURN 1
		END
		
	/*
	
	attempt to reset account user
	
	*/
		
	SELECT @count = COUNT(1)
	FROM tblAccountUser AU
	WHERE AU.username = @username
	
	IF (@count) = 0 
		BEGIN
		SET @Return_Code = 1 -- not found
		SET @Error_Description_Code = 'AccountUserResetPassword_UsernameNotFound'   
		END
		
	IF (@count) > 1
		BEGIN
		SET @Return_Code = 2 -- more than 1 account (very BAD)
		SET @Error_Description_Code = 'AccountUserResetPassword_DuplicateAccount'    
		RETURN 1
		END
		
	IF (@count) = 1
		BEGIN
		SELECT TOP 1
			@idAccountUser = AU.idAccountUser,
			@userFirstName = AU.firstName,
			@userLastName = AU.lastName,
			@userEmail = AU.email
			-- TODO: userCulture
		FROM tblAccountUser AU
		WHERE AU.username = @username
		
		IF (@userEmail IS NULL)
			BEGIN
			SET @Return_Code = 1 -- no email address
			SET @Error_Description_Code = 'AccountUserResetPassword_NoEmailAddress'   
			RETURN 1	
			END
	
		-- reset the password
		UPDATE tblAccountUser SET
			[password] = dbo.GetHashedString('SHA1', @newPassword)
		WHERE idAccountUser = @idAccountUser
		
		SELECT @Return_Code = 0 --(0 is 'success')
		RETURN 1
		END
	
	------------------------------------------------------
	/*
	
	attempt to reset account administrator
	
	*/
		
	SELECT @count = COUNT(1)
	FROM tblAccount A
	WHERE A.username = @username
	
	IF (@count) = 0 
		BEGIN
		SET @Return_Code = 1 -- not found
		SET @Error_Description_Code = 'AccountUserResetPassword_UsernameNotFound'        
		END
		
	IF (@count) > 1
		BEGIN
		SET @Return_Code = 2 -- more than 1 account (very BAD)
		SET @Error_Description_Code = 'AccountUserResetPassword_DuplicateAccount'         
		RETURN 1
		END
		
	IF (@count) = 1
		BEGIN
		SELECT TOP 1
			@idAccount = A.idAccount,
			@userFirstName = A.contactFirstName,
			@userLastName = A.contactLastName,
			@userEmail = A.contactEmail
			-- TODO: userCulture
		FROM tblAccount A
		WHERE A.username = @username
		
		IF (@userEmail IS NULL)
			BEGIN
			SET @Return_Code = 1 -- no email address
			SET @Error_Description_Code = 'AccountUserResetPassword_NoEmailAddress'       
			RETURN 1	
			END
	
		-- reset the password
		UPDATE tblAccount SET
			[password] = dbo.GetHashedString('SHA1', @newPassword)
		WHERE idAccount = @idAccount
		
		SELECT @Return_Code = 0 --(0 is 'success')
		RETURN 1
		END
	
	END
GO
-- =====================================================================
-- PROCEDURE: [AccountUser.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[AccountUser.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [AccountUser.Save]
GO

CREATE PROCEDURE [AccountUser.Save]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerAccount		INT				= 0, -- default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0, -- will fail if not specified
	
	@idAccountUser			INT				OUTPUT,
	@idAccount				INT,
	@firstName				NVARCHAR(255),
	@lastName				NVARCHAR(255),
	@email					NVARCHAR(255),
	@username				NVARCHAR(512),
	@password				NVARCHAR(512),
	@idRole					INT,
	@isActive				BIT
)
AS
	
	BEGIN
	SET NOCOUNT ON

	/* validate caller permission */
		
	DECLARE @idCallerRole INT
	SELECT @idCallerRole = 0
	SELECT @idCallerRole = idRole FROM tblAccountUser WHERE idAccountUser = @idCaller
	
	DECLARE @isCallerEditingSelf BIT
	DECLARE @isCallerSystemAdmin BIT
	DECLARE @isCallerSystemUser BIT
	DECLARE @isCallerAccountAdmin BIT
	DECLARE @isCallerAccountUser BIT
	
	SELECT @isCallerEditingSelf =	CASE WHEN @idAccountUser = @idCaller THEN 1 ELSE 0 END
	
	SELECT @isCallerSystemAdmin =	CASE WHEN @idCallerAccount = 1 
											  AND @idCaller = 1 
											  AND @idCallerRole = 1 
									THEN 1 
									ELSE 0 END
								  
	SELECT @isCallerSystemUser =	CASE WHEN @idCallerAccount = 1 
										      AND @idCaller > 1 
										      AND @idCallerRole = 1 
									THEN 1 
									ELSE 0 END
									
	SELECT @isCallerAccountAdmin =	CASE WHEN @idCallerAccount > 1 
											  AND @idCaller = 1 
											  AND @idCallerRole = 2 
									THEN 1 
									ELSE 0 END
									
	SELECT @isCallerAccountUser =	CASE WHEN @idCallerAccount > 1 
										      AND @idCaller > 1 
										      AND @idCallerRole = 2 
									THEN 1 
									ELSE 0 END
									
	-- check permissions only if caller is not editing their own account
	
	IF @isCallerEditingSelf = 0
		BEGIN
		IF -- system users cannot create/edit other system users
		   (@isCallerSystemUser = 1 AND @idAccount = 1)
		   OR
		   -- account administrators cannot create/edit users outside of their account
		   (@isCallerAccountAdmin = 1 AND @idAccount <> @idCallerAccount)
		   OR
		   -- account users cannot create/edit other users
		   (@isCallerAccountUser = 1)
			BEGIN
			SELECT @Return_Code = 3 -- caller permission error
		    SET @Error_Description_Code = 'AccountUserSave_PermissionError'
			RETURN 1
			END
		END
	
	/*
	
	validate role
	
	*/
	
	-- system users can only belong to idAccount 1
	
	IF (@idRole = 1 AND @idAccount <> 1)
		BEGIN
		SET @Return_Code = 2
		SET @Error_Description_Code = 'AccountUserSave_UserNotUnique'
		RETURN 0
		END
		 
	/*
	
	validate uniqueness across all possible login names
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblAccountUser
		WHERE (idAccountUser <> @idAccountUser AND username = @username)		
		) > 0
		BEGIN
		SET @Return_Code = 2
		SET @Error_Description_Code = 'AccountUserSave_UserNotUnique'
		RETURN 0 
		END
		
	/*
	
	save the data
	
	*/
	
	IF (@idAccountUser = 0 OR @idAccountUser is null)
		
		BEGIN
		
		-- insert the new object
		
		INSERT INTO tblAccountUser (
			idAccount,
			firstName,
			lastName,
			displayName,
			email,
			username,
			[password],
			idRole,
			isActive
		)			
		VALUES (
			@idAccount,
			@firstName,
			@lastName,
			@lastName + ', ' + @firstName,
			@email,
			@username,
			dbo.GetHashedString('SHA1', @password),
			@idRole,
			@isActive
		)
		
		-- get the new object id and return successfully
		
		SELECT @idAccountUser = SCOPE_IDENTITY()
		
		END
		
	ELSE
		
		BEGIN
		
		-- check that the object id exists
		
		IF (SELECT COUNT(1) FROM tblAccountUser WHERE idAccountUser = @idAccountUser) = 0
			
			BEGIN
				SELECT @idAccountUser = @idAccountUser
				SET @Return_Code = 1 --(1 is 'details not found')
				SET @Error_Description_Code = 'AccountUserSave_NoRecordFound'
				RETURN 0
			END
			
		-- update existing object's properties

		UPDATE tblAccountUser SET
			idAccount = @idAccount,
			firstName = @firstName,
			lastName = @lastName,
			displayName = @lastName + ', ' + @firstName,
			email = @email,
			username = @username,
			[password] = (CASE WHEN @password IS NULL THEN [password] ELSE dbo.GetHashedString('SHA1', @password) END),
			idRole = @idRole,
			isActive = @isActive
		WHERE idAccountUser = @idAccountUser
		
		-- get the id and return successfully
		
		SELECT @idAccountUser = @idAccountUser
				
		END
	
	SELECT @Return_Code = 0
		
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
-- =====================================================================
-- PROCEDURE: [ClonePortalSite.Delete]   Script Date: 3/9/2017 7:40:19 PM ******/

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[ClonePortalSite.Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [ClonePortalSite.Delete]
GO


/*

 Delete site from Account database and customer manage database 

*/
CREATE PROCEDURE [dbo].[ClonePortalSite.Delete]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,	
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,
	@idSite				INT,

	@sourceDBServer		NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@idAccountOld			INT	= 0,
	@hostNameOld			NVARCHAR(50)
)
AS
	
BEGIN
SET NOCOUNT ON 

		DECLARE @sqlStr NVARCHAR(MAX)
		DECLARE @isIdSiteExists INT

	 /*
	validate that the object exists
	*/
	SET @sqlStr = 'SET @isIdSiteExists = (SELECT  COUNT(1)	FROM ['+@sourceDBServer+'].['+@sourceDBName+'].[dbo].[tblSite] 
				WHERE idSite ='+ cast(@idSite AS NVARCHAR)
			 +')'

	EXECUTE SP_EXECUTESQL @sqlStr, N'@isIdSiteExists INT OUTPUT', @isIdSiteExists OUTPUT

	IF ( @isIdSiteExists = 0 )
		BEGIN
		SET @Return_Code = 1
		RETURN 1
		END

		-- calling intermediary procedure [Site.DeleteSingular] to delete site from Account database on link server.

	     SET @sqlStr = 'Exec [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[Site.DeleteSingular]'
	     +'  @idCallerSite=' + CAST(@idCallerSite AS NVARCHAR)
	     +', @callerLangString=' + ''''+ @callerLangString + ''''
	     +', @idCaller=' + CAST(@idCaller AS NVARCHAR)
	     +', @idSite =' + CAST(@idSite AS NVARCHAR)
	     +', @Return_Code = NULL'
	     +', @Error_Description_Code = NULL'
		  
		EXECUTE SP_EXECUTESQL @sqlStr

		  
		  -- delete old host name after copying the same host name as source on diffrent database 
		
	     DELETE 
	     FROM tblAccountToDomainAliasLink
	     WHERE idAccount=@idAccountOld 
	     AND hostname= @hostNameOld 
	     AND (hostname IS NOT NULL AND  hostname <> '')

	   
	   SET @Return_Code = 0
	   SET @Error_Description_Code = ''
	
END 


	
-- =====================================================================
-- PROCEDURE: [DatabaseServer.Delete]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[DatabaseServer.Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [DatabaseServer.Delete]
GO


CREATE PROCEDURE [DatabaseServer.Delete]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR (10)	OUTPUT,
	@idCallerAccount			INT				= 0, --default if not specified
	@callerLangString			NVARCHAR (10),
	@idCaller					INT,	 
	@DatabaseServer				  IDTable			READONLY	
)
AS
	
	BEGIN
	
	/*
		check to see if there is anything to do
	*/
	
	IF (SELECT COUNT(1) FROM @DatabaseServer) = 0
		BEGIN
		SET @Return_Code = 1 --(1 is 'details not found')
		SET @Error_Description_Code = 'DataBaseServerDelete_NoRecordFound'
		RETURN 1
		END
	
	
		
	/*
	
	Delete the data base(s).
	
	*/
	
	DELETE FROM tblDatabaseServer
	WHERE EXISTS (
		SELECT 1
		FROM @DatabaseServer VDBS
		WHERE tblDatabaseServer.idDatabaseServer = VDBS.id
	)
	
	SELECT @Return_Code = 0
	
	SET NOCOUNT ON
	END
	

GO



-- =====================================================================
-- PROCEDURE: [DatabaseServer.Details]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[DatabaseServer.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [DatabaseServer.Details]
GO


CREATE PROCEDURE [DatabaseServer.Details]
(
	@Return_Code					INT				OUTPUT,
	@Error_Description_Code			NVARCHAR(50)	OUTPUT,
	@idCallerAccount				INT				= 0, --default if not specified
	@callerLangString				NVARCHAR (10),
	@idCaller						INT				= 0,
	
	@idDatabaseServer							INT				OUTPUT, 
	@serverName						NVARCHAR(255)	OUTPUT,
	@networkName					NVARCHAR(255)	OUTPUT,
	@username						NVARCHAR(255)	OUTPUT,	
	@password						NVARCHAR(255)	OUTPUT,
	@isTrusted						BIT				OUTPUT
	
)
AS
	BEGIN
	SET NOCOUNT ON
	
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblDatabaseServer
		WHERE idDatabaseServer = @idDatabaseServer
		) = 0 
		BEGIN
		SET @Return_Code = 1
		RETURN 1
		END
		
	
	
	
	SELECT
		@serverName		= S.serverName,	
		@networkName	= S.networkName,		
		@username		= S.username,
		@password		= S.password,
		@isTrusted	    = s.isTrusted
		
	FROM tblDatabaseServer S
	
	WHERE S.idDatabaseServer = @idDatabaseServer
	
	
	SELECT @Return_Code = 0
		
	END
	

-- =====================================================================
-- PROCEDURE: [DatabaseServer.DoesServerNameExists]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[DatabaseServer.DoesServerNameExists]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [DatabaseServer.DoesServerNameExists]
GO

CREATE PROCEDURE [DatabaseServer.DoesServerNameExists]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerAccount		INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,

	@serverName				NVARCHAR (255),
	@isServerNameAlredyExists	BIT				OUTPUT
)
AS

	BEGIN
	SET NOCOUNT ON
	
	/*
	
	Determines if the current session server alredy has a server name with same name.
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblDatabaseServer ATDBS
		WHERE (ATDBS.serverName = @serverName 
		 )	
		
		) = 0 
		BEGIN
		SET @isServerNameAlredyExists = 0
		END
	ELSE
		BEGIN
		SET @isServerNameAlredyExists = 1
		END
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END


GO




-- =====================================================================
-- PROCEDURE: [DatabaseServer.GetGrid]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[DatabaseServer.GetGrid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [DatabaseServer.GetGrid]
GO


CREATE PROCEDURE [DatabaseServer.GetGrid]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerAccount		INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT,
	
	@searchParam			NVARCHAR(4000),
	@pageNum				INT, 
	@pageSize				INT, 
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT
)
AS
	BEGIN
		SET NOCOUNT ON
		
		/* get caller permission */
		
		DECLARE @idUserRole INT
		SELECT @idUserRole = idRole FROM tblAccountUser WHERE idAccountUser = @idCaller		
		
		IF @searchParam = '' OR @searchParam = '*'
			BEGIN
			SET @searchParam = null
			END

		IF @searchParam IS NULL
			
			BEGIN
			
			-- return the rowcount select * from tblDataBaseServer
			
			SELECT COUNT(1) AS row_count 
			FROM tblDatabaseServer
			WHERE
			idDatabaseServer <> 1 or  @idUserRole = 1
			
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						idDatabaseServer,
						ROW_NUMBER() OVER (ORDER BY 
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN serverName END DESC,
							
							-- FIRST ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN serverName END
						)
						AS [row_number]
					FROM tblDatabaseServer
					WHERE
					 idDatabaseServer <> 1 or  @idUserRole = 1
				), 
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idDatabaseServer, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				A.idDatabaseServer, 
				A.serverName, 
				A.networkName,
				A.username,
				A.password,
				A.isTrusted,
				CONVERT(BIT, 1) AS isModifyOn,
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN tblDatabaseServer A ON A.idDatabaseServer = SelectedKeys.idDatabaseServer
			
			ORDER BY SelectedKeys.[row_number]
			
			END
			
		ELSE
			
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblDatabaseServer
			INNER JOIN CONTAINSTABLE(tblDatabaseServer, *, @searchParam) K ON K.[key] = tblDatabaseServer.idDatabaseServer
			WHERE 
			 idDatabaseServer <> 1 or @idUserRole = 1
			
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						idDatabaseServer,
						ROW_NUMBER() OVER (ORDER BY 
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN serverName END DESC,
							
							-- FIRST ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN serverName END
						)
						AS [row_number]
					FROM tblDatabaseServer
					INNER JOIN CONTAINSTABLE(tblDatabaseServer, *, @searchParam) K ON K.[key] = tblDataBaseServer.idDatabaseServer
					WHERE @idUserRole = 1
					AND idDatabaseServer <> 1
				), 
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idDatabaseServer, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				A.idDatabaseServer, 
				A.serverName, 
				A.networkName,
				A.username,
				A.password,
				A.isTrusted,
				CONVERT(BIT, 1) AS isModifyOn,
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN tblDatabaseServer A ON A.idDatabaseServer = SelectedKeys.idDatabaseServer
			
			ORDER BY SelectedKeys.[row_number]
			
			END
		
	END


GO



-- =====================================================================
-- PROCEDURE: [DatabaseServer.GetSitesFromDatabase]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[DatabaseServer.GetSitesFromDatabase]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [DatabaseServer.GetSitesFromDatabase]
GO

CREATE PROCEDURE [dbo].[DatabaseServer.GetSitesFromDatabase]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerAccount		INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT,
	
	@DBServerName			NVARCHAR(50),
	@DatabaseName			NVARCHAR(50)
	
)
AS
	BEGIN
	   SET NOCOUNT ON
		
	   DECLARE @sql NVARCHAR(MAX)
	   DECLARE @isDatabaseExists INT
	   

	BEGIN TRY

	BEGIN

	  -- check to validate exitence of passed database on server
	  SET @SQL = 'SET @isDatabaseExists = (SELECT  COUNT(1) FROM ['+@DBServerName+'].master.sys.databases 
		   WHERE name ='''+ @DatabaseName +''')'

	  EXECUTE SP_EXECUTESQL @SQL, N'@isDatabaseExists INT OUTPUT', @isDatabaseExists OUTPUT
	

	  IF @isDatabaseExists = 1     
	      BEGIN
				-- getting site information from link server database
				 SET @sql =' SELECT '
	   			 		+'  idSite,'
	   			 		+'  hostname  '
	   			 		+' FROM  [' + @DBServerName + '].['+ @DatabaseName + '].[dbo].'+ '[tblSite]'
				 		+' WHERE idSite <> 1  '  
				 		+' ORDER BY hostname  '  
				 EXEC(@sql)

				 SELECT @Return_Code = 0	
				 SELECT @Error_Description_Code=''

		  END
	   ELSE
		  BEGIN
			 SELECT @Return_Code = 1	
			 SELECT @Error_Description_Code='DatabaseServerGetSitesFromDatabase_DatabaseNotFoundOnLinkServer'
		  END

	   END
  END TRY
  BEGIN CATCH
			    SELECT @Return_Code = 1	
			    SELECT @Error_Description_Code='DatabaseServerGetSitesFromDatabase_DatabaseNotFoundOnLinkServer'
  END CATCH
  END
-- =====================================================================
-- PROCEDURE: [DatabaseServer.IdsAndNamesForDatabaseServerList]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[DatabaseServer.IdsAndNamesForDatabaseServerList]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [DatabaseServer.IdsAndNamesForDatabaseServerList]
GO

/*
Return all ids and database server name column values.
*/
CREATE PROCEDURE [DatabaseServer.IdsAndNamesForDatabaseServerList]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		INT				OUTPUT,
	@idCallerAccount			INT				= 0, --default if not specified
	@callerLangString			NVARCHAR (10),
	@idCaller					INT
)
AS
	BEGIN
	SET NOCOUNT ON
		
	--DECLARE @idCallerRole INT
	--SELECT @idCallerRole = 0
	--SELECT @idCallerRole = idRole FROM tblAccountUser WHERE idAccountUser = @idCaller
	
	--DECLARE @isCallerViewingSelf BIT
	--DECLARE @isCallerSystemAdmin BIT
	--DECLARE @isCallerSystemUser BIT
	--DECLARE @isCallerAccountAdmin BIT
	--DECLARE @isCallerAccountUser BIT
	
	--SELECT @isCallerViewingSelf =	CASE WHEN @idAccountUser = @idCaller THEN 1 ELSE 0 END
	
	--SELECT @isCallerSystemAdmin =	CASE WHEN @idCallerAccount = 1 
	--										  AND @idCaller = 1 
	--										  AND @idCallerRole = 1 
	--								THEN 1 
	--								ELSE 0 END
								  
	--SELECT @isCallerSystemUser =	CASE WHEN @idCallerAccount = 1 
	--									      AND @idCaller > 1 
	--									      AND @idCallerRole = 1 
	--								THEN 1 
	--								ELSE 0 END
									
	--SELECT @isCallerAccountAdmin =	CASE WHEN @idCallerAccount > 1 
	--										  AND @idCaller = 1 
	--										  AND @idCallerRole = 2 
	--								THEN 1 
	--								ELSE 0 END
									
	--SELECT @isCallerAccountUser =	CASE WHEN @idCallerAccount > 1 
	--									      AND @idCaller > 1 
	--									      AND @idCallerRole = 2 
	--								THEN 1 
	--								ELSE 0 END
									
	-- if caller is not viewing self, make sure the
	-- caller is not an account user
	
	--IF @isCallerViewingSelf = 0
	--	BEGIN
	--	IF -- account users cannot view any other users
	--	   (@isCallerAccountUser = 1)
	--		BEGIN
	--		SET @Return_Code = 3 -- sort of (caller not member of specified site).
	--		SET @Error_Description_Code = 'AccountUserDetails_PermissionError'
	--		RETURN 1
	--		END
	--	END
		

	--/*
	
	--validate caller permission
	
	--*/
	
	--IF @isCallerSystemAdmin <> 1 OR @isCallerViewingSelf <> 1
	
	--	BEGIN
		
	--	IF (
	--		SELECT COUNT(1)
	--		FROM tblDatabase DB 
	--		WHERE DB.idDatabase = @idDatabase
	--		AND (
	--				-- system users cannot view other system users
	--				(@isCallerSystemUser = 1 AND DB.idDatabase = 1)
	--				OR
	--				-- account administrators cannot view users outside of their account
	--				(@isCallerAccountAdmin = 1 AND AU.idAccount <> @idCallerAccount)
	--			)
	--		) > 0 
			
	--		BEGIN
	--		SET @Return_Code = 3 -- sort of (caller not member of specified site).
	--		SET @Error_Description_Code = 'AccountUserDetails_PermissionError'
	--		RETURN 1
	--		END
				
	--	END
	
	/*
	
	get the data 
	
	*/
	
	SELECT
		DBS.idDatabaseServer,		
		DBS.serverName		
	FROM tblDataBaseServer DBS
	ORDER BY DBS.serverName
	
	SELECT @Return_Code = 0
		
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
-- =====================================================================
-- PROCEDURE: [DatabaseServer.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[DatabaseServer.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [DatabaseServer.Save]
GO

CREATE PROCEDURE [DatabaseServer.Save]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerAccount		INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,	
	@idDatabaseServer		INT				OUTPUT, 
	@serverName				NVARCHAR(255),
	@networkName			NVARCHAR(255),
	@username				NVARCHAR(512),
	@password				NVARCHAR(512),
	@isTrusted				BIT
	
)
AS
	
	BEGIN
	SET NOCOUNT ON

	/*
	
	validate the unique hostname
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM  tblDatabaseServer
		WHERE serverName = @serverName and networkName=@networkName
		AND (
			@idDatabaseServer IS NULL
			OR idDatabaseServer <> @idDatabaseServer
			)
		) > 0
		
		BEGIN
		SELECT @Return_Code = 2
		SET @Error_Description_Code = 'DataBaseServerSave_ServerNameNotUnique'
		RETURN 1
		END
	
	/*
	
	save the data
	
	*/
	
	IF (@idDatabaseServer= 0 OR @idDatabaseServer is null)
		
		BEGIN
		
		-- insert the new object
		
		INSERT INTO tblDatabaseServer (
			serverName,
			networkName,
			username,
			[password],
			isTrusted
			
		)			
		VALUES (
			@serverName,	
			@networkName,
			@username,			
			@password,
			@isTrusted	
		)
		
		-- get the new object's id and return successfully
		
		SELECT @idDatabaseServer = SCOPE_IDENTITY()
		
		END
		
	ELSE
		
		BEGIN
		
		-- check that the database server id exists
		
		IF (SELECT COUNT(1) FROM tblDatabaseServer WHERE idDatabaseServer = @idDatabaseServer) < 1
			
			BEGIN
				SELECT @idDatabaseServer = @idDatabaseServer
				SELECT @Return_Code = 1
				SET @Error_Description_Code = 'DataBaseServerSave_RecordNotFound'
				RETURN 1
			END
			
		-- update existing object's properties
		
		UPDATE tblDatabaseServer SET
			serverName = @serverName,
			networkName =@networkName,
			username = @username,
			[password] = @password,
			isTrusted=@isTrusted			
		WHERE idDatabaseServer = @idDatabaseServer		
		
		-- get the id and return successfully
		
		SELECT @idDatabaseServer = @idDatabaseServer
				
		END	
	
	SELECT @Return_Code = 0
		
	END
		

GO



-- =====================================================================
-- PROCEDURE: [ExceptionLog.GetGrid]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[ExceptionLog.GetGrid]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [ExceptionLog.GetGrid]
GO

/*
Gets a listing from the exception log for one account or from all accounts.
*/
CREATE PROCEDURE [ExceptionLog.GetGrid]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,

	@searchParam			NVARCHAR(255),
	@pageNum				INT, 
	@pageSize				INT, 
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT,

	@idAccount				INT				= 0
)
AS
	BEGIN
		SET NOCOUNT ON

		-- temp table to hold the list of exceptions we want (either all exceptions, or just one account)
		CREATE TABLE #ExceptionLog
	   (
		  idExceptionLog INT NOT NULL IDENTITY(1, 1),
		  idAccount INT NOT NULL,
		  company NVARCHAR(255) NOT NULL, 
		  idSite INT NULL,
		  idUser INT NULL,
		  username NVARCHAR(512) NULL,
		  displayName NVARCHAR(768) NULL,
		  [timestamp] DATETIME NOT NULL,
		  [page] NVARCHAR(512) NOT NULL,
		  exceptionType NVARCHAR(512) NOT NULL,
		  exceptionMessage NVARCHAR(MAX) NOT NULL,
		  exceptionStackTrace NVARCHAR(MAX) NOT NULL
	   )

	   DECLARE @databaseServer NVARCHAR(100)
	   DECLARE @databaseName NVARCHAR(100)
	   DECLARE @sql NVARCHAR(MAX)

	   IF(@idAccount = 0)
			BEGIN
			
			SELECT @idAccount = MIN(idAccount) FROM tblAccount

			-- iterate through all accounts and add the exceptions for each database to the temp table
			WHILE @idAccount IS NOT NULL
			BEGIN
			
				-- set database server and database name
				SELECT
					@databaseServer = D.serverName,
					@databaseName = A.databaseName
				FROM tblAccount A
				LEFT JOIN tblDatabaseServer D ON D.idDatabaseServer = A.idDatabaseServer
				WHERE A.idAccount = @idAccount

				-- if the database exists add the exceptions to the temp exception log
				IF (EXISTS (SELECT name 
				FROM master.dbo.sysdatabases 
				WHERE ('[' + name + ']' = @databaseName 
				OR name = @databaseName)))
				BEGIN
					-- get exceptions for one account
					SET @sql =' INSERT INTO #ExceptionLog '
						+' ( '
						+'	 idAccount,'
						+'	 company,'
						+'	 idSite,'
						+'	 idUser,'
						+'	 username,'
						+'	 displayName,'
						+'	 [timestamp],'
						+'	 [page],'
						+'	 exceptionType,'
						+'	 exceptionMessage,'
						+'	 exceptionStackTrace'
						+' ) '
						+' SELECT '
						+' A.idAccount, '
						+' A.company, '
						+' E.idSite, '
						+' E.idUser, '
						+' U.username, '
						+' U.displayName, '
						+' E.[timestamp], '
						+' E.[page], '
						+' E.exceptionType, '
						+' E.exceptionMessage, '
						+' E.exceptionStackTrace ' 
						+' FROM [dbo].'+ '[tblAccount] A, '
						+' [' + @databaseServer + '].['+ @databaseName + '].[dbo].'+ '[tblExceptionLog] E '
						+' LEFT JOIN [' + @databaseServer + '].[' + @databaseName + '].[dbo].' + '[tblUser] U '
						+' ON U.idUser = E.idUser ' 
						+' WHERE A.idAccount ='+ CAST(@idAccount AS NVARCHAR) + ' AND A.isDeleted = 0'
		
					EXEC(@sql)		
				END

				SELECT @idAccount = MIN(idAccount) FROM tblAccount WHERE idAccount > @idAccount
			END
			
			END
		ELSE
			BEGIN
			-- set database server and database name
			SELECT
				@databaseServer = D.serverName,
				@databaseName = A.databaseName
			FROM tblAccount A
			LEFT JOIN tblDatabaseServer D ON D.idDatabaseServer = A.idDatabaseServer
			WHERE A.idAccount = @idAccount

			-- get exceptions for one account
			SET @sql =' INSERT INTO #ExceptionLog '
				+' ( '
				+'	 idAccount,'
				+'	 company,'
				+'	 idSite,'
				+'	 idUser,'
				+'	 username,'
				+'	 displayName,'
				+'	 [timestamp],'
				+'	 [page],'
				+'	 exceptionType,'
				+'	 exceptionMessage,'
				+'	 exceptionStackTrace'
				+' ) '
				+' SELECT '
				+' A.idAccount, '
				+' A.company, '
				+' E.idSite, '
				+' E.idUser, '
				+' U.username, '
				+' U.displayName, '
				+' E.[timestamp], '
				+' E.[page], '
				+' E.exceptionType, '
				+' E.exceptionMessage, '
				+' E.exceptionStackTrace ' 
				+' FROM [dbo].'+ '[tblAccount] A, '
				+' [' + @databaseServer + '].['+ @databaseName + '].[dbo].'+ '[tblExceptionLog] E '
				+' LEFT JOIN [' + @databaseServer + '].[' + @databaseName + '].[dbo].' + '[tblUser] U '
				+' ON U.idUser = E.idUser ' 
				+' WHERE A.idAccount ='+ CAST(@idAccount AS NVARCHAR) + ' AND A.isDeleted = 0'
		
			EXEC(@sql)		
			END
		
		IF @searchParam = '' OR @searchParam = '*'
			BEGIN
			SET @searchParam = null
			END

		IF @searchParam IS NULL
	
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM #ExceptionLog
			
			
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						idExceptionLog,
						ROW_NUMBER() OVER (ORDER BY
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'exceptionMessage' THEN exceptionMessage END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'exceptionType' THEN exceptionType END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'user' THEN user END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'timestamp' THEN [timestamp] END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'company' THEN company END) END DESC,
							
							-- FIRST ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'exceptionMessage' THEN exceptionMessage END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'exceptionType' THEN exceptionType END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'user' THEN user END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'timestamp' THEN [timestamp] END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'company' THEN company END) END
						)
						AS [row_number]
					FROM #ExceptionLog
					
				), 
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idExceptionLog, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				EXL.idExceptionLog,
				EXL.company, 
				CONVERT(VARCHAR(12), EXL.[timestamp], 107) AS [timestamp],
				LTRIM(right(CONVERT(VARCHAR(25), EXL.[timestamp], 100), 7)) AS [time],		
				CASE WHEN EXL.idUser IS NULL THEN '[N/A]' ELSE EXL.displayName + '(' + EXL.username + ')' END AS [user],
				EXL.page,
				EXL.exceptionType,
				EXL.exceptionMessage,
				EXL.exceptionStackTrace,
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN #ExceptionLog EXL ON EXL.idExceptionLog = SelectedKeys.idExceptionLog
			ORDER BY SelectedKeys.[row_number]
			
			END
			
		ELSE
			
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM #ExceptionLog
			INNER JOIN CONTAINSTABLE(tblExceptionLog, *, @searchParam) K ON K.[key] = tblExceptionLog.idExceptionLog
			
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						idExceptionLog,
						ROW_NUMBER() OVER (ORDER BY
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'exceptionMessage' THEN exceptionMessage END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'exceptionType' THEN exceptionType END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'user' THEN user END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'timestamp' THEN [timestamp] END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'company' THEN company END) END DESC,
							
							-- FIRST ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'exceptionMessage' THEN exceptionMessage END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'exceptionType' THEN exceptionType END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'user' THEN user END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'timestamp' THEN [timestamp] END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'company' THEN company END) END
						)
						AS [row_number]
					FROM #ExceptionLog
					INNER JOIN CONTAINSTABLE(tblExceptionLog, *, @searchParam) K ON K.[key] = tblExceptionLog.idExceptionLog
				), 
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idExceptionLog, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				EXL.idExceptionLog, 
				EXL.company,
				EXL.[timestamp],					
				CASE WHEN EXL.idUser IS NULL THEN '[N/A]' ELSE EXL.displayName + ' (' + EXL.username + ')' END AS [user],
				EXL.page,
				EXL.exceptionType,
				EXL.exceptionMessage,
				EXL.exceptionStackTrace,
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN #ExceptionLog EXL ON EXL.idExceptionLog = SelectedKeys.idExceptionLog
			ORDER BY SelectedKeys.[row_number]
			
			END

			DROP TABLE #ExceptionLog
	END		
			
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
-- =====================================================================
-- PROCEDURE: [JobProcessor.GetNextAccountForProcessing]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[JobProcessor.GetNextAccountForProcessing]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [JobProcessor.GetNextAccountForProcessing]
GO

CREATE PROCEDURE [JobProcessor.GetNextAccountForProcessing]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerAccount			INT				= 0, --default if not specified
	@callerLangString			NVARCHAR (10),
	@idCaller					INT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	/* 
		validate caller, only caller id/account id 1 is valid here
		the job processor should be the only process calling this
		and it will run as id 1/1; other than that, only the system
		admin will be able to run this
	*/
	
	IF @idCaller <> 1 AND @idCallerAccount <> 1
		BEGIN
		SELECT @Return_Code = 3 -- caller permission error
		RETURN 1
		END
	
	/*
	
	get top 1 account id from accounts that are not currently processing and
	have not just finished processing within the last second ordered by the
	number of milliseconds descending since last process and account id
	
	*/
	
	SELECT TOP 1
		idAccount,
		DATEDIFF(SECOND, ISNULL(dtLastJobProcessEnd,'1975-01-01 01:23:45.678'), GETUTCDATE()) AS timeDifferenceInSeconds
	FROM tblAccount A
	WHERE (
			A.isJobProcessing IS NULL 
			OR 
			A.isJobProcessing = 0
		  )
		  --AND (
				--DATEDIFF(n, A.dtLastJobProcessEnd, GETUTCDATE()) IS NULL
				--OR
				--DATEDIFF(ss, A.dtLastJobProcessEnd, GETUTCDATE()) > 1
			  --)
		  AND DATEDIFF(SECOND, ISNULL(dtLastJobProcessEnd,'1975-01-01 01:23:45.678'), GETUTCDATE()) > 60
		  AND A.isActive = 1
		  AND (
				A.isDeleted IS NULL 
				OR 
				A.isDeleted = 0
			  )
		  AND A.idAccount > 1
	ORDER BY 
	timeDifferenceInSeconds DESC,
	idAccount ASC

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
-- =====================================================================
-- PROCEDURE: [JobProcessor.ReleaseProcessingFlagForAccount]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[JobProcessor.ReleaseProcessingFlagForAccount]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [JobProcessor.ReleaseProcessingFlagForAccount]
GO

CREATE PROCEDURE [JobProcessor.ReleaseProcessingFlagForAccount]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerAccount			INT				= 0, --default if not specified
	@callerLangString			NVARCHAR (10),
	@idCaller					INT,

	@idAccount					INT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	/* 
		validate caller, only caller id/account id 1 is valid here
		the job processor should be the only process calling this
		and it will run as id 1/1; other than that, only the system
		admin will be able to run this
	*/
	
	IF @idCaller <> 1 AND @idCallerAccount <> 1
		BEGIN
		SELECT @Return_Code = 3 -- caller permission error
		RETURN 1
		END
	
	/*
	
	release the processing flag and set dtLastJobProcessStart
	
	*/
	
	UPDATE tblAccount SET
		isJobProcessing = 0,
		dtLastJobProcessEnd = GETUTCDATE()
	WHERE idAccount = @idAccount

	SELECT @Return_Code = 0
		
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
-- =====================================================================
-- PROCEDURE: [JobProcessor.SetProcessingFlagForAccount]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[JobProcessor.SetProcessingFlagForAccount]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [JobProcessor.SetProcessingFlagForAccount]
GO

CREATE PROCEDURE [JobProcessor.SetProcessingFlagForAccount]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerAccount			INT				= 0, --default if not specified
	@callerLangString			NVARCHAR (10),
	@idCaller					INT,

	@idAccount					INT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	/* 
		validate caller, only caller id/account id 1 is valid here
		the job processor should be the only process calling this
		and it will run as id 1/1; other than that, only the system
		admin will be able to run this
	*/
	
	IF @idCaller <> 1 AND @idCallerAccount <> 1
		BEGIN
		SELECT @Return_Code = 3 -- caller permission error
		RETURN 1
		END
	
	/*
	
	set the processing flag and set dtLastJobProcessStart
	
	*/
	
	UPDATE tblAccount SET
		isJobProcessing = 1,
		dtLastJobProcessStart = GETUTCDATE()
	WHERE idAccount = @idAccount

	SELECT @Return_Code = 0
		
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
-- =====================================================================
-- PROCEDURE: [PowerPointJobProcessor.GetNextAccountForProcessing]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[PowerPointJobProcessor.GetNextAccountForProcessing]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [PowerPointJobProcessor.GetNextAccountForProcessing]
GO

CREATE PROCEDURE [PowerPointJobProcessor.GetNextAccountForProcessing]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerAccount			INT				= 0, --default if not specified
	@callerLangString			NVARCHAR (10),
	@idCaller					INT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	/* 
		validate caller, only caller id/account id 1 is valid here
		the job processor should be the only process calling this
		and it will run as id 1/1; other than that, only the system
		admin will be able to run this
	*/
	
	IF @idCaller <> 1 AND @idCallerAccount <> 1
		BEGIN
		SELECT @Return_Code = 3 -- caller permission error
		RETURN 1
		END
	
	/*
	
	get top 1 account id from accounts that are not currently processing and
	have not just finished processing within the last second ordered by the
	number of milliseconds descending since last process and account id
	
	*/
	
	SELECT TOP 1
		idAccount,
		DATEDIFF(SECOND, ISNULL(dtLastPowerPointJobProcessEnd,'1975-01-01 01:23:45.678'), GETUTCDATE()) AS timeDifferenceInSeconds
	FROM tblAccount A
	WHERE (
			A.isPowerPointJobProcessing IS NULL 
			OR 
			A.isPowerPointJobProcessing = 0
		  )
		  --AND (
				--DATEDIFF(n, A.dtLastPowerPointJobProcessEnd, GETUTCDATE()) IS NULL
				--OR
				--DATEDIFF(ss, A.dtLastPowerPointJobProcessEnd, GETUTCDATE()) > 1
			  --)
		  AND DATEDIFF(SECOND, ISNULL(dtLastPowerPointJobProcessEnd,'1975-01-01 01:23:45.678'), GETUTCDATE()) > 60
		  AND A.isActive = 1
		  AND (
				A.isDeleted IS NULL 
				OR 
				A.isDeleted = 0
			  )
		  AND A.idAccount > 1
	ORDER BY 
	timeDifferenceInSeconds DESC,
	idAccount ASC

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
-- =====================================================================
-- PROCEDURE: [PowerPointJobProcessor.ReleaseProcessingFlagForAccount]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[PowerPointJobProcessor.ReleaseProcessingFlagForAccount]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [PowerPointJobProcessor.ReleaseProcessingFlagForAccount]
GO

CREATE PROCEDURE [PowerPointJobProcessor.ReleaseProcessingFlagForAccount]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerAccount			INT				= 0, --default if not specified
	@callerLangString			NVARCHAR (10),
	@idCaller					INT,

	@idAccount					INT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	/* 
		validate caller, only caller id/account id 1 is valid here
		the job processor should be the only process calling this
		and it will run as id 1/1; other than that, only the system
		admin will be able to run this
	*/
	
	IF @idCaller <> 1 AND @idCallerAccount <> 1
		BEGIN
		SELECT @Return_Code = 3 -- caller permission error
		RETURN 1
		END
	
	/*
	
	release the processing flag and set dtLastPowerPointJobProcessStart
	
	*/
	
	UPDATE tblAccount SET
		isPowerPointJobProcessing = 0,
		dtLastPowerPointJobProcessEnd = GETUTCDATE()
	WHERE idAccount = @idAccount

	SELECT @Return_Code = 0
		
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
-- =====================================================================
-- PROCEDURE: [PowerPointJobProcessor.SetProcessingFlagForAccount]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[PowerPointJobProcessor.SetProcessingFlagForAccount]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [PowerPointJobProcessor.SetProcessingFlagForAccount]
GO

CREATE PROCEDURE [PowerPointJobProcessor.SetProcessingFlagForAccount]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerAccount			INT				= 0, --default if not specified
	@callerLangString			NVARCHAR (10),
	@idCaller					INT,

	@idAccount					INT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	/* 
		validate caller, only caller id/account id 1 is valid here
		the job processor should be the only process calling this
		and it will run as id 1/1; other than that, only the system
		admin will be able to run this
	*/
	
	IF @idCaller <> 1 AND @idCallerAccount <> 1
		BEGIN
		SELECT @Return_Code = 3 -- caller permission error
		RETURN 1
		END
	
	/*
	
	set the processing flag and set dtLastPowerPointJobProcessStart
	
	*/
	
	UPDATE tblAccount SET
		isPowerPointJobProcessing = 1,
		dtLastPowerPointJobProcessStart = GETUTCDATE()
	WHERE idAccount = @idAccount

	SELECT @Return_Code = 0
		
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
-- =====================================================================
-- PROCEDURE: [System.AuthenticateLogin]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF exists (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.AuthenticateLogin]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.AuthenticateLogin]
GO

/*

Authenticates credentials for login to the system.

*/
CREATE PROCEDURE [dbo].[System.AuthenticateLogin]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idAccount				INT				OUTPUT,
	@idAccountUser			INT				OUTPUT,
	@userFirstName			NVARCHAR(255)	OUTPUT,
	@userLastName			NVARCHAR(255)	OUTPUT,
	@userCulture			NVARCHAR(20)	OUTPUT,
	
	@username				NVARCHAR(512),
	@password				NVARCHAR(512)
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @count INT
	SET @count = 0
	
	/* 
	
	attempt to authenticate the system administrator user, this
	is the only account that should be named "administrator"
	
	*/
	
	IF @username = 'admin' OR @username = 'administrator'
		BEGIN
		
		SELECT @count = COUNT(1)
		FROM tblAccountUser AU
		WHERE AU.username = 'administrator'
		AND AU.[password] = dbo.GetHashedString('SHA1', @password)
		AND AU.idAccount = 1
		
		IF (@count) = 0 
		BEGIN
		SET @Return_Code = 3 -- failed login
		SET @Error_Description_Code = 'SystemAuthenticateLogin_Failed'   
		RETURN 1
		END
		
		IF (@count) > 1 
		BEGIN
		SET @Return_Code = 2 -- more than 1 "administrator" account (very BAD)
		SET @Error_Description_Code = 'SystemAuthenticateLogin_DuplicateAccount'    
		RETURN 1
		END
	
		SELECT TOP 1
			@idAccount = 1,
			@idAccountUser = AU.idAccountUser,
			@userFirstName = AU.firstName,
			@userLastName = AU.lastName,
			@userCulture = 'en-US' --L.code
		FROM tblAccountUser AU
		WHERE AU.username = 'administrator'
		AND AU.[password] = dbo.GetHashedString('SHA1', @password)
		AND AU.idAccount = 1
			
		SELECT @Return_Code = 0 --(0 is 'success')
		RETURN 1
		
		END
		
	ELSE
		BEGIN
		
		/*
		
		attempt to authenticate as an account user
		
		*/
		
		SELECT @count = COUNT(1)
		FROM tblAccountUser AU
		WHERE AU.username = @username
		AND AU.[password] = dbo.GetHashedString('SHA1', @password)
		AND AU.idAccount > 1
		
		IF (@count) = 0 
		BEGIN
		SET @Return_Code = 3 -- failed login
		SET @Error_Description_Code = 'SystemAuthenticateLogin_Failed'
		END
		
		IF (@count) > 1 
		BEGIN
		SET @Return_Code = 2 -- more than 1 account (very BAD)
		SET @Error_Description_Code = 'SystemAuthenticateLogin_DuplicateAccount' 
		RETURN 1
		END
		
		IF (@count) = 1
		BEGIN
		SELECT TOP 1
			@idAccount = AU.idAccount,
			@idAccountUser = AU.idAccountUser,
			@userFirstName = AU.firstName,
			@userLastName = AU.lastName,
			@userCulture = 'en-US' --L.code
		FROM tblAccountUser AU
		WHERE AU.username = @username
		AND AU.[password] = dbo.GetHashedString('SHA1', @password)
		AND AU.idAccount > 1
		
		SELECT @Return_Code = 0 --(0 is 'success')
		RETURN 1
		END
		
		/*
		
		attempt to authenticate as an account administrator
		
		*/
		
		SELECT @count = COUNT(1)
		FROM tblAccount A
		WHERE A.username = @username
		AND A.[password] = dbo.GetHashedString('SHA1', @password)
		
		IF (@count) = 0 
		BEGIN
		SET @Return_Code = 3 -- failed login
		SET @Error_Description_Code = 'SystemAuthenticateLogin_Failed'
		END
		
		IF (@count) > 1 
		BEGIN
		SET @Return_Code = 2 -- more than 1 account (very BAD)
		SET @Error_Description_Code = 'SystemAuthenticateLogin_DuplicateAccount'
		RETURN 1
		END
		
		IF (@count) = 1
		BEGIN
		SELECT TOP 1
			@idAccount = A.idAccount,
			@idAccountUser = 1,
			@userFirstName = A.contactFirstName,
			@userLastName = A.contactLastName,
			@userCulture = 'en-US' --L.code
		FROM tblAccount A
		WHERE A.username = @username
		AND A.[password] = dbo.GetHashedString('SHA1', @password)
		
		SELECT @Return_Code = 0 --(0 is 'success')
		RETURN 1
		END
		
		/*
		
		attempt to authenticate as an system user
		
		*/
		
		SELECT @count = COUNT(1)
		FROM tblAccountUser AU
		WHERE AU.username = @username
		AND AU.[password] = dbo.GetHashedString('SHA1', @password)
		AND AU.idAccount = 1
		
		IF (@count) = 0 
		BEGIN
		SET @Return_Code = 3 -- failed login
		SET @Error_Description_Code = 'SystemAuthenticateLogin_Failed'
		END
		
		IF (@count) > 1 
		BEGIN
		SET @Return_Code = 2 -- more than 1 account (very BAD)
		SET @Error_Description_Code = 'SystemAuthenticateLogin_DuplicateAccount'
		RETURN 1
		END
		
		IF (@count) = 1
		BEGIN
		SELECT TOP 1
			@idAccount = 1,
			@idAccountUser = AU.idAccountUser,
			@userFirstName = AU.firstName,
			@userLastName = AU.lastName,
			@userCulture = 'en-US' --L.code
		FROM tblAccountUser AU
		WHERE AU.username = @username
		AND AU.[password] = dbo.GetHashedString('SHA1', @password)
		AND AU.idAccount = 1
		
		SELECT @Return_Code = 0 --(0 is 'success')
		RETURN 1
		END
		
		END
		
		RETURN 1
		
	END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.ClonePortal]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.ClonePortal]
GO


/*

Copy source site data to destination site

*/
CREATE PROCEDURE [System.ClonePortal]
(
	@Return_Code			INT	OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,	
	@fullCopyFlag			BIT,		-- 1 for full copy and 0 for partial copy
	@isNewHostName			BIT,		-- 1 for same as source, and 0 for new portal name
	@idSiteSource			INT,
	@idSiteDestination		INT,		-- in case of partial copy '@idSiteDestination' value get by CS code, and in case of full copy '@idSiteDestination' update by current inserted idSite.
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50),
	@destinationHostName    NVARCHAR(50),
	@idAccount				INT				= NULL,
	@idCaller				INT	
)
AS

BEGIN

    SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY	
    
	   DECLARE @sql NVARCHAR(MAX)
	   DECLARE @subSql VARCHAR(MAX)					
	   DECLARE @isHostNameExistSql INT	
	   DECLARE @idAccountDestination INT
	   

	 /*
	  
	   Replace double slash with single slash
	 
	 */
	   SET @sourceDBServer = REPLACE(@sourceDBServer, '\\', '\')
	   SET @destinationDBServer = REPLACE(@destinationDBServer, '\\', '\')
	   
	   CREATE TABLE #ObjectMapping
	   (
		  [oldId] INT NOT NULL,
		  [newId] INT NOT NULL,
		  objectName NVARCHAR(100)
	   )
	   	

	/*
	
	validate the unique hostname
	
	*/
	 -- validate unique host name in Account database
	 SET @sql = ' SET @isHostNameExistSql = (SELECT COUNT(*) FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblSite] 
				WHERE hostname = ' + '''' + @destinationHostName + ''''
			 +')'
  
	EXECUTE SP_EXECUTESQL @sql, N'@isHostNameExistSql INT OUTPUT', @isHostNameExistSql OUTPUT
		
	-- Note: full copy will always occur between two different databases, if we have taken destination hostname same as source host name
	-- if copying site for full copy and host name already exists in account DB's tblSite, return with error 
	IF (@isHostNameExistSql > 0  AND @fullCopyFlag = 1)
	BEGIN
		SELECT @Return_Code = 2
		SET @Error_Description_Code = 'SystemClonePortal_HostNameNotUnique'
		PRINT @Error_Description_Code
		RETURN 1
	END

    /*

	TRANSACTION BEGIN

	*/
	--begin distributed transaction
	BEGIN DISTRIBUTED TRANSACTION

	   
	    -- check condition for the full copy
	IF @fullCopyFlag = 1
	BEGIN
-- =============================================================================
-- LOGIC START -- copy tblSite object for full copy ----------------------------


	   -- insert records from source site to destination table Site
	   SET @sql =' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblSite]'
				+' ( '
				+'	  hostname, '
				+'	  password, '
				+'	  isActive, '
				+'	  dtExpires, '
				+'	  title, '
				+'	  company, '
				+'	  contactName, '
				+'	  contactEmail, '
				+'	  userLimit, '
				+'	  kbLimit, '
				+'	  idLanguage, '
				+'	  idTimezone' 
				+' ) '
				+' SELECT ' 
				+'''' +@destinationHostName +''''
				+',	CP.password, '
				+'	CP.isActive, '
				+'	CP.dtExpires, '
				+'	CP.title, '
				+'	CP.company, '
				+'	CP.contactName, '
				+'	CP.contactEmail,' 
				+'	CP.userLimit,' 
				+'	CP.kbLimit, '
				+'	CP.idLanguage,'
				+'	CP.idTimezone '
				+' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblSite] CP'
				+' WHERE CP.idSite = '+ CAST(@idSiteSource AS NVARCHAR) 
		
	   EXEC (@sql)
	   
	   -- temporary table to hold the idSite of source and destination site tables
	   CREATE TABLE #SiteIdMappings
			 (
			     oldSourceId INT NOT NULL, 
			     newDestinationId INT NOT NULL  	   
			 )	
	  
	   -- inserting idSite of source Site table and destination Site table inside temporary table for mapping	
	   SET @sql =' INSERT INTO #SiteIdMappings '
				+' ( '
				+'	 oldSourceId,'
				+'	 newDestinationId'
				+' ) '
				+' SELECT '
				+' SS.idSite, '
				+' DS.idSite' 
				+' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblSite] DS '
				+' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblSite] SS '
				+' ON (SS.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
				+' AND (SS.isActive = DS.isActive ) ' 
				+' AND (SS.dtExpires = DS.dtExpires OR (SS.dtExpires IS NULL AND DS.dtExpires IS NULL)) '
				+' AND (SS.title = DS.title ) '
				+' AND (SS.company = DS.company ) '
				+' AND (SS.contactEmail = DS.contactEmail OR (SS.contactEmail IS NULL AND DS.contactEmail IS NULL))'					
				+' )'
				+' WHERE SS.idSite IS NOT NULL AND SS.idSite ='+ CAST(@idSiteSource AS NVARCHAR)
		
	   EXEC(@sql)	
				
		
	   -- SiteLanguage data move logic start
	   
	   -- insert records from source site to destination table SiteLanguage
	   SET @sql =' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblSiteLanguage]'
				+' ( '
				+'   idSite, '
				+'   idLanguage, '
				+'   title, '
				+'   company'
				+' ) '
				+' SELECT '
				+'  TTS.newDestinationId,  '
				+'  SL.idLanguage, '
				+'  SL.title, '
				+'  SL.company  '
				+' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblSiteLanguage]  SL'
				+' LEFT JOIN #SiteIdMappings TTS on TTS.oldSourceId = SL.idSite'
				+' WHERE SL.idSite = '+ CAST(@idSiteSource AS NVARCHAR) 
		
	   EXEC(@sql)
	   
	   -- get new created destination site id 
	   DECLARE @newIdSite INT

	   SET @sql = ' SET @newIdSite = (SELECT Max(idSite)'
                 +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblSite] )'

	  
	  
	   EXECUTE SP_EXECUTESQL @sql, N'@newIdSite INT OUTPUT', @newIdSite OUTPUT
	   
	   -- set new created destination site id 
	   SET @idSiteDestination = @newIdSite

		
	   -- droping temporary table site 
	   DROP TABLE #SiteIdMappings

	   
	   
-- ===============================================================================================
-- LOGIC START -- copy tblAccountToDomainAliasLink object for full copy with new host name ----

-- not taking the fully qualified name because table tblAccountToDomainAliasLink exist inside the local database

	
	   SET @sql = ' INSERT INTO [tblAccountToDomainAliasLink]'
				+' ( '
				+'	idAccount,'  
				+'	hostname, '
				+'	domain '
				+' ) '
				+' SELECT '
				+   CAST(@idAccount AS NVARCHAR) 
				+',' 
				+ '''' + @destinationHostName + ''''
				+', ''''' 
								
	   EXEC(@sql)		   
	   

-- =============================================================================
-- LOGIC START -- copy tblSiteToDomainAliasLink object for full copy -----------


	   -- insert records from source site to destination table SiteToDomainAliasLink
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblSiteToDomainAliasLink]'
	   			 +' ( '
	   			 +'  idSite,'  
	   			 +'  domain '
	   			 +' ) '
	   			 +' SELECT '
	   			 +   CAST(@idSiteDestination AS NVARCHAR)
				 +',' 
	   			 + '''' + @destinationHostName + ''''
						
	   EXEC(@sql)	
	   
    END

------------------------------------------------------------------------------
-- full copy end



-- ===============================================================================
-- LOGIC START -- copy tblSiteParam object for both full copy and partial copy ---

      
	   -- insert records from source site to destination table SiteParam
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblSiteParam]'
	   			 +' ( '
	   			 +'   idSite,'
	   			 +'	 param,'
	   			 +'	 value'
	   			 +'  ) '
	   			 +' SELECT '
	   			 +    CAST(@idSiteDestination AS NVARCHAR)
	   			 +' , SP.param,'
	   			 +'   SP.value '
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblSiteParam] SP'
	   			 +' WHERE SP.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			 +' AND SP.idSite != 1'    --  idSite = 1, already generated while creating database structure

	   EXEC(@sql)

-- ============================================================================================
-- LOGIC START -- copy tblSiteAvailableLanguage object for both full copy and partial copy ----
		
	   -- insert records from source site to destination table SiteAvailableLanguage
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblSiteAvailableLanguage]'
				 +' ( '
				 +'  idSite,'     
				 +'  idLanguage ' 			 
				 +' ) '
				 +'SELECT '
				 +   CAST(@idSiteDestination AS NVARCHAR)
				 +', SAL.idLanguage '
				 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblSiteAvailableLanguage] SAL '
				 +' WHERE SAL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
			
	   EXEC(@sql)		

-- =============================================================================
-- LOGIC START -- copy tblCatalog object for both full copy and partial copy ----

	   -- insert records from source site to destination table catalog
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCatalog]'
				  +' ( '
				  +'  idSite, 
				 	 idParent, 
				 	 title, 
				 	 [order], 
				 	 shortDescription, 
				 	 longDescription, 
				 	 isPrivate, 
				 	 isClosed, 
				 	 dtCreated, 
				 	 dtModified, 
				 	 cost, 
				 	 costType, 
				 	 searchTags'
				 +' ) '
				 +' SELECT '
				 +   CAST(@idSiteDestination AS NVARCHAR)
				 +', NULL, '     ---- this null value of idParent will be updated from temporary CatalogIdMappings table below
				 +'	C.title, 
				 	C.[order], 
				 	C.shortDescription, 
				 	C.longDescription, 
				 	C.isPrivate, 
				 	C.isClosed, 
				 	C.dtCreated, 
				 	C.dtModified, 
				 	C.cost,
				 	C.costType, 
				 	C.searchTags 
				    FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCatalog] C'
				 +' WHERE C.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
						
	   EXEC(@sql)
	   
	   -- temporary table to hold the idCatalog of source and destination Catalog tables
	   CREATE TABLE #CatalogIdMappings 
	   (
	   	oldSourceId INT NOT NULL, 
	   	newDestinationId INT NOT NULL  	   
	   )
	   
	   -- inserting idCatalog of source Catalog table and destination Catalog table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #CatalogIdMappings'
				  +' ( '
				  +'  oldSourceId,
				  	 newDestinationId'
				  +' ) '
				  +' SELECT 
				      SRC.idCatalog,
					 DST.idCatalog '
				  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCatalog] DST'
				  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCatalog] SRC'
				  +' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
				  +' AND (SRC.title = DST.title)'
				  +' AND (SRC.dtCreated = DST.dtCreated OR (SRC.dtCreated IS NULL AND DST.dtCreated IS NULL))'
				  +' AND (SRC.dtModified = DST.dtModified OR (SRC.dtModified IS NULL AND DST.dtModified IS NULL))'
				  +' )'
				  +' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)

	   EXEC(@sql)
	   
	   -- update idParent from temp table catalog
	   SET @sql =	   ' UPDATE DSTC'
	   			  +'  SET idParent = TEMP.newDestinationId'
	   			  +' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCatalog] SRCC'
	   			  +' INNER JOIN  [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCatalog] DSTC'
	   			  +' ON( SRCC.title = DSTC.title'
	   			  +' AND (SRCC.dtCreated = DSTC.dtCreated OR (SRCC.dtCreated IS NULL AND DSTC.dtCreated IS NULL))'
	   			  +' AND (SRCC.dtModified = DSTC.dtModified OR (SRCC.dtModified IS NULL AND DSTC.dtModified IS NULL))'
	   			  +' AND SRCC.idSite ='+ CAST(@idSiteSource AS NVARCHAR)+ ')'
	   			  +' INNER JOIN  #CatalogIdMappings TEMP'
	   			  +' ON(TEMP.oldSourceId = SRCC.idParent)'
	   			  +' WHERE DSTC.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   			  +' AND SRCC.idParent IS NOT NULL'
	   
	   EXEC(@sql)	
	   
	   
	   -- catalog language data move logic start

	   -- insert records from source site to destination table CatalogLanguage
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCatalogLanguage]'
				  +' ( '
				  +'   idSite, 
				  	  idCatalog, 
				  	  idLanguage, 
				  	  title, 
				  	  shortDescription, 
				  	  longDescription, 
				  	  searchTags'
				  +'  ) '
				  +' SELECT '
				  +    CAST(@idSiteDestination AS NVARCHAR)
				  +' , TTC.newDestinationId, 
				  	  CL.idLanguage, 
				  	  CL.title, 
				  	  CL.shortDescription, 
				  	  CL.longDescription, 
				  	  CL.searchTags  
				     FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCatalogLanguage] CL'
				  +' LEFT JOIN #CatalogIdMappings TTC on (TTC.oldSourceId = CL.idCatalog)'
				  +' WHERE CL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
								
	   EXEC(@sql)
	   
	   -- catalog language move data logic end		


-- =============================================================================
-- LOGIC START -- copy tblCourse object for both full copy and partial copy ----

	
	   -- insert records from source site to destination table Course
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCourse]'
				  +' ( '
			       +'   idSite, 
                	 	  title, 
                	 	  coursecode, 
                	 	  revcode, 
                	 	  avatar, 
                	 	  cost, 
                	 	  credits, 
                	 	  [minutes], 
                	 	  shortDescription,  
                	 	  longDescription, 
                	 	  objectives, 
                	 	  socialMedia,  
                	 	  isPublished, 
                	 	  isClosed, 
                	 	  isLocked, 
                	 	  isPrerequisiteAny, 
                	 	  isFeedActive, 
                	 	  isFeedModerated, 
                	 	  isFeedOpenSubscription, 
                	 	  dtCreated, 
                	 	  dtModified, 
                	 	  isDeleted, 
                	 	  dtDeleted, 
                	 	  [order], 
                	 	  disallowRating, 
                	 	  forceLessonCompletionInOrder, 
                	 	  selfEnrollmentDueInterval, 
                	 	  selfEnrollmentDueTimeframe, 
                	 	  selfEnrollmentExpiresFromStartInterval, 
                	 	  selfEnrollmentExpiresFromStartTimeframe, 
                	 	  selfEnrollmentExpiresFromFirstLaunchInterval, 
                	 	  selfEnrollmentExpiresFromFirstLaunchTimeframe, 
                	 	  searchTags'
                	 +'  ) '
                	 +' SELECT '
				 +     CAST(@idSiteDestination AS NVARCHAR)
                	 +' ,  C.title, 
                	 	  C.coursecode, 
                	 	  C.revcode, 
                	 	  C.avatar, 
                	 	  C.cost, 
                	 	  C.credits, 
                	 	  C.[minutes], 
                	 	  C.shortDescription,  
                	 	  C.longDescription, 
                	 	  C.objectives, 
                	 	  C.socialMedia,  
                	 	  C.isPublished, 
                	 	  C.isClosed, 
                	 	  C.isLocked, 
                	 	  C.isPrerequisiteAny, 
                	 	  C.isFeedActive, 
                	 	  C.isFeedModerated, 
                	 	  C.isFeedOpenSubscription, 
                	 	  C.dtCreated, 
                	 	  C.dtModified, 
                	 	  C.isDeleted, 
                	 	  C.dtDeleted, 
                	 	  C.[order], 
                	 	  C.disallowRating, 
                	 	  C.forceLessonCompletionInOrder, 
                	 	  C.selfEnrollmentDueInterval, 
                	 	  C.selfEnrollmentDueTimeframe, 
                	 	  C.selfEnrollmentExpiresFromStartInterval, 
                	 	  C.selfEnrollmentExpiresFromStartTimeframe, 
                	 	  C.selfEnrollmentExpiresFromFirstLaunchInterval, 
                	 	  C.selfEnrollmentExpiresFromFirstLaunchTimeframe, 
                	 	  C.searchTags 
                	    FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCourse] C'
                	 +' WHERE C.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
							          										
	   EXEC(@sql)
	   
	   -- temporary table to hold the idCourse of source and destination Course tables
	   CREATE TABLE #CourseIdMappings 
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL  	   
	   )
	   
	   -- inserting idCourse of source Course table and destination Course table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #CourseIdMappings'
	   		  	  +' ( '
	   		  	  +'	   oldSourceId,
	   		  	  	   newDestinationId'
	   		  	  +' ) '
	   		  	  +' SELECT 
					   SRC.idCourse, 
					   DST.idCourse '
	   		  	  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCourse] DST'
	   		  	  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCourse] SRC'
	   		  	  +' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   		  	  +' AND (SRC.title = DST.title)'
	   		  	  +' AND (SRC.dtCreated = DST.dtCreated OR (SRC.dtCreated IS NULL AND DST.dtCreated IS NULL))'
	   		  	  +' AND (SRC.dtModified = DST.dtModified OR (SRC.dtModified IS NULL AND DST.dtModified IS NULL))'
	   		  	  +' AND (SRC.dtDeleted = DST.dtDeleted OR (SRC.dtDeleted IS NULL AND DST.dtDeleted IS NULL))'
	   		  	  +' )'
	   		  	  +' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)

				
	   	
	   EXEC(@sql)	
						
	   -- course language data move logic start
	   
	   -- insert records from source site to destination table CourseLanguage
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCourseLanguage]'
	   			+ ' ( '
	   			+ '  idSite, 
	   			     idCourse, 
	   				idLanguage, 
	   				title, 
	   				shortDescription, 
	   				longDescription, 
	   				searchTags'
	   			+ ' ) '
	   			+' SELECT '
				+    CAST(@idSiteDestination AS NVARCHAR)
	   			+' , TTC.newDestinationId, 
	   				CL.idLanguage, 
	   				CL.title, 
	   				CL.shortDescription, 
	   				CL.longDescription, 
	   				CL.searchTags 
	   			   FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCourseLanguage] CL'
	   			+' LEFT JOIN #CourseIdMappings TTC on (TTC.oldSourceId = CL.idCourse)'
	   			+' WHERE CL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 				
	   			
	   EXEC(@sql)

	   -- course language data move logic end

	   -- insert temp table data for user in order to return it for moving file system object
	   INSERT INTO #ObjectMapping
	   ([oldId],[newId],objectName)
	   SELECT oldSourceId, newDestinationId, 'courses' FROM #CourseIdMappings


-- =========================================================================================
-- LOGIC START -- copy tblCourseToCatalogLink object for both full copy and partial copy ---

	   -- insert records from source site to destination table CourseToCatalogLink
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCourseToCatalogLink]'
	   			  +'  ( '
	   			  +'	   idSite, 
	   			  	   idCatalog, 
	   			  	   idCourse, 
	   			  	   [order]'
	   			  +'  ) '
	   			  +' SELECT '
				  +	   CAST(@idSiteDestination AS NVARCHAR)
	   			  +' ,  TTCatalog.newDestinationId, 
	   			  	   TTCourse.newDestinationId, 
	   			  	   CTCL.[order] 
	   			     FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCourseToCatalogLink] CTCL'
	   			  +' LEFT JOIN #CatalogIdMappings TTCatalog on (TTCatalog.oldSourceId = CTCL.idCatalog )'
	   			  +' LEFT JOIN #CourseIdMappings TTCourse on (TTCourse.oldSourceId = CTCL.idCourse )'
	   			  +' WHERE CTCL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			
	   EXEC(@sql)



-- =============================================================================
-- LOGIC START -- copy tblGroup object for both full copy and partial copy-----------

                
	   -- insert records from source site to destination table Group
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblGroup]'
				  +' ( '
			       +'   idSite, 
				       name,
				       primaryGroupToken,
				       objectGUID,
				       distinguishedName,
				       avatar ,
				       shortDescription,
				       longDescription,
				       isSelfJoinAllowed,
				       isFeedActive,
				       isFeedModerated,
				       membershipIsPublicized,
				       searchTags'
                	  +'  ) '
                	  +' SELECT '
				  +	  CAST(@idSiteDestination AS NVARCHAR)
                	  +' , G.name,
				       G.primaryGroupToken,
				       G.objectGUID,
				       G.distinguishedName,
				       G.avatar ,
				       G.shortDescription,
				       G.longDescription,
				       G.isSelfJoinAllowed,
				       G.isFeedActive,
				       G.isFeedModerated,
				       G.membershipIsPublicized,
				       G.searchTags 
                	     FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblGroup] G'
                	  +' WHERE G.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
                										
	   EXEC(@sql)

	   -- temporary table to hold the idGroup of source and destination Group tables
	   CREATE TABLE #GroupIdMappings 
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL  	   
	   )

	   -- inserting idGroup of source Group table and destination Group table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #GroupIdMappings '
	   			  +'  ( '
	   			  +'    oldSourceId,
	   			  	   newDestinationId'
	   			  +'  ) '
	   			  +' SELECT 
					   SRC.idGroup, 
					   DST.idGroup'
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblGroup] DST'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblGroup] SRC'
	   			  +' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (SRC.name = DST.name)'
	   			  +' AND (SRC.shortDescription = DST.shortDescription OR (SRC.shortDescription IS NULL AND DST.shortDescription IS NULL))'
	   			  +' AND (SRC.longDescription = DST.longDescription OR (SRC.longDescription IS NULL AND DST.longDescription IS NULL))'
	   			  +' AND (SRC.objectGUID = DST.objectGUID OR (SRC.objectGUID IS NULL AND DST.objectGUID IS NULL))'
	   			  +' AND (SRC.primaryGroupToken = DST.primaryGroupToken OR (SRC.primaryGroupToken IS NULL AND DST.primaryGroupToken IS NULL))'
	   			  +' AND (SRC.isSelfJoinAllowed = DST.isSelfJoinAllowed OR (SRC.isSelfJoinAllowed IS NULL AND DST.isSelfJoinAllowed IS NULL))'
	   			  +' )'
	   			  +' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   
	   EXEC(@sql)	
	   
	   				
	   -- group language data move logic start
	   
	   -- insert records from source site to destination table GroupLanguage
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblGroupLanguage]'
	   			  +'  ( '
	   			  +'   idSite, 
	   			       idGroup, 
	   			  	  idLanguage, 
	   			  	  name, 
	   			  	  shortDescription, 
	   			  	  longDescription, 
	   			  	  searchTags'
	   			  +'  ) '
	   			  +' SELECT '
				  +	  CAST(@idSiteDestination AS NVARCHAR)
	   			  +' , TTG.newDestinationId, 
	   			  	  GL.idLanguage, 
	   			  	  GL.name, 
	   			  	  GL.shortDescription, 
	   			  	  GL.longDescription, 
	   			  	  GL.searchTags 
	   			     FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblGroupLanguage] GL'
	   			  +' LEFT JOIN #GroupIdMappings TTG on (TTG.oldSourceId = GL.idGroup)'
	   			  +' WHERE GL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 	
	   			
	   EXEC(@sql)
	   
	   -- group language data move logic end

	   -- insert temp table data for group in order to return it for moving file system object
	   INSERT INTO #ObjectMapping
	   ([oldId],[newId],objectName)
	   SELECT oldSourceId, newDestinationId, 'groups' FROM #GroupIdMappings	



	   
-- ========================================================================================
-- LOGIC START -- copy tblCatalogAccessToGroupLink object for full copy and partial copy---


	   --insert records from source site to destination table CatalogAccessToGroupLink
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCatalogAccessToGroupLink]'
	     		  +' ( '
	     		  +'  idSite,
	   		   	 	 idGroup,
	   		   	 	 idCatalog,
	   		   	 	 created'
	     		  +' ) '
	     		  +' SELECT '
	   			  +    CAST(@idSiteDestination AS NVARCHAR)
	     		  +' , TTG.newDestinationId,
	   		   	 	  TTC.newDestinationId,
	   		   	 	  CATGL.created
	     		     FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCatalogAccessToGroupLink] CATGL'
	     		  +' LEFT JOIN #CatalogIdMappings TTC on (TTC.oldSourceId = CATGL.idCatalog )'
	   			  +' LEFT JOIN #GroupIdMappings TTG on (TTG.oldSourceId = CATGL.idGroup )'
	   	
		--EXEC(@sql)
		

-- =============================================================================
-- LOGIC START -- copy tblLesson object for both full copy and partial copy ----

	   -- insert records from source site to destination table Lesson
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblLesson]'
	     		+ ' ( '
	     		+ '  idSite, 
	     			idCourse, 
	     			title, 
	     			revcode, 
	     			shortDescription, 
	     			longDescription, 
	     			dtCreated, 
	     			dtModified, 
	     			isDeleted, 
	     			dtDeleted, 
	     			[order], 
	     			searchTags'
	     		+ ' ) '
	     		+' SELECT '
				+    CAST(@idSiteDestination AS NVARCHAR)
	     		+' , TTC.newDestinationId, 
	     			L.title, 
	     			L.revcode, 
	     			L.shortDescription, 
	     			L.longDescription, 
	     			L.dtCreated, 
	     			L.dtModified, 
	     			L.isDeleted, 
	     			L.dtDeleted, 
	     			L.[order], 
	     			L.searchTags 
	     		   FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblLesson] L'
	     		+' LEFT JOIN #CourseIdMappings TTC on (TTC.oldSourceId = L.idCourse )'
	     		+' WHERE L.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
		
	   EXEC(@sql)
	   	
	   -- temporary table to hold the idLesson of source and destination Lesson tables
	   CREATE TABLE #LessonIdMappings 
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL 	   
	   )
	   
	   -- inserting idLesson of source Lesson table and destination Lesson table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #LessonIdMappings'
				 + ' ( '
				 + '	  oldSourceId,
				 	  newDestinationId'
				 + ' ) '
				 +' SELECT 
				 	  SRC.idLesson, 
				 	  DST.idLesson '
				 +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblLesson] DST'
				 +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblLesson] SRC'
				 +' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
				 +' AND (SRC.title = DST.title)'
				 +' AND (SRC.dtCreated = DST.dtCreated OR (SRC.dtCreated IS NULL AND DST.dtCreated IS NULL))'
				 +' AND (SRC.dtModified = DST.dtModified OR (SRC.dtModified IS NULL AND DST.dtModified IS NULL))'
				 +' AND (SRC.dtDeleted = DST.dtDeleted OR (SRC.dtDeleted IS NULL AND DST.dtDeleted IS NULL))'
				 +')'
				 +' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
		
	
	   EXEC(@sql)
	   
	   -- lesson language data move logic start

	   -- insert records from source site to destination table LessonLanguage
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblLessonLanguage]'
              		  +' ( '
              		  +'	   idSite, 
              		  	   idLesson, 
              		  	   idLanguage, 
              		  	   title, 
              		  	   shortDescription, 
              		  	   longDescription, 
              		  	   searchTags'
              		  +' ) '
              		  +' SELECT '
				  +	   CAST(@idSiteDestination AS NVARCHAR)
              		  +' ,  TTL.newDestinationId, 
              		  	   LL.idLanguage, 
              		  	   LL.title, 
              		  	   LL.shortDescription, 
              		  	   LL.longDescription, 
              		  	   LL.searchTags 
              		     FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblLessonLanguage] LL'
              		  +' LEFT JOIN #LessonIdMappings TTL on (TTL.oldSourceId = LL.idLesson )'
              		  +' WHERE LL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   EXEC(@sql)
	                
	   -- lesson language data move logic end

	   -- insert temp table data for user in order to return it for moving file system object
	   INSERT INTO #ObjectMapping
	   ([oldId],[newId],objectName)
	   SELECT oldSourceId, newDestinationId, 'lessons' FROM #LessonIdMappings
	   

------------------------------------------------------------------------------
	   
	   -- check condition for the full copy
	   IF @fullCopyFlag = 1
	   BEGIN


-- =============================================================================
-- LOGIC START -- copy tblUser object for full copy ----------------------------

		
	   -- temporary table to hold the idUser of source and destination User tables
	   CREATE TABLE #UserIdMappings 
	   (
	   	  oldSourceId INT NOT NULL,
	   	  newDestinationId INT NOT NULL 	   
	   )
	   
	   -- initially insert row for admin record
	   INSERT INTO #UserIdMappings(
	   	  oldSourceId,
	   	  newDestinationId
	   )
	   VALUES
	     (
	   	  1,
	   	  1
		)
		
	   -- user insert query exceed the length of nvarchar.
	   DECLARE @sqlStrUser VARCHAR(MAX)
	   
	   -- insert records from source site to destination table User
	   SET @sqlStrUser =  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblUser]'
	   				 +' ( '
	   				 +'    idSite, 
	   					  firstName, 
	   					  middleName,	
	   					  lastName, 
	   					  displayName, 
	   					  email, 
	   					  username, 
	   					  [password], 
	   					  isDeleted, 
	   					  dtDeleted,
	   					  idTimezone, 
	   					  isActive, 
	   					  mustchangePassword,
	   					  dtCreated, 
	   					  dtExpires, 
	   					  idLanguage, 
	   					  dtModified, 
	   					  employeeID, 
	   					  company, 
	   					  [address], 
	   					  city, 
	   					  province, 
	   					  postalcode, 
	   					  country, 
	   					  phonePrimary, 
	   					  phoneWork, 
	   					  phoneFax, 
	   					  phoneHome, 
	   					  phoneMobile, 
	   					  phonePager, 
	   					  phoneOther, 
	   					  department, 
	   					  division, 
	   					  region, 
	   					  jobTitle, 
	   					  jobClass, 
	   					  gender, 
	   					  race, 
	   					  dtDOB, 
	   					  dtHire, 
	   					  dtTerm, 
	   					  avatar,	
	   					  objectGUID,	
	   					  activationGUID,
	   					  distinguishedName,	
	   					  dtLastLogin,	
	   					  dtSessionExpires,	
	   					  activeSessionId,	
	   					  dtLastPermissionCheck,
	   					  field00, 
	   					  field01, 
	   					  field02, 
	   					  field03, 
	   					  field04, 
	   					  field05, 
	   					  field06, 
	   					  field07, 
	   					  field08, 
	   					  field09, 
	   					  field10, 
	   					  field11, 
	   					  field12, 
	   					  field13, 
	   					  field14, 
	   					  field15, 
	   					  field16, 
	   					  field17, 
	   					  field18, 
	   					  field19'
	   			     +' ) '
	   			     +' SELECT '
					+	  CAST(@idSiteDestination AS NVARCHAR)
	   			     +' ,	  U.firstName, 
	   					  U.middleName, 
	   					  U.lastName, 
	   					  U.displayName, 
	   					  U.email, 
	   					  U.username, 
	   					  U.[password],
	   					  U.isDeleted,
	   					  U.dtDeleted, 
	   					  U.idTimezone, 
	   					  U.isActive, 
	   					  U.mustchangePassword, 
	   					  U.dtCreated, 
	   					  U.dtExpires, 
	   					  U.idLanguage, 
	   					  U.dtModified, 
	   					  U.employeeID, 
	   					  U.company, 
	   					  U.[address], 
	   					  U.city, 
	   					  U.province, 
	   					  U.postalcode, 
	   					  U.country, 
	   					  U.phonePrimary, 
	   					  U.phoneWork, 
	   					  U.phoneFax, 
	   					  U.phoneHome, 
	   					  U.phoneMobile, 
	   					  U.phonePager, 
	   					  U.phoneOther, 
	   					  U.department, 
	   					  U.division, 
	   					  U.region, 
	   					  U.jobTitle, 
	   					  U.jobClass, 
	   					  U.gender, 
	   					  U.race, 
	   					  U.dtDOB, 
	   					  U.dtHire, 
	   					  U.dtTerm,
	   					  U.avatar,	
	   					  U.objectGUID,	
	   					  U.activationGUID,
	   					  U.distinguishedName,	
	   					  U.dtLastLogin,	
	   					  U.dtSessionExpires,	
	   					  U.activeSessionId,	
	   					  U.dtLastPermissionCheck, '
	   			
		 
		  SET @subSql = '  U.field00, 
		  				  U.field01, 
		  				  U.field02, 
		  				  U.field03, 
		  				  U.field04, 
		  				  U.field05, 
		  				  U.field06, 
		  				  U.field07, 
		  				  U.field08, 
		  				  U.field09, 
		  				  U.field10, 
		  				  U.field11, 
		  				  U.field12, 
		  				  U.field13, 
		  				  U.field14, 
		  				  U.field15, 
		  				  U.field16, 
		  				  U.field17, 
		  				  U.field18,
		  				  U.field19 
	   			      FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblUser] U '
	   			   +' WHERE U.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			   +' AND U.idUser != 1 ' -- idUser 1 is already inserted while creating db structure.
	   
	   SET @sqlStrUser = @sqlStrUser + @subSql
	   
	   EXEC(@sqlStrUser)
	   

	   SET @sql =  ' INSERT INTO #UserIdMappings'
	   			  +' ( '
	   			  +'	   oldSourceId,
	   			  	   newDestinationId'
	   			  +' ) '
	   			  +' SELECT 
					   SRC.idUser, 
					   DST.idUser'
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblUser] DST'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblUser] SRC'
	   			  +' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (SRC.firstName = DST.firstName)'
	   			  +' AND (SRC.lastName = DST.lastName)'
	   			  +' AND (SRC.email = DST.email OR (SRC.email IS NULL AND DST.email IS NULL))'
	   			  +' AND (SRC.dtCreated = DST.dtCreated )'
	   			  +' AND (SRC.dtModified = DST.dtModified OR (SRC.dtModified IS NULL AND DST.dtModified IS NULL))'
	   			  +' AND (SRC.dtDeleted = DST.dtDeleted OR (SRC.dtDeleted IS NULL AND DST.dtDeleted IS NULL))'
	   			  +' )'
	   			  +' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   			  +' AND DST.idUser != 1' -- idUser 1 is already inserted while creating db structure.
	
	   EXEC(@sql)
	   
	   -- insert temp table data for user in order to return it for moving file system object
	   INSERT INTO #ObjectMapping
	   ([oldId],[newId],objectName)
	   SELECT oldSourceId, newDestinationId, 'users' FROM #UserIdMappings


	   
    End

------------------------------------------------------------------------------
-- full copy end


-- =================================================================================================
-- LOGIC START -- copy tblQuizSurvey object for both full copy and partial copy with condition -----

	   -- insert records from source site to destination table QuizSurvey
	   SET @sql =  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblQuizSurvey]'
							+ ' ( '
			                	+ '  idSite,
								[type],
								identifier,	
								guid,
								data,
								isDraft,
								idContentPackage,	
								dtCreated,
								dtModified,
								idAuthor'
                				+ ' ) '
                				+' SELECT '+ CAST(@idSiteDestination AS NVARCHAR)
                				+'  ,QS.[type],
								QS.identifier,	
								QS.guid,
								QS.data,
								QS.isDraft,
								NULL,	
								QS.dtCreated,
								QS.dtModified,'

		IF(@fullCopyFlag = 1) -- idAuthor is available for full copy only, because tblUser moved only for full copy.
		  BEGIN
			 SET @subSql =' TTU.newDestinationId
							FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblQuizSurvey] QS'
                				+' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = QS.idAuthor)'
							+' WHERE QS.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
            END
		ELSE
		  BEGIN -- otherwise 1 is inserted in this field as administrator id
			  SET @subSql = ' 1
							FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblQuizSurvey] QS'
                				+' WHERE QS.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
		  END
	
		SET @sql = @sql + @subSql
		EXEC(@sql)		
	
	   -- temporary table to hold the idQuizSurvey of source and destination QuizSurvey tables
	   CREATE TABLE #QuizSurveyIdMappings 
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL  	   
	   )

	   -- inserting idQuizSurvey of source QuizSurvey table and destination QuizSurvey table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #QuizSurveyIdMappings'
	   			  +' ( '
	   			  +'	   oldSourceId,
	   			  	   newDestinationId '
	   			  +' ) '
	   			  +' SELECT 
					   SRC.idQuizSurvey, 
					   DST.idQuizSurvey '
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblQuizSurvey] DST'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblQuizSurvey] SRC'
	   			  +' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (SRC.guid = DST.guid)'
	   			  +' AND (SRC.dtCreated = DST.dtCreated)'
	   			  +' )'
	   			  +' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   	
	   EXEC(@sql)

-- ====================================================================================================
-- LOGIC START -- copy tblContentPackage object for both full copy and partial copy with condition ----


	   -- insert records from source site to destination table ContentPackage
	   SET @sql =   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblContentPackage]'
				+' ( '
				+'   idSite,
					name,
					[path],
					kb,
					idContentPackageType,
					idSCORMPackageType,
					manifest,
					dtCreated,
					dtModified,
					isMediaUpload,
					idMediaType,
					contentTitle,
					originalMediaFilename,
					isVideoMedia3rdParty,
					videoMediaEmbedCode,
					enableAutoplay,
					allowRewind,
					allowFastForward,
					allowNavigation,
					allowResume,
					minProgressForCompletion,
					isProcessing,
					isProcessed,
					dtProcessed,
					idQuizSurvey'
				+'  ) '
				+' SELECT '+ CAST(@idSiteDestination AS NVARCHAR)
				+' , CP.name'
				+' , CONCAT(''/warehouse'',REPLACE(REPLACE(CP.path, SUBSTRING ( CP.path ,CHARINDEX(''-'',CP.path),
				     (CHARINDEX(''-'',CP.path,CHARINDEX(''-'',CP.path)+1))-CHARINDEX(''-'',CP.path)+1) , ''-''+ '''+cast(@idSiteDestination as nvarchar)+''' +''-''), 
				     SUBSTRING ( REPLACE(CP.path, SUBSTRING ( CP.path ,CHARINDEX(''-'',CP.path), (CHARINDEX(''-'',CP.path,CHARINDEX(''-'',CP.path)+1))-CHARINDEX(''-'',CP.path)+1) , ''-''+ '''+cast(@idSiteDestination as nvarchar)+''' +''-'') ,CHARINDEX(''/'',REPLACE(CP.path, 
				     SUBSTRING ( CP.path ,CHARINDEX(''-'',CP.path), (CHARINDEX(''-'',CP.path,CHARINDEX(''-'',CP.path)+1))-CHARINDEX(''-'',CP.path)+1) , ''-''+ '''+cast(@idSiteDestination as nvarchar)+''' +''-''))+1, (CHARINDEX(''-'',REPLACE(CP.path, 
				     SUBSTRING ( CP.path ,CHARINDEX(''-'',CP.path), (CHARINDEX(''-'',CP.path,CHARINDEX(''-'',CP.path)+1))-CHARINDEX(''-'',CP.path)+1) , ''-''+ '''+cast(@idSiteDestination as nvarchar)+''' +''-'')) - CHARINDEX(''/'',REPLACE(CP.path, 
				     SUBSTRING ( CP.path ,CHARINDEX(''-'',CP.path), (CHARINDEX(''-'',CP.path,CHARINDEX(''-'',CP.path)+1))-CHARINDEX(''-'',CP.path)+1) , ''-''+ '''+cast(@idSiteDestination as nvarchar)+''' +''-''))-1)) , '''+cast(@idAccount as nvarchar)+''')) '
				+',	CP.kb,
					CP.idContentPackageType,
					CP.idSCORMPackageType,
					CP.manifest,
					CP.dtCreated,
					CP.dtModified,
					CP.isMediaUpload,
					CP.idMediaType,
					CP.contentTitle,
					CP.originalMediaFilename,
					CP.isVideoMedia3rdParty,
					CP.videoMediaEmbedCode,
					CP.enableAutoplay,
					CP.allowRewind,
					CP.allowFastForward,
					CP.allowNavigation,
					CP.allowResume,
					CP.minProgressForCompletion,
					CP.isProcessing,
					CP.isProcessed,
					CP.dtProcessed,
					TTQS.newDestinationId'
				+' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblContentPackage] CP'
				+' LEFT JOIN #QuizSurveyIdMappings TTQS on (TTQS.oldSourceId = CP.idQuizSurvey)'
				+' WHERE CP.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
		 
	   EXEC(@sql)
		
	   -- temporary table to hold the idContentPackage of source and destination ContentPackage tables
	   CREATE TABLE #ContentPackageIdMappings 
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL  	   
	   )
	   
	   -- inserting idContentPackage of source ContentPackage table and destination ContentPackage table inside temporary table for mapping
	   SET @sql =   ' INSERT INTO #ContentPackageIdMappings'
				+' ( '
				+'	 oldSourceId,
					 newDestinationId'
				+' ) '
				+' SELECT 
					 SRC.idContentPackage, 
					 DST.idContentPackage'
				+' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblContentPackage] DST'
				+' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblContentPackage] SRC'
				+' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
				+' AND (SRC.name = DST.name)'
				+' AND (SRC.dtCreated = DST.dtCreated )'
				+' AND (SRC.dtModified = DST.dtModified )'
				+' )'
				+' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)

	   EXEC(@sql)

	   
	   -- There is cross dependency between tblContentPackage and tblQuizSurvey, So we need to update tblQuizSurvey
	   -- and set idContentPackage after we have moved tblContentPackage data.
	    SET @sql =  ' UPDATE DSTQS'
	    		+ '  SET idContentPackage = TEMP.newDestinationId'
	    		+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblQuizSurvey] SRCQS'
	    		+ ' INNER JOIN  [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblQuizSurvey] DSTQS'
	    		+ ' ON( SRCQS.guid = DSTQS.guid'
	    		+ ' AND (SRCQS.dtCreated = DSTQS.dtCreated)'
	    		+ ' AND SRCQS.idSite ='+ CAST(@idSiteSource AS NVARCHAR)+ ')'
	    		+ ' INNER JOIN  #ContentPackageIdMappings TEMP'
	    		+ ' ON(TEMP.oldSourceId = SRCQS.idContentPackage)'
	    		+'  WHERE DSTQS.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	    		+'  AND SRCQS.idContentPackage IS NOT NULL'

		EXEC(@sql)				
		
	   
	    /*
	   Drop temporary table QuizSurveyIdMappings, temporary table QuizSurvey will be dorp in both conditions full copy and partial copy
	   */
	   DROP TABLE #QuizSurveyIdMappings


-- =================================================================================
-- LOGIC START -- copy tblResourceType object for both full copy and partial copy---
		
	   -- insert records from source site to destination table ResourceType
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblResourceType]'
	   		       +' ( '
	                 +'	  idSite, 
	   		   	       resourceType,
	   		   	       isDeleted,
	   		   	       dtDeleted'
	   		       +' ) '
	   		       +' SELECT '
	   		       +	  CAST(@idSiteDestination AS NVARCHAR)
	   		       +' , RT.resourceType,
	   		   	       RT.isDeleted,
	   		   	       RT.dtDeleted 
	   		          FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblResourceType] RT'
	                 +' WHERE RT.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
                										
	   EXEC(@sql)
	   
	   -- temporary table to hold the idResourceType of source and destination ResourceType tables
	   CREATE TABLE #ResourceTypeIdMappings 
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL  	   
	   )
	   
	   -- inserting idResourceType of source ResourceType table and destination ResourceType table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #ResourceTypeIdMappings'
				  +' ( '
				  +'	   oldSourceId,
				  	   newDestinationId'
				  +' ) '
				  +' SELECT 
					   SRC.idResourceType, 
					   DST.idResourceType'
				  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblResourceType] DST'
				  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblResourceType] SRC'
				  +' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
				  +' AND (SRC.resourceType = DST.resourceType)'
				  +' AND (SRC.isDeleted = DST.isDeleted)'
				  +' AND (SRC.dtDeleted = DST.dtDeleted OR (SRC.dtDeleted IS NULL AND DST.dtDeleted IS NULL))'
				  +'	)'
				  +' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
			
	   EXEC(@sql)					
	   		
	   -- resourceType language data move logic start
	   
	   -- insert records from source site to destination table ResourceTypeLanguage
	   SET @sql =	    ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblResourceTypeLanguage]'
	   			  + ' ( '
	   			  + '  idSite, 
	   			       idResourceType, 
	   			  	  idLanguage, 
	   			  	  resourceType '
	   			  + ' ) '
	   			  +' SELECT '
				  +	  CAST(@idSiteDestination AS NVARCHAR)
	   			  +' , TTRT.newDestinationId, 
	   			  	  RTL.idLanguage, 
	   			       RTL.resourceType 
	   			     FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblResourceTypeLanguage] RTL '
	   			  +' LEFT JOIN #ResourceTypeIdMappings TTRT on (TTRT.oldSourceId = RTL.idResourceType)'
	   			  +' WHERE RTL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   						
	   			
	   EXEC(@sql)

	   -- resourceType language data move logic end

	   
------------------------------------------------------------------------------
    	   
	   -- check condition for the full copy
	   IF @fullCopyFlag = 1
	   BEGIN

	   
-- =============================================================================
-- LOGIC START -- copy tblResource object for full copy ------------------------


	   -- insert records from source site to destination table Resource
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblResource]'
				  +'  ( '
			       +'  idSite, 
				      name,
				      idParentResource,
				      idOwner,
				      idResourceType,
				      description,
				      identifier,
				      isMoveable,
				      isAvailable,
				      capacity,
				      locationRoom,
				      locationBuilding,
				      locationCity,
				      locationProvince,
				      locationCountry,
				      ownerWithinSystem,
				      ownerName,
				      ownerEmail,
				      ownerContactInformation,
				      isDeleted'
                	  +'  ) '
                	  +' SELECT '
				  +	 CAST(@idSiteDestination AS NVARCHAR)
                	  +' ,R.name, '
				  +'  NULL, '             -- this null value of idParentResource will be updated from temporary ResourceIdMappings table below  
				  +'  TTU.newDestinationId,
				      TTRT.newDestinationId,
				      R.description,
				      R.identifier,
				      R.isMoveable,
				      R.isAvailable,
				      R.capacity,
				      R.locationRoom,
				      R.locationBuilding,
				      R.locationCity,
				      R.locationProvince,
				      R.locationCountry,
				      R.ownerWithinSystem,
				      R.ownerName,
				      R.ownerEmail,
				      R.ownerContactInformation,
				      R.isDeleted 
                	     FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblResource] R'
                	  +' LEFT JOIN #ResourceTypeIdMappings TTRT on (TTRT.oldSourceId = R.idResourceType)'
				  +' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = R.idOwner)'
				  +' WHERE R.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
     										
	   EXEC(@sql)

	   -- temporary table to hold the idResource of source and destination Resource tables
	   CREATE TABLE #ResourceIdMappings 
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL  	   
	   )
	   
	   -- inserting idResource of source Resource table and destination Resource table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #ResourceIdMappings'
	   		  	  +'  ( '
	   		  	  +'	   oldSourceId,
	   		  	  	   newDestinationId'
	   		  	  +'  ) '
	   		  	  +' SELECT 
				  	   SRC.idResource, 
				  	   DST.idResource'
	   		  	  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblResource] DST'
	   		  	  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblResource] SRC'
	   		  	  +' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   		  	  +' AND (SRC.name = DST.name)'
	   		  	  +' AND (SRC.locationRoom = DST.locationRoom OR (SRC.locationRoom IS NULL AND DST.locationRoom IS NULL))'
	   		  	  +' AND (SRC.locationBuilding = DST.locationBuilding OR (SRC.locationBuilding IS NULL AND DST.locationBuilding IS NULL))'
	   		  	  +' AND (SRC.locationCity = DST.locationCity OR (SRC.locationCity IS NULL AND DST.locationCity IS NULL))'
	   		  	  +' AND (SRC.ownerEmail = DST.ownerEmail OR (SRC.ownerEmail IS NULL AND DST.ownerEmail IS NULL))'
	   		  	  +' AND (SRC.dtDeleted = DST.dtDeleted OR (SRC.dtDeleted IS NULL AND DST.dtDeleted IS NULL))'
	   		  	  +')'
	   		  	  +' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   
	  EXEC(@sql)
	   
	   -- update idParentResource from temp table resource
	   SET @sql =	   ' UPDATE DSTR'
	   		  	  +'  SET idParentResource = TEMP.newDestinationId'
	   		  	  +' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblResource] SRCR'
	   		  	  +' INNER JOIN  [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblResource] DSTR'
	   		  	  +' ON(SRCR.name = DSTR.name AND (SRCR.ownerEmail = DSTR.ownerEmail OR (SRCR.ownerEmail IS NULL AND DSTR.ownerEmail IS NULL)) 
	   		  	     AND (SRCR.locationRoom = DSTR.locationRoom OR (SRCR.locationRoom IS NULL AND DSTR.locationRoom IS NULL))
	   		  	     AND (SRCR.locationBuilding = DSTR.locationBuilding OR (SRCR.locationBuilding IS NULL AND DSTR.locationBuilding IS NULL)) 
	   		  	     AND (SRCR.locationCity = DSTR.locationCity OR (SRCR.locationCity IS NULL AND DSTR.locationCity IS NULL))
	   		  	     AND SRCR.idSite ='+ CAST(@idSiteSource AS NVARCHAR) +')'
	   		  	  +' INNER JOIN  #ResourceIdMappings TEMP'
	   		  	  +' ON(TEMP.oldSourceId = SRCR.idParentResource)'
	   		  	  +' WHERE DSTR.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   		  	  +' AND SRCR.idParentResource IS NOT NULL'
	   
	   EXEC(@sql)	
											
						
	   -- resource language data move logic start
	   
	   -- insert records from source site to destination table ResourceLanguage
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblResourceLanguage]'
	   			 + ' ( '
	   			 + '	  idSite, 
	   				  idResource, 
	   				  idLanguage, 
	   				  name, 
	   				  description'
	   			 + ' ) '
	   			 +' SELECT '
				 +	  CAST(@idSiteDestination AS NVARCHAR)
	   			 +' ,  TTR.newDestinationId, 
	   			 	  RL.idLanguage, 
	   			 	  RL.name, 
	   			 	  RL.description 
	   			    FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblResourceLanguage] RL'
	   			 +' LEFT JOIN #ResourceIdMappings TTR on (TTR.oldSourceId = RL.idResource)'
	   			 +' WHERE RL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 			
	   			
	   EXEC(@sql)

	   -- resource language data move logic end


	   END
----------------------------------------------------------------------------------
--full copy end


	   /*
	   Drop temporary table ResourceTypeIdMappings, temporary table ResourceType will be dorp in both conditions full copy and partial copy 
	   */
	   DROP TABLE #ResourceTypeIdMappings


-- ====================================================================================
-- LOGIC START -- copy tblStandUpTraining object for both full copy and partial copy ---


	   -- insert records from source site to destination table StandUpTraining
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblStandUpTraining]'
	   			  +'  ( '
	   			  +'    idSite, 
	   			  	   title,
	   			  	   [description],
	   			  	   isStandaloneEnroll,
	   			  	   isRestrictedEnroll,
	   			  	   isRestrictedDrop,
	   			  	   searchTags, 
					   isDeleted,
					   dtDeleted,
					   avatar '
	   			  +'  ) '
	   			  +' SELECT '
				  +	   CAST(@idSiteDestination AS NVARCHAR)
	   			  +' ,  ST.title,
	   			  	   ST.[description],
	   			  	   ST.isStandaloneEnroll,
	   			  	   ST.isRestrictedEnroll,
	   			  	   ST.isRestrictedDrop,
					   ST.searchTags, 
					   ST.isDeleted,
					   ST.dtDeleted,
					   ST.avatar '
	   			  +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblStandUpTraining] ST '
	   			  +' WHERE ST.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			
	   EXEC(@sql)
	   					
			 						
	   -- temporary table to hold the idStandUpTraining of source and destination StandUpTraining tables
	   CREATE TABLE #StandUpTrainingIdMappings 
	   (
		  oldSourceId INT NOT NULL, 
		  newDestinationId INT NOT NULL  	   
	   )
	   
	   -- inserting idStandUpTraining of source StandUpTraining table and destination StandUpTraining table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #StandUpTrainingIdMappings'
	   			  +'  ( '
	   			  +'   oldSourceId,
	   			  	  newDestinationId'
	   			  +'  ) '
	   			  +' SELECT 
					  SRC.idStandUpTraining, 
					  DST.idStandUpTraining'
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblStandUpTraining] DST'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblStandUpTraining] SRC'
	   			  +' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (SRC.title = DST.title)'
	   			  +' AND (SRC.dtDeleted = DST.dtDeleted OR (SRC.dtDeleted IS NULL AND DST.dtDeleted IS NULL))'
	   			  +' AND (SRC.description = DST.description OR (SRC.description IS NULL AND DST.description IS NULL))'
	   			  +')'
	   			  +' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   		
	   EXEC(@sql)
	   
	   	
	   -- standUpTraining language data move logic start
	   				
	   -- insert records from source site to destination table StandUpTrainingLanguage
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblStandUpTrainingLanguage]'
	   		      + ' ( '
	   		      + '   idSite, 
	   			       idStandUpTraining, 
	   			       idLanguage, 
	   			       title, 
	   			       Description, 
	   			       searchTags'
	   		      + ' ) '
	   		      +' SELECT '
				 +	  CAST(@idSiteDestination AS NVARCHAR)
	   		      +' ,  TTST.newDestinationId, 
	   		       	  STL.idLanguage, 
	   		       	  STL.title, 
	   		      	  STL.Description, 
	   		       	  STL.searchTags 
	   		         FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblStandUpTrainingLanguage] STL '
	   		      +' LEFT JOIN #StandUpTrainingIdMappings TTST on (TTST.oldSourceId = STL.idStandUpTraining )'
	   		      +' WHERE STL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   		
	   
	   EXEC(@sql)

	   -- standUpTraining language data move logic end

	   -- insert temporary table StandUpTrainingIdMappings data, in order to return it for moving file system object
		INSERT INTO #ObjectMapping
		([oldId],[newId],objectName)
		SELECT oldSourceId, newDestinationId, 'standuptraining' FROM #StandUpTrainingIdMappings

   
------------------------------------------------------------------------------

   	   -- check condition for the full copy
	   IF @fullCopyFlag = 1
	   BEGIN

-- =============================================================================
-- LOGIC START -- copy tblStandUpTrainingInstance object for full copy ---------


	   -- insert records from source site to destination table StandUpTrainingInstance
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblStandUpTrainingInstance]'
	   			  +'  ( '
	   			  +'	   idSite,
	   			  	   idStandupTraining,
	   			  	   title,
	   			  	   [description],
	   			  	   seats,
	   			  	   waitingSeats,
	   			  	   [type],
	   			  	   urlRegistration,
	   			  	   urlAttend,
	   			  	   city,
	   			  	   province,
	   			  	   locationDescription,
	   			  	   idInstructor,
					   isDeleted,
					   dtDeleted '
	   			  +'  ) '
	   			  +' SELECT '
				  +	   CAST(@idSiteDestination AS NVARCHAR)
	   			  +' ,  TTST.newDestinationId,
	   			  	   STI.title,
	   			  	   STI.[description],
	   			  	   STI.seats,
	   			  	   STI.waitingSeats,
	   			  	   STI.[type],
	   			  	   STI.urlRegistration,
	   			  	   STI.urlAttend,
	   			  	   STI.city,
	   			  	   STI.province,
	   			  	   STI.locationDescription,
	   			  	   TTU.newDestinationId, 
					   STI.isDeleted,
					   STI.dtDeleted '
	   			  +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblStandUpTrainingInstance] STI'
	   			  +' LEFT JOIN #StandUpTrainingIdMappings TTST on (TTST.oldSourceId = STI.idStandUpTraining )'
	   			  +' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = STI.idInstructor )'
	   			  +' WHERE STI.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 	
	   EXEC(@sql)
	   					
	   -- temporary table to hold the idStandUpTrainingInstance of source and destination StandUpTrainingInstance tables
	   CREATE TABLE #StandUpTrainingInstanceIdMappings
	   (
	   	oldSourceId INT NOT NULL, 
	   	newDestinationId INT NOT NULL 	   
	   )

	   -- inserting idStandUpTrainingInstance of source StandUpTrainingInstance table and destination StandUpTrainingInstance table inside temporary table for mapping
	   SET @sql =	     ' INSERT INTO #StandUpTrainingInstanceIdMappings'
				   + ' ( '
				   + '	 oldSourceId,
				   		 newDestinationId'
				   + ' ) '
				   +' SELECT 
				   		 SRC.idStandUpTrainingInstance, 
				   		 DST.idStandUpTrainingInstance '
				   +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblStandUpTrainingInstance] DST'
				   +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblStandUpTrainingInstance] SRC'
				   +' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
				   +' AND (SRC.title = DST.title)'
				   +' AND (SRC.dtDeleted = DST.dtDeleted OR (SRC.dtDeleted IS NULL AND DST.dtDeleted IS NULL))'
				   +' AND (SRC.description = DST.description OR (SRC.description IS NULL AND DST.description IS NULL))'
				   +' )'
				   +' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
				
	   EXEC(@sql)

		-- standUpTrainingInstance language data move logic start
						
		-- insert records from source site to destination table StandUpTrainingInstanceLanguage
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblStandUpTrainingInstanceLanguage]'
				  +' ( '
				  +'    idSite,
				  	   idStandUpTrainingInstance,
				  	   idLanguage,
				  	   title,
				  	   [description],
				  	   locationDescription'
				  +' ) '
				  +' SELECT '
				  +	   CAST(@idSiteDestination AS NVARCHAR)
				  +' ,  TTSTI.newDestinationId,
				  	   STIL.idLanguage,
				  	   STIL.title,
				  	   STIL.[description],
				  	   STIL.locationDescription 
				     FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblStandUpTrainingInstanceLanguage] STIL'
				  +' LEFT JOIN #StandUpTrainingInstanceIdMappings TTSTI on (TTSTI.oldSourceId = STIL.idStandUpTrainingInstance )'
				  +' WHERE STIL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
				
	   EXEC(@sql)

		-- standUpTrainingInstance language data move logic end

		
-- =============================================================================
-- LOGIC START -- copy tblResourceToObjectLink object for full copy ------------


	   -- insert records from source site to destination table ResourceToObjectLink
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblResourceToObjectLink]'
	   			  +' ( '
	   			  +'    idSite,
				  	   idResource,
				  	   idObject,
				  	   objectType,
				  	   dtStart,
				  	   dtEnd,
				  	   isOutsideUse,
				  	   isMaintenance'
	   			  +' ) '
	   			  +' SELECT '
				  +	   CAST(@idSiteDestination AS NVARCHAR)
	   			  +' ,  TTR.newDestinationId,'
	   			  +'	   CASE RTOL.objectType
				  	     WHEN ''standuptraining'' THEN
				  	   	 TTSTI.newDestinationId
				  	     ELSE
				  	   	 0
					   END'
				  +' ,  RTOL.objectType,
				  	   RTOL.dtStart,
				  	   RTOL.dtEnd,
				  	   RTOL.isOutsideUse,
				  	   RTOL.isMaintenance 
	   			     FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblResourceToObjectLink] RTOL'
	   			  +' LEFT JOIN #ResourceIdMappings TTR on (TTR.oldSourceId = RTOL.idResource)'
	   			  +' LEFT JOIN #StandUpTrainingInstanceIdMappings TTSTI on (TTSTI.oldSourceId = RTOL.idObject)'
				  +' WHERE RTOL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   				
	   EXEC(@sql)


	END	

------------------------------------------------------------------------------
-- full copy end	

	
-- ==========================================================================================
-- LOGIC START -- copy tblLessonToContentLink object for both full copy and partial copy ----


	   -- insert records from source site to destination table LessonToContentLink
	   SET @sql =   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblLessonToContentLink]'
				+' ( '
				+'    idSite,
					 idLesson,
					 idContentType,
					 idAssignmentDocumentType,
					 assignmentResourcePath,
					 allowSupervisorsAsProctor,
					 allowCourseExpertsAsProctor,
					 idObject'
				+' ) '
				+' SELECT '+ CAST(@idSiteDestination AS NVARCHAR)
				+',	 TTL.newDestinationId,
					 LTCL.idContentType,
					 LTCL.idAssignmentDocumentType,
					 LTCL.assignmentResourcePath,
					 LTCL.allowSupervisorsAsProctor,
					 LTCL.allowCourseExpertsAsProctor,
					 CASE LTCL.idContentType
					   WHEN 1 THEN
						  TTCP.newDestinationId
					   WHEN 2 THEN
						  TTST.newDestinationId
					   WHEN 3 THEN 
						  NULL
					   WHEN 4 THEN
						  TTCP.newDestinationId
					   END '
	   			    +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblLessonToContentLink] LTCL'
	   			    +' LEFT JOIN #LessonIdMappings TTL on (TTL.oldSourceId = LTCL.idLesson)'
	   			    +' LEFT JOIN #ContentPackageIdMappings TTCP on (TTCP.oldSourceId = LTCL.idObject)'
				    +' LEFT JOIN #StandUpTrainingIdMappings TTST on (TTST.oldSourceId = LTCL.idObject)'
				    +' WHERE LTCL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
		  
	   EXEC(@sql)


	   /*
	   Drop temporary table ContentPackageIdMappings, temporary table ContentPackage will be dorp in both conditions full copy and partial copy 
	   */	   
	   DROP TABLE #ContentPackageIdMappings


-- =============================================================================
-- LOGIC START -- copy tblRole object for both full copy and partial copy -------


	   -- insert records from source site to destination table Role
	   SET @sql =	  'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblRole]'
	   			 +' ( '
	   			 +'	 idSite,
	   				 name'
	   			 +' ) '
	   			 +' SELECT '
				 +    CAST(@idSiteDestination AS NVARCHAR)
	   			 +' , R.name
	   			    FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblRole] R'
	   			 +' WHERE R.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   		      +' AND R.idRole != 1' -- idRole 1 is already inserted while creating db structure.
	   			
	   EXEC(@sql)
	   
	     -- temporary table to hold the idRole of source and destination Role tables
	   	CREATE TABLE #RoleIdMappings
	   	(
	   		oldSourceId INT NOT NULL, 
	   		newDestinationId INT NOT NULL 	   
	   	)
	   
	   	-- initially insert row for temporary table role record
	   	INSERT INTO #RoleIdMappings(
	   			oldSourceId,
	   			newDestinationId
	   		)
	   		VALUES
	   		(
	   			1,
	   			1
	   		)

	   -- inserting idRole of source Role table and destination Role table inside temporary table for mapping
	   SET @sql =	  'INSERT INTO #RoleIdMappings'
	   			 +' ( '
	   			 +'	   oldSourceId,
	   			 	   newDestinationId'
	   			 +' ) '
	   			 +' SELECT 
					   SRC.idRole,
					   DST.idRole '
	   			 +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblRole] DST'
	   			 +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblRole] SRC'
	   			 +' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			 +' AND (SRC.name = DST.name)'
	   			 +'  )'
	   			 +' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   			 +' AND DST.idRole != 1' -- idRole 1 is already inserted while creating db structure.
	   
	   EXEC(@sql)
	   			

	   -- role language data move logic start
	   
	   -- insert records from source site to destination table RoleLanguage
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblRoleLanguage]'
	   			+ ' ( '
	   			+ ' idSite,
	   			    idRole,
	   			    idLanguage,
	   			    name'
	   			+ ' ) '
	   			+' SELECT '
				+    CAST(@idSiteDestination AS NVARCHAR)
	   			+' , TTR.newDestinationId, 
	   				RL.idLanguage, 
	   				RL.name
	   			   FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblRoleLanguage] RL'
	   			+' LEFT JOIN #RoleIdMappings TTR on (TTR.oldSourceId = RL.idRole)'
	   			+' WHERE RL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			+' AND RL.idRole != 1' -- idRole 1 is already inserted while creating db structure.
	   						
	   			
	   EXEC(@sql)

	   -- role language data move logic end
	   

-- ==========================================================================================
-- LOGIC START -- copy tblRoleToPermissionLink object for both full copy and partial copy ---


	   -- insert records from source site to destination table RoleToPermissionLink
	   SET @sql =	  'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblRoleToPermissionLink]'
	   			 +' ( '
	   			 +'  idSite,
	   			     idRole,
	   		          idPermission,
	   		          scope'
	   			 +'  ) '
	   			 +' SELECT '
				 +	 CAST(@idSiteDestination AS NVARCHAR)
	   			 +' , TTR.newDestinationId,
	   		           RTPL.idPermission,
	   		           RTPL.scope
	   			    FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblRoleToPermissionLink] RTPL'
	   			 +' LEFT JOIN #RoleIdMappings TTR on (TTR.oldSourceId = RTPL.idRole )'
	   		      +' WHERE RTPL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   		      +' AND RTPL.idRole != 1' -- idRole 1 is already inserted while creating db structure.
	   		
	   EXEC(@sql)
	   


-- =================================================================================
-- LOGIC START -- copy tblRuleset object for both full copy and partial copy -------


	   -- insert records from source site to destination table Ruleset
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblRuleset]'
	     		  +' ( '
	     		  +'	  [idSite],
	   			  	  [isAny],
	   			  	  [label]'
	     		  +'  ) '
	     		  +' SELECT '
				  +	   CAST(@idSiteDestination AS NVARCHAR)
	     		  +' ,  R.[isAny],
	   			  	   R.[label] 
	     		     FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblRuleset] R'
	     		  +' WHERE R.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	     
	   EXEC(@sql)
		  						
	   -- temporary table to hold the idRuleset of source and destination Ruleset tables
	   CREATE TABLE #RulesetIdMappings 
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL 	   
	   )

	   -- inserting idRuleset of source Ruleset table and destination Ruleset table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #RulesetIdMappings'
	   			  +' ( '
	   			  +'	   oldSourceId,
	   			  	   newDestinationId '
	   			  +' ) '
	   			  +' SELECT 
					   SRC.idRuleset, 
					   DST.idRuleset '
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblRuleset] DST'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblRuleset] SRC'
	   			  +' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (SRC.label = DST.label)'
	   			  +' AND (SRC.isAny = DST.isAny)'
	   			  +' )'
	   			  +' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   
	   EXEC(@sql)
	   
	   -- ruleset language data move logic start

	   -- insert records from source site to destination table RulesetLanguage
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblRulesetLanguage]'
	   			  +' ( '
	   			  +'   idSite, 
	   			  	  idRuleset, 
	   			  	  idLanguage, 
	   			  	  label'
	   			  +'  ) '
	   			  +' SELECT '
				  +	  CAST(@idSiteDestination AS NVARCHAR)
	   			  +' , TTR.newDestinationId, 
	   			  	  RL.idLanguage, 
	   			  	  RL.label 
	   			     FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblRulesetLanguage] RL'
	   			  +' LEFT JOIN #RulesetIdMappings TTR on (TTR.oldSourceId = RL.idRuleset )'
	   			  +' WHERE RL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   		
	   EXEC(@sql)
		     	              
	   -- ruleset language data move logic end


-- =============================================================================
-- LOGIC START -- copy tblRule object for both full copy and partial copy -------


	   -- insert records from source site to destination table Rule
	   SET @sql =	   'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblRule]'
	   			  +' ( '
	   			  +'   idSite ,
	   				  idRuleSet ,
	   				  userField ,
	   				  operator ,
	   				  textValue ,
	   				  dateValue ,
	   				  numValue ,
	   				  bitValue'
	   			  +'  ) '
	   			  +' SELECT '
				  +	  CAST(@idSiteDestination AS NVARCHAR)
	   			  +' , TTR.newDestinationId, 
	   			  	  R.userField ,
	   				  R.operator ,
	   				  R.textValue ,
	   				  R.dateValue ,
	   				  R.numValue ,
	   				  R.bitValue 
	   			     FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblRule] R'
	   			  +' LEFT JOIN #RulesetIdMappings TTR on (TTR.oldSourceId = R.idRuleSet )'
	   			  +' WHERE R.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			
	   EXEC(@sql)
	   


-- ========================================================================================
-- LOGIC START -- copy tblRuleSetToGroupLink object for both full copy and partial copy ----


	   -- insert records from source site to destination table RuleSetToGroupLink
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblRuleSetToGroupLink]'
	   			  +' ( '
	   			  +'	   idSite,
	   			  	   idRuleSet,
	   			  	   idGroup'
	   			  +' ) '
	   			  +' SELECT '
				  +	   CAST(@idSiteDestination AS NVARCHAR)
	   			  +' ,  TTR.newDestinationId, 
	   			  	   TTG.newDestinationId 
	   			     FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblRuleSetToGroupLink] RTGL'
	   			  +' LEFT JOIN #RuleSetIdMappings TTR on (TTR.oldSourceId = RTGL.idRuleSet )'
	   			  +' LEFT JOIN #GroupIdMappings TTG on (TTG.oldSourceId = RTGL.idGroup )'
	   			  +' WHERE RTGL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			
	   EXEC(@sql)
	   


-- =======================================================================================
-- LOGIC START -- copy tblRuleSetToRoleLink object for both full copy and partial copy ----

	   -- insert records from source site to destination table RuleSetToRoleLink
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblRuleSetToRoleLink]'
	   			  +' ( '
	   			  +'  idSite,
	   				 idRuleSet,
	   				 idRole'
	   			  +' ) '
	   			  +' SELECT '
				  +	  CAST(@idSiteDestination AS NVARCHAR)
	   			  +' , TTR.newDestinationId, 
	   			  	  TTRole.newDestinationId 
	   			     FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblRuleSetToRoleLink] RTRL'
	   			  +' LEFT JOIN #RuleSetIdMappings TTR on (TTR.oldSourceId = RTRL.idRuleSet )'
	   			  +' LEFT JOIN #RoleIdMappings TTRole on (TTRole.oldSourceId = RTRL.idRole )'
	   			  +' WHERE RTRL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			
	   EXEC(@sql)


------------------------------------------------------------------------------
	   
	   -- check condition for the full copy
	   IF @fullCopyFlag = 1
	   BEGIN



	    /*
	   Drop temporary table ResourceIdMappings
	   */
	   DROP TABLE #ResourceIdMappings


-- ===================================================================================
-- LOGIC START -- copy tblStandUpTrainingInstanceMeetingTime object for full copy ----


	   -- insert records from source site to destination table StandUpTrainingInstanceMeetingTime
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblStandUpTrainingInstanceMeetingTime]'
	   			  +' ( '
	   			  +'   idSite,
	   			  	  idStandUpTrainingInstance,
	   			  	  dtStart,
	   			  	  dtEnd,
	   			  	  [minutes],
	   			  	  idTimezone'
	   			  +' ) '
	   			  +' SELECT '
	   			  +	  CAST(@idSiteDestination AS NVARCHAR)
	   			  +' , TTSTI.newDestinationId,
	   			  	  STIMT.dtStart,
	   			  	  STIMT.dtEnd,
	   			  	  STIMT.[minutes],
	   			  	  STIMT.idTimezone 
	   			     FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblStandUpTrainingInstanceMeetingTime] STIMT'
	   			  +' LEFT JOIN #StandUpTrainingInstanceIdMappings TTSTI on (TTSTI.oldSourceId = STIMT.idStandUpTrainingInstance )'
	   			  +' WHERE STIMT.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			
	   EXEC(@sql)


-- ==================================================================================
-- LOGIC START -- copy tblStandupTrainingInstanceToUserLink object for full copy ----


	   -- insert records from source site to destination table StandupTrainingInstanceToUserLink
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblStandupTrainingInstanceToUserLink]'
	   			  +' ( '
	   			  +'   idUser,
	   			  	  idStandupTrainingInstance,
	   			  	  completionStatus,
	   			  	  successStatus,
	   			  	  score,
	   			  	  isWaitingList,
	   			  	  dtCompleted'
	   			  +'  ) '
	   			  +' SELECT '
	   			  +'   TTU.newDestinationId,
	   			  	  TTSTI.newDestinationId,
	   			  	  STITUL.completionStatus,
	   			  	  STITUL.successStatus,
	   			  	  STITUL.score,
	   			  	  STITUL.isWaitingList,
	   			  	  STITUL.dtCompleted 
	   			     FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblStandupTrainingInstanceToUserLink] STITUL'
	   			  +' INNER JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblUser] U'
	   			  +' ON(STITUL.idUser = U.idUser AND U.idSite = '+ CAST(@idSiteSource as nvarchar)+ ')'
	   			  +' LEFT JOIN #StandUpTrainingInstanceIdMappings TTSTI on (TTSTI.oldSourceId = STITUL.idStandUpTrainingInstance)'
	   			  +' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = STITUL.idUser)'
	   			  +' WHERE U.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   EXEC(@sql)


-- =============================================================================
-- LOGIC START -- copy tblUserToGroupLink object for full copy -----------------


	   -- insert records from source site to destination table UserToGroupLink
	   SET @sql =	  'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblUserToGroupLink]'
	   			 +' ( '
	   			 +'   idSite,
	   			 	 idUser,
	   			 	 idGroup,
	   			 	 idRuleSet,
	   			 	 created,
	   			 	 selfJoined'
	   			 +'  ) '
	   			 +' SELECT '
				 +    CAST(@idSiteDestination AS NVARCHAR)
	   			 +' , TTU.newDestinationId,
	   			 	 TTG.newDestinationId,
	   			 	 TTRS.newDestinationId,
	   			 	 UTGL.created,
	   			 	 UTGL.selfJoined
	   			    FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblUserToGroupLink] UTGL'
	   			 +' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = UTGL.idUser )'
	   			 +' LEFT JOIN #GroupIdMappings TTG on (TTG.oldSourceId = UTGL.idGroup )'
	   			 +' LEFT JOIN #RulesetIdMappings TTRS on (TTRS.oldSourceId = UTGL.idRuleset )'
	   			 +' WHERE UTGL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   EXEC(@sql)



-- =============================================================================
-- LOGIC START -- copy tblUserToRoleLink object for full copy ------------------


	   -- insert records from source site to destination table UserToRoleLink
	   SET @sql =	  'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblUserToRoleLink]'
	   			 +' ( '
	   			 +'   idSite,
	   			      idUser,
	   			      idRole,
	   			      idRuleSet,
	   			      created'
	   			 +'  ) '
	   			 +' SELECT '
				 +    CAST(@idSiteDestination AS NVARCHAR)
	   			 +' , TTU.newDestinationId,
	   			      TTR.newDestinationId,
	   			      TTRS.newDestinationId,
	   			      UTRL.created
	   			    FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblUserToRoleLink] UTRL'
	   			 +' LEFT JOIN #RoleIdMappings TTR on (TTR.oldSourceId = UTRL.idRole )'
	   			 +' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = UTRL.idUser )'
	   			 +' LEFT JOIN #RulesetIdMappings TTRS on (TTRS.oldSourceId = UTRL.idRuleset )'
	   			 +' WHERE UTRL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   EXEC(@sql)



-- =============================================================================
-- LOGIC START -- copy tblGroupToRoleLink object for full copy -----------------


	   -- insert records from source site to destination table GroupToRoleLink
	   SET @sql =	  'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblGroupToRoleLink]'
	   			 +' ( '
	   			 +'   idSite,
	   				 idGroup,
	   				 idRole,
	   				 idRuleSet,
	   				 created '
	   			 +' ) '
	   			 +' SELECT '
				 +	  CAST(@idSiteDestination AS NVARCHAR)
	   			 +' ,  TTG.newDestinationId,
	   				  TTR.newDestinationId,
	   				  TTRS.newDestinationId,
	   				  GTRL.created
	   			    FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblGroupToRoleLink] GTRL'
	   			 +' LEFT JOIN #RoleIdMappings TTR on (TTR.oldSourceId = GTRL.idRole )'
	   			 +' LEFT JOIN #GroupIdMappings TTG on (TTG.oldSourceId = GTRL.idGroup )'
	   			 +' LEFT JOIN #RulesetIdMappings TTRS on (TTRS.oldSourceId = GTRL.idRuleset )'
	   			 +' WHERE GTRL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   EXEC(@sql)

    END

------------------------------------------------------------------------------
-- full copy end


	    /*
	   Drop temporary Table RoleIdMappings, temporary table Role will be dorp in both conditions full copy and partial copy
	   */
	   DROP TABLE #RoleIdMappings

-- ===================================================================================
-- LOGIC START -- copy tblLearningPath object for both full copy and partial copy-----


	   -- insert records from source site to destination table LearningPath
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblLearningPath]'
	   			 +' ( '
	   			 +'    idSite,'
	   			 +'	  name,'
	   			 +'	  shortDescription,'
	   			 +'	  longDescription,'
	   			 +'	  cost,'
	   			 +'	  isPublished,'
	   			 +'	  isClosed,'
	   			 +'	  forceCompletionInOrder,'
	   			 +'	  dtCreated,'
	   			 +'	  dtModified,'
	   			 +'	  isDeleted,'
	   			 +'	  dtDeleted,'
	   			 +'	  avatar,'
	   			 +'	  searchTags'
	   			 +'  ) '
	   			 +' SELECT '
	   			 +     CAST(@idSiteDestination AS NVARCHAR)
	   			 +'	, LP.name,'
	   			 +'	  LP.shortDescription,'
	   			 +'	  LP.longDescription,'
	   			 +'	  LP.cost,'
	   			 +'	  LP.isPublished,'
	   			 +'	  LP.isClosed,'
	   			 +'	  LP.forceCompletionInOrder,'
	   			 +'	  LP.dtCreated,'
	   			 +'	  LP.dtModified,'
	   			 +'	  LP.isDeleted,'
	   			 +'	  LP.dtDeleted,'
	   			 +'	  LP.avatar,'
	   			 +'	  LP.searchTags '
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblLearningPath] LP'
	   			 +' WHERE LP.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   	
	   EXEC(@sql)
	   		
	   -- temporary table to hold the idLearningPath of source and destination LearningPath tables
	   CREATE TABLE #LearningPathIdMappings
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL  	   
	   )	

	   -- inserting idLearningPath of source LearningPath table and destination LearningPath table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #LearningPathIdMappings '
	   			  +' ( '
	   			  +'	   oldSourceId,
	   			  	   newDestinationId'
	   			  +' ) '
	   			  +' SELECT 
					   SLP.idLearningPath, 
					   DLP.idLearningPath '
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblLearningPath] DLP'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblLearningPath] SLP'
	   			  +' ON (SLP.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (SLP.dtCreated = DLP.dtCreated) '
	   			  +' AND (SLP.dtModified = DLP.dtModified OR (SLP.dtModified IS NULL AND DLP.dtModified IS NULL))'					
	   			  +' AND (SLP.dtDeleted = DLP.dtDeleted OR (SLP.dtDeleted IS NULL AND DLP.dtDeleted IS NULL))' 
	   			  +' )'
	   			  +' WHERE DLP.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   			
	   EXEC(@sql)	 
	   	    	
	   -- learningPathLanguage data move logic start
	   
	   -- insert records from source site to destination table LearningPathLanguage
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblLearningPathLanguage]'
	   			  +' ( '
	   			  +'	  idSite,'
	   			  +'	  idLearningPath,'
	   			  +'	  idLanguage,'
	   			  +'	  name,'
	   			  +'	  shortDescription,'
	   			  +'	  longDescription,'
	   			  +'	  searchTags'
	   			  +'  ) '
	   			  +' SELECT '
				  +	   CAST(@idSiteDestination AS NVARCHAR)
	   			  +'	,  TTLP.newDestinationId, '
	   			  +'	   LPL.idLanguage, '
	   			  +'	   LPL.name, '
	   			  +'	   LPL.shortDescription, '
	   			  +'	   LPL.longDescription, '
	   			  +'	   LPL.searchTags '
				  +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblLearningPathLanguage] LPL '
	   			  +' LEFT JOIN #LearningPathIdMappings TTLP on (TTLP.oldSourceId = LPL.idLearningPath ) '
	   			  +' WHERE LPL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   	
	   EXEC(@sql)
	   
	   -- learningPathLanguage data move logic end

	   -- insert temp table data for group in order to return it for moving file system object
	   INSERT INTO #ObjectMapping
	   ([oldId],[newId],objectName)
	   SELECT oldSourceId, newDestinationId, 'learningPaths' FROM #LearningPathIdMappings	
	   



-- ================================================================================
-- LOGIC START -- copy tblCoupenCode object for both full copy and partial copy ---


	   -- insert records from source site to destination table CouponCode
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCouponCode]'
	   			 +' ( '
	   			 +'	   idSite,'
	   			 +'	   code,'
	   			 +'	   comments,'
	   			 +'	   usesAllowed,'
	   			 +'	   discount,'
	   			 +'	   discountType,'
	   			 +'	   forCourse,'
	   			 +'	   forCatalog,'
	   			 +'	   forLearningPath,'
	   			 +'	   dtStart,'
	   			 +'	   dtEnd, '
	   			 +'	   isSingleUsePerUser,'
	   			 +'	   isDeleted,'
	   			 +'	   dtDeleted,'
	   			 +'	   forStandupTraining '
	   			 +' ) '
	   			 +' SELECT '
				 +	   CAST(@idSiteDestination AS NVARCHAR)
	   			 +',	   CC.code,'
	   			 +'	   CC.comments,'
	   			 +'	   CC.usesAllowed,'
	   			 +'	   CC.discount,'
	   			 +'	   CC.discountType ,'
	   			 +'	   TTC.newDestinationId,'
	   			 +'	   TTCat.newDestinationId,'
	   			 +'	   TTLP.newDestinationId,'
	   			 +'	   CC.dtStart,'
	   			 +'	   CC.dtEnd,'
	   			 +'	   CC.isSingleUsePerUser,'
	   			 +'	   CC.isDeleted,'
	   			 +'	   CC.dtDeleted, '
	   			 +'	   TTST.newDestinationId '
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCouponCode] CC '
				 +' LEFT JOIN #CourseIdMappings TTC on (TTC.oldSourceId = CC.forCourse ) '
				 +' LEFT JOIN #CatalogIdMappings TTCat on (TTCat.oldSourceId = CC.forCatalog ) '
				 +' LEFT JOIN #LearningPathIdMappings TTLP on (TTLP.oldSourceId = CC.forLearningPath )'
				 +' LEFT JOIN #StandUpTrainingIdMappings TTST on (TTST.oldSourceId = CC.forStandupTraining )'
	   			 +' WHERE CC.idSite = '+ CAST(@idSiteSource AS NVARCHAR) 
		
	   EXEC(@sql)
	   


	   -- temporary table to hold the idCouponCode of source and destination CouponCode tables
	   CREATE TABLE #CouponCodeIdMappings
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL  	   
	   )	

	   -- inserting idCouponCode of source CouponCode table and destination CouponCode table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #CouponCodeIdMappings '
				  +' ( '
				  +'	   oldSourceId,
				  	   newDestinationId'
				  +' ) '
				  +' SELECT 
					   SCC.idCouponCode, 
					   DCC.idCouponCode '
				  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCouponCode] DCC'
				  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCouponCode] SCC'
				  +' ON (SCC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
				  +' AND (SCC.code = DCC.code )'
				  +' AND (SCC.dtDeleted = DCC.dtDeleted OR (SCC.dtDeleted IS NULL AND DCC.dtDeleted IS NULL))'
				  +' AND (SCC.dtStart = DCC.dtStart OR (SCC.dtStart IS NULL AND DCC.dtStart IS NULL))'
				  +' AND (SCC.dtEnd = DCC.dtEnd OR (SCC.dtEnd IS NULL AND DCC.dtEnd IS NULL))'					
				  +' )'
				  +' WHERE DCC.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
						
	   EXEC(@sql)	


-- =====================================================================================================
-- LOGIC START -- copy tblCouponCodeToStandupTrainingLink object for both full copy and partial copy ---


	   -- insert records from source site to destination table CouponCodeToStandupTrainingLink
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCouponCodeToStandupTrainingLink]'
	   			 +' ( '
	   			 +'	 idCouponCode,' 
	   			 +'	 idStandupTraining '
	   			 +' ) '
	   			 +' SELECT '
	   			 +'   TTCC.newDestinationId,'
	   			 +'   TTST.newDestinationId '
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCouponCodeToStandupTrainingLink] CCSTL'
	   			 +' INNER JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCouponCode] CC ON CC.idCouponCode= CCSTL.idCouponCode '
	   			 +' LEFT JOIN #CouponCodeIdMappings TTCC ON (TTCC.oldSourceId = CCSTL.idCouponCode  )'
	   			 +' LEFT JOIN #StandUpTrainingIdMappings TTST ON (TTST.oldSourceId = CCSTL.idStandupTraining )'
	   			 +' WHERE CC.idSite = '+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   	
	   EXEC(@sql)

	     

-- =============================================================================================
-- LOGIC START -- copy tblCouponCodeToCatalogLink object for both full copy and partial copy ---


	   -- insert records from source site to destination table CouponCodeToCatalogLink
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCouponCodeToCatalogLink]'
	   			 +' ( '
	   			 +'	 idCouponCode,' 
	   			 +'	 idCatalog'
	   			 +' ) '
	   			 +' SELECT '
	   			 +'   TTCC.newDestinationId,'
	   			 +'   TTC.newDestinationId '
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCouponCodeToCatalogLink] CCTCL'
	   			 +' INNER JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCouponCode] CC ON CC.idCouponCode= CCTCL.idCouponCode '
	   			 +' LEFT JOIN #CouponCodeIdMappings TTCC ON (TTCC.oldSourceId = CCTCL.idCouponCode  )'
	   			 +' LEFT JOIN #CatalogIdMappings TTC ON (TTC.oldSourceId = CCTCL.idCatalog )'
	   			 +' WHERE CC.idSite = '+ CAST(@idSiteSource AS NVARCHAR) 
	   	
	   EXEC(@sql)



-- ==================================================================================================
-- LOGIC START -- copy tblCouponCodeToLearningPathLink object for both full copy and partial copy ---


	   -- insert records from source site to destination table CouponCodeToLearningPathLink
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCouponCodeToLearningPathLink]'
	   			 +'  ( '
	   			 +'   idCouponCode,' 
	   			 +'   idLearningPath'
	   			 +'   ) '
	   			 +' SELECT '
	   			 +'   TTCC.newDestinationId,'
	   			 +'	 TTLP.newDestinationId '
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCouponCodeToLearningPathLink] CCLPL'
	   			 +' INNER JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCouponCode] CC ON CC.idCouponCode= CCLPL.idCouponCode '
	   			 +' LEFT JOIN #CouponCodeIdMappings TTCC on (TTCC.oldSourceId = CCLPL.idCouponCode)'
	   			 +' LEFT JOIN #LearningPathIdMappings TTLP on (TTLP.oldSourceId = CCLPL.idLearningPath)'
	   		 	 +' WHERE CC.idSite = '+ CAST(@idSiteSource AS NVARCHAR) 
	   			
	   EXEC(@sql)



-- ============================================================================================
-- LOGIC START -- copy tblCouponCodeToCourseLink object for both full copy and partial copy ---


	   -- insert records from source site to destination table CouponCodeToCourseLink
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCouponCodeToCourseLink]'
	   			 +' ( '
	   			 +'	 idCouponCode,' 
	   			 +'	 idCourse '
	   			 +'  ) '
	   			 +' SELECT '
	   			 +'	 TTCC.newDestinationId,'
	   			 +'	 TTC.newDestinationId ' 
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCouponCodeToCourseLink] CCTCL'
	   			 +' INNER JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCouponCode] CC ON CC.idCouponCode= CCTCL.idCouponCode '
	   			 +' LEFT JOIN #CouponCodeIdMappings TTCC on (TTCC.oldSourceId = CCTCL.idCouponCode )'
	   			 +' LEFT JOIN #CourseIdMappings TTC on (TTC.oldSourceId = CCTCL.idCourse )'
	   		 	 +' WHERE CC.idSite = '+ CAST(@idSiteSource AS NVARCHAR) 

	   EXEC(@sql)


	  /*
	   Drop temporary table StandUpTrainingIdMappings, temporary table Catalog will be dorp in both conditions full copy and partial copy
	   */	   
	   DROP TABLE #StandUpTrainingIdMappings 

	   /*
	   Drop temporary table CatalogIdMappings, temporary table Catalog will be dorp in both conditions full copy and partial copy 
	   */
	   DROP TABLE #CatalogIdMappings


-- ==============================================================================================
-- LOGIC START -- copy tblLearningPathToCourseLink object for both full copy and partial copy ---


	   -- insert records from source site to destination table LearningPathToCourseLink
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblLearningPathToCourseLink]'
	   			 +' ( '
	   		      +'	 idSite,'
	   			 +'	 idLearningPath,'
	   			 +'	 idCourse,'
	   			 +'	 [order]'
	   			 +' ) '
	   			 +' SELECT '
	   			 +    CAST(@idSiteDestination AS NVARCHAR)
	   			 +' , TTLP.newDestinationId, '
	   			 +'   TTC.newDestinationId,'	
	   			 +'   LPCL.[order]'									
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblLearningPathToCourseLink] LPCL '
	   			 +' LEFT JOIN #LearningPathIdMappings TTLP on (TTLP.oldSourceId = LPCL.idLearningPath )'
	   			 +' LEFT JOIN #CourseIdMappings TTC on (TTC.oldSourceId = LPCL.idCourse )'
	   			 +' WHERE LPCL.idSite = '+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   EXEC(@sql)


-- ===================================================================================================
-- LOGIC START -- copy tblRuleSetLearningPathEnrollment object for both full copy and partial copy ---


	   -- insert records from source site to destination table RuleSetLearningPathEnrollment
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblRuleSetLearningPathEnrollment]'
	   			  +' ( '
	   			  +'	  idSite,'
	   			  +'	  idLearningPath,'   
	   			  +'	  idTimezone,'
	   			  +'	  priority,'
	   			  +'	  label,'
	   			  +'	  dtStart,'
	   			  +'	  dtEnd,'
	   			  +'	  delayInterval,'
	   			  +'	  delayTimeframe,'
	   			  +'	  dueInterval,'
	   			  +'	  dueTimeframe'
	   			  +'  ) '
	   			  +' SELECT '
				  +    CAST(@idSiteDestination AS NVARCHAR)
	   			  +' , TTLP.newDestinationId,'
	   			  +'	  RSLPE.idTimezone,'
	   			  +'	  RSLPE.priority,'
	   			  +'	  RSLPE.label,'
	   			  +'	  RSLPE.dtStart,'
	   			  +'	  RSLPE.dtEnd,'
	   			  +'	  RSLPE.delayInterval,'
	   			  +'	  RSLPE.delayTimeframe,'
	   			  +'	  RSLPE.dueInterval,'
	   			  +'	  RSLPE.dueTimeframe'
	   			  +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblRuleSetLearningPathEnrollment] RSLPE'
	   			  +' LEFT JOIN #LearningPathIdMappings TTLP on (TTLP.oldSourceId = RSLPE.idLearningPath )'
	   			  +' WHERE RSLPE.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 	
	   
	   EXEC(@sql)

	   -- temporary table to hold the idRuleSetLearningPathEnrollment of source and destination RuleSetLearningPathEnrollment tables
	   CREATE TABLE #RuleSetLearningPathEnrollmentIdMappings
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL  	   
	   )	

	   -- inserting idRuleSetLearningPathEnrollment of source RuleSetLearningPathEnrollment table and destination RuleSetLearningPathEnrollment table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #RuleSetLearningPathEnrollmentIdMappings '
	   			  +' ( '
	   			  +'	  oldSourceId,
	   			  	  newDestinationId'
	   			  +'  ) '
	   			  +' SELECT 
					  SCC.idRuleSetLearningPathEnrollment, 
					  DCC.idRuleSetLearningPathEnrollment '
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblRuleSetLearningPathEnrollment] DCC'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblRuleSetLearningPathEnrollment] SCC'
	   			  +' ON (SCC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (SCC.label = DCC.label)' 	
	   		       +' AND (SCC.dtStart = DCC.dtStart)'		
	   		       +' AND (SCC.dtEnd = DCC.dtEnd OR (SCC.dtEnd IS NULL AND DCC.dtEnd IS NULL))'		
	   			  +' )'
	   			  +' WHERE DCC.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   
	   EXEC(@sql)	
		
	   -- ruleSetLearningPathEnrollmentLanguage data move logic start
	   
	   -- insert records from source site to destination table RuleSetLearningPathEnrollmentLanguage
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblRuleSetLearningPathEnrollmentLanguage]'
	   			  +' ( '
	   			  +'	   idSite,'
	   			  +'	   idRuleSetLearningPathEnrollment,'
	   			  +'	   idLanguage,'
	   			  +'	   label '
	   			  +'  ) '
	   			  +' SELECT '
				  +	   CAST(@idSiteDestination AS NVARCHAR)
	   			  +' ,  TTRSLPE.newDestinationId,'
	   			  +'	   RSLPEL.idLanguage,'
	   			  +'	   RSLPEL.label '
	   			  +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblRuleSetLearningPathEnrollmentLanguage] RSLPEL'
	   			  +' LEFT JOIN #RuleSetLearningPathEnrollmentIdMappings TTRSLPE on (TTRSLPE.oldSourceId = RSLPEL.idRuleSetLearningPathEnrollment )'
	   			  +' WHERE RSLPEL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   	
	   EXEC(@sql)
	   
	   -- ruleSetLearningPathEnrollmentLanguage data move logic end

------------------------------------------------------------------------------

	   -- check condition for the full copy
	   IF @fullCopyFlag = 1
	   BEGIN		


-- =============================================================================
-- LOGIC START -- copy tblLearningPathEnrollment object for full copy ----------


	   -- insert records from source site to destination table LearningPathEnrollment
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblLearningPathEnrollment]'
	   			  +' ( '
	   			  +'	  idSite,'
	   			  +'	  idLearningPath,'  
	   			  +'	  idUser,'    
	   			  +'	  idRuleSetLearningPathEnrollment,'   				
	   			  +'	  idTimezone,' 
	   			  +'	  title,' 
	   			  +'	  dtStart,' 
	   			  +'	  dtDue,' 
	   			  +'	  dtCompleted,' 
	   			  +'	  dtCreated,' 
	   			  +'	  dueInterval,' 
	   			  +'	  dueTimeframe' 
	   			  +'  ) '
	   			  +' SELECT '
				  +     CAST(@idSiteDestination AS NVARCHAR)
	   			  +' ,  TTLP.newDestinationId,'
	   			  +'	   TTU.newDestinationId,'
	   			  +'	   TTRSLPE.newDestinationId,'
	   			  +'	   LPE.idTimezone,'
	   			  +'	   LPE.title,'
	   			  +'	   LPE.dtStart,'
	   			  +'	   LPE.dtDue,'
	   			  +'	   LPE.dtCompleted,'
	   			  +'	   LPE.dtCreated,'
	   			  +'	   LPE.dueInterval,'
	   			  +'	   LPE.dueTimeframe'
	   			  +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblLearningPathEnrollment] LPE'
	   			  +' LEFT JOIN #LearningPathIdMappings TTLP on (TTLP.oldSourceId = LPE.idLearningPath )'
	   			  +' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = LPE.idUser )'
	   			  +' LEFT JOIN #RuleSetLearningPathEnrollmentIdMappings TTRSLPE on (TTRSLPE.oldSourceId = LPE.idRuleSetLearningPathEnrollment )'
	   			  +' WHERE LPE.idSite ='+ CAST(@idSiteSource AS NVARCHAR)
	   
	   EXEC(@sql)
	   
	   -- temporary table to hold the idLearningPathEnrollment of source and destination LearningPathEnrollment tables
	   CREATE TABLE #LearningPathEnrollmentIdMappings
	   (
	   	oldSourceId INT NOT NULL, 
	   	newDestinationId INT NOT NULL  	   
	   )	
	   
	   -- inserting idLearningPathEnrollment of source and destination LearningPathEnrollment tables inside temp table
	   SET @sql =	   ' INSERT INTO #LearningPathEnrollmentIdMappings '
	   			  +' ( '
	   			  +'	  oldSourceId,
	   			  	  newDestinationId'
	   			  +' ) '
	   			  +' SELECT 
				  	  SLPE.idLearningPathEnrollment, 
				  	  DLPE.idLearningPathEnrollment '
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblLearningPathEnrollment] DLPE'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblLearningPathEnrollment] SLPE'
	   			  +' ON (SLPE.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (SLPE.dtStart = DLPE.dtStart )'
	   		       +' AND (SLPE.dtCompleted = DLPE.dtCompleted OR (SLPE.dtCompleted IS NULL AND DLPE.dtCompleted IS NULL))'		
	   		       +' AND (SLPE.dtCreated = DLPE.dtCreated )'		
	   			  +' )'
	   			  +' WHERE DLPE.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   			
	   EXEC(@sql)	
	   
-- =============================================================================
-- LOGIC START -- copy tblLearningPathEnrollmentApprover object for full copy ----------


	   -- insert records from source site to destination table LearningPathEnrollmentApprover
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblLearningPathEnrollmentApprover]'
	   			  +' ( '
	   			  +'	  idSite,'
	   			  +'	  idLearningPath,'  
	   			  +'	  idUser'    
	   			  +'  ) '
	   			  +' SELECT '
				  +     CAST(@idSiteDestination AS NVARCHAR)
	   			  +' ,  TTLP.newDestinationId,'
	   			  +'	   TTU.newDestinationId'
	   			  +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblLearningPathEnrollmentApprover] LPEA'
	   			  +' LEFT JOIN #LearningPathIdMappings TTLP on (TTLP.oldSourceId = LPEA.idLearningPath )'
	   			  +' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = LPEA.idUser )'
	   			  +' WHERE LPEA.idSite ='+ CAST(@idSiteSource AS NVARCHAR)
	   
	   EXEC(@sql)

  End
---------------------------------------------------------------------------------------------
-- end full copy


-- ================================================================================================================
-- LOGIC START -- copy tblRuleSetToRuleSetLearningPathEnrollmentLink object for both full copy and partial copy ---


	   -- insert records from source site to destination table RuleSetToRuleSetLearningPathEnrollmentLink
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblRuleSetToRuleSetLearningPathEnrollmentLink]'
	   			 +' ( '
	   		      +'	 idSite,'
	   			 +'	 idRuleSet,'  
	   			 +'	 idRuleSetLearningPathEnrollment '
	   			 +' ) '
	   			 +' SELECT '
	   			 +    CAST(@idSiteDestination AS NVARCHAR)
	   			 +' , TTRS.newDestinationId, '
	   			 +'   TTRSLPE.newDestinationId '						
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblRuleSetToRuleSetLearningPathEnrollmentLink] RSRSLPEL '
	   			 +' LEFT JOIN #RuleSetLearningPathEnrollmentIdMappings TTRSLPE on (TTRSLPE.oldSourceId = RSRSLPEL.idRuleSetLearningPathEnrollment )'
	   			 +' LEFT JOIN #RuleSetIdMappings TTRS on (TTRS.oldSourceId = RSRSLPEL.idRuleSet )'
	   			 +' WHERE RSRSLPEL.idSite = '+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   EXEC(@sql)


	   /*
	   Drop temporary table RuleSetLearningPathEnrollmentIdMappings
	   */
	   DROP TABLE #RuleSetLearningPathEnrollmentIdMappings



-- ================================================================================
-- LOGIC START -- copy tblCertificate object for both full copy and partial copy---

	   -- insert records from source site to destination table Certificate
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCertificate]'
	   			  +' ( '
	   			  +'   idSite,
	   				  name,
	   				  issuingOrganization,
	   				  [description],
	   				  credits,
	   				  code,
	   				  [filename],
	   				  isActive,
	   				  activationDate,
	   				  expiresInterval,
	   				  expiresTimeframe,
	   				  objectType,
	   				  idObject,
	   				  isDeleted,
	   				  dtDeleted'
	   			  +'  ) '
	   			  +' SELECT '
				  +	  CAST(@idSiteDestination AS NVARCHAR)
	   			  +' , C.name,
	   				  C.issuingOrganization,
	   				  C.[description],
	   				  C.credits,
	   				  C.code,
	   				  C.[filename],
	   				  C.isActive,
	   				  C.activationDate,
	   				  C.expiresInterval,
	   				  C.expiresTimeframe,
	   				  C.objectType,'
	   			  +'   CASE C.objectType
	   					   WHEN 1 THEN
	   					  	 TTC.newDestinationId
	   					   WHEN 2 THEN
	   					  	 TTLP.newDestinationId
	   					   ELSE
	   					  	 NULL
	   					   END'
	   			  +',  C.isDeleted,
	   			       C.dtDeleted					
	   			  	FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertificate] C'
	   			  +' LEFT JOIN #CourseIdMappings TTC on (TTC.oldSourceId = C.idObject)'
	   			  +' LEFT JOIN #LearningPathIdMappings TTLP on (TTLP.oldSourceId = C.idObject)'
	   			  +' WHERE C.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   		
	   EXEC(@sql)

							
	   -- temporary table to hold the idCertificate of source and destination Certificate tables
	   CREATE TABLE #CertificateIdMappings 
	   (
	   	oldSourceId INT NOT NULL, 
	   	newDestinationId INT NOT NULL 	   
	   )

	   -- inserting idCertificate of source Certificate table and destination Certificate table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #CertificateIdMappings'
	   			  +' ( '
	   			  +'	   oldSourceId,
	   			  	   newDestinationId'
	   			  +' ) '
	   			  +' SELECT 
					   SRC.idCertificate, 
					   DST.idCertificate '
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCertificate] DST'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertificate] SRC'
	   			  +' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (SRC.name = DST.name)'
	   			  +' AND (SRC.activationDate = DST.activationDate OR (SRC.activationDate IS NULL AND DST.activationDate IS NULL))'
	   			  +' AND (SRC.issuingOrganization = DST.issuingOrganization OR (SRC.issuingOrganization IS NULL AND DST.issuingOrganization IS NULL))'
	   			  +' AND (SRC.dtDeleted = DST.dtDeleted OR (SRC.dtDeleted IS NULL AND DST.dtDeleted IS NULL))'
	   			  +' )'
	   			  +' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   		
	   EXEC(@sql)

	   -- certificate language data move logic start

	   -- insert records from source site to destination table CertificateLanguage
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCertificateLanguage]'
	   			  +' ( '
	   			  +'   idSite, 
	   			  	  idCertificate, 
	   			  	  idLanguage, 
	   			  	  name, 
	   			  	  [description] '
	   			  +' ) '
	   			  +' SELECT '
				  +	  CAST(@idSiteDestination AS NVARCHAR)
	   			  +' , TTC.newDestinationId, 
	   			  	  CL.idLanguage, 
	   			  	  CL.name, 
	   			  	  CL.[description] 
	   			     FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertificateLanguage] CL'
	   			  +' LEFT JOIN #CertificateIdMappings TTC on (TTC.oldSourceId = CL.idCertificate )'
	   			  +' WHERE CL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   		
	   EXEC(@sql)
              	              
	   -- certificate language data move logic end

	   -- insert temp table data for group in order to return it for moving file system object
	   INSERT INTO #ObjectMapping
	   ([oldId],[newId],objectName)
	   SELECT oldSourceId, newDestinationId, 'certificates' FROM #CertificateIdMappings
	   

------------------------------------------------------------------------------

	   -- check condition for the full copy
	   IF @fullCopyFlag = 1
	   BEGIN		


-- =============================================================================
-- LOGIC START -- copy tblCertificateImport object for full copy ---------------


	   -- insert records from source site to destination table CertificateImport
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCertificateImport]'
	   			  +' ( '
	   			  +'	  [idSite],
	   			  	  [idUser],
	   			  	  [timestamp],
	   			  	  [idUserImported],
	   			  	  [importFileName]'
	   			  +' ) '
	   			  +' SELECT '
				  +	  CAST(@idSiteDestination AS NVARCHAR)
	   			  +' , TTUser.newDestinationId,
	   			  	  CI.[timestamp],
	   			  	  TTUserImported.newDestinationId,
	   			  	  CI.[importFileName] 
	   			     FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertificateImport] CI'
	   			  +' LEFT JOIN #UserIdMappings TTUser on (TTUser.oldSourceId = CI.idUser )'
	   			  +' LEFT JOIN #UserIdMappings TTUserImported on (TTUserImported.oldSourceId = CI.idUserImported )'
	   			  +' WHERE CI.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			
	   EXEC(@sql)
	   					
	   -- temporary table to hold the idCertificateImport of source and destination CertificateImport tables
	   CREATE TABLE #CertificateImportIdMappings 
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL 	   
	   )

	   -- inserting idCertificateImport of source CertificateImport table and destination CertificateImport table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #CertificateImportIdMappings'
	   			  +' ( '
	   			  +'	   oldSourceId,
	   			  	   newDestinationId'
	   			  +' ) '
	   			  +' SELECT 
					   SRC.idCertificateImport, 
					   DST.idCertificateImport'
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCertificateImport] DST'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertificateImport] SRC'
	   			  +' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (SRC.[timestamp] = DST.[timestamp])'
	   			  +' AND (SRC.[importFileName] = DST.[importFileName] OR (SRC.[importFileName] IS NULL AND DST.[importFileName] IS NULL))'
	   			  +' )'
	   			  +' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   		
	   EXEC(@sql)



-- =============================================================================
-- LOGIC START -- copy tblCertificateRecord object for full copy ---------------


	   -- insert records from source site to destination table CertificateRecord
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCertificateRecord]'
              		  +' ( '
              		  +'   idSite,
				  	  idCertificate,
				  	  idUser,
				  	  idAwardedBy,
				  	  [timestamp],
				  	  expires,
				  	  idTimezone,
				  	  code,
				  	  credits,
				  	  awardData,
				  	  idCertificateImport'
              		  +' ) '
              		  +' SELECT '
				  +	  CAST(@idSiteDestination AS NVARCHAR)
              		  +' , TTC.newDestinationId, 
              		  	  TTU.newDestinationId, 
              		  	  TTAB.newDestinationId, 
              		  	  CR.[timestamp],
				  	  CR.expires,
				  	  CR.idTimezone,
				  	  CR.code,
				  	  CR.credits,
				  	  CR.awardData,
				  	  TTCI.newDestinationId
              		     FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertificateRecord] CR'
              		  +' LEFT JOIN #CertificateIdMappings TTC on (TTC.oldSourceId = CR.idCertificate )'
				  +' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = CR.idUser )'
				  +' LEFT JOIN #UserIdMappings TTAB on (TTAB.oldSourceId = CR.idAwardedBy )'
				  +' LEFT JOIN #CertificateImportIdMappings TTCI on (TTCI.oldSourceId = CR.idCertificateImport )'
              		  +' WHERE CR.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
              			
	   EXEC(@sql)
	   
	   -- temporary table to hold the idCertificateRecord of source and destination CertificateRecord tables
	   CREATE TABLE #CertificateRecordIdMappings 
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL 	   
	   )

	   -- inserting idCertificateRecord of source CertificateRecord table and destination CertificateRecord table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #CertificateRecordIdMappings'
	   			  +' ( '
	   			  +'	   oldSourceId,
	   			  	   newDestinationId'
	   			  +'  ) '
	   			  +' SELECT 
					   SRC.idCertificateRecord, 
					   DST.idCertificateRecord'
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCertificateRecord] DST'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertificateRecord] SRC'
	   			  +' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (SRC.[timestamp] = DST.[timestamp])'
	   			  +' AND (SRC.code = DST.code OR (SRC.code IS NULL AND DST.code IS NULL))'
	   			  +' AND (SRC.credits = DST.credits OR (SRC.credits IS NULL AND DST.credits IS NULL))'
	   			  +' )'
	   			  +' INNER JOIN #CertificateIdMappings TTC on (TTC.oldSourceId = SRC.idCertificate AND TTC.newDestinationId =  DST.idCertificate)'
	   			  +' INNER JOIN #UserIdMappings TTU on (TTU.oldSourceId = SRC.idUser AND TTU.newDestinationId = DST.idUser)'
	   			  +' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   		
	   EXEC(@sql)



	   /*
	   Drop temporary Table CertificateImportIdMappings
	   */
	   DROP TABLE #CertificateImportIdMappings


-- =============================================================================
-- LOGIC START -- copy tblCourseExpert object for full copy --------------------


	   -- insert records from source site to destination table CourseExpert
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCourseExpert]'
	   			  +' ( '
	   			  +'	   idSite,
	   			  	   idCourse,
	   			  	   idUser'
	   			  +'  ) '
	   			  +' SELECT '
				  +	  CAST(@idSiteDestination AS NVARCHAR)
	   			  +' , TTC.newDestinationId,'
	   			  +'   TTU.newDestinationId '
	   			  +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCourseExpert] CE'
	   			  +' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = CE.idUser )'
	   			  +' LEFT JOIN #CourseIdMappings TTC on (TTC.oldSourceId = CE.idCourse )'
	   			  +' WHERE CE.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 	
	   
	   EXEC(@sql)


-- =============================================================================
-- LOGIC START -- copy tblCourseEnrollmentApprover object for full copy --------------------


	   -- insert records from source site to destination table CourseEnrollmentApprover
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCourseEnrollmentApprover]'
	   			  +' ( '
	   			  +'	   idSite,
	   			  	   idCourse,
	   			  	   idUser'
	   			  +'  ) '
	   			  +' SELECT '
				  +	  CAST(@idSiteDestination AS NVARCHAR)
	   			  +' , TTC.newDestinationId,'
	   			  +'   TTU.newDestinationId '
	   			  +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCourseEnrollmentApprover] CEA'
	   			  +' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = CEA.idUser )'
	   			  +' LEFT JOIN #CourseIdMappings TTC on (TTC.oldSourceId = CEA.idCourse )'
	   			  +' WHERE CEA.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 	
	   
	   EXEC(@sql)

	   

-- =============================================================================
-- LOGIC START -- copy tblCourseFeedMessage object for full copy ---------------


	   -- insert records from source site to destination table CourseFeedMessage
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCourseFeedMessage]'
	   			  +' ( '
	   			  +'	   idSite,
	   			  	   idCourse,
	   			  	   idLanguage,
	   			  	   idAuthor,
	   			  	   idParentCourseFeedMessage,
	   			  	   [message] ,
	   			  	   [timestamp],
	   			  	   dtApproved,
	   			  	   idApprover,
	   			  	   IsApproved '
	   			  +'  ) '
	   			  +' SELECT '
				  +	   CAST(@idSiteDestination AS NVARCHAR)
	   			  +' ,  TTC.newDestinationId, '
	   			  +'	   CFM.idLanguage,'
	   			  +'	   TTAuthor.newDestinationId,'
	   			  +'	   NULL,'			    -- this null value of idParentCourseFeedMessage will be updated from temporary CourseFeedMessageIdMappings table below
	   			  +'	   CFM.[message],   
	   			  	   CFM.[timestamp],
	   			  	   CFM.dtApproved,
	   			  	   TTApprover.newDestinationId,
	   			  	   CFM.IsApproved '
	   			  +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCourseFeedMessage] CFM'
	   			  +' LEFT JOIN #UserIdMappings TTAuthor on (TTAuthor.oldSourceId = CFM.idAuthor )'
	   			  +' LEFT JOIN #UserIdMappings TTApprover on (TTApprover.oldSourceId = CFM.idApprover )'
	   			  +' LEFT JOIN #CourseIdMappings TTC on (TTC.oldSourceId = CFM.idCourse )'
	   			  +' WHERE CFM.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   		
	   EXEC(@sql)


	   -- temporary table to hold the idCourseFeedMessage of source and destination CourseFeedMessage tables
	   CREATE TABLE #CourseFeedMessageIdMappings 
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL  	   
	   )

	   -- inserting idCourseFeedMessage of source CourseFeedMessage table and destination CourseFeedMessage table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #CourseFeedMessageIdMappings'
	   			  +' ( '
	   			  +'	   oldSourceId,
	   			  	   newDestinationId'
	   			  +' ) '
	   			  +' SELECT 
					   SRC.idCourseFeedMessage, 
					   DST.idCourseFeedMessage'
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCourseFeedMessage] DST'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCourseFeedMessage] SRC'
	   			  +' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (SRC.[timestamp] = DST.[timestamp])'
	   			  +' AND (SRC.message = DST.message)'
	   			  +' )'
	   			  +' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   
	   EXEC(@sql)
	   
	   
	   -- update idParentCourseFeedMessage from temp table CourseFeedMessage
	   SET @sql =	  ' UPDATE DSTC'
	   			 +'	 SET idParentCourseFeedMessage = TEMP.newDestinationId'
	   			 +' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCourseFeedMessage] SRCC'
	   			 +' INNER JOIN  [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCourseFeedMessage] DSTC'
	   			 +' ON(SRCC.[timestamp] = DSTC.[timestamp] AND SRCC.message = DSTC.message AND SRCC.idSite ='+ CAST(@idSiteSource AS NVARCHAR)+ ')'
	   			 +' INNER JOIN  #CourseFeedMessageIdMappings TEMP'
	   			 +' ON(TEMP.oldSourceId = SRCC.idParentCourseFeedMessage)'
	   			 +' WHERE DSTC.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   			 +' AND SRCC.idParentCourseFeedMessage IS NOT NULL'
	   
	   EXEC(@sql)	
	   
	   /*
	   Drop temporary table TempTableCourseFeedMessage
	   */
	   DROP TABLE #CourseFeedMessageIdMappings



-- =============================================================================
-- LOGIC START -- copy tblCourseFeedModerator object for full copy -------------


	   -- insert records from source site to destination table CourseFeedModerator
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCourseFeedModerator]'
	   			  +' ( '
	   			  +'    idSite,
	   		 		   idCourse,
	   			  	   idModerator'
	   			  +' ) '
	   			  +' SELECT '
				  +	   CAST(@idSiteDestination AS NVARCHAR)
	   			  +' ,  TTC.newDestinationId,
	   			  	   TTU.newDestinationId'
	   			  +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCourseFeedModerator] CFM'
	   			  +' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = CFM.idModerator )'
	   			  +' LEFT JOIN #CourseIdMappings TTC on (TTC.oldSourceId = CFM.idCourse )'
	   			  +' WHERE CFM.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			
	   EXEC(@sql)



-- =============================================================================
-- LOGIC START -- copy tblCourseRating object for full copy --------------------


	   -- insert records from source site to destination table CourseRating
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCourseRating]'
	   			  +' ( '
	   			  +'	  idSite,
	   			  	  idCourse,
	   			  	  idUser,
	   			  	  rating,
	   			  	  [timestamp]'
	   			  +' ) '
	   			  +' SELECT '
				  +    CAST(@idSiteDestination AS NVARCHAR)
	   			  +' , TTC.newDestinationId,
	   			  	  TTU.newDestinationId,
	   			  	  CR.rating,
	   			  	  CR.[timestamp]'
	   			  +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCourseRating] CR'
	   			  +' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = CR.idUser )'
	   			  +' LEFT JOIN #CourseIdMappings TTC on (TTC.oldSourceId = CR.idCourse )'
	   			  +' WHERE CR.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   EXEC(@sql)


-- =============================================================================
-- LOGIC START -- copy tblActivityImport object for full copy ------------------


	   -- insert records from source site to destination table ActivityImport
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblActivityImport]'
	   			  +' ( '
	   			  +'	  [idSite],
	   			  	  [idUser],
	   			  	  [timestamp],
	   			  	  [idUserImported],
	   			  	  [importFileName]'
	   			  +' ) '
	   			  +' SELECT '
				  +	  CAST(@idSiteDestination AS NVARCHAR)
	   			  +',  TTUser.newDestinationId,
	   			  	  AI.[timestamp],
	   			  	  TTUserImported.newDestinationId,
	   			  	  AI.[importFileName] 
	   			     FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblActivityImport] AI'
	   			  +' LEFT JOIN #UserIdMappings TTUser on (TTUser.oldSourceId = AI.idUser )'
	   			  +' LEFT JOIN #UserIdMappings TTUserImported on (TTUserImported.oldSourceId = AI.idUserImported )'
	   			  +' WHERE AI.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			
	   EXEC(@sql)
							
							
	   -- temporary table to hold the idActivityImport of source and destination ActivityImport tables
	   CREATE TABLE #ActivityImportIdMappings
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL 	   
	   )

	   -- inserting idActivityImport of source ActivityImport table and destination ActivityImport table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #ActivityImportIdMappings'
	   			  +' ( '
	   			  +'   oldSourceId,
	   			  	  newDestinationId'
	   			  +' ) '
	   			  +' SELECT 
				  	 SRC.idActivityImport, 
				  	 DST.idActivityImport '
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblActivityImport] DST'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblActivityImport] SRC'
	   			  +' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (SRC.timestamp = DST.timestamp)'
	   			  +' AND (SRC.importFileName = DST.importFileName OR (SRC.importFileName IS NULL AND DST.importFileName IS NULL))'
	   			  +')'
	   			  +' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   
	   EXEC(@sql)


    END

------------------------------------------------------------------------------
-- full copy end



-- ==============================================================================================
-- LOGIC START -- copy tblCourseToPrerequisiteLink object for both full copy and partial copy ---


	   -- insert records from source site to destination table CourseToPrerequisiteLink
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCourseToPrerequisiteLink]'
	   			  +' ( '
	   			  +'   idSite,
	   			  	  idCourse,
	   			  	  idPrerequisite '
	   			  +'  ) '
	   			  +' SELECT TOP 1 '
				  +	   CAST(@idSiteDestination AS NVARCHAR)
	   			  +' ,  TTCourse.newDestinationId
	   			  	 ,  TTPrerequisite.newDestinationId '
	   			  +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCourseToPrerequisiteLink] CTPL'
				  +' LEFT JOIN #CourseIdMappings TTCourse on (TTCourse.oldSourceId = CTPL.idCourse )'
	   			  +' LEFT JOIN #CourseIdMappings TTPrerequisite on (TTPrerequisite.oldSourceId = CTPL.idPrerequisite )'
	   			  +' WHERE CTPL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
				
	   EXEC(@sql)


-- ==============================================================================================
-- LOGIC START -- copy tblCourseToScreenshotLink object for both full copy and partial copy ---


	   -- insert records from source site to destination table CourseToScreenshotLink
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCourseToScreenshotLink]'
	   			  +' ( '
	   			  +'   idSite,
	   			  	  idCourse,
	   			  	  filename '
	   			  +'  ) '
	   			  +' SELECT '
				  +	   CAST(@idSiteDestination AS NVARCHAR)
	   			  +' ,  TTCourse.newDestinationId,
	   			  	   CTSL.filename '
	   			  +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCourseToScreenshotLink] CTSL'
	   			  +' LEFT JOIN #CourseIdMappings TTCourse on (TTCourse.oldSourceId = CTSL.idCourse )'
	   			  +' WHERE CTSL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			
	   EXEC(@sql)



-- =====================================================================================
-- LOGIC START -- copy tblGroupEnrollment object for both full copy and partial copy ---


	   -- insert records from source site to destination table GroupEnrollment
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblGroupEnrollment]'
	   			  +' ( '
	             	  +'   idSite,
	   			       idCourse,
	   			       idGroup,
	   			       idTimezone,
	   			       isLockedByPrerequisites,
	   			       dtStart,
	   			       dtCreated,
	   			       dueInterval,
	   			       dueTimeframe,
	   			       expiresFromStartInterval,
	   			       expiresFromStartTimeframe,
	   			       expiresFromFirstLaunchInterval,
	   			       expiresFromFirstLaunchTimeframe '
	   			  +'  ) '
	   			  +' SELECT '
				  +     CAST(@idSiteDestination AS NVARCHAR)
	   			  +' ,  TTC.newDestinationId,
	   				   TTG.newDestinationId,
	   				   GE.idTimezone,
	   				   GE.isLockedByPrerequisites,
	   				   GE.dtStart,
	   				   GE.dtCreated,
	   				   GE.dueInterval,
	   				   GE.dueTimeframe,
	   				   GE.expiresFromStartInterval,
	   				   GE.expiresFromStartTimeframe,
	   				   GE.expiresFromFirstLaunchInterval,
	   				   GE.expiresFromFirstLaunchTimeframe 
	   			     FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblGroupEnrollment] GE '
	   			  +' LEFT JOIN #GroupIdMappings TTG on (TTG.oldSourceId = GE.idGroup)'
	   			  +' LEFT JOIN #CourseIdMappings TTC on (TTC.oldSourceId = GE.idCourse)'
	   			  +' WHERE GE.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   										
	   EXEC(@sql)
	   
	   -- temporary table to hold the idGroupEnrollment of source and destination GroupEnrollment tables
	   CREATE TABLE #GroupEnrollmentIdMappings 
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL  	   
	   )

	   -- inserting idGroupEnrollment of source GroupEnrollment table and destination GroupEnrollment table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #GroupEnrollmentIdMappings'
				  +' ( '
				  +'	  oldSourceId,
				  	  newDestinationId'
				  +' ) '
				  +' SELECT 
					   SRC.idGroupEnrollment, 
					   DST.idGroupEnrollment '
				  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblGroupEnrollment] DST'
				  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblGroupEnrollment] SRC'
				  +' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
				  +' AND (SRC.dtStart = DST.dtStart)'
				  +' AND (SRC.dtCreated = DST.dtCreated)'
				  +' )'
				  +' INNER JOIN #GroupIdMappings TTG on (TTG.oldSourceId = SRC.idGroup AND TTG.newDestinationId =  DST.idGroup)'
				  +' INNER JOIN #CourseIdMappings TTC on (TTC.oldSourceId = SRC.idCourse AND TTC.newDestinationId = DST.idCourse)'
				  +' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
			
	   EXEC(@sql)



-- =======================================================================================
-- LOGIC START -- copy tblRuleSetEnrollment object for both full copy and partial copy ---


	   -- insert records from source site to destination table RuleSetEnrollment
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblRuleSetEnrollment]'
	   			  +' ( '
	   			  +'   idSite,
	   				  idCourse,
	   				  idTimezone,
	   				  [priority],
	   				  label,
	   				  isLockedByPrerequisites,
	   				  isFixedDate,
	   				  dtStart,
	   				  dtEnd,
	   				  dtCreated,
	   				  delayInterval,
	   				  delayTimeframe,
	   				  dueInterval,
	   				  dueTimeframe,
	   				  recurInterval,
	   				  recurTimeframe,
	   				  expiresFromStartInterval,
	   				  expiresFromStartTimeframe,
	   				  expiresFromFirstLaunchInterval,
	   				  expiresFromFirstLaunchTimeframe,
	   				  forceReassignment'
	   			  + '  ) '
	   			  +' SELECT '
				  +	   CAST(@idSiteDestination AS NVARCHAR)
	   			  +' ,  TTC.newDestinationId,
	   				   RE.idTimezone,
	   				   RE.[priority],
	   				   RE.label,
	   				   RE.isLockedByPrerequisites,
	   				   RE.isFixedDate,
	   				   RE.dtStart,
	   				   RE.dtEnd,
	   				   RE.dtCreated,
	   				   RE.delayInterval,
	   				   RE.delayTimeframe,
	   				   RE.dueInterval,
	   				   RE.dueTimeframe,
	   				   RE.recurInterval,
	   				   RE.recurTimeframe,
	   				   RE.expiresFromStartInterval,
	   				   RE.expiresFromStartTimeframe,
	   				   RE.expiresFromFirstLaunchInterval,
	   				   RE.expiresFromFirstLaunchTimeframe,
	   				   RE.forceReassignment 
	   			     FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblRuleSetEnrollment] RE'
	   			  +' LEFT JOIN #CourseIdMappings TTC on (TTC.oldSourceId = RE.idCourse )'
	   			  +' WHERE RE.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   EXEC(@sql)
	   				
	   -- temporary table to hold the idRuleSetEnrollment of source and destination RuleSetEnrollment tables
	   CREATE TABLE #RuleSetEnrollmentIdMappings 
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL 	   
	   )
	   
	   -- inserting idRuleSetEnrollment of source RuleSetEnrollment table and destination RuleSetEnrollment table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #RuleSetEnrollmentIdMappings'
	   			  +' ( '
	   			  +'	  oldSourceId,
	   			  	  newDestinationId'
	   			  +'  ) '
	   			  +' SELECT 
					   SRC.idRuleSetEnrollment, 
					   DST.idRuleSetEnrollment '
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblRuleSetEnrollment] DST'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblRuleSetEnrollment] SRC'
	   			  +' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (SRC.label = DST.label)'
	   			  +' AND (SRC.dtStart = DST.dtStart)'
	   			  +' AND (SRC.dtEnd = DST.dtEnd OR (SRC.dtEnd IS NULL AND DST.dtEnd IS NULL))'
	   			  +' AND (SRC.dtCreated = DST.dtCreated)'
	   			  +' )'
	   			  +' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   	
	   EXEC(@sql)

	   -- ruleSetEnrollment language data move logic start

	   -- insert records from source site to destination table RuleSetEnrollmentLanguage
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblRuleSetEnrollmentLanguage]'
	   			  +' ( '
	   			  +'   idSite, 
	   			  	  idRuleSetEnrollment, 
	   			  	  idLanguage, 
	   			  	  label '
	   			  +'  ) '
	   			  +' SELECT '
	   			  +	  CAST(@idSiteDestination AS NVARCHAR)
	   			  +' , TTRE.newDestinationId, 
	   			  	  REL.idLanguage, 
	   			  	  REL.label 
	   			     FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblRuleSetEnrollmentLanguage] REL'
	   			  +' LEFT JOIN #RuleSetEnrollmentIdMappings TTRE on (TTRE.oldSourceId = REL.idRuleSetEnrollment )'
	   			  +' WHERE REL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	           
	   EXEC(@sql)
	                 
	   -- ruleSetEnrollment language data move logic end

	   
-- ====================================================================================================
-- LOGIC START -- copy tblRuleSetToRuleSetEnrollmentLink object for both full copy and partial copy ---


	   -- insert records from source site to destination table RuleSetToRuleSetEnrollmentLink
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblRuleSetToRuleSetEnrollmentLink]'
	   			  +' ( '
	   			  +'	   idSite,
	   				   idRuleSet,
	   				   idRuleSetEnrollment '
	   			  + ' ) '
	   			  +' SELECT TOP 1 '
				  +	   CAST(@idSiteDestination AS NVARCHAR)
	   			  +' ,  TTR.newDestinationId, 
	   			  	   TTRE.newDestinationId 
	   			     FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblRuleSetToRuleSetEnrollmentLink] RTREL'
	   			  +' LEFT JOIN #RuleSetIdMappings TTR on (TTR.oldSourceId = RTREL.idRuleSet )'
	   			  +' LEFT JOIN #RuleSetEnrollmentIdMappings TTRE on (TTRE.oldSourceId = RTREL.idRuleSetEnrollment )'
	   			  +' WHERE RTREL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			
	   EXEC(@sql)



-- ==============================================================================================
-- LOGIC START -- copy tblDocumentRepositoryFolder object for both full copy and partial copy ---


	   -- insert records from source site to destination table DocumentRepositoryFolder
	   SET @sql =	  'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblDocumentRepositoryFolder]'
	   			 +' ( '
	   			 +'   idSite,
	   			      idDocumentRepositoryObjectType,
	   			      idObject,
	   			      [Name]'
	   			 + '  ) '
	   			 +' SELECT '
				 +	  CAST(@idSiteDestination AS NVARCHAR)
	   			 +' ,  DRF.idDocumentRepositoryObjectType'
				 +' ,  DRF.idObject'
	   			 --+'    CASE DRF.idDocumentRepositoryObjectType
	   			 --	     WHEN 1 THEN
	   			 --	   	 TTG.newDestinationId
	   			 --	     WHEN 2 THEN
	   			 --	   	 TTC.newDestinationId
	   			 --	     WHEN 3 THEN 
	   			 --	   	 TTLP.newDestinationId
	   			--	  END'
	   			 +' ,  DRF.[Name] 
	   			    FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblDocumentRepositoryFolder] DRF'
	   			 +' LEFT JOIN #GroupIdMappings TTG on (TTG.oldSourceId = DRF.idObject )'
	   			 +' LEFT JOIN #CourseIdMappings TTC on (TTC.oldSourceId = DRF.idObject )'
	   			 +' LEFT JOIN #LearningPathIdMappings TTLP on (TTLP.oldSourceId = DRF.idObject )'
	   			 +' WHERE DRF.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			
	   EXEC(@sql)
	   				
	   -- temporary table to hold the idDocumentRepositoryFolder of source and destination DocumentRepositoryFolder tables
	   CREATE TABLE #DocumentRepositoryFolderIdMappings 
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL 	   
	   )
	   
	   -- inserting idDocumentRepositoryFolder of source DocumentRepositoryFolder table and destination DocumentRepositoryFolder table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #DocumentRepositoryFolderIdMappings'
	   			  +' ( '
	   			  +'	  oldSourceId,
	   			  	  newDestinationId'
	   			  +' ) '
	   			  +' SELECT 
					   SRC.idDocumentRepositoryFolder, 
					   DST.idDocumentRepositoryFolder '
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblDocumentRepositoryFolder] DST'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblDocumentRepositoryFolder] SRC'
	   			  +' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (SRC.name = DST.name)'
	   			  +' AND (SRC.idDocumentRepositoryObjectType = DST.idDocumentRepositoryObjectType)'
	   			  +' AND (SRC.dtDeleted = DST.dtDeleted OR (SRC.dtDeleted IS NULL AND DST.dtDeleted IS NULL))'
	   			  +')'
	   			  +' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   
	   EXEC(@sql)


-- ==========================================================================================================
-- LOGIC START -- copy tblEventEmailNotification object for both full copy and partial copy with condition---


	   -- insert records from source site to destination table EventEmailNotification
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblEventEmailNotification]'
	   			 +' ( '
	   			 +'   idSite,'
	   			 +'	 name,'
	   			 +'	 idEventType,'  
	   			 +'	 idEventTypeRecipient,'
	   			 +'	 [from],'
	   			 +'	 isActive,'
	   			 +'	 dtActivation,'
	   			 +'	 interval,'
	   			 +'	 timeframe,'
	   			 +'	 [priority], '
				  +'	 idObject '
	   			 +'  ) '
	   			 +' SELECT '
	   			 +	 CAST(@idSiteDestination AS NVARCHAR)
	   			 +',  EEN.name,'
	   			 +'	 EEN.idEventType,'
	   			 +'	 EEN.idEventTypeRecipient,'
	   			 +'	 EEN.[from],'
	   			 +'	 EEN.isActive,'
	   			 +'	 EEN.dtActivation,'
	   			 +'	 EEN.interval,'
	   			 +'	 EEN.timeframe,'
	   			 +'	 EEN.[priority], '
				

		IF(@fullCopyFlag = 1) 
		  BEGIN
			 SET @subSql = '	 CASE 
								WHEN EEN.idEventType >= 100 AND EEN.idEventType < 200 THEN 
									TTUO.newDestinationId
								WHEN EEN.idEventType >= 200 AND EEN.idEventType < 300 THEN 
									TTCO.newDestinationId
								WHEN EEN.idEventType >= 300 AND EEN.idEventType < 400 THEN 
									TTLO.newDestinationId
								WHEN EEN.idEventType >= 500 AND EEN.idEventType < 600 THEN  
									TTLPO.newDestinationId
								WHEN EEN.idEventType >= 700 AND EEN.idEventType < 800 THEN 
									TTCEO.newDestinationId
	   				   	 	 END '
	   				      +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblEventEmailNotification] EEN '
	   				   	 +' LEFT JOIN #UserIdMappings TTUO on (TTUO.oldSourceId = EEN.idObject ) '
	   				   	 +' LEFT JOIN #CourseIdMappings TTCO on (TTCO.oldSourceId = EEN.idObject ) '
	   				   	 +' LEFT JOIN #LessonIdMappings TTLO on (TTLO.oldSourceId = EEN.idObject ) '
	   				   	 +' LEFT JOIN #LearningPathIdMappings TTLPO on (TTLPO.oldSourceId = EEN.idObject ) '
	   				   	 +' LEFT JOIN #CertificateIdMappings TTCEO on (TTCEO.oldSourceId = EEN.idObject )'
	   				   	 +' WHERE EEN.idSite ='+ CAST(@idSiteSource AS NVARCHAR)
						 +' AND  EEN.idEventType NOT BETWEEN 400 AND 499 '
		  END
		  ELSE		   -- idUser and idCertificate are not move in partial copy. 
		  BEGIN
			  SET @subSql = '  CASE 
	   				   	 	 	 WHEN EEN.idEventType >= 200 AND EEN.idEventType < 300 THEN 
	   				   	 	   		TTCO.newDestinationId
	   				   	 	 	 WHEN EEN.idEventType >= 300 AND EEN.idEventType < 400 THEN 
	   				   	 	 		TTLO.newDestinationId
	   				   	 	 	 WHEN EEN.idEventType >= 500 AND EEN.idEventType < 600 THEN  
	   				   	 	 		TTLPO.newDestinationId
								 WHEN EEN.idEventType >= 700 AND EEN.idEventType < 800 THEN 
									TTCEO.newDestinationId
	   				   	 	 END '
	   				      +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblEventEmailNotification] EEN '
	   				   	 +' LEFT JOIN #CourseIdMappings TTCO on (TTCO.oldSourceId = EEN.idObject ) '
	   				   	 +' LEFT JOIN #LessonIdMappings TTLO on (TTLO.oldSourceId = EEN.idObject ) '
	   				   	 +' LEFT JOIN #LearningPathIdMappings TTLPO on (TTLPO.oldSourceId = EEN.idObject ) '
						 +' LEFT JOIN #CertificateIdMappings TTCEO on (TTCEO.oldSourceId = EEN.idObject )'
	   				   	 +' WHERE EEN.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   				   	 +' AND  EEN.idEventType NOT BETWEEN 100 AND 199 '
	   				   	 +' AND  EEN.idEventType NOT BETWEEN 400 AND 499 '
	   				   	
		  END

	   SET @sql = @sql + @subSql
	   EXEC(@sql)
							
	   -- temporary table to hold the idEventEmailNotification of source and destination EventEmailNotification tables
	   CREATE TABLE #EventEmailNotificationIdMappings
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL  	   
	   )	

	   -- inserting idEventEmailNotification of source EventEmailNotification table and destination EventEmailNotification table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #EventEmailNotificationIdMappings '
	   			  +' ( '
	   			  +'	  oldSourceId,
	   			  	  newDestinationId'
	   			  +' ) '
	   			  +' SELECT 
	   				   SEEN.idEventEmailNotification, 
	   				   DEEN.idEventEmailNotification '
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblEventEmailNotification] DEEN'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblEventEmailNotification] SEEN'
	   			  +' ON (SEEN.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   		       +' AND (SEEN.[from] = DEEN.[from] OR (SEEN.[from] IS NULL AND DEEN.[from] IS NULL))'
	   			  +' AND (SEEN.dtActivation = DEEN.dtActivation OR (SEEN.dtActivation IS NULL AND DEEN.dtActivation IS NULL))'					
	   			  +' AND (SEEN.timeframe = DEEN.timeframe OR (SEEN.timeframe IS NULL AND DEEN.timeframe IS NULL))' 
	   			  +' )'
	   			  +' WHERE DEEN.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
			
	   EXEC(@sql)	 
	   		    	
	   -- eventEmailNotificationLanguage data move logic start
	   
	   -- insert records from source site to destination table EventEmailNotificationLanguage
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblEventEmailNotificationLanguage] '
	   			  +' ( '
	   			  +'	  idSite,'
	   			  +'	  idEventEmailNotification,' 
	   			  +'	  idLanguage,'
	   			  +'	  name '
	   			  +' ) '
	   			  +' SELECT '
	   			  +	  CAST(@idSiteDestination AS NVARCHAR)
	   			  +',  TTEEN.newDestinationId,'
	   			  +'   EENL.idLanguage,'
	   			  +'   EENL.name '
	   			  +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblEventEmailNotificationLanguage] EENL '
	   			

		  IF(@fullCopyFlag = 1) 
		  BEGIN
			 SET @subSql = ' LEFT JOIN #EventEmailNotificationIdMappings TTEEN on (TTEEN.oldSourceId = EENL.idEventEmailNotification )'
	   				    +' WHERE EENL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 

		  END
		  ELSE
		  BEGIN
			SET @subSql = ' INNER JOIN #EventEmailNotificationIdMappings TTEEN on (TTEEN.oldSourceId = EENL.idEventEmailNotification )'
	   				   +' WHERE EENL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
		  END
	   
	   SET @sql = @sql + @subSql 
	    
	   EXEC(@sql)
	   
	   -- eventEmailNotificationLanguage data move logic end


	   -- insert temp table data for user in order to return it for moving file system object
	   INSERT INTO #ObjectMapping
	   ([oldId],[newId],objectName)
	   SELECT oldSourceId, newDestinationId, 'emailNotifications' FROM #EventEmailNotificationIdMappings


------------------------------------------------------------------------------
	  

	   -- check condition for the full copy
	   IF @fullCopyFlag = 1
	   BEGIN


-- =======================================================================
-- LOGIC START -- copy tblDocumentRepositoryItem object for full copy ---


	   -- insert records from source site to destination table DocumentRepositoryItem
	   SET @sql =	 ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblDocumentRepositoryItem]'
				+' ( '
				+'  idSite,
				    idDocumentRepositoryObjectType,
				    label,
				    [fileName],
				    kb,
				    searchTags,
				    isPrivate,
				    idLanguage,
				    isAllLanguages,
				    dtCreated,
				    isDeleted,
				    dtDeleted,
				    idObject,
				    idDocumentRepositoryFolder,
				    idOwner'
				+' ) '
				+' SELECT '+ CAST(@idSiteDestination AS NVARCHAR)
				+' ,DRI.idDocumentRepositoryObjectType,'
				+'  DRI.label,
				    DRI.[fileName],
				    DRI.kb,
				    DRI.searchTags,
				    DRI.isPrivate,
				    DRI.idLanguage,
				    DRI.isAllLanguages,
				    DRI.dtCreated, 
				    DRI.isDeleted,
				    DRI.dtDeleted,'
				+ '	 CASE DRI.idDocumentRepositoryObjectType
								WHEN 1 THEN
								    TTG.newDestinationId
								WHEN 2 THEN
								    TTC.newDestinationId
								WHEN 3 THEN 
								    TTLP.newDestinationId
							 END'
				 +', TTDRF.newDestinationId,
					  TTU.newDestinationId '
				 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblDocumentRepositoryItem] DRI'
              		 +' LEFT JOIN #GroupIdMappings TTG on (TTG.oldSourceId = DRI.idObject )'
				 +' LEFT JOIN #CourseIdMappings TTC on (TTC.oldSourceId = DRI.idObject )'
				 +' LEFT JOIN #LearningPathIdMappings TTLP on (TTLP.oldSourceId = DRI.idObject )'
				 +' LEFT JOIN #DocumentRepositoryFolderIdMappings TTDRF on (TTDRF.oldSourceId = DRI.idDocumentRepositoryFolder )'
				 +' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = DRI.idOwner )'
              		 +' WHERE DRI.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 


            EXEC(@sql)

		 
		  -- temporary table to hold the idDocumentRepositoryItem of source and destination DocumentRepositoryItem tables
		  CREATE TABLE #DocumentRepositoryItemIdMappings
		  (
		  	oldSourceId INT NOT NULL, 
		  	newDestinationId INT NOT NULL 	   
		  )
		  
		  -- inserting idDocumentRepositoryItem of source DocumentRepositoryItem table and destination DocumentRepositoryItem table inside temporary table for mapping
		  SET @sql = 'INSERT INTO #DocumentRepositoryItemIdMappings'
				  +' ( '
				  +'  oldSourceId,
				  	newDestinationId'
				  +' ) '
				  +' SELECT 1, DST.idDocumentRepositoryItem' --SRC.idDocumentRepositoryItem, DST.idDocumentRepositoryItem'
				  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblDocumentRepositoryItem] DST'
				  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblDocumentRepositoryItem] SRC'
				  +' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
				  +' AND (SRC.label = DST.label)'
				  +' AND (SRC.dtCreated = DST.dtCreated )'
				  +' AND (SRC.dtDeleted = DST.dtDeleted OR (SRC.dtDeleted IS NULL AND DST.dtDeleted IS NULL))'
				  +')'
				  +' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)


		
				
	   EXEC(@sql)

		 
	   -- DocumentRepositoryItem language data move start
		  
	   -- insert records from source site to destination table DocumentRepositoryItemLanguage
	   SET @sql =  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblDocumentRepositoryItemLanguage]'
				   +' ( '
				   +'  idSite,
				       idDocumentRepositoryItem,
				       idLanguage,
				       label,
				       searchTags'
				   +' ) '
				   +' SELECT '+ CAST(@idSiteDestination AS NVARCHAR)
				   +' , TTDRI.newDestinationId, '
				   +'	DRIL.idLanguage, '
				   +'	DRIL.label, '
				   +'	DRIL.searchTags ' 
				   +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblDocumentRepositoryItemLanguage] DRIL '
				   +' LEFT JOIN #DocumentRepositoryItemIdMappings TTDRI on (TTDRI.oldSourceId = DRIL.idDocumentRepositoryItem)'
				   +' WHERE DRIL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
		
	   EXEC(@sql)				

	   -- documentRepositoryItem language data move logic end
	   
	   /*
	   Drop temporary table DocumentRepositoryItemIdMappings
	   */
	   DROP TABLE #DocumentRepositoryItemIdMappings


-- =============================================================================
-- LOGIC START -- copy tblEnrollment object for full copy ----------------------


	   -- insert records from source site to destination table Enrollment
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblEnrollment]'
	   			 +' ( '
	   			 +'   idSite,' 
	   			 +'   idCourse,'      
	   			 +'   idUser,'
	   			 +'   idGroupEnrollment,'
	   			 +'   idRuleSetEnrollment,'
	   			 +'   idLearningPathEnrollment,'
	   			 +'   idTimezone,'
	   			 +'   isLockedByPrerequisites, '
	   			 +'   code, '
	   			 +'   revcode, '
	   			 +'   title, '
	   			 +'   dtStart, '
	   			 +'   dtDue, '
	   			 +'   dtExpiresFromStart, '
	   			 +'   dtExpiresFromFirstLaunch, '
	   			 +'   dtFirstLaunch, '
	   			 +'   dtCompleted, '
	   			 +'   dtCreated, '
	   			 +'   dtLastSynchronized, '
	   			 +'   dueInterval, '
	   			 +'   dueTimeframe, '
	   			 +'   expiresFromStartInterval, '
	   			 +'   expiresFromStartTimeframe, '
	   			 +'   expiresFromFirstLaunchInterval, '
	   			 +'   expiresFromFirstLaunchTimeframe, '
	   			 +'   idActivityImport, '
	   			 +'   credits '
	   			 +'  ) '
	   			 +' SELECT '
	   			 +    CAST(@idSiteDestination AS NVARCHAR)
	   			 +' , TTC.newDestinationId,'
	   			 +'	 TTU.newDestinationId,'
	   			 +'	 TTGE.newDestinationId,'
	   			 +'	 TTRSE.newDestinationId,'
	   			 +'	 TTLPE.newDestinationId,'
	   			 +'	 E.idTimezone,'
	   			 +'	 E.isLockedByPrerequisites,'
	   			 +'	 E.code,'
	   			 +'	 E.revcode,'
	   			 +'	 E.title,'
	   			 +'	 E.dtStart,'
	   			 +'	 E.dtDue,'
	   			 +'	 E.dtExpiresFromStart,'
	   			 +'	 E.dtExpiresFromFirstLaunch,'
	   			 +'	 E.dtFirstLaunch,'
	   			 +'	 E.dtCompleted,'
	   			 +'	 E.dtCreated,'
	   			 +'	 E.dtLastSynchronized,'
	   			 +'	 E.dueInterval,'
	   			 +'	 E.dueTimeframe,'
	   			 +'	 E.expiresFromStartInterval,'
	   			 +'	 E.expiresFromStartTimeframe,'
	   			 +'	 E.expiresFromFirstLaunchInterval,'
	   			 +'	 E.expiresFromFirstLaunchTimeframe,'
	   			 +'	 TTAI.newDestinationId,'
	   			 +'	 E.credits '
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblEnrollment] E '
	   			 +' LEFT JOIN #CourseIdMappings TTC on (TTC.oldSourceId = E.idCourse ) '
	   			 +' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = E.idUser ) '
	   			 +' LEFT JOIN #GroupEnrollmentIdMappings TTGE on (TTGE.oldSourceId = E.idGroupEnrollment ) '
	   			 +' LEFT JOIN #RuleSetEnrollmentIdMappings TTRSE on (TTRSE.oldSourceId = E.idRuleSetEnrollment ) '
	   			 +' LEFT JOIN #LearningPathEnrollmentIdMappings TTLPE on (TTLPE.oldSourceId = E.idLearningPathEnrollment ) '
	   			 +' LEFT JOIN #ActivityImportIdMappings TTAI on (TTAI.oldSourceId = E.idActivityImport ) '
	   			 +' WHERE E.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   EXEC(@sql)				
	   		

	   -- temporary table to hold the idRuleSetEnrollment of source and destination RuleSetEnrollment tables
	   CREATE TABLE #EnrollmentIdMappings
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL  	   
	   )	

	   -- inserting idRuleSetEnrollment of source RuleSetEnrollment table and destination RuleSetEnrollment table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #EnrollmentIdMappings '
	   			  +' ( '
	   			  +'	   oldSourceId,
	   			  	   newDestinationId'
	   			  +' ) '
	   			  +' SELECT '
	   			  +'	   SE.idEnrollment,'
	   			  +'	   DE.idEnrollment'
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblEnrollment] DE'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblEnrollment] SE'
	   			  +' ON (SE.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (SE.dtStart = DE.dtStart )'
	   			  +' AND (SE.dtCompleted = DE.dtCompleted OR (SE.dtCompleted IS NULL AND DE.dtCompleted IS NULL))'	
	   			  +' AND (SE.dtCreated = DE.dtCreated )'			
	   			  +'  )'
	   			  +' INNER JOIN #CourseIdMappings TTC on (TTC.oldSourceId = SE.idCourse AND TTC.newDestinationId =  DE.idCourse)'
	   			  +' INNER JOIN #UserIdMappings TTU on (TTU.oldSourceId = SE.idUser AND TTU.newDestinationId = DE.idUser)'
	   			  +' WHERE DE.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   			
	   EXEC(@sql)
	   	 
	   

-- =============================================================================
-- LOGIC START -- copy tblEnrollmentRequest object for full copy ----------------------


	   -- insert records from source site to destination table Enrollment Request
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblEnrollmentRequest]'
	   			 +' ( '
	   			 +'   idSite,' 
	   			 +'   idCourse,'      
	   			 +'   idUser,'
	   			 +'   timestamp,'
	   			 +'   dtApproved,'
	   			 +'   dtDenied,'
	   			 +'   idApprover'
	   			 +'  ) '
	   			 +' SELECT '
	   			 +    CAST(@idSiteDestination AS NVARCHAR)
	   			 +' , TTC.newDestinationId,'
	   			 +'	 TTU.newDestinationId,'
	   			 +'	 ER.timestamp,'
	   			 +'	 ER.dtApproved,'
	   			 +'	 ER.dtDenied,'
	   			 +'	 TTUA.newDestinationId'
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblEnrollmentRequest] ER '
	   			 +' LEFT JOIN #CourseIdMappings TTC on (TTC.oldSourceId = ER.idCourse ) '
	   			 +' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = ER.idUser ) '
				 +' LEFT JOIN #UserIdMappings TTUA on (TTU.oldSourceId = ER.idApprover ) '
	   			 +' WHERE ER.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   EXEC(@sql)				


-- =============================================================================
-- LOGIC START -- copy tblData-Lesson object for full copy ---------------------


	   -- insert records from source site to destination table Data-Lesson
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblData-Lesson]'
	   			 +' ( '
	   			 +'   idSite,'     
	   			 +'   idEnrollment,'   							
	   			 +'   idLesson,'
	   			 +'   idTimezone,'
	   			 +'   title,'
	   			 +'   revcode,'
	   			 +'   [order],'
	   			 +'   contentTypeCommittedTo,'
	   			 +'   dtCommittedToContentType,'
	   			 +'   dtCompleted,'
	   			 +'   resetForContentChange,'
	   			 +'   preventPostCompletionLaunchForContentChange '
	   			 +'  ) '
	   			 +' SELECT '
	   			 +    CAST(@idSiteDestination AS NVARCHAR)
	   			 +',  TTE.newDestinationId,'
	   			 +'	 TTL.newDestinationId,'
	   			 +'	 DL.idTimezone,'
	   			 +'	 DL.title,'
	   			 +'	 DL.revcode,'
	   			 +'	 DL.[order],'
	   			 +'	 DL.contentTypeCommittedTo,'
	   			 +'	 DL.dtCommittedToContentType,'
	   			 +'	 DL.dtCompleted,'
	   			 +'	 DL.resetForContentChange,'
	   			 +'	 DL.preventPostCompletionLaunchForContentChange '
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblData-Lesson] DL '
	   			 +' LEFT JOIN #EnrollmentIdMappings TTE on (TTE.oldSourceId = DL.idEnrollment ) '
	   			 +' LEFT JOIN #LessonIdMappings TTL on (TTL.oldSourceId = DL.idLesson ) '
	   			 +' WHERE DL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   EXEC(@sql)					
	   
	   -- temporary table to hold the idData-Lesson of source and destination Data-Lesson tables
	   CREATE TABLE #DataLessonIdMappings
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL  	   
	   )
	   
	   -- inserting idData-Lesson of source Data-Lesson table and destination Data-Lesson table inside temporary table for mapping	
	   SET @sql =	   ' INSERT INTO #DataLessonIdMappings '
	   			  +' ( '
	   			  +'	   oldSourceId,
	   			  	   newDestinationId'
	   			  +' ) '
	   			  +' SELECT '
	   			  +'	   SDL.[idData-Lesson],'
	   			  +'	   DDL.[idData-Lesson]'
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblData-Lesson] DDL'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblData-Lesson] SDL'
	   			  +' ON (SDL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (SDL.title = DDL.title)'
	   			  +' AND (SDL.[order] = DDL.[order])'
	   			  +' AND (SDL.revcode = DDL.revcode OR (SDL.revcode IS NULL AND DDL.revcode IS NULL))'
	   			  +' AND (SDL.dtCommittedToContentType = DDL.dtCommittedToContentType OR (SDL.dtCommittedToContentType IS NULL AND DDL.dtCommittedToContentType IS NULL))'
	   			  +' AND (SDL.dtCompleted = DDL.dtCompleted OR (SDL.dtCompleted IS NULL AND DDL.dtCompleted IS NULL))'
	   			  +' )'
	   			  +' INNER JOIN #EnrollmentIdMappings TTE on (TTE.oldSourceId = SDL.idEnrollment AND TTE.newDestinationId =  DDL.idEnrollment)'
	   			  +' INNER JOIN #LessonIdMappings TTL on ((TTL.oldSourceId = SDL.idLesson AND TTL.newDestinationId =  DDL.idLesson))'
	   			  +' WHERE DDL.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   			  +' AND DDL.idLesson IS NOT NULL'
	   			  +' UNION '
	   			  +' SELECT '
	   			  +'	   SDL.[idData-Lesson],'
	   			  +'	   DDL.[idData-Lesson]'
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblData-Lesson] DDL'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblData-Lesson] SDL'
	   			  +' ON (SDL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (SDL.title = DDL.title )'
	   			  +' AND (SDL.[order] = DDL.[order])'
	   			  +' AND (SDL.revcode = DDL.revcode OR (SDL.revcode IS NULL AND DDL.revcode IS NULL))'
	   			  +' AND (SDL.dtCommittedToContentType = DDL.dtCommittedToContentType OR (SDL.dtCommittedToContentType IS NULL AND DDL.dtCommittedToContentType IS NULL))'
	   			  +' AND (SDL.dtCompleted = DDL.dtCompleted OR (SDL.dtCompleted IS NULL AND DDL.dtCompleted IS NULL))'
	   			  +' )'
	   			  +' INNER JOIN #EnrollmentIdMappings TTE on (TTE.oldSourceId = SDL.idEnrollment AND TTE.newDestinationId =  DDL.idEnrollment)'
	   			  +' WHERE DDL.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   			  +' AND DDL.idLesson IS NULL'
	   
	   EXEC(@sql)	


	     -- insert temp table data for user in order to return it for moving file system object
	   INSERT INTO #ObjectMapping
	   ([oldId],[newId],objectName)
	   SELECT oldSourceId, newDestinationId, 'dataLesson' FROM #DataLessonIdMappings
	   

-- =============================================================================
-- LOGIC START -- copy tblEventLog object for full copy ------------------------


	   -- insert records from source site to destination table EventLog
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblEventLog]'
	   			 +' ( '
	   			 +'  idSite,'
	   			 +'  idEventType,'
	   			 +'  timestamp,'
	   			 +'  eventDate,'
	   			 +'  idObject,'
	   			 +'  idObjectRelated,'
	   			 +'  idObjectUser,'
	   			 +'  idExecutingUser'
	   			 +' ) '
	   			 +'SELECT '
	   			 +   CAST(@idSiteDestination AS NVARCHAR)
	   			 +', EL.idEventType,'
	   			 +'  EL.timestamp,'
	   			 +'  EL.eventDate,'
	   			 +'	CASE 
	   					WHEN EL.idEventType >= 100 AND EL.idEventType < 200 THEN 
	   		  				TTUO.newDestinationId
	   					WHEN EL.idEventType >= 200 AND EL.idEventType < 300 THEN 
	   		  				TTCO.newDestinationId
	   					WHEN EL.idEventType >= 300 AND EL.idEventType < 400 THEN 
	   						TTLO.newDestinationId
	   					When EL.idEventType >= 400 AND EL.idEventType < 500 THEN 
	   						TTSTIO.newDestinationId
	   					WHEN EL.idEventType >= 500 AND EL.idEventType < 600 THEN  
	   						TTLPO.newDestinationId
	   					WHEN EL.idEventType >= 700 AND EL.idEventType < 800 THEN 
	   						TTCEO.newDestinationId
	   				
	   				END ,'
	   		
	   			 +'  CASE 
	   					WHEN EL.idEventType >= 100 AND EL.idEventType < 200 THEN 
	   		  				TTUO.newDestinationId
	   					WHEN EL.idEventType >= 200 AND EL.idEventType < 300 THEN 
	   		  				TTEOR.newDestinationId
	   					WHEN EL.idEventType >= 300 AND EL.idEventType < 400 THEN 
	   						TTDLOR.newDestinationId
	   					When EL.idEventType >= 400 AND EL.idEventType < 500 THEN 
	   						TTSTIO.newDestinationId
	   					WHEN EL.idEventType >= 500 AND EL.idEventType < 600 THEN  
	   						TTLPEOR.newDestinationId
	   					WHEN EL.idEventType >= 700 AND EL.idEventType < 800 THEN 
	   						TTCROR.newDestinationId
	   				   						
	   			     END ,'
	   			 +'  TTOU.newDestinationId, '
	   			 +'  TTUE.newDestinationId '
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblEventLog] EL'
	   			 +' LEFT JOIN #UserIdMappings TTUO on (TTUO.oldSourceId = EL.idObject )'
	   			 +' LEFT JOIN #UserIdMappings TTOU on (TTOU.oldSourceId = EL.idObjectUser ) ' 
	   			 +' LEFT JOIN #CourseIdMappings TTCO on (TTCO.oldSourceId = EL.idObject )'
	   			 +' LEFT JOIN #LessonIdMappings TTLO on (TTLO.oldSourceId = EL.idObject )'
	   			 +' LEFT JOIN #StandUpTrainingInstanceIdMappings TTSTIO on (TTSTIO.oldSourceId = EL.idObject )'
	   			 +' LEFT JOIN #LearningPathIdMappings TTLPO on (TTLPO.oldSourceId = EL.idObject )'
	   			 +' LEFT JOIN #CertificateIdMappings TTCEO on (TTCEO.oldSourceId = EL.idObject )'	

	   			 +' LEFT JOIN #EnrollmentIdMappings TTEOR on (TTEOR.oldSourceId = EL.idObjectRelated ) '
	   			 +' LEFT JOIN #DataLessonIdMappings TTDLOR on (TTDLOR.oldSourceId = EL.idObjectRelated ) '
	   			 +' LEFT JOIN #LearningPathEnrollmentIdMappings TTLPEOR on (TTLPEOR.oldSourceId = EL.idObjectRelated ) '
	   			 +' LEFT JOIN #CertificateRecordIdMappings TTCROR on (TTCROR.oldSourceId = EL.idObjectRelated ) '  
	   			 +' LEFT JOIN #UserIdMappings TTUE on (TTUE.oldSourceId = EL.idExecutingUser )'
	   			 +' WHERE EL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 


	   EXEC(@sql)	

	   -- temporary table to hold the idEventLog of source and destination EventLog tables
	   CREATE TABLE #EventLogIdMappings
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL  	   
	   )	
	   
	   -- inserting idEventLog of source EventLog table and destination EventLog table inside temporary table for mapping	
	   SET @sql =	   ' INSERT INTO #EventLogIdMappings '
	   			  +' ( '
	   			  +'	  oldSourceId,
	   			  	  newDestinationId '
	   			  +' ) '
	   			  +' SELECT 
				  	   SEL.idEventLog, 
				  	   DEL.idEventLog '
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblEventLog] DEL'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblEventLog] SEL'
	   			  +' ON (SEL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (SEL.eventDate = DEL.eventDate )  OR (SEL.eventDate IS NULL AND DEL.eventDate IS NULL))'	
	   			  +' AND (SEL.idEventType = DEL.idEventType) '					
	   			  +' AND (SEL.timestamp = DEL.timestamp) '					
	   			  +' WHERE DEL.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   		
	   EXEC(@sql)	
	   
	   
-- =============================================================================
-- LOGIC START -- copy tblEventEmailQueue object for full copy -----------------

	   -- insert records from source site to destination table EventEmailQueue
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblEventEmailQueue]'
	   			 +' ( '
	   			 +'   idSite,'   
	   			 +'	 idEventLog,'
	   			 +'	 idEventType ,'  
	   			 +'	 idEventEmailNotification,'
	   			 +'	 idEventTypeRecipient,'
	   			 +'	 idObject,'
	   			 +'	 idObjectRelated,'
	   			 +'	 objectType,'
	   			 +'	 idObjectUser,'
	   			 +'	 objectUserFullName,'
	   			 +'	 objectUserFirstName,'
	   			 +'	 objectUserLogin,'
	   			 +'	 objectUserEmail,'
	   			 +'	 idRecipient,'
	   			 +'	 recipientLangString,'
	   			 +'	 recipientFullName,'
	   			 +'	 recipientFirstName,'
	   			 +'	 recipientLogin,'
	   			 +'	 recipientEmail,'
	   			 +'	 recipientTimezone,'
	   			 +'	 recipientTimezoneDotNetName,'
	   			 +'	 [from],'
	   			 +'	 [priority],'
	   			 +'	 dtEvent,'
	   			 +'	 dtAction,'
	   			 +'	 dtActivation,'
	   			 +'	 dtSent,'
	   			 +'	 statusDescription '
	   			 +'  ) '
	   			 +' SELECT '
	   			 +	 CAST(@idSiteDestination AS NVARCHAR)
	   			 +',	 TTELog.newDestinationId,'
	   			 +'	 EEQ.idEventType ,'  
	   			 +'	 TTEEN.newDestinationId,'
	   			 +'	 EEQ.idEventTypeRecipient,'
	   			 +'	 CASE EEQ.objectType
	   			 		 WHEN ''user'' THEN 
	   			   			TTU.newDestinationId
	   			 		 WHEN ''courseenrollment'' THEN 
	   			   			TTC.newDestinationId
	   			 		 WHEN ''lesson'' THEN 
	   			 			TTL.newDestinationId
	   			 		 When ''standuptraininginstance'' THEN 
	   			 			TTSTI.newDestinationId
	   			 		 WHEN ''learningpathenrollment'' THEN  
	   			 			TTLP.newDestinationId
	   			 		 WHEN ''certificate'' THEN 
	   			 			TTCE.newDestinationId
	   			 		 ELSE  
	   			 			TTELog.newDestinationId
	   			 	 END ,'
	   			 
	   			 +'	 CASE EEQ.objectType
	   			 		 WHEN ''user'' THEN 
	   			   			TTU.newDestinationId
	   			 		 WHEN ''courseenrollment'' THEN 
	   			   			TTEOR.newDestinationId
	   			 		 WHEN ''lesson'' THEN 
	   			 			TTDLOR.newDestinationId
	   			 		 When ''standuptraininginstance'' THEN 
	   			 			TTSTI.newDestinationId
	   			 		 WHEN ''learningpathenrollment'' THEN  
	   			 			TTLPEOR.newDestinationId
	   			 		 WHEN ''certificate'' THEN 
	   			 			TTCROR.newDestinationId
	   			 		 ELSE  
	   			 			TTELog.newDestinationId
	   			 	 END ,'
	   			 +'	 EEQ.objectType,'
	   			 +'	 TTOU.newDestinationId,'
	   			 +'	 EEQ.objectUserFullName,'
	   			 +'	 EEQ.objectUserFirstName,'
	   			 +'	 EEQ.objectUserLogin,'
	   			 +'	 EEQ.objectUserEmail,'
	   			 +'	 TTUR.newDestinationId,'
	   			 +'	 EEQ.recipientLangString,'
	   			 +'	 EEQ.recipientFullName,'
	   			 +'	 EEQ.recipientFirstName,'
	   			 +'	 EEQ.recipientLogin,'
	   			 +'	 EEQ.recipientEmail,'
	   			 +'	 EEQ.recipientTimezone,'
	   			 +'	 EEQ.recipientTimezoneDotNetName,'
	   			 +'	 EEQ.[from],'
	   			 +'	 EEQ.[priority],'
	   			 +'	 EEQ.dtEvent,'
	   			 +'	 EEQ.dtAction,'
	   			 +'	 EEQ.dtActivation,'
	   			 +'	 EEQ.dtSent,'
	   			 +'	 EEQ.statusDescription '
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblEventEmailQueue] EEQ '
	   			 +' LEFT JOIN #EventLogIdMappings TTELog on (TTELog.oldSourceId = EEQ.idEventLog ) '
	   			 +' LEFT JOIN #EventEmailNotificationIdMappings TTEEN on (TTEEN.oldSourceId = EEQ.idEventEmailNotification ) '
	   			 +' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = EEQ.idObject ) ' 
	   			 +' LEFT JOIN #UserIdMappings TTOU on (TTOU.oldSourceId = EEQ.idObjectUser ) ' 
	   			 +' LEFT JOIN #CourseIdMappings TTC on (TTC.oldSourceId = EEQ.idObject )'
	   			 +' LEFT JOIN #LessonIdMappings TTL on (TTL.oldSourceId = EEQ.idObject ) '
	   			 +' LEFT JOIN #StandUpTrainingInstanceIdMappings TTSTI on (TTSTI.oldSourceId = EEQ.idObject ) '
	   			 +' LEFT JOIN #LearningPathIdMappings TTLP on (TTLP.oldSourceId = EEQ.idObject ) '
	   			 +' LEFT JOIN #CertificateIdMappings TTCE on (TTCE.oldSourceId = EEQ.idObject ) '
	   			 +' LEFT JOIN #EnrollmentIdMappings TTEOR on (TTEOR.oldSourceId = EEQ.idObjectRelated ) '
	   			 +' LEFT JOIN #DataLessonIdMappings TTDLOR on (TTDLOR.oldSourceId = EEQ.idObjectRelated )'
	   			 +' LEFT JOIN #LearningPathEnrollmentIdMappings TTLPEOR on (TTLPEOR.oldSourceId = EEQ.idObjectRelated ) '
	   			 +' LEFT JOIN #CertificateRecordIdMappings TTCROR on (TTCROR.oldSourceId = EEQ.idObjectRelated ) '
	   			 +' LEFT JOIN #UserIdMappings TTUR on (TTUR.oldSourceId = EEQ.idRecipient ) ' 
	   			 +' WHERE EEQ.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   EXEC(@sql)

	   

	   /*
	   DROP temporary table EventEmailNotificationIdMappings
	   */
	   DROP TABLE #EventEmailNotificationIdMappings
	   
	   /*
	   Drop temporary table EventLogIdMappings
	   */
	   DROP TABLE #EventLogIdMappings
	    
	   /* 
	   Drop temporary table StandUpTrainingInstanceIdMappings
	   */
	   DROP TABLE #StandUpTrainingInstanceIdMappings
	     
	   /* 
	   Drop temporary table CertificateIdMappings
	   */
	   DROP TABLE #CertificateIdMappings
	   
	   /*  
	   Drop temporary table CertificateRecordIdMappings
	   */
	   DROP TABLE #CertificateRecordIdMappings
	   
	   /*
	   Drop temporary table ActivityImportIdMappings
	   */
	   DROP TABLE #ActivityImportIdMappings
	   

-- =============================================================================
-- LOGIC START -- copy tblUserToLearningPathLink object for full copy ----------


	   -- insert records from source site to destination table UserToLearningPathLink
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblUserToLearningPathLink]'
	   			 +' ( '
	   			 +'	 idSite,' 
	   			 +'	 idUser,' 
	   			 +'	 idLearningPath,' 
	   			 +'	 idRuleSet,' 
	   			 +'	 created'  
	   			 +' ) '
	   			 +' SELECT '
	   		  	 +    CAST(@idSiteDestination AS NVARCHAR)
	   			 +',  TTU.newDestinationId,' 
	   			 +'   TTLP.newDestinationId,' 
	   			 +'	 TTRS.newDestinationId,' 
	   			 +'	 ULPL.created '  
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblUserToLearningPathLink] ULPL'
	   			 +' INNER JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblLearningPath] LP ON LP.idLearningPath= ULPL.idLearningPath '
	   			 +' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = ULPL.idUser)'
	   			 +' LEFT JOIN #LearningPathIdMappings TTLP on (TTLP.oldSourceId = ULPL.idLearningPath)'
	   			 +' LEFT JOIN #RuleSetIdMappings TTRS on (TTRS.oldSourceId = ULPL.idRuleSet)'
	   			 +' WHERE LP.idSite = '+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   EXEC(@sql)


	   /*
	   Drop temporary table LearningPathEnrollmentIdMappings
	   */
	   DROP TABLE #LearningPathEnrollmentIdMappings

	   
    END

------------------------------------------------------------------------------
-- full copy end


	     /*
	   Drop temporary table DocumentRepositoryFolderIdMappings, temporary table DocumentRepositoryFolder will be dorp in both conditions full copy and partial copy 
	   */
	   DROP TABLE #DocumentRepositoryFolderIdMappings

	   /*
	   Drop temporary table RuleSetEnrollmentIdMappings, temporary table RuleSetEnrollment will be dorp in both conditions full copy and partial copy 
	   */
	   DROP TABLE #RuleSetEnrollmentIdMappings

	   /*
	   Drop temporary tableGroupEnrollmentIdMappings, temporary table GroupEnrollment will be dorp in both conditions full copy and partial copy 
	   */
	   DROP TABLE #GroupEnrollmentIdMappings
	   
	   /*
	   Drop temporary table RulesetIdMappings, temporary table Ruleset will be dorp in both conditions full copy and partial copy 
	   */
	   DROP TABLE #RulesetIdMappings

    	   /*   
	   Drop temporary table LearningPathIdMappings, temporary table LearningPath will be dorp in both conditions full copy and partial copy 
	   */
	   DROP TABLE #LearningPathIdMappings
	   
	   /*  
	   Drop temporary table LessonIdMappings, temporary table Lesson will be dorp in both conditions full copy and partial copy 
	   */
	   DROP TABLE #LessonIdMappings

------------------------------------------------------------------------------

	   
	   -- check condition for the full copy
	   IF @fullCopyFlag = 1
	   BEGIN



-- =============================================================================
-- LOGIC START -- copy tblData-SCO object for full copy ------------------------


	   -- insert records from source site to destination table Data-SCO
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblData-SCO]'
	   			 +' ( '
	   			 +'   idSite,'  
	   			 +'   [idData-Lesson],'   							
	   			 +'   idTimezone,'
	   			 +'   manifestIdentifier,'
	   			 +'   completionStatus,'
	   			 +'   successStatus,'
	   			 +'   scoreScaled,'
	   			 +'   totalTime,'
	   			 +'   [timestamp],'
	   			 +'   actualAttemptCount,'
	   			 +'   effectiveAttemptCount,'
	   			 +'   proctoringUser '
	   			 +' ) '
	   			 +' SELECT '
	   			 +    CAST(@idSiteDestination AS NVARCHAR)
	   			 +',  TTDL.newDestinationId,'
	   			 +'	 DS.idTimezone,'
	   			 +'	 DS.manifestIdentifier,'
	   			 +'	 DS.completionStatus,'
	   			 +'	 DS.successStatus,'
	   			 +'	 DS.scoreScaled,'
	   			 +'	 DS.totalTime,'
	   			 +'	 DS.[timestamp],'
	   			 +'	 DS.actualAttemptCount,'
	   			 +'	 DS.effectiveAttemptCount,'
	   			 +'	 TTU.newDestinationId '
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblData-SCO] DS '
	   			 +' LEFT JOIN #DataLessonIdMappings TTDL on (TTDL.oldSourceId = DS.[idData-Lesson] ) '
	   			 +' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = DS.[proctoringUser] ) '
	   			 +' WHERE DS.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   EXEC(@sql)					
	   
	   -- temporary table to hold the idData-SCO of source and destination Data-SCO tables
	   CREATE TABLE #DataSCOIdMappings
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL  	   
	   )	
	
	   -- inserting idData-SCO of source Data-SCO table and destination Data-SCO table inside temporary table for mapping	
	   SET @sql =	   ' INSERT INTO #DataSCOIdMappings '
	   			  +' ( '
	   			  +'	 oldSourceId,
	   			  	 newDestinationId'
	   			  +'  ) '
	   			  +' SELECT '
	   			  +'	 SDS.[idData-SCO],'
	   			  +'	 DDS.[idData-SCO]'
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblData-SCO] DDS'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblData-SCO] SDS'
	   			  +' ON (SDS.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (SDS.completionStatus = DDS.completionStatus )'
	   			  +' AND (SDS.successStatus = DDS.successStatus )'
	   			  +' AND (SDS.scoreScaled = DDS.scoreScaled OR (SDS.scoreScaled IS NULL AND DDS.scoreScaled IS NULL))'
	   			  +' AND (SDS.totalTime = DDS.totalTime OR (SDS.totalTime IS NULL AND DDS.totalTime IS NULL))'
	   			  +' AND (SDS.[timestamp] = DDS.[timestamp] OR (SDS.[timestamp] IS NULL AND DDS.[timestamp] IS NULL))'
	   			  +'  )'
	   			  +' WHERE DDS.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   			
	   EXEC(@sql)
	   


-- =============================================================================
-- LOGIC START -- copy tblData-HomeworkAssignment object for full copy ---------


	   -- insert records from source site to destination table Data-HomeworkAssignment
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblData-HomeworkAssignment]'
	   			 +' ( '
	   			 +'	 idSite,'     
	   			 +'	 [idData-Lesson],' 							
	   			 +'	 uploadedAssignmentFilename,'
	   			 +'	 dtUploaded,'
	   			 +'	 completionStatus,'
	   			 +'	 successStatus,'
	   			 +'	 score,'
	   			 +'	 [timestamp],'
	   			 +'	 proctoringUser'
	   			 +' ) '
	   			 +' SELECT '
	   			 +    CAST(@idSiteDestination AS NVARCHAR)
	   			 +',  TTDL.newDestinationId,'
	   			 +'	 DHA.uploadedAssignmentFilename,'
	   			 +'	 DHA.dtUploaded,'
	   			 +'	 DHA.completionStatus,'
	   			 +'	 DHA.successStatus,'
	   			 +'	 DHA.score,'
	   			 +'	 DHA.[timestamp],'
	   			 +'	 TTU.newDestinationId '
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblData-HomeworkAssignment] DHA '
	   			 +' LEFT JOIN #DataLessonIdMappings TTDL on (TTDL.oldSourceId = DHA.[idData-Lesson] ) '
	   			 +' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = DHA.[proctoringUser] ) '
	   			 +' WHERE DHA.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   EXEC(@sql)	



-- =============================================================================
-- LOGIC START -- copy tblxAPIoAuthConsumer object for full copy ---------------


	   -- insert records from source site to destination table xAPIoAuthConsumer
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblxAPIoAuthConsumer]'
	   			 +' ( '
	   			 +'   idSite,'  
	   			 +'   label,'
	   			 +'   oAuthKey,'
	   			 +'   oAuthSecret,'
	   			 +'   isActive '
	   			 +' ) '
	   			 +' SELECT '
	   			 +    CAST(@idSiteDestination AS NVARCHAR)
	   			 +',  XAPIAC.label,'
	   			 +'   XAPIAC.oAuthKey,'
	   			 +'   XAPIAC.oAuthSecret,'
	   			 +'   XAPIAC.isActive '		
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblxAPIoAuthConsumer] XAPIAC'
	   			 +' WHERE XAPIAC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   								
	   EXEC(@sql)	
			
	   -- temporary table to hold the idxAPIoAuthConsumer of source and destination xAPIoAuthConsumer tables
	   CREATE TABLE #xAPIoAuthConsumerIdMappings
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL  	   
	   )	

	   -- inserting idxAPIoAuthConsumer of source xAPIoAuthConsumer table and destination xAPIoAuthConsumer table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #xAPIoAuthConsumerIdMappings '
	   			  +' ( '
	   			  +'	 oldSourceId,
	   			  	 newDestinationId'
	   			  +'  ) '
	   			  +' SELECT 
					   SP.idxAPIoAuthConsumer, 
					   DP.idxAPIoAuthConsumer '
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblxAPIoAuthConsumer] DP'
	   			  +' INNER JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblxAPIoAuthConsumer] SP'
	   			  +' ON (SP.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (SP.label = DP.label OR (SP.label IS NULL AND DP.label IS NULL))' 
	   			  +' AND (SP.oAuthKey = DP.oAuthKey OR (SP.oAuthKey IS NULL AND DP.oAuthKey IS NULL))' 
	   			  +' AND (SP.oAuthSecret = DP.oAuthSecret OR (SP.oAuthSecret IS NULL AND DP.oAuthSecret IS NULL))'					
	   			  +' )'
	   			  +' WHERE DP.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   			
	   EXEC(@sql)	



-- =============================================================================
-- LOGIC START -- copy tblData-TinCan object for full copy ---------------------


	   -- insert records from source site to destination table Data-TinCan
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblData-TinCan]'
	   			 +' ( '
	   			 +'  [idData-Lesson], '
	   		   	 +'  [idData-TinCanParent] , '
	   		   	 +'  idSite,  '
	   		   	 +'	idEndpoint, '
	   		   	 +'	isInternalAPI, '
	   		   	 +'	statementId, '
	   		   	 +'	actor, '
	   		   	 +'	verbId, '
	   		   	 +'	verb, '
	   		   	 +'	activityId, '
	   		   	 +'	mboxObject, '
	   		      +'  mboxSha1SumObject, '
	   		   	 +'	openIdObject, '
	   		   	 +'	accountObject, '
	   		   	 +'	mboxActor, '
	   		   	 +'	mboxSha1SumActor, '
	   		   	 +'	openIdActor, '
	   		   	 +'	accountActor, '
	   		   	 +'	mboxAuthority, '
	   		   	 +'	mboxSha1SumAuthority, '
	   		      +'  openIdAuthority, '
	   		   	 +'	accountAuthority, '
	   		   	 +'	mboxTeam, '
	   		   	 +'	mboxSha1SumTeam, '
	   		   	 +'	openIdTeam, '
	   		   	 +'	accountTeam, '
	   		   	 +'	mboxInstructor, '
	   		   	 +'	mboxSha1SumInstructor, '
	   		   	 +'	openIdInstructor, '
	   		      +'  accountInstructor, '
	   		   	 +'	[object], '
	   		   	 +'	registration, '
	   		   	 +'	[statement], '
	   		   	 +'	isVoidingStatement, '
	   		   	 +'	isStatementVoided, '
	   		   	 +'	dtStored, '
	   		   	 +'	scoreScaled '
	   			 +' ) '
	   			 +' SELECT '
	   			 +'  TTDL.newDestinationId, '
	   		   	 +'  NULL , '			     -- this null value of [idData-TinCanParent] will be updated from temporary DataTinCanIdMappings table below 
	   		   	 +   CAST(@idSiteDestination AS NVARCHAR)
	   		   	 +',	TTXAAC.newDestinationId, '
	   		   	 +'	DTC.isInternalAPI, '
	   		   	 +'	DTC.statementId, '
	   		   	 +'	DTC.actor, '
	   		   	 +'	DTC.verbId, '
	   		   	 +'	DTC.verb, '
	   		   	 +'	DTC.activityId, '
	   		   	 +'	DTC.mboxObject, '
	   		      +'  DTC.mboxSha1SumObject, '
	   		   	 +'	DTC.openIdObject, '
	   		   	 +'	DTC.accountObject, '
	   		   	 +'	DTC.mboxActor, '
	   		   	 +'	DTC.mboxSha1SumActor, '
	   		   	 +'	DTC.openIdActor, '
	   		   	 +'	DTC.accountActor, '
	   		   	 +'	DTC.mboxAuthority, '
	   		   	 +'	DTC.mboxSha1SumAuthority, '
	   		      +'  DTC.openIdAuthority, '
	   		   	 +'	DTC.accountAuthority, '
	   		   	 +'	DTC.mboxTeam, '
	   		   	 +'	DTC.mboxSha1SumTeam, '
	   		   	 +'	DTC.openIdTeam, '
	   		   	 +'	DTC.accountTeam, '
	   		   	 +'	DTC.mboxInstructor, '
	   		   	 +'	DTC.mboxSha1SumInstructor, '
	   		   	 +'	DTC.openIdInstructor, '
	   		      +'  DTC.accountInstructor, '
	   		   	 +'	DTC.[object], '
	   		   	 +'	DTC.registration, '
	   		   	 +'	DTC.[statement], '
	   		   	 +'	DTC.isVoidingStatement, '
	   		   	 +'	DTC.isStatementVoided, '
	   		   	 +'	DTC.dtStored, '
	   		   	 +'	DTC.scoreScaled '
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblData-TinCan] DTC '
	   			 +' LEFT JOIN #DataLessonIdMappings TTDL on TTDL.oldSourceId = DTC.[idData-Lesson] '
	   			 +' LEFT JOIN #xAPIoAuthConsumerIdMappings TTXAAC on TTXAAC.oldSourceId = DTC.[idEndpoint] '
	   			 +' WHERE DTC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   EXEC(@sql)					
	   
	   -- temporary table to hold the idData-TinCan of source and destination Data-TinCan tables
	   CREATE TABLE #DataTinCanIdMappings
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL  	   
	   )	
	   
	   -- inserting idData-TinCan of source Data-TinCan table and destination Data-TinCan table inside temporary table for mapping	
	   SET @sql =	   ' INSERT INTO #DataTinCanIdMappings '
	   			  +' ( '
	   			  +'	  oldSourceId,
	   			  	  newDestinationId'
	   			  +' ) '
	   			  +' SELECT '
	   			  +'	  SDS.[idData-TinCan],'
	   			  +'	  DDS.[idData-TinCan]'
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblData-TinCan] DDS'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblData-TinCan] SDS'
	   			  +' ON (SDS.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (SDS.statementId = DDS.statementId OR (SDS.statementId IS NULL AND DDS.statementId IS NULL))'
	   			  +' AND (SDS.dtStored = DDS.dtStored )'
	   			  +' )'
	   			  +' WHERE DDS.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   			
	   EXEC(@sql)
	   
	   -- update [idData-TinCanParent] from temp table DataTinCan
	   SET @sql =	  ' UPDATE DDTC'
	   			 +'  SET [idData-TinCanParent] = TEMP.newDestinationId'
	   			 +' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblData-TinCan] SDTC'
	   			 +' INNER JOIN  [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblData-TinCan] DDTC'
	   			 +' ON( SDTC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			 +' AND (SDTC.statementId = DDTC.statementId OR (SDTC.statementId IS NULL AND DDTC.statementId IS NULL))'
	   			 +' AND (SDTC.dtStored = DDTC.dtStored OR (SDTC.dtStored IS NULL AND DDTC.dtStored IS NULL))'
	   			 +' )'
	   			 +' INNER JOIN  #DataTinCanIdMappings TEMP'
	   			 +' ON(TEMP.oldSourceId = SDTC.[idData-TinCanParent])'
	   			 +' WHERE DDTC.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   			 +' AND SDTC.[idData-TinCanParent] IS NOT NULL'
	   
	   EXEC(@sql)	



-- =============================================================================
-- LOGIC START -- copy tblxAPIoAuthNonce object for full copy ------------------


	   -- insert records from source site to destination table xAPIoAuthNonce
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblxAPIoAuthNonce]'
	   			 +' ( '			   
	   		   	 +'   idxAPIoAuthConsumer,  '   
	   		   	 +'	 nonce '   					 
	   			 +' ) '
	   			 +' SELECT '
	   		   	 +'	 TTXAAC.newDestinationId, '
	   		   	 +'	 xAPIAN.nonce '
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblxAPIoAuthNonce] xAPIAN '  
	   			 +' INNER JOIN  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblxAPIoAuthConsumer] xAPIAC  ON  xAPIAN.idxAPIoAuthConsumer= xAPIAC.idxAPIoAuthConsumer ' 
	   			 +' LEFT JOIN #xAPIoAuthConsumerIdMappings TTXAAC on TTXAAC.oldSourceId = xAPIAN.[idxAPIoAuthConsumer] '
	   			 +' WHERE xAPIAC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
		
	   
	   EXEC(@sql)	



-- =============================================================================
-- LOGIC START -- copy tblData-TinCanProfile object for full copy --------------


	   -- insert records from source site to destination table Data-TinCanProfile
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblData-TinCanProfile]'
	   			 +' ( '			   
	   		   	 +'   idSite,  '   
	   		   	 +'	 idEndpoint, '
	   		   	 +'	 profileId, '
	   		   	 +'	 activityId, '
	   		   	 +'	 agent, '
	   		   	 +'	 contentType, '
	   		   	 +'	 docContents, '
	   		   	 +'	 dtUpdated '
	   			 +' ) '
	   			 +' SELECT '
	   		   	 +    CAST(@idSiteDestination AS NVARCHAR)
	   		   	 +',	 TTXAAC.newDestinationId, '
	   		   	 +'	 DTCP.profileId, '
	   		   	 +'	 DTCP.activityId, '
	   		   	 +'	 DTCP.agent, '
	   		   	 +'	 DTCP.contentType, '
	   		   	 +'	 DTCP.docContents, '
	   		   	 +'	 DTCP.dtUpdated  '
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblData-TinCanProfile] DTCP '
	   			 +' LEFT JOIN #xAPIoAuthConsumerIdMappings TTXAAC on TTXAAC.oldSourceId = DTCP.[idEndpoint] '
	   			 +' WHERE DTCP.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   EXEC(@sql)		



-- =============================================================================
-- LOGIC START -- copy tblData-TinCanState object for full copy ----------------


	   -- insert records from source site to destination table Data-TinCanState
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblData-TinCanState]'
	   			 +' ( '			   
	   		   	 +'   idSite,  '   
	   		   	 +'	 idEndpoint, '   					 
	   		   	 +'	 stateId, '
	   		   	 +'	 agent, '
	   		   	 +'	 activityId, '
	   		   	 +'	 registration, '
	   		   	 +'	 contentType, '
	   		   	 +'	 docContents, '
	   		   	 +'	 dtUpdated '
	   			 +' ) '
	   			 +' SELECT '
	   		   	 +    CAST(@idSiteDestination AS NVARCHAR)
	   		   	 +',	 TTXAAC.newDestinationId, '
	   		   	 +'	 DTCS.stateId, '
	   		   	 +'	 DTCS.agent, '
	   		   	 +'	 DTCS.activityId, '
	   		   	 +'	 DTCS.registration, '
	   		   	 +'	 DTCS.contentType, '
	   		   	 +'	 DTCS.docContents,  '
	   		   	 +'	 DTCS.dtUpdated  '
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblData-TinCanState] DTCS '
	   			 +' LEFT JOIN #xAPIoAuthConsumerIdMappings TTXAAC on TTXAAC.oldSourceId = DTCS.[idEndpoint] '
	   			 +' WHERE DTCS.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   EXEC(@sql)	

	
	   /*
	   Drop temporary table xAPIoAuthConsumerIdMappings
	   */
	   DROP TABLE #xAPIoAuthConsumerIdMappings
	   
	   /*
	   Drop temporary table DataLessonIdMappings
	   */
	   DROP TABLE #DataLessonIdMappings



-- =============================================================================
-- LOGIC START -- copy tblData-TinCanContextActivities object for full copy ----


	   -- insert records from source site to destination table Data-TinCanContextActivities
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblData-TinCanContextActivities]'
	   			 +' ( '
	   			 +'   [idData-TinCan],'          
	   			 +'   idSite,'          
	   			 +'   activityId,' 							
	   			 +'   activityType,'
	   			 +'   objectType '
	   			 +' ) '
	   			 +' SELECT '
	   			 +'   TTDTC.newDestinationId,'
	   			 +    CAST(@idSiteDestination AS NVARCHAR)
	   			 +',  DTCA.activityId,'
	   			 +'	 DTCA.activityType,'
	   			 +'	 DTCA.objectType '
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblData-TinCanContextActivities] DTCA '
	   			 +' LEFT JOIN #DataTinCanIdMappings TTDTC on (TTDTC.oldSourceId = DTCA.[idData-TinCan] ) '
	   			 +' WHERE DTCA.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   EXEC(@sql)	


	   /*
	   Drop temporary table DataTinCanIdMappings
	   */
	   DROP TABLE #DataTinCanIdMappings



-- =============================================================================
-- LOGIC START -- copy tblData-SCOInt object for full copy ---------------------


	   -- insert records from source site to destination table Data-SCOInt
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblData-SCOInt]'
	   			 +' ( '
	   			 +'   idSite,'        
	   			 +'   [idData-SCO],' 							
	   			 +'   [identifier],'
	   			 +'   [timestamp], '
	   			 +'   [result], '
	   			 +'   [latency], '
	   			 +'   [scoIdentifier] '
	   			 +' ) '
	   			 +' SELECT '
	   			 +    CAST(@idSiteDestination AS NVARCHAR)
	   			 +',  TTDS.newDestinationId,'
	   			 +'   DSI.identifier,'
	   			 +'	 DSI.[timestamp],'
	   			 +'	 DSI.result, '
	   			 +'	 DSI.latency, '
	   			 +'	 DSI.scoIdentifier '
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblData-SCOInt] DSI '
	   			 +' LEFT JOIN #DataSCOIdMappings TTDS on (TTDS.oldSourceId = DSI.[idData-SCO] ) '
	   			 +' WHERE DSI.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   
	   EXEC(@sql)		


-- =============================================================================
-- LOGIC START -- copy tblData-SCOObj object for full copy ---------------------


	   -- insert records from source site to destination table Data-SCOObj
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblData-SCOObj]'
	   			 +' ( '
	   			 +'   idSite,'        
	   			 +'   [idData-SCO],' 			 
	   			 +'   [identifier],'
	   			 +'   [scoreScaled], '
	   			 +'   [completionStatus], '
	   			 +'   [successStatus] '
	   			 +' ) '
	   			 +' SELECT '
	   			 +    CAST(@idSiteDestination AS NVARCHAR)
	   			 +',  TTDS.newDestinationId,'
	   			 +'   DSO.identifier,'
	   			 +'	 DSO.scoreScaled,'
	   			 +'	 DSO.completionStatus, '
	   			 +'	 DSO.successStatus '
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblData-SCOObj] DSO '
	   			 +' LEFT JOIN #DataSCOIdMappings TTDS on (TTDS.oldSourceId = DSO.[idData-SCO] ) '
	   			 +' WHERE DSO.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   EXEC(@sql)				


	   /*
	   Drop temporary table DataSCOIdMappings
	   */
	   DROP TABLE #DataSCOIdMappings



-- =============================================================================
-- LOGIC START -- copy tblPurchase object for full copy ------------------------


	   -- insert records from source site to destination table Purchase
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblPurchase]'
	   			 +' ( '
	   			 +'   idUser,'
	   			 +'   orderNumber,'
	   			 +'   timeStamp,'
	   			 +'   ccnum,'
	   			 +'   currency,'
	   			 +'   idSite,'
	   			 +'   dtPending,'
	   			 +'   failed,'
	   			 +'   transactionId '
	   			 +' ) '
	   			 +' SELECT '
	   			 +'   TTU.newDestinationId,'
	   			 +'   TP.orderNumber,'
	   			 +'   TP.timeStamp,'
	   			 +'   TP.ccnum,'
	   			 +'   TP.currency,'
	   			 +    CAST(@idSiteDestination AS NVARCHAR)
	   			 +',  TP.dtPending,'
	   			 +'   TP.failed, '
	   			 +'	 TP.transactionId '
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblPurchase] TP '
	   			 +' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = TP.idUser ) '
	   			 +' WHERE TP.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   								
	   EXEC(@sql)	
		
	   -- temporary table to hold the idPurchase of source and destination Purchase tables
	   CREATE TABLE #PurchaseIdMappings
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL  	   
	   )	
	   
	   -- inserting idPurchase of source Purchase table and destination Purchase table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #PurchaseIdMappings '
	   			  +' ( '
	   			  +'	  oldSourceId,
	   			  	  newDestinationId'
	   			  +' ) '
	   			  +' SELECT 
					   SP.idPurchase, 
					   DP.idPurchase '
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblPurchase] DP'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblPurchase] SP'
	   			  +' ON (SP.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (SP.orderNumber = DP.orderNumber )' 
	   			  +' AND (SP.timeStamp = DP.timeStamp OR (SP.timeStamp IS NULL AND DP.timeStamp IS NULL))' 
	   			  +' AND (SP.dtPending = DP.dtPending OR (SP.dtPending IS NULL AND DP.dtPending IS NULL))'					
	   			  +' )'
	   			  +' WHERE DP.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   			
	   EXEC(@sql)	

	  

-- =============================================================================
-- LOGIC START -- copy tblTransactionItem object for full copy -----------------


	   -- insert records from source site to destination table TransactionItem
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblTransactionItem]'
	   			 +' ( '
	   			 +'   idParentTransactionItem,'
	   			 +'   idPurchase,'
	   			 +'   idEnrollment,'
	   			 +'   idUser,'
	   			 +'   userName,'
	   			 +'   idAssigner,'
	   			 +'   assignerName,'
	   			 +'   itemId,'
	   			 +'   itemName,'
	   			 +'   itemType,'
	   			 +'   description,'
	   			 +'   cost,'
	   			 +'   paid,'
	   			 +'   idCouponCode,'
	   			 +'   couponCode,'
	   			 +'   dtAdded,'
	   			 +'   confirmed,'
	   			 +'   idSite'
	   			 +'  ) '
	   			 +' SELECT '
	   			 +'   NULL ,'     -- this null value of idParentTransactionItem will be updated from temporary TransactionItemIdMappings table below
	   			 +'   TTP.newDestinationId,'  
	   			 +'   TTE.newDestinationId,'
	   			 +'   TTU.newDestinationId,'
	   			 +'   TI.userName,'
	   			 +'   TTUA.newDestinationId,'
	   			 +'   TI.assignerName,'
	   			 +'   TI.itemId,'
	   			 +'   TI.itemName,'
	   			 +'   TI.itemType,'
	   			 +'   TI.description,'
	   			 +'   TI.cost,'
	   			 +'   TI.paid,'
	   			 +'   TTCC.newDestinationId,'
	   			 +'   TI.couponCode,'
	   			 +'   TI.dtAdded,'
	   			 +'   TI.confirmed , '
	   			 +    CAST(@idSiteDestination AS NVARCHAR)
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblTransactionItem] TI '
	   			 +' LEFT JOIN #PurchaseIdMappings TTP on (TTP.oldSourceId = TI.idPurchase ) '
	   			 +' LEFT JOIN #EnrollmentIdMappings TTE on (TTE.oldSourceId = TI.idEnrollment ) '
	   			 +' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = TI.idUser ) '
	   			 +' LEFT JOIN #UserIdMappings TTUA on (TTUA.oldSourceId = TI.idAssigner )'
	   			 +' LEFT JOIN #CouponCodeIdMappings TTCC on (TTCC.oldSourceId = TI.idCouponCode ) '
	   			 +' WHERE TI.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   EXEC(@sql)
	  
	   -- temporary table to hold the idTransactionItem of source and destination TransactionItem tables
	   CREATE TABLE #TransactionItemIdMappings
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL  	   
	   )	
	   
	   -- inserting idTransactionItem of source TransactionItem table and destination TransactionItem table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #TransactionItemIdMappings '
	   			  +' ( '
	   			  +'	   oldSourceId,
	   				   newDestinationId'
	   			  +' ) '
	   			  +' SELECT '
	   			  +'	   STI.[idTransactionItem],'
	   			  +'	   DTI.[idTransactionItem] '
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblTransactionItem] DTI'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblTransactionItem] STI'
	   			  +' ON (STI.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (STI.itemName = DTI.itemName OR (STI.itemName IS NULL AND DTI.itemName IS NULL))'
	   			  +' AND (STI.userName = DTI.userName OR (STI.userName IS NULL AND DTI.userName IS NULL))'
	   			  +' AND (STI.dtAdded = DTI.dtAdded OR (STI.dtAdded IS NULL AND DTI.dtAdded IS NULL))'
	   			  +' AND (STI.confirmed = DTI.confirmed OR (STI.confirmed IS NULL AND DTI.confirmed IS NULL))'
	   			  +' )'
	   			  +' WHERE DTI.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   			
	   EXEC(@sql)


	   -- update idParentTransactionItem from Temp Table TransactionItem
	   SET @sql =	   ' UPDATE DTI '
	   			  +'  SET idParentTransactionItem = TEMP.newDestinationId'
	   			  +' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblTransactionItem] STI'
	   			  +' INNER JOIN  [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblTransactionItem] DTI'
	   			  +' ON( STI.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (STI.itemName = DTI.itemName OR (STI.itemName IS NULL AND DTI.itemName IS NULL))'
	   			  +' AND (STI.userName = DTI.userName OR (STI.userName IS NULL AND DTI.userName IS NULL))'
	   			  +' AND (STI.dtAdded = DTI.dtAdded OR (STI.dtAdded IS NULL AND DTI.dtAdded IS NULL))'
	   			  +' AND (STI.confirmed = DTI.confirmed OR (STI.confirmed IS NULL AND DTI.confirmed IS NULL))'
	   			  +'  )'
	   			  +' INNER JOIN  #TransactionItemIdMappings TEMP'
	   			  +' ON(TEMP.oldSourceId = STI.idParentTransactionItem)'
	   			  +' WHERE DTI.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   			  +' AND STI.idParentTransactionItem IS NOT NULL'
	   
	   EXEC(@sql)	

	   
	   /*
	   Drop temporary table TransactionItemIdMappings
	   */
	   DROP TABLE #TransactionItemIdMappings

	   /*
	   Drop temporary table PurchaseIdMappings
	   */
	   DROP TABLE #PurchaseIdMappings
	    
	   /*   
	   Drop temporary table EnrollmentIdMappings
	   */
	   DROP TABLE #EnrollmentIdMappings
	   
	   /*
	   Drop temporary table CouponCodeIdMappings
	   */
	   DROP TABLE #CouponCodeIdMappings


    END

---------------------------------------------------------------------------------------
-- full copy end


-- =====================================================================================
--LOGIC START -- copy tblCertificationType object both full copy and partial copy ------

			-- insert records from source site to destination table CertificationType
	          SET @sql =  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCertificationType]'
              		+ ' ( '
              		+ '   idSite,
					 title,
					 shortDescription,
					 isDeleted,
					 dtDeleted'
              		+ ' ) '
              		+' SELECT '+ CAST(@idSiteDestination AS NVARCHAR)
              		+' ,  CT.title,
					 CT.shortDescription,
				      CT.isDeleted,
				      CT.dtDeleted					
              		 FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertificationType] CT'
				+' WHERE CT.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
				
              
              EXEC(@sql)
						
				-- temporary table to hold the idCertificationType of source and destination CertificationType tables
				CREATE TABLE #CertificationTypeIdMappings
				(
					oldSourceId INT NOT NULL, 
					newDestinationId INT NOT NULL 	   
				)
				
				-- inserting idCertificationType of source CertificationType table and destination CertificationType table inside temporary table for mapping
				SET @sql =  ' INSERT INTO #CertificationTypeIdMappings'
						+ ' ( '
						+ '  oldSourceId,
							newDestinationId'
						+ ' ) '
						+' SELECT SRC.idCertificationType, DST.idCertificationType'
						+' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCertificationType] DST'
						+' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertificationType] SRC'
						+' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
						+' AND (SRC.title = DST.title)'
						+' AND (SRC.isDeleted = DST.isDeleted)'
						+' AND (SRC.shortDescription = DST.shortDescription OR (SRC.shortDescription IS NULL AND DST.shortDescription IS NULL))'
						+')'
						+' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
			
				EXEC(@sql)


-- ============================================================================================
--LOGIC START -- copy tblCertification object both full copy and partial copy with condition---

	 -- insert records from source site to destination table Certification
	 SET @sql =  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCertification]'
              			+ ' ( '
              			+ '   idSite,
						 idCertificationType, 
						 title,
						 shortDescription,
						 searchTags,
						 dtCreated,
						 dtModified,
						 isDeleted,
						 dtDeleted,
						 initialAwardExpiresInterval,
						 initialAwardExpiresTimeframe,
						 renewalExpiresInterval,
						 renewalExpiresTimeframe,
						 accreditingOrganization,
						 certificationContactName,
						 certificationContactEmail,
						 certificationContactPhoneNumber,
						 isPublished,
						 isClosed,
						 isAnyModule,
						 idCertificationContact'
              			+ ' ) '
              			+' SELECT '+ CAST(@idSiteDestination AS NVARCHAR)
              			+' ,  TTCT.newDestinationId, 
						 C.title,
						 C.shortDescription,
						 C.searchTags,
						 C.dtCreated,
						 C.dtModified,
						 C.isDeleted,
						 C.dtDeleted,
						 C.initialAwardExpiresInterval,
						 C.initialAwardExpiresTimeframe,
						 C.renewalExpiresInterval,
						 C.renewalExpiresTimeframe,
						 C.accreditingOrganization,
						 C.certificationContactName,
						 C.certificationContactEmail,
						 C.certificationContactPhoneNumber,
						 C.isPublished,
						 C.isClosed,
						 C.isAnyModule,'

		IF(@fullCopyFlag = 1) -- idCertificationContact is available for full copy only, because tblUser moved only for full copy.
		  BEGIN
			 SET @subSql =' TTU.newDestinationId '				
              			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertification] C'
					 +' LEFT JOIN #CertificationTypeIdMappings TTCT on (TTCT.oldSourceId = C.idCertificationType)'
					 +' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = C.idCertificationContact)'
					 +' WHERE C.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
					
            END
		ELSE				  -- idCertificationContact is available for full copy only, so in partial copy we send Admin user id "1" .
		  BEGIN				 
			  SET @subSql = ' 1 '	
			               +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertification] C'
					     +' LEFT JOIN #CertificationTypeIdMappings TTCT on (TTCT.oldSourceId = C.idCertificationType)'
					     +' WHERE C.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
		  END
		  
		SET @sql = @sql + @subSql
		EXEC(@sql)		
                       
							
				-- temporary table to hold the idCertification of source and destination Certification tables
				CREATE TABLE #CertificationIdMappings
				(
					oldSourceId INT NOT NULL, 
					newDestinationId INT NOT NULL 	   
				)
				
				-- inserting idCertification of source Certification table and destination Certification table inside temporary table for mapping
				SET @sql =  ' INSERT INTO #CertificationIdMappings'
						+ ' ( '
						+ '  oldSourceId,
							newDestinationId'
						+ ' ) '
						+' SELECT SRC.idCertification, DST.idCertification'
						+' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCertification] DST'
						+' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertification] SRC'
						+' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
						+' AND (SRC.title = DST.title)'
						+' AND (SRC.dtCreated = DST.dtCreated)'
						+')'
						+' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
						
				EXEC(@sql)

				

              	--insert tamplate record into table CertificationLanguage
              	SET @sql =  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCertificationLanguage]'
              			+ ' ( '
              			+ '  idSite, 
              				idCertification, 
              				idLanguage, 
              				title, 
              				shortDescription,
						searchTags'
              			+ ' ) '
              			+' SELECT '+ CAST(@idSiteDestination AS NVARCHAR)
              			+' , TTC.newDestinationId, 
              				CL.idLanguage, 
              				CL.title, 
              				CL.shortDescription,
						CL.searchTags 
              			FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertificationLanguage] CL'
              			+' LEFT JOIN #CertificationIdMappings TTC on (TTC.oldSourceId = CL.idCertification )'
              			+' WHERE CL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
              			
                            
              	
              	EXEC(@sql)
              	              
	   /*    
	   Drop temporary table CertificationTypeIdMappings, temporary table CertificationType will be dorp in both conditions full copy and partial copy
	   */
	   DROP TABLE #CertificationTypeIdMappings

-- =============================================================================
 --LOGIC START -- copy tblCertificationModule object both full copy and partial copy ----------------

		   -- insert records from source site to destination table CertificationModule
		   SET @sql =  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCertificationModule]'
              			+ ' ( '
              			+ '   idSite,
						 idCertification,
						 title,
						 shortDescription,
						 isAnyRequirementSet,
						 dtCreated,
						 dtModified,
						 isDeleted,
						 dtDeleted'
              			+ ' ) '
              			+' SELECT '+ CAST(@idSiteDestination AS NVARCHAR)
              			+' ,  TTC.newDestinationId,
						 CM.title,
						 CM.shortDescription,
						 CM.isAnyRequirementSet,
						 CM.dtCreated,
						 CM.dtModified,
						 CM.isDeleted,
						 CM.dtDeleted					
              			 FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertificationModule] CM'
					 +' LEFT JOIN #CertificationIdMappings TTC on (TTC.oldSourceId = CM.idCertification)'
					+' WHERE CM.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
					
              			
              
              EXEC(@sql)
					
				-- temporary table to hold the idCertificationModule of source and destination CertificationModule tables
				CREATE TABLE #CertificationModuleIdMappings
				(
					oldSourceId INT NOT NULL, 
					newDestinationId INT NOT NULL 	   
				)
				
				-- inserting idCertificationModule of source CertificationModule table and destination CertificationModule table inside temporary table for mapping
				SET @sql =  ' INSERT INTO #CertificationModuleIdMappings'
						+ ' ( '
						+ '  oldSourceId,
							newDestinationId'
						+ ' ) '
						+' SELECT SRC.idCertificationModule, DST.idCertificationModule'
						+' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCertificationModule] DST'
						+' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertificationModule] SRC'
						+' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
						+' AND (SRC.title = DST.title)'
						+' AND (SRC.dtCreated = DST.dtCreated OR (SRC.dtCreated IS NULL AND DST.dtCreated IS NULL))'
						+' AND (SRC.dtModified = DST.dtModified OR (SRC.dtModified IS NULL AND DST.dtModified IS NULL))'
						+')'
						+' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
						
				EXEC(@sql)

				
              	--insert tamplate record into table CertificationModuleLanguage
              	SET @sql =  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCertificationModuleLanguage]'
              			+ ' ( '
              			+ '  idSite, 
              				idCertificationModule, 
              				idLanguage, 
              				title, 
              				shortDescription'
              			+ ' ) '
              			+' SELECT '+ CAST(@idSiteDestination AS NVARCHAR)
              			+' , TTCM.newDestinationId, 
              				CML.idLanguage, 
              				CML.title, 
              				CML.shortDescription
              			FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertificationModuleLanguage] CML'
              			+' LEFT JOIN #CertificationModuleIdMappings TTCM on (TTCM.oldSourceId = CML.idCertificationModule )'
              			+' WHERE CML.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
              			
              	EXEC(@sql)
				
-- ====================================================================================================
 --LOGIC START -- copy tblCertificationModuleRequirementSet object both full copy and partial copy -----

		  -- insert records from source site to destination table CertificationModuleRequirementSet
		  SET @sql =  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCertificationModuleRequirementSet]'
              			+ ' ( '
              			+ '   idSite,
						 idCertificationModule,
						 isAny,
						 label'
              			+ ' ) '
              			+' SELECT '+ CAST(@idSiteDestination AS NVARCHAR)
              			+' ,  TTCM.newDestinationId,
						 CMRS.isAny,
						 CMRS.label					
              			 FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertificationModuleRequirementSet] CMRS'
					 +' LEFT JOIN #CertificationModuleIdMappings TTCM on (TTCM.oldSourceId = CMRS.idCertificationModule)'
					+' WHERE CMRS.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
					
              			
              
              EXEC(@sql)
				
				-- temporary table to hold the idCertificationModuleRequirementSet of source and destination CertificationModuleRequirementSet tables
				CREATE TABLE #CertificationModuleRequirementSetIdMappings
				(
					oldSourceId INT NOT NULL, 
					newDestinationId INT NOT NULL 	   
				)
				
				-- inserting idCertificationModuleRequirementSet of source CertificationModuleRequirementSet table and destination CertificationModuleRequirementSet table inside temporary table for mapping
				SET @sql =  ' INSERT INTO #CertificationModuleRequirementSetIdMappings'
						+ ' ( '
						+ '  oldSourceId,
							newDestinationId'
						+ ' ) '
						+' SELECT SRC.idCertificationModuleRequirementSet, DST.idCertificationModuleRequirementSet'
						+' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCertificationModuleRequirementSet] DST'
						+' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertificationModuleRequirementSet] SRC'
						+' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
						+' AND (SRC.label = DST.label)'
						+' AND (SRC.isAny = DST.isAny)'
						+')'
						+' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
						
				EXEC(@sql)

              	--insert tamplate record into table CertificationModuleRequirementSetLanguage
              	SET @sql =  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCertificationModuleRequirementSetLanguage]'
              			+ ' ( '
              			+ '  idSite, 
              				idCertificationModuleRequirementSet, 
              				idLanguage, 
              				label'
              			+ ' ) '
              			+' SELECT '+ CAST(@idSiteDestination AS NVARCHAR)
              			+' , TTCMRS.newDestinationId, 
              				CMRSL.idLanguage, 
              				CMRSL.label
              			FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertificationModuleRequirementSetLanguage] CMRSL'
              			+' LEFT JOIN #CertificationModuleRequirementSetIdMappings TTCMRS on (TTCMRS.oldSourceId = CMRSL.idCertificationModuleRequirementSet )'
              			+' WHERE CMRSL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
              			
              	
              	EXEC(@sql)
				
	   /*    
	   Drop temporary table CertificationModuleIdMappings, temporary table Course will be dorp in both conditions full copy and partial copy
	   */
	   DROP TABLE #CertificationModuleIdMappings


-- =============================================================================
  --LOGIC START -- copy tblCertificationModuleRequirement object both full copy and partial copy ----------------
   
		 -- insert records from source site to destination table CertificationModuleRequirement
		  SET @sql =  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCertificationModuleRequirement]'
              			+ ' ( '
              			+ '   idSite,
						 idCertificationModuleRequirementSet,
						 label,
						 shortDescription,
						 requirementType,
						 courseCompletionIsAny,
						 forceCourseCompletionInOrder,
						 numberCreditsRequired,
						 documentUploadFileType,
						 documentationApprovalRequired, 
						 allowSupervisorsToApproveDocumentation,
						 allowExpertsToApproveDocumentation,
						 courseCreditEligibleCoursesIsAll '
              			+ ' ) '
              			+' SELECT '+ CAST(@idSiteDestination AS NVARCHAR)
              			+' ,  TTCMRS.newDestinationId,
						 CMR.label,
						 CMR.shortDescription,
						 CMR.requirementType,
						 CMR.courseCompletionIsAny,
						 CMR.forceCourseCompletionInOrder,
						 CMR.numberCreditsRequired,
						 CMR.documentUploadFileType,
						 CMR.documentationApprovalRequired, 
						 CMR.allowSupervisorsToApproveDocumentation,
						 CMR.allowExpertsToApproveDocumentation,
						 CMR.courseCreditEligibleCoursesIsAll '					
              			+' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertificationModuleRequirement] CMR'
					+' LEFT JOIN #CertificationModuleRequirementSetIdMappings TTCMRS on (TTCMRS.oldSourceId = CMR.idCertificationModuleRequirementSet)'
					+' WHERE CMR.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
					
              EXEC(@sql)
							
				-- temporary table to hold the idCertificationModuleRequirement of source and destination CertificationModuleRequirement tables
				CREATE TABLE #CertificationModuleRequirementIdMappings
				(
					oldSourceId INT NOT NULL, 
					newDestinationId INT NOT NULL 	   
				)

				-- inserting idCertificationModuleRequirement of source CertificationModuleRequirement table and destination CertificationModuleRequirement table inside temporary table for mapping
				SET @sql =  ' INSERT INTO #CertificationModuleRequirementIdMappings'
						+ ' ( '
						+ '  oldSourceId,
							newDestinationId'
						+ ' ) '
						+' SELECT SRC.idCertificationModuleRequirement, DST.idCertificationModuleRequirement'
						+' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCertificationModuleRequirement] DST'
						+' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertificationModuleRequirement] SRC'
						+' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
						+' AND (SRC.label = DST.label)'
						+' AND (SRC.shortDescription = DST.shortDescription OR (SRC.shortDescription IS NULL AND DST.shortDescription IS NULL))'
						+' AND (SRC.requirementType = DST.requirementType)'
						+')'
						+' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
						
				EXEC(@sql)

			-- insert records from source site to destination table CertificationModuleRequirementLanguage	
              	SET @sql =  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCertificationModuleRequirementLanguage]'
              			+ ' ( '
              			+ '  idSite, 
              				idCertificationModuleRequirement, 
              				idLanguage, 
              				label, 
              				shortDescription'
              			+ ' ) '
              			+' SELECT '+ CAST(@idSiteDestination AS NVARCHAR)
              			+' , TTCMR.newDestinationId, 
              				CMRL.idLanguage, 
              				CMRL.label, 
              				CMRL.shortDescription
              			FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertificationModuleRequirementLanguage] CMRL'
              			+' LEFT JOIN #CertificationModuleRequirementIdMappings TTCMR on (TTCMR.oldSourceId = CMRL.idCertificationModuleRequirement )'
              			+' WHERE CMRL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
              			
              	EXEC(@sql)

	   /*    
	   Drop temporary table CertificationModuleRequirementSetIdMappings, temporary table Course will be dorp in both conditions full copy and partial copy
	   */
	   DROP TABLE #CertificationModuleRequirementSetIdMappings


-- =============================================================================
  --LOGIC START -- copy tblCertificationModuleRequirementToCourseLink object both full copy and partial copy ----------------

              -- insert records from source site to destination table CertificationModuleRequirementToCourseLink
		    SET @sql =  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCertificationModuleRequirementToCourseLink]'
              			+ ' ( '
              			+ '   idSite,
						 idCertificationModuleRequirement,
						 idCourse,
						 [order]'
              			+ ' ) '
              			+' SELECT '+ CAST(@idSiteDestination AS NVARCHAR)
              			+' ,  TTCMR.newDestinationId,
						 TTC.newDestinationId,
						 CMRTCL.[order]					
              			 FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertificationModuleRequirementToCourseLink] CMRTCL'
					 +' LEFT JOIN #CertificationModuleRequirementIdMappings TTCMR on (TTCMR.oldSourceId = CMRTCL.idCertificationModuleRequirement)'
					 +' LEFT JOIN #CourseIdMappings TTC on (TTC.oldSourceId = CMRTCL.idCourse)'
					+' WHERE CMRTCL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
					
              			
              
              EXEC(@sql)

-- =============================================================================
  --LOGIC START -- copy tblCertificationToCourseCreditLink object both full copy and partial copy ----------------

		  -- insert records from source site to destination table CertificationToCourseCreditLink
		  SET @sql =  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCertificationToCourseCreditLink]'
              			+ ' ( '
              			+ '   idSite,
						 idCertification,
						 idCourse,
						 credits'
              			+ ' ) '
              			+' SELECT '+ CAST(@idSiteDestination AS NVARCHAR)
              			+' ,  TTCM.newDestinationId,
						 TTC.newDestinationId,
						 CTCCL.credits					
              			 FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertificationToCourseCreditLink] CTCCL'
					 +' LEFT JOIN #CertificationIdMappings TTCM on (TTCM.oldSourceId = CTCCL.idCertification)'
					 +' LEFT JOIN #CourseIdMappings TTC on (TTC.oldSourceId = CTCCL.idCourse)'
					+' WHERE CTCCL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
					
              EXEC(@sql)
		
	    
	   /*    
	   Drop temporary table CourseIdMappings, temporary table Course will be dorp in both conditions full copy and partial copy
	   */
	   DROP TABLE #CourseIdMappings

------------------------------------------------------------------------------


	-- check condition for the full copy
	IF @fullCopyFlag = 1
	BEGIN


-- =============================================================================
-- LOGIC START -- copy tblCertificationToUserLink object for full copy ----------------

		  -- insert records from source site to destination table  CertificationToUserLink
			 SET @sql =  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCertificationToUserLink]'
              			+ ' ( '
              			+ '   idSite,
						 idCertification,
						 idUser,
						 initialAwardDate,
						 certificationTrack,
						 currentExpirationDate,
						 currentExpirationInterval,
						 currentExpirationTimeframe,
						 certificationId'
              			+ ' ) '
              			+' SELECT '+ CAST(@idSiteDestination AS NVARCHAR)
              			+' ,  TTC.newDestinationId,
						 TTU.newDestinationId,
						 CTUL.initialAwardDate,
						 CTUL.certificationTrack,
						 CTUL.currentExpirationDate,
						 CTUL.currentExpirationInterval,
						 CTUL.currentExpirationTimeframe,
						 CTUL.certificationId					
              			 FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertificationToUserLink] CTUL'
					 +' LEFT JOIN #CertificationIdMappings TTC on (TTC.oldSourceId = CTUL.idCertification)'
					 +' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = CTUL.idUser)'
					+' WHERE CTUL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
					
              
              EXEC(@sql)


				-- temporary table to hold the idCertificationToUserLink of source and destination CertificationToUserLink tables
				CREATE TABLE #CertificationToUserLinkIdMappings
				(
					oldSourceId INT NOT NULL, 
					newDestinationId INT NOT NULL 	   
				)

				-- inserting idCertificationToUserLink of source CertificationToUserLink table and destination CertificationToUserLink table inside temporary table for mapping
				SET @sql =  ' INSERT INTO #CertificationToUserLinkIdMappings'
						+ ' ( '
						+ '  oldSourceId,
							newDestinationId'
						+ ' ) '
						+' SELECT SRC.idCertificationToUserLink, DST.idCertificationToUserLink'
						+' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCertificationToUserLink] DST'
						+' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertificationToUserLink] SRC'
						+' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
						+' AND (SRC.initialAwardDate = DST.initialAwardDate OR (SRC.initialAwardDate IS NULL AND DST.initialAwardDate IS NULL))'
						+' AND (SRC.currentExpirationDate = DST.currentExpirationDate OR (SRC.currentExpirationDate IS NULL AND DST.currentExpirationDate IS NULL))'
						+' AND (SRC.certificationTrack = DST.certificationTrack)'
						+')'
						+' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
						
				EXEC(@sql)

-- ======================================================================================
-- LOGIC START -- copy [tblData-CertificationModuleRequirement] object for full copy ----

			 -- insert records from source site to destination table Data-CertificationModuleRequirement
			 SET @sql =  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblData-CertificationModuleRequirement]'
              			+ ' ( '
              			+ '   idSite,
						 idCertificationToUserLink,
						 idCertificationModuleRequirement,
						 label,
						 revcode,
						 dtCompleted,
						 completionDocumentationFilePath'
              			+ ' ) '
              			+' SELECT '+ CAST(@idSiteDestination AS NVARCHAR)
              			+' ,  TTCTUL.newDestinationId,
						 TTCMR.newDestinationId,
						 DCMR.label,
						 DCMR.revcode,
						 DCMR.dtCompleted,
						 DCMR.completionDocumentationFilePath					
              			 FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblData-CertificationModuleRequirement] DCMR'
					 +' LEFT JOIN #CertificationToUserLinkIdMappings TTCTUL on (TTCTUL.oldSourceId = DCMR.idCertificationToUserLink)'
					 +' LEFT JOIN #CertificationModuleRequirementIdMappings TTCMR on (TTCMR.oldSourceId = DCMR.idCertificationModuleRequirement)'
					 +' WHERE DCMR.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
					
              
              EXEC(@sql)
			  
	   /*
	   DROP temporary table CertificationToUserLinkIdMappings
	   */
	   DROP TABLE #CertificationToUserLinkIdMappings

-- =============================================================================
-- LOGIC START -- copy tblGroupFeedMessage object for full copy ----------------


	   -- insert records from source site to destination table GroupFeedMessage
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblGroupFeedMessage] '
	   			  +' ( '
	   			  +'   idSite,
	   			  	  idGroup,
	   			  	  idLanguage,
	   			  	  idAuthor,
	   			  	  idParentGroupFeedMessage,
	   			  	  [message] ,
	   			  	  [timestamp],
	   			  	  dtApproved,
	   			  	  idApprover,
	   			  	  IsApproved'
	   			  +'  ) '
	   			  +' SELECT '
				  +	   CAST(@idSiteDestination AS NVARCHAR)
	   			  +' ,  TTG.newDestinationId, '
	   			  +'	   GFM.idLanguage, '
	   			  +'	   TTAuthor.newDestinationId, '
	   			  +'	   NULL, '				 -- this null value of idParentGroupFeedMessage will be updated from temporary GroupFeedMessageIdMappings table below
	   			  +'	   GFM.[message], 
	   			  	   GFM.[timestamp],
	   			  	   GFM.dtApproved,
	   			  	   TTApprover.newDestinationId,
	   			  	   GFM.IsApproved'
	   			  +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblGroupFeedMessage] GFM'
	   			  +' LEFT JOIN #UserIdMappings TTAuthor on (TTAuthor.oldSourceId = GFM.idAuthor )'
	   			  +' LEFT JOIN #UserIdMappings TTApprover on (TTApprover.oldSourceId = GFM.idApprover )'
	   			  +' LEFT JOIN #GroupIdMappings TTG on (TTG.oldSourceId = GFM.idGroup )'
	   			  +' WHERE GFM.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   		
	   EXEC(@sql)
	   
	   -- temporary table to hold the idGroupFeedMessage of source and destination GroupFeedMessage tables
	   CREATE TABLE #GroupFeedMessageIdMappings 
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL  	   
	   )
	   
	   -- inserting idGroupFeedMessage of source GroupFeedMessage table and destination GroupFeedMessage table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #GroupFeedMessageIdMappings'
	   			  +' ( '
	   			  +'	  oldSourceId,
	   			  	  newDestinationId'
	   			  +' ) '
	   			  +' SELECT 
					   SRC.idGroupFeedMessage, 
					   DST.idGroupFeedMessage'
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblGroupFeedMessage] DST'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblGroupFeedMessage] SRC'
	   			  +' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (SRC.[timestamp] = DST.[timestamp])'
	   			  +' AND (SRC.message = DST.message)'
	   			  +')'
	   			  +' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   
	   EXEC(@sql)
	   
	    
	   -- update idParentGroupFeedMessage from Temp Table GroupFeedMessage
	   SET @sql =	   ' UPDATE DSTC'
	   			  +'  SET idParentGroupFeedMessage = TEMP.newDestinationId'
	   			  +' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblGroupFeedMessage] SRCC'
	   			  +' INNER JOIN  [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblGroupFeedMessage] DSTC'
	   			  +' ON(SRCC.[timestamp] = DSTC.[timestamp] AND SRCC.message = DSTC.message AND SRCC.idSite ='+ CAST(@idSiteSource AS NVARCHAR)+ ')'
	   			  +' INNER JOIN  #GroupFeedMessageIdMappings TEMP'
	   			  +' ON(TEMP.oldSourceId = SRCC.idParentGroupFeedMessage)'
	   			  +' WHERE DSTC.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   			  +' AND SRCC.idParentGroupFeedMessage IS NOT NULL'
	   
	   EXEC(@sql)	
	   
	   /*
	   DROP temporary table GroupFeedMessageIdMappings
	   */
	   DROP TABLE #GroupFeedMessageIdMappings

              

-- =============================================================================
-- LOGIC START -- copy tblGroupFeedModerator object for full copy --------------


	   -- insert records from source site to destination table GroupFeedModerator
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblGroupFeedModerator]'
	   			  +' ( '
	   			  +'	  idSite,
	   				  idGroup,
	   				  idModerator'
	   			  +' ) '
	   			  +' SELECT '
				  +	   CAST(@idSiteDestination AS NVARCHAR)
	   			  +' ,  TTG.newDestinationId,
	   			  	   TTU.newDestinationId'
	   			  +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblGroupFeedModerator] GFM'
	   			  +' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = GFM.idModerator )'
	   			  +' LEFT JOIN #GroupIdMappings TTG on (TTG.oldSourceId = GFM.idGroup )'
	   			  +' WHERE GFM.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   	
	   EXEC(@sql)


-- =============================================================================
-- LOGIC START -- copy tblUserToSupervisorLink object for full copy ------------


	   -- insert records from source site to destination table UserToSupervisorLink
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblUserToSupervisorLink]'
	   			  +'  ( '
	   			  +'	   idSite,
	   			  	   idUser,
	   			  	   idSupervisor'
	   			  +' ) '
	   			  +' SELECT '
				  +    CAST(@idSiteDestination AS NVARCHAR)
	   			  +' , TTU.newDestinationId '
	   			  +' , TTS.newDestinationId '
	   			  +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblUserToSupervisorLink] UTSL'
	   			  +' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = UTSL.idUser )'
	   			  +' LEFT JOIN #UserIdMappings TTS on (TTS.oldSourceId = UTSL.idSupervisor )'
	   			  +' WHERE UTSL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			
	   EXEC(@sql)
	   
-- =============================================================================
-- LOGIC START -- copy tblInboxMessage object for full copy --------------------


	   -- insert records from source site to destination table InboxMessage
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblInboxMessage] '
	   			  +' ( '
	   			  +'   idSite,
	   			  	  idParentInboxMessage,
	   			  	  idRecipient,
	   			  	  idSender,
	   			  	  subject,
	   			  	  message,
	   			  	  isDraft,
	   			  	  isSent,
	   			  	  dtSent,
	   			  	  isRead,
	   			  	  dtRead,
	   			  	  isRecipientDeleted,
	   			  	  dtRecipientDeleted,
	   			  	  isSenderDeleted,	
	   			  	  dtSenderDeleted,
	   			  	  dtCreated'
	   			  +'  ) '
	   			  +' SELECT '
				  +	  CAST(@idSiteDestination AS NVARCHAR) 
	   			  +' , NULL ,'			 -- this null value of idParentInboxMessage will be updated from temporary InboxMessageIdMappings table below 
	   			  +'	  TTR.newDestinationId,  
	   			  	  TTS.newDestinationId,
	   			  	  IM.subject,
	   			  	  IM.message,
	   			  	  IM.isDraft,
	   			  	  IM.isSent,
	   			  	  IM.dtSent,
	   			  	  IM.isRead,
	   			  	  IM.dtRead,
	   			  	  IM.isRecipientDeleted,
	   			  	  IM.dtRecipientDeleted,
	   			  	  IM.isSenderDeleted,	
	   			  	  IM.dtSenderDeleted,
	   			  	  IM.dtCreated 
	   			  	FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblInboxMessage] IM'
	   			  +' LEFT JOIN #UserIdMappings TTR on (TTR.oldSourceId = IM.idRecipient )'
	   			  +' LEFT JOIN #UserIdMappings TTS on (TTS.oldSourceId = IM.idSender )'
	   			  +' WHERE IM.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   							
	   EXEC(@sql)

	   -- temporary table to hold the idInboxMessage of source and destination InboxMessage tables
	   CREATE TABLE #InboxMessageIdMappings 
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL  	   
	   )

	   -- inserting idInboxMessage of source InboxMessage table and destination InboxMessage table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #InboxMessageIdMappings'
	   		   	  +' ( '
	   		   	  +'   oldSourceId,
	   		   	  	  newDestinationId'
	   		   	  +' ) '
	   		   	  +' SELECT 
					   SRC.idInboxMessage, 
					   DST.idInboxMessage '
	   		   	  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblInboxMessage] DST'
	   		   	  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblInboxMessage] SRC'
	   		   	  +' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   		   	  +' AND (SRC.message = DST.message)'
	   		   	  +' AND (SRC.dtCreated = DST.dtCreated)'
	   		   	  +' AND (SRC.dtSent = DST.dtSent OR (SRC.dtSent IS NULL AND DST.dtSent IS NULL))'
	   		   	  +' )'
	   		   	  +' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   
	   EXEC(@sql)
	   
			
	   -- update idParentInboxMessage from Temp Table InboxMessage
	   SET @sql =	   ' UPDATE DSTIM'
	   		   	  +'  SET idParentInboxMessage = TEMP.newDestinationId'
	   		   	  +' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblInboxMessage] SRCIM'
	   		   	  +' INNER JOIN  [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblInboxMessage] DSTIM'
	   		   	  +' ON(SRCIM.dtCreated = DSTIM.dtCreated AND SRCIM.[message] = DSTIM.[message] AND SRCIM.idSite ='+ CAST(@idSiteSource AS NVARCHAR)+ ')'
	   		   	  +' INNER JOIN  #InboxMessageIdMappings TEMP'
	   		   	  +' ON(TEMP.oldSourceId = SRCIM.idParentInboxMessage)'
	   		   	  +' WHERE DSTIM.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   		   	  +' AND SRCIM.idParentInboxMessage IS NOT NULL'
	   
	   EXEC(@sql)	
	   

	   /*
	   Drop temporary table InboxMessageIdMappings
	   */
	   DROP TABLE #InboxMessageIdMappings
	   


-- =============================================================================
-- LOGIC START -- copy tblReport object for full copy --------------------------


	   -- insert records from source site to destination table Report
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblReport]'
	   			 +' ( '
	   			 +'   idSite,'  
	   			 +'	 idUser,'
	   			 +'	 idDataset,'
	   			 +'	 title,'
	   			 +'	 fields,'
	   			 +'	 filter,'
	   			 +'	 [order],'
	   			 +'	 isPublic,'
	   			 +'	 dtCreated,'
	   			 +'	 dtModified,'
	   			 +'	 isDeleted,'
	   			 +'	 dtDeleted '
	   			 +'  ) '
	   			 +' SELECT '
	   			 +     CAST(@idSiteDestination AS NVARCHAR)
	   			 +',   TTU.newDestinationId,'
	   			 +'	  R.idDataset,'
	   			 +'	  R.title,'
	   			 +'	  R.fields,'
	   			 +'	  R.filter,'
	   			 +'	  R.[order],'
	   			 +'	  R.isPublic,'
	   			 +'	  R.dtCreated,'
	   			 +'	  R.dtModified,'
	   			 +'	  R.isDeleted,'
	   			 +'	  R.dtDeleted '
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblReport] R'
	   			 +' INNER JOIN #UserIdMappings TTU on (TTU.oldSourceId = R.idUser ) '
	   			 +' WHERE R.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   	
	   EXEC(@sql)
	   		
	   -- temporary table to hold the idReport of source and destination Report tables
	   CREATE TABLE #ReportIdMappings
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL  	   
	   )	
	   
	   -- inserting idReport of source Report table and destination Report table inside temporary table for mapping		
	   SET @sql =	   ' INSERT INTO #ReportIdMappings '
	   			  +' ( '
	   			  +'	  oldSourceId,
	   			  	  newDestinationId'
	   			  +' ) '
	   			  +' SELECT 
					   SR.idReport, 
					   DR.idReport '
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblReport] DR'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblReport] SR'
	   			  +' ON (SR.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   		       +' AND (SR.dtCreated = DR.dtCreated OR (SR.dtCreated IS NULL AND DR.dtCreated IS NULL))'
	   			  +' AND (SR.dtModified = DR.dtModified OR (SR.dtModified IS NULL AND DR.dtModified IS NULL))'					
	   			  +' AND (SR.dtDeleted = DR.dtDeleted OR (SR.dtDeleted IS NULL AND DR.dtDeleted IS NULL))' 
	   			  +' )'
	   			  +' WHERE DR.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   				
	   EXEC(@sql)	 
	     	
	   -- reportLanguage data move logic start

	   -- insert records from source site to destination table ReportLanguage
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblReportLanguage]'
	   			  +' ( '
	   			  +'	  idSite,' 
	   			  +'	  idReport,'
	   			  +'	  idLanguage,'
	   			  +'	  title '
	   			  +' )  '
	   			  +' SELECT '
	   			  +	  CAST(@idSiteDestination AS NVARCHAR)
	   			  +',  TTR.newDestinationId,'
	   			  +'	  RL.idLanguage,'
	   			  +'	  RL.title '
	   			  +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblReportLanguage] RL'
	   			  +' LEFT JOIN #ReportIdMappings TTR on (TTR.oldSourceId = RL.idReport )'
	   			  +' WHERE RL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   EXEC(@sql)
	   
	   -- reportLanguage data move logic end



-- =============================================================================
-- LOGIC START -- copy tblReportFile object for full copy ----------------------

		
	   -- insert records from source site to destination table Report file
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblReportFile]'
	   			 +' ( '
	   			 +'   idSite,'                 
	   			 +'	 idReport,'
	   			 +'	 idUser,'
	   			 +'	 [filename],'
	   			 +'	 dtCreated,'
	   			 +'	 htmlKb,'
	   			 +'	 csvKb,'
	   			 +'	 pdfKb '
	   			 +'  ) '
	   			 +' SELECT '
	   			 +     CAST(@idSiteDestination AS NVARCHAR)
	   			 +',   TTR.newDestinationId,'
	   			 +'    TTU.newDestinationId,'
	   			 +'	  RF.[filename],'
	   			 +'	  RF.dtCreated,'
	   			 +'	  RF.htmlKb,'
	   			 +'	  RF.csvKb,'
	   			 +'	  RF.pdfKb '	
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblReportFile] RF'
	   			 +' INNER JOIN #ReportIdMappings TTR on (TTR.oldSourceId = RF.idReport ) '
	   			 +' INNER JOIN #UserIdMappings TTU on (TTU.oldSourceId = RF.idUser ) '
	   			 +' WHERE RF.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   EXEC(@sql)
	   


-- =============================================================================
-- LOGIC START -- copy tblReportSubscription object for full copy --------------

	
	   -- insert records from source site to destination table ReportSubscription
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblReportSubscription]'
	   			 +' ( '
	   			 +'   idSite,'    
	   			 +'	 idUser,'
	   			 +'	 idReport,'
	   			 +'	 dtStart,'
	   			 +'	 dtNextAction,'
	   			 +'	 recurInterval,'
	   			 +'	 recurTimeframe,'
	   			 +'	 token '
	   			 +'  ) '
	   			 +' SELECT '
	   			 +     CAST(@idSiteDestination AS NVARCHAR)
	   			 +' ,  TTU.newDestinationId,'
	   			 +'    TTR.newDestinationId,'
	   			 +'	  RS.dtStart,'
	   			 +'	  RS.dtNextAction,'
	   			 +'	  RS.recurInterval,'
	   			 +'	  RS.recurTimeframe,'
	   			 +'	  RS.token '	
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblReportSubscription] RS'
	   			 +' INNER JOIN #ReportIdMappings TTR on (TTR.oldSourceId = RS.idReport ) '
	   			 +' INNER JOIN #UserIdMappings TTU on (TTU.oldSourceId = RS.idUser ) '
	   			 +' WHERE RS.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   EXEC(@sql)



-- =================================================================================
-- LOGIC START -- copy tblReportToReportShortcutsWidgetLink object for full copy ---

		
	   -- insert records from source site to destination table ReportToReportShortcutsWidgetLink
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblReportToReportShortcutsWidgetLink]'
	   			 +' ( '
	   			 +'   idSite,'                
	   			 +'	 idReport,'
	   			 +'	 idUser '
	   			 +'  ) '
	   			 +' SELECT '
	   			 +     CAST(@idSiteDestination AS NVARCHAR)
	   			 +',   TTR.newDestinationId,'
	   			 +'    TTU.newDestinationId '
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblReportToReportShortcutsWidgetLink] RRSWL'
	   			 +' INNER JOIN #ReportIdMappings TTR on (TTR.oldSourceId = RRSWL.idReport ) '
	   			 +' INNER JOIN #UserIdMappings TTU on (TTU.oldSourceId = RRSWL.idUser ) '
	   			 +' WHERE RRSWL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   EXEC(@sql)
	   


-- =============================================================================
-- LOGIC START -- copy tblSystem object for full copy --------------------------
	   --insert records from source site to destination table tblSystem
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblSystem]'
	   			 +' ( '
	   			 +'    lastEventEmailQueueCascade '
	   			 +' ) '
	   			 
	   			 +' SELECT '
	   			 +'    lastEventEmailQueueCascade '
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblSystem] '
	   		
	   --EXEC(@sql)
	   
-- =============================================================================

	   /*
	   Drop temporary table ReportIdMappings
	   */
	   DROP TABLE #ReportIdMappings
	   
	   /*
	   Drop temporary table UserIdMappings
	   */
	   DROP TABLE #UserIdMappings

    END

------------------------------------------------------------------------------
  -- full copy end

  	   /*
	   Drop temporary table GroupIdMappings, temporary table will be dorp in both conditions full copy and partial copy
	   */
	   DROP TABLE #GroupIdMappings

	   /*    
	   Drop temporary table CertificationModuleRequirementIdMappings, temporary table will be dorp in both conditions full copy and partial copy
	   */
	   DROP TABLE #CertificationModuleRequirementIdMappings

	   /*    
	   Drop temporary table CourseIdMappings, temporary table Course will be dorp in both conditions full copy and partial copy
	   */
	   DROP TABLE #CertificationIdMappings


	   -- insert new idSite into the objectMapping that will be used on moving file object.
	   INSERT INTO #ObjectMapping
	   ([oldId],[newId],objectName)
	   SELECT @idSiteSource, @idSiteDestination, 'site'

	  
		 /* LOGIC END */

			 SET @Return_Code = 0
			 SET @Error_Description_Code	= 'SystemClonePortal_InsertedSuccessfully'
			 SELECT * FROM #ObjectMapping
			 DROP TABLE #ObjectMapping 

	    COMMIT TRANSACTION
	   
	   --TRANSACTION END

	END TRY
		--END TRY

	BEGIN CATCH
		
			
		SET @Return_Code = 1
		SET @Error_Description_Code	= 'SystemClonePortal_PortalMigrationFailed_RollbackOccured'
		
		
		IF @@TRANCOUNT > 0
			BEGIN 				
				ROLLBACK TRANSACTION
			END	     
			--INSERT INTO Clone_Portal_Error_Log ([Timestamp], ErrorMessage) VALUES (GETUTCDATE(), ERROR_MESSAGE())
	END CATCH
		
	SET XACT_ABORT OFF		
END 
--main begin end

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.ClonePortal.CatalogCourse]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.ClonePortal.CatalogCourse]
GO


/*

CLONE PORTAL - CATALOG / COURSE / LESSON / LEARNING PATH DATA
OCCURS FOR FULL AND PARTIAL COPY

*/
CREATE PROCEDURE [System.ClonePortal.CatalogCourse]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,	

	@idSiteSource			INT,
	@idSiteDestination		INT,
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50)
)
AS

BEGIN

    SET NOCOUNT ON
    SET XACT_ABORT ON

	INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2.0: Copy CatalogCourse - Initialized', 0)
    
    BEGIN TRY	
    
		DECLARE @sql NVARCHAR(MAX)

		/*
	  
		replace double slash with single slash. WHY DO WE DO THIS?!? Need to find out!
	 
		*/

		SET @sourceDBServer = REPLACE(@sourceDBServer, '\\', '\')
		SET @destinationDBServer = REPLACE(@destinationDBServer, '\\', '\')
		
		/*
		
		CATALOGS
		
		*/	

		-- copy catalogs
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCatalog] '
				 + '( '
				 + '	idSite, '
				 + '	idParent, '
				 + '	title, '
				 + '	[order], '
				 + '	shortDescription, '
				 + '	longDescription, '
				 + '	isPrivate, '
				 + '	isClosed, '
				 + '	dtCreated, '
				 + '	dtModified, '
				 + '	cost, '
				 + '	costType, '
				 + '	searchTags, '
				 + '	avatar '
				 + ') '
				 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	NULL, '     -- null value of idParent will be updated from CatalogIdMappings table below
				 + '	C.title, '
				 + '	C.[order], '
				 + '	C.shortDescription, '
				 + '	C.longDescription, '
				 + '	C.isPrivate, '
				 + '	C.isClosed, '
				 + '	C.dtCreated, '
				 + '	C.dtModified, '
				 + '	C.cost, '
				 + '	C.costType, '
				 + '	C.searchTags, '
				 + '	C.avatar '
				 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCatalog] C '
				 + 'WHERE C.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
						
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2.1: Copy CatalogCourse - Catalogs Inserted', 0)
	   
		-- insert the mapping for source to destination catalog ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
				 + '( '
				 + '	sourceId, '
				 + '	destinationId, '
				 + '	[object] '
				 + ') '
				 + 'SELECT '
				 + '	SRC.idCatalog, '
				 + '	DST.idCatalog, '
				 + '	''catalog'' '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCatalog] DST '
				 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCatalog] SRC '
				 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
				 + '	AND (SRC.title = DST.title) '
				 + '	AND (SRC.dtCreated = DST.dtCreated OR (SRC.dtCreated IS NULL AND DST.dtCreated IS NULL)) '
				 + '	AND (SRC.dtModified = DST.dtModified OR (SRC.dtModified IS NULL AND DST.dtModified IS NULL)) '
				 + ') '
				 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)

		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2.2: Copy CatalogCourse - Catalog Mappings Created', 0)
	   
		-- update catalog parent ids from id mappings table
		SET @sql = 'UPDATE DSTC '
	   			 + '	SET idParent = TEMP.destinationId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCatalog] SRCC '
	   			 + 'INNER JOIN  [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCatalog] DSTC '
	   			 + 'ON (SRCC.title = DSTC.title '
	   			 + '	AND (SRCC.dtCreated = DSTC.dtCreated OR (SRCC.dtCreated IS NULL AND DSTC.dtCreated IS NULL)) '
	   			 + '	AND (SRCC.dtModified = DSTC.dtModified OR (SRCC.dtModified IS NULL AND DSTC.dtModified IS NULL)) '
	   			 + '	AND SRCC.idSite = ' + CAST(@idSiteSource AS NVARCHAR)+ ') '
	   			 + 'INNER JOIN  [dbo].[ClonePortal_IDMappings] TEMP '
	   			 + 'ON (TEMP.sourceId = SRCC.idParent AND TEMP.object = ''catalog'') '
	   			 + 'WHERE DSTC.idSite = ' + CAST(@idSiteDestination AS NVARCHAR) + ' '
	   			 + 'AND SRCC.idParent IS NOT NULL '
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2.3: Copy CatalogCourse - Catalog Parent Ids Updated', 0)	   
	   	   
		-- copy catalog languages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCatalogLanguage] '
				 + '( '
				 + '	idSite, '
				 + '	idCatalog, '
				 + '	idLanguage, '
				 + '	title, '
				 + '	shortDescription, '
				 + '	longDescription, '
				 + '	searchTags '
				 + ') '
				 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	TTC.destinationId, '
				 + '	CL.idLanguage, '
				 + '	CL.title, '
				 + '	CL.shortDescription, '
				 + '	CL.longDescription, '
				 + '	CL.searchTags  '
				 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCatalogLanguage] CL '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = CL.idCatalog AND TTC.object = ''catalog'') '
				 + 'WHERE CL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
								
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2.4: Copy CatalogCourse - Catalog Languages Inserted', 0)

		/*

		COURSES

		*/

		-- copy courses
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCourse] '
				 + '( '
				 + '	idSite, '
				 + '	title, '
				 + '	coursecode, '
				 + '	revcode, '
				 + '	avatar, '
				 + '	cost, '
				 + '	credits, '
				 + '	[minutes], '
				 + '	shortDescription, '
				 + '	longDescription, '
				 + '	objectives, '
				 + '	socialMedia, '
				 + '	isPublished, '
				 + '	isClosed, '
				 + '	isLocked, '
				 + '	isPrerequisiteAny, '
				 + '	isFeedActive, '
				 + '	isFeedModerated, '
				 + '	isFeedOpenSubscription, '
				 + '	dtCreated, '
				 + '	dtModified, '
				 + '	isDeleted, '
				 + '	dtDeleted, '
				 + '	[order], '
				 + '	disallowRating, '
				 + '	forceLessonCompletionInOrder, '
				 + '	selfEnrollmentDueInterval, '
				 + '	selfEnrollmentDueTimeframe, '
				 + '	selfEnrollmentExpiresFromStartInterval, '
				 + '	selfEnrollmentExpiresFromStartTimeframe, '
				 + '	selfEnrollmentExpiresFromFirstLaunchInterval, '
				 + '	selfEnrollmentExpiresFromFirstLaunchTimeframe, '
				 + '	searchTags, '
				 + '	requireFirstLessonToBeCompletedBeforeOthers, '
				 + '	lockLastLessonUntilOthersCompleted, '
				 + '	isSelfEnrollmentApprovalRequired, '
				 + '	isApprovalAllowedByCourseExperts, '
				 + '	isApprovalAllowedByUserSupervisor, '
				 + '	shortcode '
				 + ') '
				 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	C.title  + '' ##'' + CAST(C.idCourse AS NVARCHAR) + ''##'', '
				 + '	C.coursecode, '
				 + '	C.revcode, '
				 + '	C.avatar, '
				 + '	C.cost, '
				 + '	C.credits, '
				 + '	C.[minutes], '
				 + '	C.shortDescription, '
				 + '	C.longDescription, '
				 + '	C.objectives, '
				 + '	C.socialMedia, '
				 + '	C.isPublished, '
				 + '	C.isClosed, '
				 + '	C.isLocked, '
				 + '	C.isPrerequisiteAny, '
				 + '	C.isFeedActive, '
				 + '	C.isFeedModerated, '
				 + '	C.isFeedOpenSubscription, '
				 + '	C.dtCreated, '
				 + '	C.dtModified, '
				 + '	C.isDeleted, '
				 + '	C.dtDeleted, '
				 + '	C.[order], '
				 + '	C.disallowRating, '
				 + '	C.forceLessonCompletionInOrder, '
				 + '	C.selfEnrollmentDueInterval, '
				 + '	C.selfEnrollmentDueTimeframe, '
				 + '	C.selfEnrollmentExpiresFromStartInterval, '
				 + '	C.selfEnrollmentExpiresFromStartTimeframe, '
				 + '	C.selfEnrollmentExpiresFromFirstLaunchInterval, '
				 + '	C.selfEnrollmentExpiresFromFirstLaunchTimeframe, '
				 + '	C.searchTags, '
				 + '	C.requireFirstLessonToBeCompletedBeforeOthers, '
				 + '	C.lockLastLessonUntilOthersCompleted, '
				 + '	C.isSelfEnrollmentApprovalRequired, '
				 + '	C.isApprovalAllowedByCourseExperts, '
				 + '	C.isApprovalAllowedByUserSupervisor, '
				 + '	C.shortcode '
				 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCourse] C '
				 + 'WHERE C.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
							          										
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2.5: Copy CatalogCourse - Courses Inserted', 0)
	   
		-- insert the mapping for source to destination course id
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	SRC.idCourse, '
	   			 + '	DST.idCourse, '
	   			 + '	''courses'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCourse] DST '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCourse] SRC '
	   			 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
				 + '	AND SRC.title + '' ##'' + CAST(SRC.idCourse AS NVARCHAR) + ''##'' = DST.title) '
	   			 + 'WHERE DST.idSite =' + CAST(@idSiteDestination AS NVARCHAR)
				 		
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2.6: Copy CatalogCourse - Course Mappings Created', 0)

		-- clean up the ##idCourse## additions we made to course titles, we did that to uniquely distinguish courses
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblCourse] SET '
				 + '	title = REPLACE(DC.title, '' ##'' + CAST(SC.sourceID AS NVARCHAR) + ''##'', '''') '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblCourse] DC '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] SC ON SC.destinationID = DC.idCourse AND SC.object = ''courses'' '
				 + 'WHERE DC.idSite = ' + CAST(@idSiteDestination AS NVARCHAR) + ' AND SC.sourceID IS NOT NULL'

		EXEC (@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2.7: Copy CatalogCourse - Course Titles Cleaned Up', 0)
						
		-- copy course languages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCourseLanguage] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idCourse, '
	   			 + '	idLanguage, '
	   			 + '	title, '
	   			 + '	shortDescription, '
	   			 + '	longDescription, '
	   			 + '	objectives, '
	   			 + '	searchTags '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTC.destinationId, '
	   			 + '	CL.idLanguage, '
	   			 + '	CL.title, '
	   			 + '	CL.shortDescription, '
	   			 + '	CL.longDescription, '
	   			 + '	CL.objectives, '
	   			 + '	CL.searchTags '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCourseLanguage] CL '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = CL.idCourse AND TTC.object = ''courses'') '
	   			 + 'WHERE CL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 				
	   			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2.8: Copy CatalogCourse - Course Languages Inserted', 0)

		-- copy course to catalog links
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCourseToCatalogLink] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idCatalog, '
	   			 + '	idCourse, '
	   			 + '	[order] '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTCatalog.destinationId, '
	   			 + '	TTCourse.destinationId, '
	   			 + '	CTCL.[order] '
	   			 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCourseToCatalogLink] CTCL '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCatalog on (TTCatalog.sourceId = CTCL.idCatalog AND TTCatalog.object = ''catalog'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCourse on (TTCourse.sourceId = CTCL.idCourse AND TTCourse.object = ''courses'') '
	   			 + 'WHERE CTCL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2.9: Copy CatalogCourse - Course To Catalog Links Inserted', 0)

		-- copy course to prerequisite links
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCourseToPrerequisiteLink] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idCourse, '
	   			 + '	idPrerequisite '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTCourse.destinationId, '
	   			 + '	TTPrerequisite.destinationId '
	   			 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCourseToPrerequisiteLink] CTPL '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCourse on (TTCourse.sourceId = CTPL.idCourse AND TTCourse.object = ''courses'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTPrerequisite on (TTPrerequisite.sourceId = CTPL.idPrerequisite AND TTPrerequisite.object = ''courses'') '
	   			 + 'WHERE CTPL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
				
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2.10: Copy CatalogCourse - Course To Prerequisite Links Inserted', 0)

		-- copy course to screenshot links
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCourseToScreenshotLink] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idCourse, '
	   			 + '	[filename] '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTCourse.destinationId, '
	   			 + '	CTSL.[filename] '
	   			 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCourseToScreenshotLink] CTSL '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCourse on (TTCourse.sourceId = CTSL.idCourse AND TTCourse.object = ''courses'') '
	   			 + 'WHERE CTSL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2.11: Copy CatalogCourse - Course To Screenshot Links Inserted', 0)

		/*

		MODULES (LESSONS)

		*/

		-- copy modules (lessons)
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblLesson] '
				 + '( '
				 + '	idSite, '
				 + '	idCourse, '
				 + '	title, '
				 + '	revcode, '
				 + '	shortDescription, '
				 + '	longDescription, '
				 + '	dtCreated, '
				 + '	dtModified, '
				 + '	isDeleted, '
				 + '	dtDeleted, '
				 + '	[order], '
				 + '	searchTags, '
				 + '	isOptional '
				 + ') '
				 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	TTC.destinationId, '
				 + '	L.title  + '' ##'' + CAST(L.idLesson AS NVARCHAR) + ''##'', '
				 + '	L.revcode, '
				 + '	L.shortDescription, '
				 + '	L.longDescription, '
				 + '	L.dtCreated, '
				 + '	L.dtModified, '
				 + '	L.isDeleted, '
				 + '	L.dtDeleted, '
				 + '	L.[order], '
				 + '	L.searchTags, '
				 + '	L.isOptional '
				 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblLesson] L '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = L.idCourse AND TTC.object = ''courses'') '
				 + 'WHERE L.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
		
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2.12: Copy CatalogCourse - Modules Inserted', 0)
	   
		-- insert the mapping for source to destination course id
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
				 + '( '
				 + '	sourceId, '
				 + '	destinationId, '
				 + '	[object] '
				 + ') '
				 + 'SELECT '
				 + '	SRC.idLesson, '
				 + '	DST.idLesson, '
				 + '	''lesson'' '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblLesson] DST '
				 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblLesson] SRC '
				 + 'ON (SRC.idSite =' + CAST(@idSiteSource AS NVARCHAR)
				 + '	AND SRC.title + '' ##'' + CAST(SRC.idLesson AS NVARCHAR) + ''##'' = DST.title) '
				 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
		
	
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2.13: Copy CatalogCourse - Module Mappings Created', 0)

		-- clean up the ##idLesson## additions we made to lesson titles, we did that to uniquely distinguish lessons
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblLesson] SET '
				 + '	title = REPLACE(DL.title, '' ##'' + CAST(SL.sourceID AS NVARCHAR) + ''##'', '''') '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblLesson] DL '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] SL ON SL.destinationID = DL.idLesson AND SL.object = ''lesson'' '
				 + 'WHERE SL.sourceID IS NOT NULL '

		EXEC (@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2.14: Copy CatalogCourse - Module Titles Cleaned Up', 0)

		-- copy lesson languages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblLessonLanguage] '
				+ '( '
				+ '	idSite, '
				+ '	idLesson, '
				+ '	idLanguage, '
				+ '	title, '
				+ '	shortDescription, '
				+ '	longDescription, '
				+ '	searchTags '
				+ ') '
				+ 'SELECT '
				+	CAST(@idSiteDestination AS NVARCHAR) + ', '
				+ '	TTL.destinationId, '
				+ '	LL.idLanguage, '
				+ '	LL.title, '
				+ '	LL.shortDescription, '
				+ '	LL.longDescription, '
				+ '	LL.searchTags '
				+ 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblLessonLanguage] LL '
				+ 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTL on (TTL.sourceId = LL.idLesson AND TTL.object = ''lesson'') '
				+ 'WHERE LL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2.15: Copy CatalogCourse - Module Languages Inserted', 0)
	                
		/*

		LEARNING PATHS

		*/

		-- copy learning paths
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblLearningPath] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	name, '
	   			 + '	shortDescription, '
	   			 + '	longDescription, '
	   			 + '	cost, '
	   			 + '	isPublished, '
	   			 + '	isClosed, '
	   			 + '	forceCompletionInOrder, '
	   			 + '	dtCreated, '
	   			 + '	dtModified, '
	   			 + '	isDeleted, '
	   			 + '	dtDeleted, '
	   			 + '	avatar, '
	   			 + '	searchTags, '
				 + '	shortcode '
	   			 + ') '
	   			 + 'SELECT '
	   			 +	CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	LP.name  + '' ##'' + CAST(LP.idLearningPath AS NVARCHAR) + ''##'', '
	   			 + '	LP.shortDescription, '
	   			 + '	LP.longDescription, '
	   			 + '	LP.cost, '
	   			 + '	LP.isPublished, '
	   			 + '	LP.isClosed, '
	   			 + '	LP.forceCompletionInOrder, '
	   			 + '	LP.dtCreated, '
	   			 + '	LP.dtModified, '
	   			 + '	LP.isDeleted, '
	   			 + '	LP.dtDeleted, '
	   			 + '	LP.avatar, '
	   			 + '	LP.searchTags, '
				 + '	LP.shortcode '
	   			 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblLearningPath] LP '
	   			 + 'WHERE LP.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   	
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2.16: Copy CatalogCourse - Learning Paths Inserted', 0)

		-- insert the mapping for source to destination learning path id
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	SLP.idLearningPath, '
	   			 + '	DLP.idLearningPath, '
	   			 + '	''learningPaths'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblLearningPath] DLP '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblLearningPath] SLP '
	   			 + 'ON (SLP.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
				 + '	AND SLP.name + '' ##'' + CAST(SLP.idLearningPath AS NVARCHAR) + ''##'' = DLP.name) '
	   			 + 'WHERE DLP.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
	   			
		EXEC(@sql)	 

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2.17: Copy CatalogCourse - Learning Path Mappings Created', 0)

		-- clean up the ##idLearningPath## additions we made to learning path names, we did that to uniquely distinguish lessons
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblLearningPath] SET '
				 + '	name = REPLACE(DLP.name, '' ##'' + CAST(SLP.sourceID AS NVARCHAR) + ''##'', '''') '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblLearningPath] DLP '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] SLP ON SLP.destinationID = DLP.idLearningPath AND SLP.object = ''learningPaths'' '
				 + 'WHERE SLP.sourceID IS NOT NULL '

		EXEC (@sql)	   

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2.18: Copy CatalogCourse - Learning Path Titles Cleaned Up', 0)
			   
		-- copy learning path languages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblLearningPathLanguage] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idLearningPath, '
	   			 + '	idLanguage, '
	   			 + '	name, '
	   			 + '	shortDescription, '
	   			 + '	longDescription, '
	   			 + '	searchTags '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTLP.destinationId, '
	   			 + '	LPL.idLanguage, '
	   			 + '	LPL.name, '
	   			 + '	LPL.shortDescription, '
	   			 + '	LPL.longDescription, '
	   			 + '	LPL.searchTags '
				 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblLearningPathLanguage] LPL '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTLP on (TTLP.sourceId = LPL.idLearningPath AND TTLP.object = ''learningPaths'') '
	   			 + 'WHERE LPL.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
	   	
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2.19: Copy CatalogCourse - Learning Path Languages Inserted', 0)

		-- copy learning path to course links
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblLearningPathToCourseLink] '
	   			+ '( '
	   			+ '	idSite, '
	   			+ '	idLearningPath, '
	   			+ '	idCourse, '
	   			+ '	[order] '
	   			+ ') '
	   			+ 'SELECT '
	   			+	CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			+ '	TTLP.destinationId, '
	   			+ '	TTC.destinationId, '
	   			+ '	LPCL.[order] '
	   			+ 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblLearningPathToCourseLink] LPCL '
	   			+ 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTLP on (TTLP.sourceId = LPCL.idLearningPath AND TTLP.object = ''learningPaths'') '
	   			+ 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = LPCL.idCourse AND TTC.object = ''courses'') '
	   			+ 'WHERE LPCL.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2.20: Copy CatalogCourse - Learning Path Course Links Inserted', 0)

		-- copy rule set learning path enrollments
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblRuleSetLearningPathEnrollment] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idLearningPath, '
	   			 + '	idTimezone, '
	   			 + '	priority, '
	   			 + '	label, '
	   			 + '	dtStart, '
	   			 + '	dtEnd, '
	   			 + '	delayInterval, '
	   			 + '	delayTimeframe, '
	   			 + '	dueInterval, '
	   			 + '	dueTimeframe, '
				 + '	expiresFromStartInterval, '
				 + '	expiresFromStartTimeframe, '
				 + '	expiresFromFirstLaunchInterval, '
				 + '	expiresFromFirstLaunchTimeframe '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTLP.destinationId, '
	   			 + '	RSLPE.idTimezone, '
	   			 + '	RSLPE.priority, '
				 + '	RSLPE.label  + '' ##'' + CAST(RSLPE.idRuleSetLearningPathEnrollment AS NVARCHAR) + ''##'', '
	   			 + '	RSLPE.dtStart, '
	   			 + '	RSLPE.dtEnd, '
	   			 + '	RSLPE.delayInterval, '
	   			 + '	RSLPE.delayTimeframe, '
	   			 + '	RSLPE.dueInterval, '
	   			 + '	RSLPE.dueTimeframe, '
				 + '	RSLPE.expiresFromStartInterval, '
				 + '	RSLPE.expiresFromStartTimeframe, '
				 + '	RSLPE.expiresFromFirstLaunchInterval, '
				 + '	RSLPE.expiresFromFirstLaunchTimeframe '
	   			 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblRuleSetLearningPathEnrollment] RSLPE '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTLP on (TTLP.sourceId = RSLPE.idLearningPath AND TTLP.object = ''learningPaths'') '
	   			 + 'WHERE RSLPE.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 	
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2.21: Copy CatalogCourse - RuleSet Learning Path Enrollments Inserted', 0)

		-- insert the mapping for source to destination rule set learning path enrollment id
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	SCC.idRuleSetLearningPathEnrollment, '
	   			 + '	DCC.idRuleSetLearningPathEnrollment, '
	   			 + '	''rulesetlearningpathenrollment'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblRuleSetLearningPathEnrollment] DCC '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblRuleSetLearningPathEnrollment] SCC '
	   			 + 'ON (SCC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
				 + '	AND SCC.label + '' ##'' + CAST(SCC.idRuleSetLearningPathEnrollment AS NVARCHAR) + ''##'' = DCC.label) '
	   			 + 'WHERE DCC.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)		
	   
		EXEC(@sql)	

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2.22: Copy CatalogCourse - RuleSet Learning Path Enrollment Mappings Created', 0)

		-- clean up the ##idRuleSetLearningPathEnrollment## additions we made to learning path names, we did that to uniquely distinguish lessons
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRuleSetLearningPathEnrollment] SET '
				 + '	label = REPLACE(DCC.label, '' ##'' + CAST(SCC.sourceID AS NVARCHAR) + ''##'', '''') '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRuleSetLearningPathEnrollment] DCC '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] SCC ON SCC.destinationID = DCC.idRuleSetLearningPathEnrollment AND SCC.object = ''rulesetlearningpathenrollment'' '
				 + 'WHERE SCC.sourceID IS NOT NULL '

		EXEC (@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2.23: Copy CatalogCourse - Ruleset Learning Path Enrollment Titles Cleaned Up', 0)
	   
		-- copy ruleset learning path enrollment languages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblRuleSetLearningPathEnrollmentLanguage] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idRuleSetLearningPathEnrollment, '
	   			 + '	idLanguage, '
	   			 + '	label '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTRSLPE.destinationId, '
	   			 + '	RSLPEL.idLanguage, '
	   			 + '	RSLPEL.label '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblRuleSetLearningPathEnrollmentLanguage] RSLPEL '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTRSLPE on (TTRSLPE.sourceId = RSLPEL.idRuleSetLearningPathEnrollment AND TTRSLPE.object = ''rulesetlearningpathenrollment'') '
	   			 + 'WHERE RSLPEL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   	
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2.24: Copy CatalogCourse - RuleSet Learning Path Enrollment Languages Inserted', 0)

		/*

		RETURN

		*/

		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemClonePortal_CatalogCourse_InsertedSuccessfully'

	END TRY

	/*

	CATCH AND LOG ERRORS

	*/

	BEGIN CATCH
			
		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2: Copy CatalogCourse - ERROR - ' + CAST(ERROR_LINE() AS NVARCHAR) + ' | ' + CAST(ERROR_MESSAGE() AS NVARCHAR(MAX)), 1)
		SET @Return_Code = 1
		SET @Error_Description_Code	= 'SystemClonePortal_CatalogCourse_PortalMigrationFailed'
		
	END CATCH
		
	SET XACT_ABORT OFF

END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.ClonePortal.CertificateCertification]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.ClonePortal.CertificateCertification]
GO


/*

CLONE PORTAL - CERTIFICATE / CERTIFICATION DATA
OCCURS FOR FULL AND PARTIAL COPY

*/
CREATE PROCEDURE [System.ClonePortal.CertificateCertification]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,

	@fullCopyFlag			BIT,
	@idSiteSource			INT,
	@idSiteDestination		INT,
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50)
)
AS

BEGIN

	SET NOCOUNT ON
	SET XACT_ABORT ON

	INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.0: Copy CertificateCertification - Initialized', 0)
    
    BEGIN TRY	
    
		DECLARE @sql NVARCHAR(MAX)

		/*
	  
		replace double slash with single slash. WHY DO WE DO THIS?!? Need to find out!
	 
		*/

		SET @sourceDBServer = REPLACE(@sourceDBServer, '\\', '\')
		SET @destinationDBServer = REPLACE(@destinationDBServer, '\\', '\')

		/*

		CERTIFICATES

		*/	  

		-- copy certificates
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCertificate] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	name, '
	   			 + '	issuingOrganization, '
	   			 + '	[description], '
	   			 + '	credits, '
	   			 + '	code, '
	   			 + '	[filename], '
	   			 + '	isActive, '
	   			 + '	activationDate, '
	   			 + '	expiresInterval, '
	   			 + '	expiresTimeframe, '
	   			 + '	objectType, '
	   			 + '	idObject, '
	   			 + '	isDeleted, '
	   			 + '	dtDeleted, '
	   			 + '	reissueBasedOnMostRecentCompletion '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '	   			 
				 + '	C.name  + '' ##'' + CAST(C.idCertificate AS NVARCHAR) + ''##'', '
	   			 + '	C.issuingOrganization, '
	   			 + '	C.[description], '
	   			 + '	C.credits, '
	   			 + '	C.code, '
	   			 + '	C.[filename], '
	   			 + '	C.isActive, '
	   			 + '	C.activationDate, '
	   			 + '	C.expiresInterval, '
	   			 + '	C.expiresTimeframe, '
	   			 + '	C.objectType, '
	   			 + '	CASE C.objectType '
	   			 + '		WHEN 1 THEN '
	   			 + '			TTC.destinationId '
	   			 + '		WHEN 2 THEN '
	   			 + '			TTLP.destinationId '
	   			 + '	ELSE '
	   			 + '		NULL '
	   			 + '	END, '
	   			 + '	C.isDeleted, '
	   			 + '	C.dtDeleted, '
	   			 + '	C.reissueBasedOnMostRecentCompletion '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCertificate] C '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = C.idObject AND TTC.object = ''courses'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTLP on (TTLP.sourceId = C.idObject AND TTLP.object = ''learningPaths'') '
	   			 + 'WHERE C.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   		
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.1: Copy CertificateCertification - Certificates Inserted', 0)

		-- insert the mapping for source to destination certificate ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	SRC.idCertificate, '
	   			 + '	DST.idCertificate, '
	   			 + '	''certificates'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCertificate] DST '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCertificate] SRC '
	   			 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
				 + '	AND SRC.name + '' ##'' + CAST(SRC.idCertificate AS NVARCHAR) + ''##'' = DST.name) '	   			 
	   			 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
	   		
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.2: Copy CertificateCertification - Certificate Mappings Created', 0)

		-- clean up the ##idCertificate## additions we made to certificate names, we did that to uniquely distinguish courses
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblCertificate] SET '
				 + '	name = REPLACE(DC.name, '' ##'' + CAST(SC.sourceID AS NVARCHAR) + ''##'', '''') '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblCertificate] DC '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] SC ON SC.destinationID = DC.idCertificate AND SC.object = ''certificates'' '
				 + 'WHERE DC.idSite = ' + CAST(@idSiteDestination AS NVARCHAR) + ' AND SC.sourceID IS NOT NULL'

		EXEC (@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.3: Copy CertificateCertification - Certificate Names Cleaned Up', 0)

		-- copy certificate languages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCertificateLanguage] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idCertificate, '
	   			 + '	idLanguage, '
	   			 + '	name, '
	   			 + '	[description], '
				 + '	[filename] '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTC.destinationId, '
	   			 + '	CL.idLanguage, '
	   			 + '	CL.name, '
	   			 + '	CL.[description], '
				 + '	CL.[filename] '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCertificateLanguage] CL '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = CL.idCertificate AND TTC.object = ''certificates'') '
	   			 + 'WHERE CL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   		
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.4: Copy CertificateCertification - Certificate Languages Inserted', 0)

		/*

		CERTIFICATE RECORDS - FULL COPY ONLY

		*/
			   
		IF @fullCopyFlag = 1
			BEGIN

			-- copy certificate import records
			SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCertificateImport] '
	   				 + '( '
	   				 + '	[idSite], '
	   				 + '	[idUser], '
	   				 + '	[timestamp], '
	   				 + '	[idUserImported], '
	   				 + '	[importFileName] '
	   				 + ') '
	   				 + 'SELECT '
					 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   				 + '	TTUser.destinationId, '
	   				 + '	CI.[timestamp], '
	   				 + '	TTUserImported.destinationId, '
	   				 + '	CI.[importFileName] '
	   				 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCertificateImport] CI '
	   				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTUser on (TTUser.sourceId = CI.idUser AND TTUser.object = ''users'') '
	   				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTUserImported on (TTUserImported.sourceId = CI.idUserImported AND TTUserImported.object = ''users'') '
	   				 + 'WHERE CI.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
	   			
			EXEC(@sql)

			INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.5: Copy CertificateCertification - Certificate Import Records Inserted', 0)

			-- insert the mapping for source to destination certificate import id
			SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   				 + '( '
	   				 + '	sourceId, '
	   				 + '	destinationId, '
	   				 + '	[object] '
	   				 + ') '
	   				 + 'SELECT '
	   				 + '	SRC.idCertificateImport, '
	   				 + '	DST.idCertificateImport, '
	   				 + '	''certificateimport'' '
	   				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCertificateImport] DST '
	   				 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCertificateImport] SRC '
	   				 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   				 + '	AND (SRC.[timestamp] = DST.[timestamp]) '
	   				 + '	AND (SRC.[importFileName] = DST.[importFileName] OR (SRC.[importFileName] IS NULL AND DST.[importFileName] IS NULL))) '
	   				 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
	   		
			EXEC(@sql)

			INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.6: Copy CertificateCertification - Certificate Import Record Mappings Created', 0)

			-- copy certificate records
			SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCertificateRecord] '
              		 + '( '
              		 + '	idSite, '
              		 + '	idCertificate, '
              		 + '	idUser, '
              		 + '	idAwardedBy, '
              		 + '	[timestamp], '
              		 + '	expires, '
              		 + '	idTimezone, '
              		 + '	code, '
              		 + '	credits, '
              		 + '	awardData, '
              		 + '	idCertificateImport '
              		 + ') '
              		 + 'SELECT '
					 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
              		 + '	TTC.destinationId, '
              		 + '	TTU.destinationId, '
              		 + '	TTAB.destinationId, '
              		 + '	CR.[timestamp], '
              		 + '	CR.expires, '
              		 + '	CR.idTimezone, '
              		 + '	CR.code, '
              		 + '	CR.credits, '
              		 + '	CR.awardData, '
              		 + '	TTCI.destinationId '
              		 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCertificateRecord] CR '
              		 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = CR.idCertificate AND TTC.object = ''certificates'') '
					 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = CR.idUser AND TTU.object = ''users'') '
					 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTAB on (TTAB.sourceId = CR.idAwardedBy AND TTAB.object = ''users'') '
					 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCI on (TTCI.sourceId = CR.idCertificateImport AND TTCI.object = ''certificateimport'') '
              		 + 'WHERE CR.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
              			
			EXEC(@sql)

			INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.7: Copy CertificateCertification - Certificate Records Inserted', 0)

			-- insert the mapping for source to destination certificate record id
			SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   				 + '( '
	   				 + '	sourceId, '
	   				 + '	destinationId, '
	   				 + '	[object] '
	   				 + ') '
	   				 + 'SELECT '
	   				 + '	SRC.idCertificateRecord, '
	   				 + '	DST.idCertificateRecord, '
	   				 + '	''certificaterecord'' '
	   				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCertificateRecord] DST '
	   				 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCertificateRecord] SRC '
	   				 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   				 + '	AND (SRC.[timestamp] = DST.[timestamp]) '
	   				 + '	AND (SRC.code = DST.code OR (SRC.code IS NULL AND DST.code IS NULL)) '
	   				 + '	AND (SRC.credits = DST.credits OR (SRC.credits IS NULL AND DST.credits IS NULL))) '
	   				 + ' INNER JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = SRC.idCertificate AND TTC.destinationId =  DST.idCertificate AND TTC.object = ''certificates'') '
	   				 + ' INNER JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = SRC.idUser AND TTU.destinationId = DST.idUser AND TTU.object = ''users'') '
	   				 + ' WHERE DST.idSite =' + CAST(@idSiteDestination AS NVARCHAR)
	   		
			EXEC(@sql)

			INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.8: Copy CertificateCertification - Certificate Record Mappings Created', 0)

			END

		/*

		CERTIFICATIONS

		*/

		-- copy certifications
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCertification] '
				 + '( '
				 + '	idSite, '
				 + '	title, '
				 + '	shortDescription, '
				 + '	searchTags, '
				 + '	dtCreated, '
				 + '	dtModified, '
				 + '	isDeleted, '
				 + '	dtDeleted, '
				 + '	initialAwardExpiresInterval, '
				 + '	initialAwardExpiresTimeframe, '
				 + '	renewalExpiresInterval, '
				 + '	renewalExpiresTimeframe, '
				 + '	accreditingOrganization, '
				 + '	certificationContactName, '
				 + '	certificationContactEmail, '
				 + '	certificationContactPhoneNumber, '
				 + '	isPublished, '
				 + '	isClosed, '
				 + '	isAnyModule, '
				 + '	isRenewalAnyModule '
				 + ') '
				 + 'SELECT ' 
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	C.title + '' ##'' + CAST(C.idCertification AS NVARCHAR) + ''##'', '
				 + '	C.shortDescription, '
				 + '	C.searchTags, '
				 + '	C.dtCreated, '
				 + '	C.dtModified, '
				 + '	C.isDeleted, '
				 + '	C.dtDeleted, '
				 + '	C.initialAwardExpiresInterval, '
				 + '	C.initialAwardExpiresTimeframe, '
				 + '	C.renewalExpiresInterval, '
				 + '	C.renewalExpiresTimeframe, '
				 + '	C.accreditingOrganization, '
				 + '	C.certificationContactName, '
				 + '	C.certificationContactEmail, '
				 + '	C.certificationContactPhoneNumber, '
				 + '	C.isPublished, '
				 + '	C.isClosed, '
				 + '	C.isAnyModule, '
				 + '	C.isRenewalAnyModule '
				 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCertification] C '
				 + 'WHERE C.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
		
		EXEC(@sql)
		
		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.9: Copy CertificateCertification - Certifications Inserted', 0)
				
		-- insert the mapping for source to destination certification id
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
				 + '( '
				 + '	sourceId, '
				 + '	destinationId, '
				 + '	[object] '
				 + ') '
				 + 'SELECT '
				 + '	SRC.idCertification, '
				 + '	DST.idCertification, '
				 + '	''certification'' '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCertification] DST '
				 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCertification] SRC '
				 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
				 + '	AND SRC.title + '' ##'' + CONVERT(NVARCHAR, SRC.idCertification) + ''##'' = DST.title) '
				 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
						
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.10: Copy CertificateCertification - Certification Mappings Created', 0)

		-- clean up the ##idCertification## additions we made to titles, we did that to uniquely distinguish certifications
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblCertification] SET '
				 + '	title = REPLACE(DC.title, '' ##'' + CONVERT(NVARCHAR, SC.sourceID) + ''##'', '''') '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblCertification] DC '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] SC ON SC.destinationID = DC.idCertification AND SC.object = ''certification'' '
				 + 'WHERE SC.sourceID IS NOT NULL'

		EXEC (@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.11: Copy CertificateCertification - Certification Titles Cleaned Up', 0)

		-- copy certification languages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCertificationLanguage] '
				 + '( '
				 + '	idSite, '
				 + '	idCertification, '
				 + '	idLanguage, '
				 + '	title, '
				 + '	shortDescription, '
				 + '	searchTags '
				 + ') '
				 + 'SELECT ' 
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	TTC.destinationId, '
				 + '	CL.idLanguage, '
				 + '	CL.title, '
				 + '	CL.shortDescription, '
				 + '	CL.searchTags '
				 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCertificationLanguage] CL '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = CL.idCertification AND TTC.object = ''certification'') '
				 + 'WHERE CL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
				              	
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.12: Copy CertificateCertification - Certification Languages Inserted', 0)

		-- copy certification modules
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCertificationModule] '
				 + '( '
				 + '	idSite, '
				 + '	idCertification, '
				 + '	title, '
				 + '	shortDescription, '
				 + '	isAnyRequirementSet, '
				 + '	dtCreated, '
				 + '	dtModified, '
				 + '	isDeleted, '
				 + '	dtDeleted, '
				 + '	isInitialRequirement '
				 + ') '
				 + 'SELECT ' 
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	TTC.destinationId, '
				 + '	CM.title, '
				 + '	CM.shortDescription, '
				 + '	CM.isAnyRequirementSet, '
				 + '	CM.dtCreated, '
				 + '	CM.dtModified, '
				 + '	CM.isDeleted, '
				 + '	CM.dtDeleted, '
				 + '	CM.isInitialRequirement '
				 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCertificationModule] CM '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = CM.idCertification AND TTC.object = ''certification'') '
				 + 'WHERE CM.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
					
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.13: Copy CertificateCertification - Certification Modules Inserted', 0)

		-- insert the mapping for source to destination certification module id
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
				 + '( '
				 + '	sourceId, '
				 + '	destinationId, '
				 + '	[object] '
				 + ') '
				 + 'SELECT '
				 + '	SRC.idCertificationModule, '
				 + '	DST.idCertificationModule, '
				 + '	''certificationmodule'' '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCertificationModule] DST '
				 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCertificationModule] SRC '
				 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
				 + '	AND (SRC.title = DST.title) '
				 + '	AND (SRC.dtCreated = DST.dtCreated OR (SRC.dtCreated IS NULL AND DST.dtCreated IS NULL)) '
				 + '	AND (SRC.dtModified = DST.dtModified OR (SRC.dtModified IS NULL AND DST.dtModified IS NULL))) '
				 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
						
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.14: Copy CertificateCertification - Certification Module Mappings Created', 0)
				
		-- copy certification module mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCertificationModuleLanguage] '
				 + '( '
				 + '	idSite, '
				 + '	idCertificationModule, '
				 + '	idLanguage, '
				 + '	title, '
				 + '	shortDescription '
				 + ') '
				 + 'SELECT ' 
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	TTCM.destinationId, '
				 + '	CML.idLanguage, '
				 + '	CML.title, '
				 + '	CML.shortDescription '
				 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCertificationModuleLanguage] CML '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCM on (TTCM.sourceId = CML.idCertificationModule AND TTCM.object = ''certificationmodule'') '
				 + 'WHERE CML.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
              			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.15: Copy CertificateCertification - Certification Module Languages Inserted', 0)

		-- copy certification module requirement sets
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCertificationModuleRequirementSet] '
				 + '( '
				 + '	idSite, '
				 + '	idCertificationModule, '
				 + '	isAny, '
				 + '	label '				 
				 + ') '
				 + 'SELECT ' 
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	TTCM.destinationId, '
				 + '	CMRS.isAny, '
				 + '	CMRS.label  + '' ##'' + CAST(CMRS.idCertificationModuleRequirementSet AS NVARCHAR) + ''##'' '
				 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCertificationModuleRequirementSet] CMRS '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCM on (TTCM.sourceId = CMRS.idCertificationModule AND TTCM.object = ''certificationmodule'') '
				 + 'WHERE CMRS.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
					
		EXEC(@sql)
		
		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.16: Copy CertificateCertification - Certification Module Requirement Sets Inserted', 0)
				
		-- insert the mapping for source to destination certification module requirement set id
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
				 + '( '
				 + '	sourceId, '
				 + '	destinationId, '
				 + '	[object] '
				 + ') '
				 + 'SELECT '
				 + '	SRC.idCertificationModuleRequirementSet, '
				 + '	DST.idCertificationModuleRequirementSet, '
				 + '	''certificationmodulerequirementset'' '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCertificationModuleRequirementSet] DST '
				 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCertificationModuleRequirementSet] SRC '
				 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
				 + '	AND SRC.label + '' ##'' + CAST(SRC.idCertificationModuleRequirementSet AS NVARCHAR) + ''##'' = DST.label) '
				 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
						
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.17: Copy CertificateCertification - Certification Module Requirement Set Mappings Created', 0)

		-- copy certification module requirement set languages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCertificationModuleRequirementSetLanguage] '
				 + '( '
				 + '	idSite, '
				 + '	idCertificationModuleRequirementSet, '
				 + '	idLanguage, '
				 + '	label '
				 + ') '
				 + 'SELECT ' 
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	TTCMRS.destinationId, '
				 + '	CMRSL.idLanguage, '
				 + '	CMRSL.label '
				 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCertificationModuleRequirementSetLanguage] CMRSL '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCMRS on (TTCMRS.sourceId = CMRSL.idCertificationModuleRequirementSet AND TTCMRS.object = ''certificationmodulerequirementset'') '
				 + 'WHERE CMRSL.idSite = ' + CAST(@idSiteSource AS NVARCHAR)

		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.18: Copy CertificateCertification - Certification Module Requirement Set Languages Inserted', 0)
   
		-- copy certification module requirements
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCertificationModuleRequirement] '
				 + '( '
				 + '	idSite, '
				 + '	idCertificationModuleRequirementSet, '
				 + '	label, '
				 + '	shortDescription, '
				 + '	requirementType, '
				 + '	courseCompletionIsAny, '
				 + '	forceCourseCompletionInOrder, '
				 + '	numberCreditsRequired, '
				 + '	documentUploadFileType, '
				 + '	documentationApprovalRequired, '
				 + '	allowSupervisorsToApproveDocumentation, '
				 + '	allowExpertsToApproveDocumentation, '
				 + '	courseCreditEligibleCoursesIsAll '
				 + ') '
				 + 'SELECT ' 
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	TTCMRS.destinationId, '				 
				 + '	CMR.label  + '' ##'' + CAST(CMR.idCertificationModuleRequirement AS NVARCHAR) + ''##'', '
				 + '	CMR.shortDescription, '
				 + '	CMR.requirementType, '
				 + '	CMR.courseCompletionIsAny, '
				 + '	CMR.forceCourseCompletionInOrder, '
				 + '	CMR.numberCreditsRequired, '
				 + '	CMR.documentUploadFileType, '
				 + '	CMR.documentationApprovalRequired, '
				 + '	CMR.allowSupervisorsToApproveDocumentation, '
				 + '	CMR.allowExpertsToApproveDocumentation, '
				 + '	CMR.courseCreditEligibleCoursesIsAll '
				 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCertificationModuleRequirement] CMR '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCMRS on (TTCMRS.sourceId = CMR.idCertificationModuleRequirementSet AND TTCMRS.object = ''certificationmodulerequirementset'') '
				 + 'WHERE CMR.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
					
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.19: Copy CertificateCertification - Certification Module Requirements Inserted', 0)

		-- insert the mapping for source to destination certification module requirement id
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
				 + '( '
				 + '	sourceId, '
				 + '	destinationId, '
				 + '	[object] '
				 + ') '
				 + 'SELECT '
				 + '	SRC.idCertificationModuleRequirement, '
				 + '	DST.idCertificationModuleRequirement, '
				 + '	''certificationmodulerequirement'' '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCertificationModuleRequirement] DST '
				 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCertificationModuleRequirement] SRC '
				 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
				 + '	AND SRC.label + '' ##'' + CAST(SRC.idCertificationModuleRequirement AS NVARCHAR) + ''##'' = DST.label) '				 
				 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
						
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.20: Copy CertificateCertification - Certification Module Requirement Mappings Created', 0)

		-- copy certification module requirement languages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCertificationModuleRequirementLanguage] '
				 + '( '
				 + '	idSite, '
				 + '	idCertificationModuleRequirement, '
				 + '	idLanguage, '
				 + '	label, '
				 + '	shortDescription '
				 + ') '
				 + ' SELECT ' 
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	TTCMR.destinationId, '
				 + '	CMRL.idLanguage, '
				 + '	CMRL.label, '
				 + '	CMRL.shortDescription '
				 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCertificationModuleRequirementLanguage] CMRL '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCMR on (TTCMR.sourceId = CMRL.idCertificationModuleRequirement AND TTCMR.object = ''certificationmodulerequirement'') '
				 + 'WHERE CMRL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
              			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.21: Copy CertificateCertification - Certification Module Requirement Languages Inserted', 0)

		-- copy certification module requirement to course links
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCertificationModuleRequirementToCourseLink] '
				 + '( '
				 + '	idSite, '
				 + '	idCertificationModuleRequirement, '
				 + '	idCourse, '
				 + '	[order] '
				 + ') '
				 + 'SELECT ' 
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	TTCMR.destinationId, '
				 + '	TTC.destinationId, '
				 + '	CMRTCL.[order] '
				 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCertificationModuleRequirementToCourseLink] CMRTCL '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCMR on (TTCMR.sourceId = CMRTCL.idCertificationModuleRequirement AND TTCMR.object = ''certificationmodulerequirement'') '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = CMRTCL.idCourse AND TTC.object = ''courses'') '
				 + 'WHERE CMRTCL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
				 				
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.22: Copy CertificateCertification - Certification Module Requirement to Course Links Inserted', 0)

		-- copy certification to course credit links
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCertificationToCourseCreditLink] '
				 + '( '
				 + '	idSite, '
				 + '	idCertification, '
				 + '	idCourse, '
				 + '	credits '
				 + ') '
				 + 'SELECT ' 
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	TTCM.destinationId, '
				 + '	TTC.destinationId, '
				 + '	CTCCL.credits '
				 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCertificationToCourseCreditLink] CTCCL '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCM on (TTCM.sourceId = CTCCL.idCertification AND TTCM.object = ''certification'') '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = CTCCL.idCourse AND TTC.object = ''courses'') '
				 + 'WHERE CTCCL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
					
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.23: Copy CertificateCertification - Certification to Course Credit Links Inserted', 0)

		-- copy ruleset to certification links
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblRuleSetToCertificationLink] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idRuleSet, '
	   			 + '	idCertification '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTR.destinationId, '
	   			 + '	TTCertification.destinationId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblRuleSetToCertificationLink] RTCL '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTR on (TTR.sourceId = RTCL.idRuleSet AND TTR.object = ''ruleset'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCertification on (TTCertification.sourceId = RTCL.idCertification AND TTCertification.object = ''certification'') '
	   			 + 'WHERE RTCL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.24: Copy CertificateCertification - Ruleset to Certification Links Inserted', 0)

		/*

		CERTIFICATION TO USER LINKS - FULL COPY ONLY

		*/

		IF @fullCopyFlag = 1
			BEGIN

			-- copy certification to user links
			SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCertificationToUserLink] '
					 + '( '
					 + '	idSite, '
					 + '	idCertification, '
					 + '	idUser, '
					 + '	initialAwardDate, '
					 + '	certificationTrack, '
					 + '	currentExpirationDate, '
					 + '	currentExpirationInterval, '
					 + '	currentExpirationTimeframe, '
					 + '	certificationId, '
					 + '	lastExpirationDate, '
					 + '	idRuleSet '
					 + ') '
					 + 'SELECT ' 
					 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
					 + '	TTC.destinationId, '
					 + '	TTU.destinationId, '
					 + '	CTUL.initialAwardDate, '
					 + '	CTUL.certificationTrack, '
					 + '	CTUL.currentExpirationDate, '
					 + '	CTUL.currentExpirationInterval, '
					 + '	CTUL.currentExpirationTimeframe, '
					 + '	CTUL.certificationId, '
					 + '	CTUL.lastExpirationDate, '
					 + '	TTRS.destinationId '
					 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCertificationToUserLink] CTUL '
					 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = CTUL.idCertification AND TTC.object = ''certification'') '
					 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = CTUL.idUser AND TTU.object = ''users'') '
					 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTRS on (TTRS.sourceId = CTUL.idRuleSet AND TTRS.object = ''ruleset'') '
					 + 'WHERE CTUL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
              
			EXEC(@sql)

			INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.25: Copy CertificateCertification - Certification to User Links Inserted', 0)

			-- insert the mapping for source to destination certification to user link id
			SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
					 + '( '
					 + '	sourceId, '
					 + '	destinationId, '
					 + '	[object] '
					 + ') '
					 + 'SELECT '
					 + '	SRC.idCertificationToUserLink, '
					 + '	DST.idCertificationToUserLink, '
					 + '	''certificationtouserlink'' '
					 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCertificationToUserLink] DST '
					 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.destinationId = DST.idUser AND TTU.object = ''users'') '
					 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCertificationToUserLink] SRC '
					 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
					 + '	AND (SRC.idUser = TTU.sourceId) '
					 + '	AND (SRC.initialAwardDate = DST.initialAwardDate OR (SRC.initialAwardDate IS NULL AND DST.initialAwardDate IS NULL)) '
					 + '	AND (SRC.currentExpirationDate = DST.currentExpirationDate OR (SRC.currentExpirationDate IS NULL AND DST.currentExpirationDate IS NULL)) '
					 + '	AND (SRC.certificationTrack = DST.certificationTrack)) '
					 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
						
			EXEC(@sql)

			INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.26: Copy CertificateCertification - Certification to User Link Mappings Created', 0)

			-- insert records from source site to destination table Data-CertificationModuleRequirement
			SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblData-CertificationModuleRequirement] '
					 + '( '
					 + '	idSite, '
					 + '	idCertificationToUserLink, '
					 + '	idCertificationModuleRequirement, '
					 + '	label, '
					 + '	dtCompleted, '
					 + '	completionDocumentationFilePath, '
					 + '	dtSubmitted, '
					 + '	isApproved, '
					 + '	dtApproved, '
					 + '	idApprover '
					 + ') '
					 + 'SELECT ' 
					 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
					 + '	TTCTUL.destinationId, '
					 + '	TTCMR.destinationId, '
					 + '	DCMR.label, '
					 + '	DCMR.dtCompleted, '
					 + '	DCMR.completionDocumentationFilePath, '
					 + '	DCMR.dtSubmitted, '
					 + '	DCMR.isApproved, '
					 + '	DCMR.dtApproved, '
					 + '	TTU.destinationId '
					 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblData-CertificationModuleRequirement] DCMR '
					 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCTUL on (TTCTUL.sourceId = DCMR.idCertificationToUserLink AND TTCTUL.object = ''certificationtouserlink'') '
					 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCMR on (TTCMR.sourceId = DCMR.idCertificationModuleRequirement AND TTCMR.object = ''certificationmodulerequirement'') '
					 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = DCMR.idApprover AND TTU.object = ''users'') '
					 + 'WHERE DCMR.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
									
			EXEC(@sql)

			INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.27: Copy CertificateCertification - User Certification Module Requirement Data Inserted', 0)

			END			  
	   
		/*

		RETURN

		*/

		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemClonePortal_CertificateCertification_InsertedSuccessfully'

	END TRY

	/*

	CATCH AND LOG ERRORS

	*/

	BEGIN CATCH
		
		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9: Copy CertificateCertification - ERROR - ' + CAST(ERROR_LINE() AS NVARCHAR) + ' | ' + CAST(ERROR_MESSAGE() AS NVARCHAR(MAX)), 1)	
		SET @Return_Code = 1
		SET @Error_Description_Code	= 'SystemClonePortal_CertificateCertification_PortalMigrationFailed'
		
	END CATCH
		
	SET XACT_ABORT OFF		

END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.ClonePortal.ContentResource]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.ClonePortal.ContentResource]
GO


/*

CLONE PORTAL - CONTENT PACKAGE / RESOURCE / QUIZSURVEY DATA
OCCURS FOR FULL AND PARTIAL COPY

*/
CREATE PROCEDURE [System.ClonePortal.ContentResource]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,

	@fullCopyFlag			BIT,			-- needed for quiz/survey and resource tables	
	@idSiteSource			INT,
	@idSiteDestination		INT,
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50),
	@idAccount				INT				= NULL
)
AS

BEGIN

	SET NOCOUNT ON
    SET XACT_ABORT ON

	INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 5.0: Copy ContentResource - Initialized', 0)
    
	BEGIN TRY	
    
		DECLARE @sql NVARCHAR(MAX)
		DECLARE @subSql NVARCHAR(MAX)

		/*
	  
		replace double slash with single slash. WHY DO WE DO THIS?!? Need to find out!
	 
		*/

		SET @sourceDBServer = REPLACE(@sourceDBServer, '\\', '\')
		SET @destinationDBServer = REPLACE(@destinationDBServer, '\\', '\')

		/*

		QUIZ/SURVEY

		*/

		-- copy quizes and surveys
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblQuizSurvey] '
				 + '( '
				 + '	idSite, '
				 + '	[type], '
				 + '	identifier, '
				 + '	[guid], '
				 + '	data, '
				 + '	isDraft, '
				 + '	idContentPackage, '
				 + '	dtCreated, '
				 + '	dtModified, '
				 + '	idAuthor '
				 + ') '
				 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	QS.[type], '
				 + '	QS.identifier, '
				 + '	QS.[guid], '
				 + '	QS.data, '
				 + '	QS.isDraft, '
				 + '	NULL, '
				 + '	QS.dtCreated, '
				 + '	QS.dtModified, '

		IF(@fullCopyFlag = 1) -- idAuthor is available for full copy only, because tblUser moved only for full copy
		
			BEGIN
				
			SET @subSql = '	TTU.destinationId '
						+ 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblQuizSurvey] QS '
                		+ 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = QS.idAuthor AND TTU.object = ''users'') '
						+ 'WHERE QS.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
			END

		ELSE
			
			BEGIN -- otherwise 1 is inserted in this field as administrator id
		
			SET @subSql = '	1 '
						+ 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblQuizSurvey] QS '
                		+ 'WHERE QS.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
			END
	
		SET @sql = @sql + @subSql

		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 5.1: Copy ContentResource - Quizzes/Surveys Inserted', 0)

		-- insert the mapping for source to destination quiz/survey ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	SRC.idQuizSurvey, '
	   			 + '	DST.idQuizSurvey, '
	   			 + '	''quizsurvey'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblQuizSurvey] DST '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblQuizSurvey] SRC '
	   			 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   			 + '	AND (SRC.guid = DST.guid) '
	   			 + '	AND (SRC.dtCreated = DST.dtCreated)) '
	   			 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
	   	
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 5.2: Copy ContentResource - Quiz/Survey Mappings Created', 0)

		-- copy content packages
		DECLARE @idSourceAccount INT

		IF (@sourceDBName = @destinationDBName) 
			BEGIN
			SET @idSourceAccount = @idAccount
			END
		ELSE
			BEGIN
			SET @idSourceAccount = (SELECT DISTINCT TOP 1 idAccount FROM tblAccount where databaseName = @sourceDBName)
			END

		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblContentPackage] '
				 + '( '
				 + '	idSite, '
				 + '	name, '
				 + '	[path], '
				 + '	kb, '
				 + '	idContentPackageType, '
				 + '	idSCORMPackageType, '
				 + '	manifest, '
				 + '	dtCreated, '
				 + '	dtModified, '
				 + '	isMediaUpload, '
				 + '	idMediaType, '
				 + '	contentTitle, '
				 + '	originalMediaFilename, '
				 + '	isVideoMedia3rdParty, '
				 + '	videoMediaEmbedCode, '
				 + '	enableAutoplay, '
				 + '	allowRewind, '
				 + '	allowFastForward, '
				 + '	allowNavigation, '
				 + '	allowResume, '
				 + '	minProgressForCompletion, '
				 + '	isProcessing, '
				 + '	isProcessed, '
				 + '	dtProcessed, '
				 + '	idQuizSurvey, '
				 + '	hasManifestComplianceErrors, '
				 + '	manifestComplianceErrors, '
				 + '	openSesameGUID '
				 + ') '
				 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	CP.name, '
				 + '	REPLACE(CP.[path], ''' + CAST(@idSourceAccount AS NVARCHAR) + '-' + CAST(@idSiteSource AS NVARCHAR) + '-'', ''' + CAST(@idAccount AS NVARCHAR) + '-' + CAST(@idSiteDestination AS NVARCHAR) + '-''), '
				 + '	CP.kb, '
				 + '	CP.idContentPackageType, '
				 + '	CP.idSCORMPackageType, '
				 + '	CP.manifest, '
				 + '	CP.dtCreated, '
				 + '	CP.dtModified, '
				 + '	CP.isMediaUpload, '
				 + '	CP.idMediaType, '
				 + '	CP.contentTitle, '
				 + '	CP.originalMediaFilename, '
				 + '	CP.isVideoMedia3rdParty, '
				 + '	CP.videoMediaEmbedCode, '
				 + '	CP.enableAutoplay, '
				 + '	CP.allowRewind, '
				 + '	CP.allowFastForward, '
				 + '	CP.allowNavigation, '
				 + '	CP.allowResume, '
				 + '	CP.minProgressForCompletion, '
				 + '	CP.isProcessing, '
				 + '	CP.isProcessed, '
				 + '	CP.dtProcessed, '
				 + '	TTQS.destinationId, '
				 + '	CP.hasManifestComplianceErrors, '
				 + '	CP.manifestComplianceErrors, '
				 + '	CP.openSesameGUID '
				 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblContentPackage] CP '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTQS on (TTQS.sourceId = CP.idQuizSurvey AND TTQS.object = ''quizsurvey'') '
				 + 'WHERE CP.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
		 
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 5.3: Copy ContentResource - Content Packages Inserted', 0)
	   
		-- insert the mapping for source to destination content package ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
				 + '( '
				 + '	sourceId, '
				 + '	destinationId, '
				 + '	[object] '
				 + ') '
				 + 'SELECT '
				 + '	SRC.idContentPackage, '
				 + '	DST.idContentPackage, '
				 + '	''contentpackage'' '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblContentPackage] DST '
				 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblContentPackage] SRC '
				 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
				 + '	AND (SRC.name = DST.name) '
				 + '	AND (SRC.dtCreated = DST.dtCreated ) '
				 + '	AND (SRC.dtModified = DST.dtModified )) '
				 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)

		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 5.4: Copy ContentResource - Content Package Mappings Created', 0)
	   
		-- there is cross dependency between tblContentPackage and tblQuizSurvey, so we need to update tblQuizSurvey
		-- and set idContentPackage after we have moved tblContentPackage data
		SET @sql = 'UPDATE DSTQS '
				 + '	SET idContentPackage = TEMP.destinationId '
				 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblQuizSurvey] SRCQS '
				 + 'INNER JOIN  [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblQuizSurvey] DSTQS '
				 + 'ON (SRCQS.guid = DSTQS.guid '
				 + '	AND (SRCQS.dtCreated = DSTQS.dtCreated) '
				 + '	AND SRCQS.idSite =' + CAST(@idSiteSource AS NVARCHAR) + ') '
				 + 'INNER JOIN [dbo].[ClonePortal_IDMappings] TEMP '
				 + 'ON (TEMP.sourceId = SRCQS.idContentPackage AND TEMP.object = ''contentpackage'') '
				 + 'WHERE DSTQS.idSite = ' + CAST(@idSiteDestination AS NVARCHAR) + ' AND SRCQS.idContentPackage IS NOT NULL '

		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 5.5: Copy ContentResource - Content Package Quiz/Survey Ids Updated', 0)
		
		-- copy resource types
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblResourceType] '
	   			 + '( '
				 + '	idSite, '
				 + '	resourceType, '
				 + '	isDeleted, '
				 + '	dtDeleted '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	RT.resourceType, '
				 + '	RT.isDeleted, '
				 + '	RT.dtDeleted '
				 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblResourceType] RT '
				 + 'WHERE RT.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
                										
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 5.6: Copy ContentResource - Resource Types Inserted', 0)
	   
		-- insert the mapping for source to destination resource type ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
				 + '( '
				 + '	sourceId, '
				 + '	destinationId, '
				 + '	[object] '
				 + ') '
				 + 'SELECT '
				 + '	SRC.idResourceType, '
				 + '	DST.idResourceType, '
				 + '	''resourcetype'' '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblResourceType] DST '
				 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblResourceType] SRC '
				 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
				 + '	AND (SRC.resourceType = DST.resourceType) '
				 + '	AND (SRC.isDeleted = DST.isDeleted) '
				 + '	AND (SRC.dtDeleted = DST.dtDeleted OR (SRC.dtDeleted IS NULL AND DST.dtDeleted IS NULL))) '
				 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 5.7: Copy ContentResource - Resource Types Mapped', 0)
	   			   
		-- copy resource type languages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblResourceTypeLanguage] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idResourceType, '
	   			 + '	idLanguage, '
	   			 + '	resourceType '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTRT.destinationId, '
	   			 + '	RTL.idLanguage, '
	   			 + '	RTL.resourceType '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblResourceTypeLanguage] RTL '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTRT ON (TTRT.sourceId = RTL.idResourceType AND TTRT.object = ''resourcetype'') '
	   			 + 'WHERE RTL.idSite = ' + CAST(@idSiteSource AS NVARCHAR)	   						
	   			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 5.8: Copy ContentResource - Resource Type Languages Inserted', 0)

		-- copy resources
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblResource] '
				 + '( '
				 + '	idSite, '
				 + '	name, '
				 + '	idParentResource, '
				 + '	idOwner, '
				 + '	idResourceType, '
				 + '	[description], '
				 + '	identifier, '
				 + '	isMoveable, '
				 + '	isAvailable, '
				 + '	capacity, '
				 + '	locationRoom, '
				 + '	locationBuilding, '
				 + '	locationCity, '
				 + '	locationProvince, '
				 + '	locationCountry, '
				 + '	ownerWithinSystem, '
				 + '	ownerName, '
				 + '	ownerEmail, '
				 + '	ownerContactInformation, '
				 + '	isDeleted, '
				 + '	dtDeleted '
				 + ') '
				 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	R.name, '
				 + '	NULL, ' -- idParentResource will be updated from temporary ResourceIdMappings table below

		IF(@fullCopyFlag = 1) -- idOwner is available for full copy only, because tblUser moved only for full copy
		
			BEGIN

			SET @sql = @sql + '	TTU.destinationId, ' -- owner

			END

		ELSE

			BEGIN

			SET @sql = @sql + ' 1, '

			END

		SET @sql = @sql + '	TTRT.destinationId, '
						+ '	R.[description], '
						+ '	R.identifier, '
						+ '	R.isMoveable, '
						+ '	R.isAvailable, '
						+ '	R.capacity, '
						+ '	R.locationRoom, '
						+ '	R.locationBuilding, '
						+ '	R.locationCity, '
						+ '	R.locationProvince, '
						+ '	R.locationCountry, '
						+ '	R.ownerWithinSystem, '
						+ '	R.ownerName, '
						+ '	R.ownerEmail, '
						+ '	R.ownerContactInformation, '
						+ '	R.isDeleted, '
						+ '	R.dtDeleted '
						+ 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblResource] R '
						+ 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTRT on (TTRT.sourceId = R.idResourceType AND TTRT.object = ''resourcetype'') '

		IF(@fullCopyFlag = 1) -- idOwner is available for full copy only, because tblUser moved only for full copy

			BEGIN

			SET @sql = @sql + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = R.idOwner AND TTU.object = ''users'') '

			END

		SET @sql = @sql + 'WHERE R.idSite = ' + CAST(@idSiteSource AS NVARCHAR)		
     										
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 5.9: Copy ContentResource - Resources Inserted', 0)
	   
		-- insert the mapping for source to destination resource ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	SRC.idResource, '
	   			 + '	DST.idResource, '
	   			 + '	''resource'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblResource] DST '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblResource] SRC '
	   			 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   			 + '	AND (SRC.name = DST.name) '
	   			 + '	AND (SRC.locationRoom = DST.locationRoom OR (SRC.locationRoom IS NULL AND DST.locationRoom IS NULL)) '
	   			 + '	AND (SRC.locationBuilding = DST.locationBuilding OR (SRC.locationBuilding IS NULL AND DST.locationBuilding IS NULL)) '
	   			 + '	AND (SRC.locationCity = DST.locationCity OR (SRC.locationCity IS NULL AND DST.locationCity IS NULL)) '
	   			 + '	AND (SRC.ownerEmail = DST.ownerEmail OR (SRC.ownerEmail IS NULL AND DST.ownerEmail IS NULL)) '
	   			 + '	AND (SRC.dtDeleted = DST.dtDeleted OR (SRC.dtDeleted IS NULL AND DST.dtDeleted IS NULL))) '
	   			 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 5.10: Copy ContentResource - Resource Mappings Created', 0)
	   
		-- update idParentResource
		SET @sql = 'UPDATE DSTR '
	   			 + '	SET idParentResource = TEMP.destinationId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblResource] SRCR '
	   			 + 'INNER JOIN  [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblResource] DSTR '
	   			 + 'ON (SRCR.name = DSTR.name AND (SRCR.ownerEmail = DSTR.ownerEmail OR (SRCR.ownerEmail IS NULL AND DSTR.ownerEmail IS NULL)) '
	   		  	 + '	AND (SRCR.locationRoom = DSTR.locationRoom OR (SRCR.locationRoom IS NULL AND DSTR.locationRoom IS NULL)) '
	   		  	 + '	AND (SRCR.locationBuilding = DSTR.locationBuilding OR (SRCR.locationBuilding IS NULL AND DSTR.locationBuilding IS NULL)) '
	   		  	 + '	AND (SRCR.locationCity = DSTR.locationCity OR (SRCR.locationCity IS NULL AND DSTR.locationCity IS NULL)) '
	   		  	 + '	AND SRCR.idSite =' + CAST(@idSiteSource AS NVARCHAR) + ') '
	   			 + 'INNER JOIN [dbo].[ClonePortal_IDMappings] TEMP '
	   			 + 'ON (TEMP.sourceId = SRCR.idParentResource AND TEMP.object = ''resource'') '
	   			 + 'WHERE DSTR.idSite = ' + CAST(@idSiteDestination AS NVARCHAR) + ' AND SRCR.idParentResource IS NOT NULL'
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 5.11: Copy ContentResource - Resource Parent Ids Updated', 0)
			   
		-- copy resource languages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblResourceLanguage] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idResource, '
	   			 + '	idLanguage, '
	   			 + '	name, '
	   			 + '	[description] '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTR.destinationId, '
	   			 + '	RL.idLanguage, '
	   			 + '	RL.name, '
	   			 + '	RL.[description] '
	   			 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblResourceLanguage] RL '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTR on (TTR.sourceId = RL.idResource AND TTR.object = ''resource'') '
	   			 + 'WHERE RL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 			
	   			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 5.12: Copy ContentResource - Resource Languages Inserted', 0)

		/*

		RETURN

		*/

		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemClonePortal_ContentResource_InsertedSuccessfully'

	END TRY

	/*

	CATCH AND LOG ERRORS

	*/

	BEGIN CATCH

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 5: Copy ContentResource - ERROR - ' + CAST(ERROR_LINE() AS NVARCHAR) + ' | ' + CAST(ERROR_MESSAGE() AS NVARCHAR(MAX)), 1)
		SET @Return_Code = 1
		SET @Error_Description_Code	= 'SystemClonePortal_ContentResource_PortalMigrationFailed'
		
	END CATCH
		
	SET XACT_ABORT OFF		

END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.ClonePortal.CouponCode]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.ClonePortal.CouponCode]
GO


/*

CLONE PORTAL - COUPON CODES
OCCURS FOR FULL AND PARTIAL COPY

*/
CREATE PROCEDURE [System.ClonePortal.CouponCode]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,

	@idSiteSource			INT,
	@idSiteDestination		INT,
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50)
)
AS

BEGIN

	SET NOCOUNT ON
    SET XACT_ABORT ON

	INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 8.0: Copy CouponCode - Initialized', 0)
    
	BEGIN TRY	
    
		DECLARE @sql NVARCHAR(MAX)

		/*
	  
		replace double slash with single slash. WHY DO WE DO THIS?!? Need to find out!
	 
		*/

		SET @sourceDBServer = REPLACE(@sourceDBServer, '\\', '\')
		SET @destinationDBServer = REPLACE(@destinationDBServer, '\\', '\')

		/*

		COUPON CODES

		*/

		-- copy coupon codes
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCouponCode] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	code, '
	   			 + '	comments, '
	   			 + '	usesAllowed, '
	   			 + '	discount, '
	   			 + '	discountType, '
	   			 + '	forCourse, '
	   			 + '	forCatalog, '
	   			 + '	forLearningPath, '
	   			 + '	dtStart, '
	   			 + '	dtEnd, '
	   			 + '	isSingleUsePerUser, '
	   			 + '	isDeleted, '
	   			 + '	dtDeleted, '
	   			 + '	forStandupTraining '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	CC.code, '
	   			 + '	CC.comments, '
	   			 + '	CC.usesAllowed, '
	   			 + '	CC.discount, '
	   			 + '	CC.discountType, '
	   			 + '	CC.forCourse, '
	   			 + '	CC.forCatalog, '
	   			 + '	CC.forLearningPath, '
	   			 + '	CC.dtStart, '
	   			 + '	CC.dtEnd, '
	   			 + '	CC.isSingleUsePerUser, '
	   			 + '	CC.isDeleted, '
	   			 + '	CC.dtDeleted, '
	   			 + '	CC.forStandupTraining '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCouponCode] CC '				 
	   			 + 'WHERE CC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
		
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 8.1: Copy CouponCode - Coupon Codes Inserted', 0)

		-- insert the mapping for source to destination coupon code ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
				 + '( '
				 + '	sourceId, '
				 + '	destinationId, '
				 + '	[object] '
				 + ') '
				 + 'SELECT '
				 + '	SCC.idCouponCode, '
				 + '	DCC.idCouponCode, '
				 + '	''couponcode'' '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCouponCode] DCC '
				 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCouponCode] SCC '
				 + 'ON (SCC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
				 + '	AND (SCC.code = DCC.code) '
				 + '	AND (SCC.dtDeleted = DCC.dtDeleted OR (SCC.dtDeleted IS NULL AND DCC.dtDeleted IS NULL)) '
				 + '	AND (SCC.dtStart = DCC.dtStart OR (SCC.dtStart IS NULL AND DCC.dtStart IS NULL)) '
				 + '	AND (SCC.dtEnd = DCC.dtEnd OR (SCC.dtEnd IS NULL AND DCC.dtEnd IS NULL))) '
				 + 'WHERE DCC.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
						
		EXEC(@sql)
		
		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 8.1: Copy CouponCode - Coupon Code Mappings Created', 0)

		/*

		COUPON CODE TO OBJECT LINKS

		*/

		-- copy coupon code to ilt links
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCouponCodeToStandupTrainingLink] '
	   			 + '( '
	   			 + '	idCouponCode, ' 
	   			 + '	idStandupTraining '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	TTCC.destinationId, '
	   			 + '	TTST.destinationId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCouponCodeToStandupTrainingLink] CCSTL '
				 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCouponCode] CC ON CC.idCouponCode = CCSTL.idCouponCode '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCC ON (TTCC.sourceId = CCSTL.idCouponCode  AND TTCC.object = ''couponcode'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTST ON (TTST.sourceId = CCSTL.idStandupTraining AND TTST.object = ''standuptraining'') '
	   			 + 'WHERE CC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) + ' AND TTCC.destinationId IS NOT NULL AND TTST.destinationId IS NOT NULL'
	   	
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 8.2: Copy CouponCode - Coupon Code to ILT Links Inserted', 0)

		-- copy coupon code to catalog links
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCouponCodeToCatalogLink] '
	   			 + '( '
	   			 + '	idCouponCode, '
	   			 + '	idCatalog '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	TTCC.destinationId, '
	   			 + '	TTC.destinationId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCouponCodeToCatalogLink] CCTCL '
				 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCouponCode] CC ON CC.idCouponCode = CCTCL.idCouponCode '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCC ON (TTCC.sourceId = CCTCL.idCouponCode AND TTCC.object = ''couponcode'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC ON (TTC.sourceId = CCTCL.idCatalog AND TTC.object = ''catalog'') '
	   			 + 'WHERE CC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) + ' AND TTCC.destinationId IS NOT NULL AND TTC.destinationId IS NOT NULL'
	   	
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 8.3: Copy CouponCode - Coupon Code to Catalog Links Inserted', 0)

		-- copy coupon code to learning path links
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCouponCodeToLearningPathLink] '
	   			 + '( '
	   			 + '	idCouponCode, '
	   			 + '	idLearningPath '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	TTCC.destinationId, '
	   			 + '	TTLP.destinationId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCouponCodeToLearningPathLink] CCLPL '
				 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCouponCode] CC ON CC.idCouponCode = CCLPL.idCouponCode '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCC on (TTCC.sourceId = CCLPL.idCouponCode AND TTCC.object = ''couponcode'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTLP on (TTLP.sourceId = CCLPL.idLearningPath AND TTLP.object = ''learningPaths'') '
	   			 + 'WHERE CC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) + ' AND TTCC.destinationId IS NOT NULL AND TTLP.destinationId IS NOT NULL'
	   			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 8.4: Copy CouponCode - Coupon Code to Learning Path Links Inserted', 0)

		-- copy coupon to course links
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCouponCodeToCourseLink] '
	   			 + '( '
	   			 + '	idCouponCode, '
	   			 + '	idCourse '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	TTCC.destinationId, '
	   			 + '	TTC.destinationId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCouponCodeToCourseLink] CCTCL '
				 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCouponCode] CC ON CC.idCouponCode = CCTCL.idCouponCode '	   			 
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCC on (TTCC.sourceId = CCTCL.idCouponCode AND TTCC.object = ''couponcode'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = CCTCL.idCourse AND TTC.object = ''courses'') '
	   			 + 'WHERE CC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) + ' AND TTCC.destinationId IS NOT NULL AND TTC.destinationId IS NOT NULL'

		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 8.5: Copy CouponCode - Coupon Code to Course Links Inserted', 0)

		/*

		RETURN

		*/
			   
		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemClonePortal_CouponCode_InsertedSuccessfully'

	END TRY

	/*

	CATCH AND LOG ERRORS

	*/

	BEGIN CATCH
			
		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 8: Copy CouponCode - ERROR - ' + CAST(ERROR_LINE() AS NVARCHAR) + ' | ' + CAST(ERROR_MESSAGE() AS NVARCHAR(MAX)), 1)
		SET @Return_Code = 1
		SET @Error_Description_Code	= 'SystemClonePortal_CouponCode_PortalMigrationFailed'
		
	END CATCH
		
	SET XACT_ABORT OFF

END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.ClonePortal.Data]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.ClonePortal.Data]
GO


/*

CLONE PORTAL - MODULE/SCO/XAPI DATA
OCCURS FOR FULL COPY

*/
CREATE PROCEDURE [System.ClonePortal.Data]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,

	@idSiteSource			INT,
	@idSiteDestination		INT,
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50)
)
AS

BEGIN

	SET NOCOUNT ON
    SET XACT_ABORT ON
    
	INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 13.0: Copy Data - Initialized', 0)

    BEGIN TRY	
    
		DECLARE @sql	NVARCHAR(MAX)

		/*
	  
		replace double slash with single slash. WHY DO WE DO THIS?!? Need to find out!
	 
		*/

		SET @sourceDBServer = REPLACE(@sourceDBServer, '\\', '\')
		SET @destinationDBServer = REPLACE(@destinationDBServer, '\\', '\')
	  
		-- copy sco data
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblData-SCO] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	[idData-Lesson], '
	   			 + '	idTimezone, '
	   			 + '	manifestIdentifier, '
	   			 + '	completionStatus, '
	   			 + '	successStatus, '
	   			 + '	scoreScaled, '
	   			 + '	totalTime, '
	   			 + '	[timestamp], '
	   			 + '	actualAttemptCount, '
	   			 + '	effectiveAttemptCount, '
	   			 + '	proctoringUser '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTDL.destinationId, '
	   			 + '	DS.idTimezone, '
	   			 + '	DS.manifestIdentifier, '
	   			 + '	DS.completionStatus, '
	   			 + '	DS.successStatus, '
	   			 + '	DS.scoreScaled, '
	   			 + '	DS.totalTime, '
	   			 + '	DS.[timestamp], '
	   			 + '	DS.actualAttemptCount, '
	   			 + '	DS.effectiveAttemptCount, '
	   			 + '	TTU.destinationId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblData-SCO] DS '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTDL on (TTDL.sourceId = DS.[idData-Lesson] AND TTDL.object = ''datalesson'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = DS.[proctoringUser] AND TTU.object = ''users'') '
	   			 + 'WHERE DS.idSite = ' + CAST(@idSiteSource AS NVARCHAR) + ' AND TTDL.destinationId IS NOT NULL '
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 13.1: Copy Data - Data SCO Records Inserted', 0)
	
		-- insert the mapping for source to destination data sco ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	SDS.[idData-SCO], '
	   			 + '	DDS.[idData-SCO], '
	   			 + '	''datasco'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblData-SCO] DDS '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTDL on (TTDL.destinationId = DDS.[idData-Lesson] AND TTDL.object = ''datalesson'') '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblData-SCO] SDS '
	   			 + 'ON (SDS.idSite =' + CAST(@idSiteSource AS NVARCHAR) 
				 + '	AND (SDS.[idData-Lesson] = TTDL.sourceId)) '	   			 
	   			 + 'WHERE DDS.idSite = ' + CAST(@idSiteDestination AS NVARCHAR) + ' AND SDS.[idData-SCO] IS NOT NULL AND DDS.[idData-SCO] IS NOT NULL'
	   			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 13.2: Copy Data - Data SCO Record Mappings Created', 0)

		-- copy homework assignment data
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblData-HomeworkAssignment] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	[idData-Lesson], '
	   			 + '	uploadedAssignmentFilename, '
	   			 + '	dtUploaded, '
	   			 + '	completionStatus, '
	   			 + '	successStatus, '
	   			 + '	score, '
	   			 + '	[timestamp], '
	   			 + '	proctoringUser '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTDL.destinationId, '
	   			 + '	DHA.uploadedAssignmentFilename, '
	   			 + '	DHA.dtUploaded, '
	   			 + '	DHA.completionStatus, '
	   			 + '	DHA.successStatus, '
	   			 + '	DHA.score, '
	   			 + '	DHA.[timestamp], '
	   			 + '	TTU.destinationId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblData-HomeworkAssignment] DHA '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTDL on (TTDL.sourceId = DHA.[idData-Lesson] AND TTDL.object = ''datalesson'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = DHA.[proctoringUser] AND TTU.object = ''users'') '
	   			 + 'WHERE DHA.idSite = ' + CAST(@idSiteSource AS NVARCHAR) + ' AND TTDL.destinationId IS NOT NULL '
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 13.3: Copy Data - Homework Assignment Data Records Inserted', 0)

		-- copy xapi oauth consumers
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblxAPIoAuthConsumer] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	label, '
	   			 + '	oAuthKey, '
	   			 + '	oAuthSecret, '
	   			 + '	isActive '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	XAPIAC.label, '
	   			 + '	XAPIAC.oAuthKey, '
	   			 + '	XAPIAC.oAuthSecret, '
	   			 + '	XAPIAC.isActive '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblxAPIoAuthConsumer] XAPIAC '
	   			 + 'WHERE XAPIAC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   								
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 13.4: Copy Data - XAPI Oauth Consumers Inserted', 0)

		-- insert the mapping for source to destination xapi oauth consumer ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + ' SELECT '
	   			 + '	SP.idxAPIoAuthConsumer, '
	   			 + '	DP.idxAPIoAuthConsumer, '
	   			 + '	''xapioauthconsumer'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblxAPIoAuthConsumer] DP '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblxAPIoAuthConsumer] SP '
	   			 + 'ON (SP.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
	   			 + '	AND (SP.label = DP.label OR (SP.label IS NULL AND DP.label IS NULL)) '
	   			 + '	AND (SP.oAuthKey = DP.oAuthKey OR (SP.oAuthKey IS NULL AND DP.oAuthKey IS NULL)) '
	   			 + '	AND (SP.oAuthSecret = DP.oAuthSecret OR (SP.oAuthSecret IS NULL AND DP.oAuthSecret IS NULL))) '
	   			 + 'WHERE DP.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
	   			
		EXEC(@sql)
		
		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 13.5: Copy Data - XAPI Oauth Consumer Mappings Created', 0)

		-- copy xapi data records
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblData-TinCan] '
	   			 + '( '
	   			 + '	[idData-Lesson], '
	   			 + '	[idData-TinCanParent], '
	   			 + '	idSite, '
	   			 + '	idEndpoint, '
	   			 + '	isInternalAPI, '
	   			 + '	statementId, '
	   			 + '	actor, '
	   			 + '	verbId, '
	   			 + '	verb, '
	   			 + '	activityId, '
	   			 + '	mboxObject, '
	   			 + '	mboxSha1SumObject, '
	   			 + '	openIdObject, '
	   			 + '	accountObject, '
	   			 + '	mboxActor, '
	   			 + '	mboxSha1SumActor, '
	   			 + '	openIdActor, '
	   			 + '	accountActor, '
	   			 + '	mboxAuthority, '
	   			 + '	mboxSha1SumAuthority, '
	   			 + '	openIdAuthority, '
	   			 + '	accountAuthority, '
	   			 + '	mboxTeam, '
	   			 + '	mboxSha1SumTeam, '
	   			 + '	openIdTeam, '
	   			 + '	accountTeam, '
	   			 + '	mboxInstructor, '
	   			 + '	mboxSha1SumInstructor, '
	   			 + '	openIdInstructor, '
	   			 + '	accountInstructor, '
	   			 + '	[object], '
	   			 + '	registration, '
	   			 + '	[statement], '
	   			 + '	isVoidingStatement, '
	   			 + '	isStatementVoided, '
	   			 + '	dtStored, '
	   			 + '	scoreScaled '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	TTDL.destinationId, '
	   			 + '	NULL, '			     -- this null value of [idData-TinCanParent] will be updated from temporary DataTinCanIdMappings table below 
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTXAAC.destinationId, '
	   			 + '	DTC.isInternalAPI, '
	   			 + '	DTC.statementId, '
	   			 + '	DTC.actor, '
	   			 + '	DTC.verbId, '
	   			 + '	DTC.verb, '
	   			 + '	DTC.activityId, '
	   			 + '	DTC.mboxObject, '
	   			 + '	DTC.mboxSha1SumObject, '
	   			 + '	DTC.openIdObject, '
	   			 + '	DTC.accountObject, '
	   			 + '	DTC.mboxActor, '
	   			 + '	DTC.mboxSha1SumActor, '
	   			 + '	DTC.openIdActor, '
	   			 + '	DTC.accountActor, '
	   			 + '	DTC.mboxAuthority, '
	   			 + '	DTC.mboxSha1SumAuthority, '
	   			 + '	DTC.openIdAuthority, '
	   			 + '	DTC.accountAuthority, '
	   			 + '	DTC.mboxTeam, '
	   			 + '	DTC.mboxSha1SumTeam, '
	   			 + '	DTC.openIdTeam, '
	   			 + '	DTC.accountTeam, '
	   			 + '	DTC.mboxInstructor, '
	   			 + '	DTC.mboxSha1SumInstructor, '
	   			 + '	DTC.openIdInstructor, '
	   			 + '	DTC.accountInstructor, '
	   			 + '	DTC.[object], '
	   			 + '	DTC.registration, '
	   			 + '	DTC.[statement], '
	   			 + '	DTC.isVoidingStatement, '
	   			 + '	DTC.isStatementVoided, '
	   			 + '	DTC.dtStored, '
	   			 + '	DTC.scoreScaled '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblData-TinCan] DTC '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTDL on TTDL.sourceId = DTC.[idData-Lesson] AND TTDL.object = ''datalesson'' '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTXAAC on TTXAAC.sourceId = DTC.[idEndpoint] AND TTXAAC.object = ''xapioauthconsumer'' '
	   			 + 'WHERE DTC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) + ' AND TTDL.destinationId IS NOT NULL'
	   
		EXEC(@sql)
		
		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 13.6: Copy Data - XAPI Data Inserted', 0)
	   
		-- insert the mapping for source to destination catalog ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	SDS.[idData-TinCan], '
	   			 + '	DDS.[idData-TinCan], '
	   			 + '	''datatincan'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblData-TinCan] DDS '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblData-TinCan] SDS '
	   			 + 'ON (SDS.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   			 + '	AND (SDS.statementId = DDS.statementId OR (SDS.statementId IS NULL AND DDS.statementId IS NULL)) '
	   			 + '	AND (SDS.dtStored = DDS.dtStored)) '
	   			 + 'WHERE DDS.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
	   			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 13.7: Copy Data - XAPI Data Mappings Created', 0)
	   
		-- update [idData-TinCanParent] from temp table DataTinCan
		SET @sql = 'UPDATE DDTC '
	   			 + '	SET [idData-TinCanParent] = TEMP.destinationId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblData-TinCan] SDTC '
	   			 + 'INNER JOIN  [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblData-TinCan] DDTC '
	   			 + 'ON (SDTC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   			 + '	AND (SDTC.statementId = DDTC.statementId OR (SDTC.statementId IS NULL AND DDTC.statementId IS NULL)) '
	   			 + '	AND (SDTC.dtStored = DDTC.dtStored OR (SDTC.dtStored IS NULL AND DDTC.dtStored IS NULL))) '
	   			 + 'INNER JOIN  [dbo].[ClonePortal_IDMappings] TEMP '
	   			 + 'ON (TEMP.sourceId = SDTC.[idData-TinCanParent] AND TEMP.object = ''datatincan'') '
	   			 + 'WHERE DDTC.idSite = ' + CAST(@idSiteDestination AS NVARCHAR) + ' AND SDTC.[idData-TinCanParent] IS NOT NULL'
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 13.8: Copy Data - XAPI Data Parent Ids Updated', 0)

		-- copy xapi oauth nonces
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblxAPIoAuthNonce] '
	   			 + '( '
	   			 + '	idxAPIoAuthConsumer, '
	   			 + '	nonce '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	TTXAAC.destinationId, '
	   			 + '	xAPIAN.nonce '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblxAPIoAuthNonce] xAPIAN '
	   			 + 'INNER JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblxAPIoAuthConsumer] xAPIAC  ON  xAPIAC.idxAPIoAuthConsumer= xAPIAN.idxAPIoAuthConsumer '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTXAAC on TTXAAC.sourceId = xAPIAN.[idxAPIoAuthConsumer] AND TTXAAC.object = ''xapioauthconsumer'' '
	   			 + 'WHERE xAPIAC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 		
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 13.9: Copy Data - XAPI Oauth Nonces Inserted', 0)

		-- copy xapi profiles
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblData-TinCanProfile] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idEndpoint, '
	   			 + '	profileId, '
	   			 + '	activityId, '
	   			 + '	agent, '
	   			 + '	contentType, '
	   			 + '	docContents, '
	   			 + '	dtUpdated '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTXAAC.destinationId, '
	   			 + '	DTCP.profileId, '
	   			 + '	DTCP.activityId, '
	   			 + '	DTCP.agent, '
	   			 + '	DTCP.contentType, '
	   			 + '	DTCP.docContents, '
	   			 + '	DTCP.dtUpdated '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblData-TinCanProfile] DTCP '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTXAAC on TTXAAC.sourceId = DTCP.[idEndpoint] AND TTXAAC.object = ''xapioauthconsumer'' '
	   			 + 'WHERE DTCP.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 13.10: Copy Data - XAPI Profiles Inserted', 0)

		-- copy xapi state
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblData-TinCanState] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idEndpoint, '
	   			 + '	stateId, '
	   			 + '	agent, '
	   			 + '	activityId, '
	   			 + '	registration, '
	   			 + '	contentType, '
	   			 + '	docContents, '
	   			 + '	dtUpdated '
	   			 + ') '
	   			 + ' SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTXAAC.destinationId, '
	   			 + '	DTCS.stateId, '
	   			 + '	DTCS.agent, '
	   			 + '	DTCS.activityId, '
	   			 + '	DTCS.registration, '
	   			 + '	DTCS.contentType, '
	   			 + '	DTCS.docContents, '
	   			 + '	DTCS.dtUpdated '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblData-TinCanState] DTCS '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTXAAC on TTXAAC.sourceId = DTCS.[idEndpoint] AND TTXAAC.object = ''xapioauthconsumer'' '
	   			 + 'WHERE DTCS.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 13.11: Copy Data - XAPI State Inserted', 0)

		-- copy api context activities
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblData-TinCanContextActivities] '
	   			 + '( '
	   			 + '	[idData-TinCan], '
	   			 + '	idSite, '
	   			 + '	activityId, '
	   			 + '	activityType, '
	   			 + '	objectType '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	TTDTC.destinationId, '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	DTCA.activityId, '
	   			 + '	DTCA.activityType, '
	   			 + '	DTCA.objectType '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblData-TinCanContextActivities] DTCA '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTDTC on (TTDTC.sourceId = DTCA.[idData-TinCan] AND TTDTC.object = ''datatincan'') '
	   			 + 'WHERE DTCA.idSite = ' + CAST(@idSiteSource AS NVARCHAR) + ' AND TTDTC.destinationId IS NOT NULL'
	   
		EXEC(@sql)
		
		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 13.12: Copy Data - XAPI Context Activities Inserted', 0)	

		-- copy scorm interactions
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblData-SCOInt] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	[idData-SCO], '
	   			 + '	[identifier], '
	   			 + '	[timestamp], '
	   			 + '	[result], '
	   			 + '	[latency], '
	   			 + '	[scoIdentifier] '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTDS.destinationId, '	   			 
				 + '	DSI.identifier  + '' ##'' + CAST(DSI.[idData-SCOInt] AS NVARCHAR) + ''##'', '
	   			 + '	DSI.[timestamp], '
	   			 + '	DSI.result, '
	   			 + '	DSI.latency, '
	   			 + '	DSI.scoIdentifier '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblData-SCOInt] DSI '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTDS on (TTDS.sourceId = DSI.[idData-SCO] AND TTDS.object = ''datasco'') '
	   			 + 'WHERE DSI.idSite = ' + CAST(@idSiteSource AS NVARCHAR) + ' AND TTDS.destinationId IS NOT NULL'
	   	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 13.13: Copy Data - SCORM Interactions Inserted', 0)

		-- insert the mapping for source to destination scorm interaction ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	SRC.[idData-SCOInt], '
	   			 + '	DST.[idData-SCOInt], '
	   			 + '	''scorminteraction'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblData-SCOInt] DST '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblData-SCOInt] SRC '
	   			 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
				 + '	AND SRC.identifier + '' ##'' + CAST(SRC.[idData-SCOInt] AS NVARCHAR) + ''##'' = DST.identifier) '
	   			 + 'WHERE DST.idSite =' + CAST(@idSiteDestination AS NVARCHAR)
				 		
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 13.14: Copy Data - SCORM Interaction Mappings Created', 0)

		-- clean up the ##idData-SCOInt## additions we made to scorm interaction identifiers, we did that to uniquely distinguish scorm interactions
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblData-SCOInt] SET '
				 + '	identifier = REPLACE(DSI.identifier, '' ##'' + CAST(SSI.sourceID AS NVARCHAR) + ''##'', '''') '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblData-SCOInt] DSI '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] SSI ON SSI.destinationID = DSI.[idData-SCOInt] AND SSI.object = ''scorminteraction'' '
				 + 'WHERE DSI.idSite = ' + CAST(@idSiteDestination AS NVARCHAR) + ' AND SSI.sourceID IS NOT NULL'

		EXEC (@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 13.15: Copy Data - SCORM Interaction Identifiers Cleaned Up', 0)

		-- copy scorm objectives
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblData-SCOObj] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	[idData-SCO], '
	   			 + '	[identifier], '
	   			 + '	[scoreScaled], '
	   			 + '	[completionStatus], '
	   			 + '	[successStatus] '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTDS.destinationId, '
	   			 + '	DSO.identifier, '
	   			 + '	DSO.scoreScaled, '
	   			 + '	DSO.completionStatus, '
	   			 + '	DSO.successStatus '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblData-SCOObj] DSO '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTDS on (TTDS.sourceId = DSO.[idData-SCO] AND TTDS.object = ''datasco'') '
	   			 + 'WHERE DSO.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 13.16: Copy Data - SCORM Objectives Inserted', 0)

		/*

		RETURN

		*/
	   
		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemClonePortal_Data_InsertedSuccessfully'

	END TRY

	/*

	CATCH AND LOG ERRORS

	*/

	BEGIN CATCH
			
		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 13: Copy Data - ERROR - ' + CAST(ERROR_LINE() AS NVARCHAR) + ' | ' + CAST(ERROR_MESSAGE() AS NVARCHAR(MAX)), 1)
		SET @Return_Code = 1
		SET @Error_Description_Code	= 'SystemClonePortal_Data_PortalMigrationFailed'
		
	END CATCH
		
	SET XACT_ABORT OFF

END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.ClonePortal.DiscussionFeed]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.ClonePortal.DiscussionFeed]
GO


/*

CLONE PORTAL - COURSE FEED / GROUP FEED DATA
OCCURS FOR FULL COPY

*/
CREATE PROCEDURE [System.ClonePortal.DiscussionFeed]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,

	@idSiteSource			INT,
	@idSiteDestination		INT,
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50)
)
AS

BEGIN

	SET NOCOUNT ON
	SET XACT_ABORT ON

	INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 10.0: Copy DiscussionFeed - Initialized', 0)
    
	BEGIN TRY	
    
		DECLARE @sql NVARCHAR(MAX)

		/*
	  
		replace double slash with single slash. WHY DO WE DO THIS?!? Need to find out!
	 
		*/

		SET @sourceDBServer = REPLACE(@sourceDBServer, '\\', '\')
		SET @destinationDBServer = REPLACE(@destinationDBServer, '\\', '\')

		/*

		COURSE FEEDS

		*/
			  
		-- copy course feed messages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCourseFeedMessage] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idCourse, '
	   			 + '	idLanguage, '
	   			 + '	idAuthor, '
	   			 + '	idParentCourseFeedMessage, '
	   			 + '	[message], '
	   			 + '	[timestamp], '
	   			 + '	dtApproved, '
	   			 + '	idApprover, '
	   			 + '	isApproved '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTC.destinationId, '
	   			 + '	CFM.idLanguage, '
	   			 + '	TTAuthor.destinationId, '
	   			 + '	NULL, '						-- this null value of idParentCourseFeedMessage will be updated from temporary CourseFeedMessageIdMappings table below
	   			 + '	CFM.[message], '
	   			 + '	CFM.[timestamp], '
	   			 + '	CFM.dtApproved, '
	   			 + '	TTApprover.destinationId, '
	   			 + '	CFM.IsApproved '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCourseFeedMessage] CFM '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTAuthor on (TTAuthor.sourceId = CFM.idAuthor AND TTAuthor.object = ''users'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTApprover on (TTApprover.sourceId = CFM.idApprover AND TTApprover.object = ''users'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = CFM.idCourse AND TTC.object = ''courses'') '
	   			 + 'WHERE CFM.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   		
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 10.1: Copy DiscussionFeed - Course Feed Messages Inserted', 0)

		-- insert the mapping for source to destination course feed message ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + ' SELECT '
	   			 + '	SRC.idCourseFeedMessage, '
	   			 + '	DST.idCourseFeedMessage, '
	   			 + '	''coursefeedmessage'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCourseFeedMessage] DST '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCourseFeedMessage] SRC '
	   			 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
	   			 + '	AND (SRC.[timestamp] = DST.[timestamp]) '
	   			 + '	AND (SRC.message = DST.message)) '
	   			 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 10.2: Copy DiscussionFeed - Course Feed Message Mappings Created', 0)
	   
		-- update idParentCourseFeedMessage from temp table CourseFeedMessage
		SET @sql = 'UPDATE DSTC '
	   			 + '	SET idParentCourseFeedMessage = TEMP.destinationId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCourseFeedMessage] SRCC '
	   			 + 'INNER JOIN  [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCourseFeedMessage] DSTC '
	   			 + 'ON(SRCC.[timestamp] = DSTC.[timestamp] AND SRCC.message = DSTC.message AND SRCC.idSite = ' + CAST(@idSiteSource AS NVARCHAR)+ ') '
	   			 + 'INNER JOIN [dbo].[ClonePortal_IDMappings] TEMP '
	   			 + 'ON (TEMP.sourceId = SRCC.idParentCourseFeedMessage AND TEMP.object = ''coursefeedmessage'') '
	   			 + 'WHERE DSTC.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
	   			 + 'AND SRCC.idParentCourseFeedMessage IS NOT NULL'
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 10.3: Copy DiscussionFeed - Course Feed Message Parent Ids Updated', 0)

		-- copy course feed moderators
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCourseFeedModerator] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idCourse, '
	   			 + '	idModerator '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTC.destinationId, '
	   			 + '	TTU.destinationId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCourseFeedModerator] CFM '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = CFM.idModerator AND TTU.object = ''users'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = CFM.idCourse AND TTC.object = ''courses'') '
	   			 + 'WHERE CFM.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 10.4: Copy DiscussionFeed - Course Feed Moderators Inserted', 0)

		-- copy course ratings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCourseRating] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idCourse, '
	   			 + '	idUser, '
	   			 + '	rating, '
	   			 + '	[timestamp] '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTC.destinationId, '
	   			 + '	TTU.destinationId, '
	   			 + '	CR.rating, '
	   			 + '	CR.[timestamp] '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCourseRating] CR '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = CR.idUser AND TTU.object = ''users'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = CR.idCourse AND TTC.object = ''courses'') '
	   			 + 'WHERE CR.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 10.5: Copy DiscussionFeed - Course Ratings Inserted', 0)

		/*

		GROUP FEEDS

		*/

		-- copy group feed messages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblGroupFeedMessage] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idGroup, '
	   			 + '	idLanguage, '
	   			 + '	idAuthor, '
	   			 + '	idParentGroupFeedMessage, '
	   			 + '	[message], '
	   			 + '	[timestamp], '
	   			 + '	dtApproved, '
	   			 + '	idApprover, '
	   			 + '	isApproved '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTG.destinationId, '
	   			 + '	GFM.idLanguage, '
	   			 + '	TTAuthor.destinationId, '
	   			 + '	NULL, '					-- this null value of idParentGroupFeedMessage will be updated from temporary GroupFeedMessageIdMappings table below
	   			 + '	GFM.[message], '
	   			 + '	GFM.[timestamp], '
	   			 + '	GFM.dtApproved, '
	   			 + '	TTApprover.destinationId, '
	   			 + '	GFM.IsApproved '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblGroupFeedMessage] GFM '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTAuthor on (TTAuthor.sourceId = GFM.idAuthor AND TTAuthor.object = ''users'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTApprover on (TTApprover.sourceId = GFM.idApprover AND TTApprover.object = ''users'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTG on (TTG.sourceId = GFM.idGroup AND TTG.object = ''groups'') '
	   			 + 'WHERE GFM.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   		
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 10.6: Copy DiscussionFeed - Group Feed Messages Inserted', 0)
	   	   
		-- insert the mapping for source to destination group feed message ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	SRC.idGroupFeedMessage, '
	   			 + '	DST.idGroupFeedMessage, '
	   			 + '	''groupfeedmessage'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblGroupFeedMessage] DST '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblGroupFeedMessage] SRC '
	   			 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   			 + '	AND (SRC.[timestamp] = DST.[timestamp]) '
	   			 + '	AND (SRC.message = DST.message)) '
	   			 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
	   
		EXEC(@sql)
	   
	    INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 10.7: Copy DiscussionFeed - Group Feed Message Mappings Created', 0)
	   
		-- update idParentGroupFeedMessage from temp table GroupFeedMessage
		SET @sql = 'UPDATE DSTC '
	   			 + '	SET idParentGroupFeedMessage = TEMP.destinationId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblGroupFeedMessage] SRCC '
	   			 + 'INNER JOIN  [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblGroupFeedMessage] DSTC '
	   			 + 'ON (SRCC.[timestamp] = DSTC.[timestamp] AND SRCC.message = DSTC.message AND SRCC.idSite = ' + CAST(@idSiteSource AS NVARCHAR)+ ') '
	   			 + 'INNER JOIN [dbo].[ClonePortal_IDMappings] TEMP '
	   			 + 'ON (TEMP.sourceId = SRCC.idParentGroupFeedMessage AND TEMP.object = ''groupfeedmessage'') '
	   			 + 'WHERE DSTC.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
	   			 + 'AND SRCC.idParentGroupFeedMessage IS NOT NULL'
	   
		EXEC(@sql)	
        
		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 10.8: Copy DiscussionFeed - Group Feed Message Parent Ids Updated', 0)

		-- copy course feed moderators
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblGroupFeedModerator] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idGroup, '
	   			 + '	idModerator '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTG.destinationId, '
	   			 + '	TTU.destinationId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblGroupFeedModerator] GFM '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = GFM.idModerator AND TTU.object = ''users'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTG on (TTG.sourceId = GFM.idGroup AND TTG.object = ''groups'') '
	   			 + 'WHERE GFM.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   	
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 10.9: Copy DiscussionFeed - Group Feed Moderators Inserted', 0)

		/*

		RETURN

		*/

		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemClonePortal_DiscussionFeed_InsertedSuccessfully'

	END TRY

	/*

	CATCH AND LOG ERRORS

	*/

	BEGIN CATCH
			
		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 10: Copy DiscussionFeed - ERROR - ' + CAST(ERROR_LINE() AS NVARCHAR) + ' | ' + CAST(ERROR_MESSAGE() AS NVARCHAR(MAX)), 1)
		SET @Return_Code = 1
		SET @Error_Description_Code	= 'SystemClonePortal_DiscussionFeed_PortalMigrationFailed'
		
	END CATCH
		
	SET XACT_ABORT OFF

END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.ClonePortal.DocumentRepositoryEventEmail]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.ClonePortal.DocumentRepositoryEventEmail]
GO


/*

CLONE PORTAL - DOCUMENT REPOSITORY / EVENT EMAIL DATA
OCCURS FOR FULL AND PARTIAL COPY

*/
CREATE PROCEDURE [System.ClonePortal.DocumentRepositoryEventEmail]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,

	@fullCopyFlag			BIT,
	@idSiteSource			INT,
	@idSiteDestination		INT,
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50)
)
AS

BEGIN

	SET NOCOUNT ON
	SET XACT_ABORT ON

	INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 12.0: Copy DocumentRepositoryEventEmail - Initialized', 0)
    
	BEGIN TRY	
    
		DECLARE @sql	NVARCHAR(MAX)
		DECLARE @subSql NVARCHAR(MAX)

		/*
	  
		replace double slash with single slash. WHY DO WE DO THIS?!? Need to find out!
	 
		*/

		SET @sourceDBServer = REPLACE(@sourceDBServer, '\\', '\')
		SET @destinationDBServer = REPLACE(@destinationDBServer, '\\', '\')

		/*

		DOCUMENT REPOSITORY

		*/

		-- copy document repository folders
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblDocumentRepositoryFolder] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idDocumentRepositoryObjectType, '
	   			 + '	idObject, '
	   			 + '	name, '
	   			 + '	isDeleted, '
	   			 + '	dtDeleted '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	DRF.idDocumentRepositoryObjectType, '
	   			 + '	CASE DRF.idDocumentRepositoryObjectType '
	   			 + '		WHEN 1 THEN '
	   			 + '			TTG.destinationId '
	   			 + '		WHEN 2 THEN '
	   			 + '			TTC.destinationId '
	   			 + '		WHEN 3 THEN '
	   			 + '			TTLP.destinationId '

		IF @fullCopyFlag = 1 -- only copy over profile file data if this is a full copy
		
		BEGIN
		SET @subSql = '		WHEN 4 THEN '
	   				+ '			TTU.destinationId '
	   				+ '		END, '
	   				+ '		DRF.name, '
	   				+ '		DRF.isDeleted, '
	   				+ '		DRF.dtDeleted '
	   				+ 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblDocumentRepositoryFolder] DRF '
	   				+ 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTG on (TTG.sourceId = DRF.idObject AND TTG.object = ''groups'') '
	   				+ 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = DRF.idObject AND TTC.object = ''courses'') '
	   				+ 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTLP on (TTLP.sourceId = DRF.idObject AND TTLP.object = ''learningPaths'') '
					+ 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = DRF.idObject AND TTU.object = ''users'') '
	   				+ 'WHERE DRF.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
		END

		ELSE

		BEGIN
		SET @subSql = '		END, '
	   				+ '		DRF.name, '
	   				+ '		DRF.isDeleted, '
	   				+ '		DRF.dtDeleted '
	   				+ 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblDocumentRepositoryFolder] DRF '
	   				+ 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTG on (TTG.sourceId = DRF.idObject AND TTG.object = ''groups'') '
	   				+ 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = DRF.idObject AND TTC.object = ''courses'') '
	   				+ 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTLP on (TTLP.sourceId = DRF.idObject AND TTLP.object = ''learningPaths'') '
	   				+ 'WHERE DRF.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
		END

		SET @sql = @sql + @subSql

		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 12.1: Copy DocumentRepositoryEventEmail - Document Repository Folders Inserted', 0)
	   
		-- insert the mapping for source to destination document repository folder ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	SRC.idDocumentRepositoryFolder, '
	   			 + '	DST.idDocumentRepositoryFolder, '
	   			 + '	''documentrepositoryfolder'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblDocumentRepositoryFolder] DST '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblDocumentRepositoryFolder] SRC '
	   			 + 'ON (SRC.idSite =' + CAST(@idSiteSource AS NVARCHAR)
	   			 + '	AND (SRC.name = DST.name) '
	   			 + '	AND (SRC.idDocumentRepositoryObjectType = DST.idDocumentRepositoryObjectType) '
	   			 + '	AND (SRC.dtDeleted = DST.dtDeleted OR (SRC.dtDeleted IS NULL AND DST.dtDeleted IS NULL))) '
	   			 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 12.2: Copy DocumentRepositoryEventEmail - Document Repository Folder Mappings Created', 0)

		-- copy document repository items
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblDocumentRepositoryItem] '
				 + '( '
				 + '	idSite, '
				 + '	idDocumentRepositoryObjectType, '
				 + '	label, '
				 + '	[fileName], '
				 + '	kb, '
				 + '	searchTags, '
				 + '	isPrivate, '
				 + '	idLanguage, '
				 + '	isAllLanguages, '
				 + '	dtCreated, '
				 + '	isDeleted, '
				 + '	dtDeleted, '
				 + '	idObject, '
				 + '	idDocumentRepositoryFolder, '
				 + '	idOwner, '
				 + '	isVisibleToUser, '
				 + '	isUploadedByUser '
				 + ') '
				 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	DRI.idDocumentRepositoryObjectType, '
				 + '	DRI.label, '
				 + '	DRI.[fileName], '
				 + '	DRI.kb, '
				 + '	DRI.searchTags, '
				 + '	DRI.isPrivate, '
				 + '	DRI.idLanguage, '
				 + '	DRI.isAllLanguages, '
				 + '	DRI.dtCreated, '
				 + '	DRI.isDeleted, '
				 + '	DRI.dtDeleted, '

		IF @fullCopyFlag = 1 -- only copy over profile file data if this is a full copy
		
		BEGIN
		SET @subSql = '	CASE DRI.idDocumentRepositoryObjectType '
					+ '		WHEN 1 THEN '
					+ '			TTG.destinationId '
					+ '		WHEN 2 THEN '
					+ '			TTC.destinationId '
					+ '		WHEN 3 THEN '
					+ '			TTLP.destinationId '
					+ '		WHEN 4 THEN '
					+ '			TTUT.destinationId '
					+ '	END AS idObject, '
					+ '	TTDRF.destinationId, '
					+ '	TTU.destinationId, '
					+ '	DRI.isVisibleToUser, '
					+ '	DRI.isUploadedByUser '
					+ 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblDocumentRepositoryItem] DRI '
					+ 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTG on (TTG.sourceId = DRI.idObject AND TTG.object = ''groups'') '
					+ 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = DRI.idObject AND TTC.object = ''courses'') '
					+ 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTLP on (TTLP.sourceId = DRI.idObject AND TTLP.object = ''learningPaths'') '
					+ 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTDRF on (TTDRF.sourceId = DRI.idDocumentRepositoryFolder AND TTDRF.object = ''documentrepositoryfolder'') '
					+ 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = DRI.idOwner AND TTU.object = ''users'') '
					+ 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTUT on (TTUT.sourceId = DRI.idObject AND TTUT.object = ''users'') '
					+ 'WHERE DRI.idSite = ' + CAST(@idSiteSource AS NVARCHAR) + ' '
					+ 'AND ((TTG.destinationId IS NOT NULL AND DRI.idDocumentRepositoryObjectType = 1) '
					+ 'OR (TTC.destinationId IS NOT NULL AND DRI.idDocumentRepositoryObjectType = 2) '
					+ 'OR (TTLP.destinationId IS NOT NULL AND DRI.idDocumentRepositoryObjectType = 3) '
					+ 'OR (TTUT.destinationId IS NOT NULL AND DRI.idDocumentRepositoryObjectType = 4))'
		END

		ELSE

		BEGIN
		SET @subSql = '	CASE DRI.idDocumentRepositoryObjectType '
					+ '		WHEN 1 THEN '
					+ '			TTG.destinationId '
					+ '		WHEN 2 THEN '
					+ '			TTC.destinationId '
					+ '		WHEN 3 THEN '
					+ '			TTLP.destinationId '					
					+ '	END AS idObject, '
					+ '	TTDRF.destinationId, '
					+ '	1, '
					+ '	DRI.isVisibleToUser, '
					+ '	DRI.isUploadedByUser '
					+ 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblDocumentRepositoryItem] DRI '
					+ 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTG on (TTG.sourceId = DRI.idObject AND TTG.object = ''groups'') '
					+ 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = DRI.idObject AND TTC.object = ''courses'') '
					+ 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTLP on (TTLP.sourceId = DRI.idObject AND TTLP.object = ''learningPaths'') '
					+ 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTDRF on (TTDRF.sourceId = DRI.idDocumentRepositoryFolder AND TTDRF.object = ''documentrepositoryfolder'') '					
					+ 'WHERE DRI.idSite = ' + CAST(@idSiteSource AS NVARCHAR) + ' '
					+ 'AND ((TTG.destinationId IS NOT NULL AND DRI.idDocumentRepositoryObjectType = 1) '
					+ 'OR (TTC.destinationId IS NOT NULL AND DRI.idDocumentRepositoryObjectType = 2) '
					+ 'OR (TTLP.destinationId IS NOT NULL AND DRI.idDocumentRepositoryObjectType = 3)) '					
		END

		SET @sql = @sql + @subSql

		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 12.3: Copy DocumentRepositoryEventEmail - Document Repository Items Inserted', 0)
		  
		-- insert the mapping for source to destination document repository item ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
				 + '( '
				 + '	sourceId, '
				 + '	destinationId, '
				 + '	[object] '
				 + ') '
				 + 'SELECT '
				 + '	SRC.idDocumentRepositoryItem, '
				 + '	DST.idDocumentRepositoryItem, '
				 + '	''documentrepositoryitem'' '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblDocumentRepositoryItem] DST '
				 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblDocumentRepositoryItem] SRC '
				 + 'ON (SRC.idSite =' + CAST(@idSiteSource AS NVARCHAR)
				 + '	AND (SRC.label = DST.label) '
	   			 + '	AND (SRC.idDocumentRepositoryObjectType = DST.idDocumentRepositoryObjectType) '
				 + '	AND (SRC.dtCreated = DST.dtCreated) '
				 + '	AND (SRC.dtDeleted = DST.dtDeleted OR (SRC.dtDeleted IS NULL AND DST.dtDeleted IS NULL))) '
				 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)

		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 12.4: Copy DocumentRepositoryEventEmail - Document Repository Item Mappings Created', 0)
		  
		-- copy document repository item languages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblDocumentRepositoryItemLanguage] '
				 + '( '
				 + '	idSite, '
				 + '	idDocumentRepositoryItem, '
				 + '	idLanguage, '
				 + '	label, '
				 + '	searchTags '
				 + ') '
				 + 'SELECT ' 
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	TTDRI.destinationId, '
				 + '	DRIL.idLanguage, '
				 + '	DRIL.label, '
				 + '	DRIL.searchTags '
				 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblDocumentRepositoryItemLanguage] DRIL '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTDRI ON (TTDRI.sourceId = DRIL.idDocumentRepositoryItem AND TTDRI.object = ''documentrepositoryitem'') '
				 + 'WHERE DRIL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) + ' AND TTDRI.destinationId IS NOT NULL'
		
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 12.5: Copy DocumentRepositoryEventEmail - Document Repository Item Languages Inserted', 0)

		/*

		EMAIL NOTIFICATIONS

		*/

		-- copy email notifications
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblEventEmailNotification] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	name, '
	   			 + '	idEventType, '
	   			 + '	idEventTypeRecipient, '
	   			 + '	[from], '
	   			 + '	isActive, '
	   			 + '	dtActivation, '
	   			 + '	interval, '
	   			 + '	timeframe, '
	   			 + '	[priority], '
	   			 + '	isDeleted, '
	   			 + '	idObject, '
				 + '	isHTMLBased, '
				 + '	attachmentType, '
				 + '	copyTo, '
				 + '	specificEmailAddress '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '	   			 
				 + '	EEN.name  + '' ##'' + CAST(EEN.idEventEmailNotification AS NVARCHAR) + ''##'', '
	   			 + '	EEN.idEventType, '
	   			 + '	EEN.idEventTypeRecipient, '
	   			 + '	EEN.[from], '
	   			 + '	EEN.isActive, '
	   			 + '	EEN.dtActivation, '
	   			 + '	EEN.interval, '
	   			 + '	EEN.timeframe, '
	   			 + '	EEN.[priority], '
	   			 + '	EEN.isDeleted, '
				 + '	CASE '
				 + '	WHEN EEN.idEventType >= 200 AND EEN.idEventType < 400 THEN ' -- course enrollment (courses and lessons)
				 + '		TTC.destinationId '				 
				 + '	WHEN EEN.idEventType >= 400 AND EEN.idEventType < 500 THEN ' -- ilt session
				 + '		TTILT.destinationId '
				 + '	WHEN EEN.idEventType >= 500 AND EEN.idEventType < 600 THEN ' -- learning path
				 + '		TTLP.destinationId '
				 + '	WHEN EEN.idEventType >= 600 AND EEN.idEventType < 700 THEN ' -- certification
				 + '		TTCRT.destinationId '
				 + '	WHEN EEN.idEventType >= 700 AND EEN.idEventType < 800 THEN ' -- certificate
				 + '		TTCER.destinationId '
				 + '	ELSE '														 -- all other events that are not object-specific
				 + '		NULL '
				 + '	END, '
				 + '	EEN.isHTMLBased, '
				 + '	EEN.attachmentType, '
				 + '	EEN.copyTo, '
				 + '	EEN.specificEmailAddress '
				 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblEventEmailNotification] EEN '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC ON (TTC.sourceId = EEN.idObject AND TTC.object = ''courses'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTILT ON (TTILT.sourceId = EEN.idObject AND TTILT.object = ''standuptraining'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTLP ON (TTLP.sourceId = EEN.idObject AND TTLP.object = ''learningPaths'') '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCRT ON (TTCRT.sourceId = EEN.idObject AND TTCRT.object = ''certification'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCER ON (TTCER.sourceId = EEN.idObject AND TTCER.object = ''certificates'') '
	   			 + 'WHERE EEN.idSite = ' + CAST(@idSiteSource AS NVARCHAR)

		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 12.6: Copy DocumentRepositoryEventEmail - Event Email Notifications Inserted', 0)

		-- insert the mapping for source to destination email notification ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	SEEN.idEventEmailNotification, '
	   			 + '	DEEN.idEventEmailNotification, '
	   			 + '	''emailNotifications'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblEventEmailNotification] DEEN '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblEventEmailNotification] SEEN '
	   			 + 'ON (SEEN.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
				 + '	AND SEEN.name + '' ##'' + CAST(SEEN.idEventEmailNotification AS NVARCHAR) + ''##'' = DEEN.name) '	   			 
	   			 + 'WHERE DEEN.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 12.7: Copy DocumentRepositoryEventEmail - Event Email Notification Mappings Created', 0)

		-- clean up the ##idEventEmailNotification## additions we made to email notification names, we did that to uniquely distinguish email notifications
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblEventEmailNotification] SET '
				 + '	name = REPLACE(DEEN.name, '' ##'' + CAST(SEEN.sourceID AS NVARCHAR) + ''##'', '''') '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblEventEmailNotification] DEEN '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] SEEN ON SEEN.destinationID = DEEN.idEventEmailNotification AND SEEN.object = ''emailNotifications'' '
				 + 'WHERE DEEN.idSite = ' + CAST(@idSiteDestination AS NVARCHAR) + ' AND SEEN.sourceID IS NOT NULL'

		EXEC (@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 12.8: Copy DocumentRepositoryEventEmail - Event Email Notification Names Cleaned Up', 0)
	   
		-- copy event email notification languages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblEventEmailNotificationLanguage] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idEventEmailNotification, '
	   			 + '	idLanguage, '
	   			 + '	name '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTEEN.destinationId, '
	   			 + '	EENL.idLanguage, '
	   			 + '	EENL.name '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblEventEmailNotificationLanguage] EENL '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTEEN on (TTEEN.sourceId = EENL.idEventEmailNotification AND TTEEN.object = ''emailNotifications'') '
				 + 'WHERE EENL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) + ' AND TTEEN.destinationId IS NOT NULL'
	    
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 12.9: Copy DocumentRepositoryEventEmail - Event Email Notification Languages Inserted', 0)	   
		
		/*
		
		EVENT LOGS & INBOX MESSAGES - FULL COPY ONLY
		
		*/	  

		IF @fullCopyFlag = 1
		BEGIN

		-- copy the event log
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblEventLog] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idEventType, '
	   			 + '	timestamp, '
	   			 + '	eventDate, '
	   			 + '	idObject, '
	   			 + '	idObjectRelated, '
	   			 + '	idObjectUser, '
	   			 + '	idExecutingUser '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	EL.idEventType, '
	   			 + '	EL.timestamp, '
	   			 + '	EL.eventDate, '
	   			 + '	CASE '
	   			 + '	WHEN EL.idEventType >= 100 AND EL.idEventType < 200 THEN ' -- user events
	   			 + '		TTU.destinationId '
	   			 + '	WHEN EL.idEventType >= 200 AND EL.idEventType < 300 THEN ' -- course enrollment events
	   			 + '		TTC.destinationId '
	   			 + '	WHEN EL.idEventType >= 300 AND EL.idEventType < 400 THEN ' -- lesson events
	   			 + '		TTL.destinationId '
	   			 + '	WHEN EL.idEventType >= 400 AND EL.idEventType < 500 THEN ' -- session events
	   			 + '		TTST.destinationId '
	   			 + '	WHEN EL.idEventType >= 500 AND EL.idEventType < 600 THEN ' -- learning path enrollment events
	   			 + '		TTLP.destinationId '
				 + '	WHEN EL.idEventType >= 600 AND EL.idEventType < 700 THEN ' -- certification events
	   			 + '		TTCRT.destinationId '
	   			 + '	WHEN EL.idEventType >= 700 AND EL.idEventType < 800 THEN ' -- certificate events
	   			 + '		TTCER.destinationId '	   				
	   			 + '	END, '
	   			 + '	CASE '
	   			 + '	WHEN EL.idEventType >= 100 AND EL.idEventType < 200 THEN ' -- user events
	   			 + '		TTU.destinationId '
	   			 + '	WHEN EL.idEventType >= 200 AND EL.idEventType < 300 THEN ' -- course enrollment events
	   			 + '		TTEN.destinationId '
	   			 + '	WHEN EL.idEventType >= 300 AND EL.idEventType < 400 THEN ' -- lesson events
	   			 + '		TTDL.destinationId '
	   			 + '	WHEN EL.idEventType >= 400 AND EL.idEventType < 500 THEN ' -- session events
	   			 + '		TTSTI.destinationId '
	   			 + '	WHEN EL.idEventType >= 500 AND EL.idEventType < 600 THEN ' -- learning path enrollment events
	   			 + '		TTLPEN.destinationId '
				 + '	WHEN EL.idEventType >= 600 AND EL.idEventType < 700 THEN ' -- certification events
	   			 + '		TTCRTUL.destinationId '
	   			 + '	WHEN EL.idEventType >= 700 AND EL.idEventType < 800 THEN ' -- certificate events
	   			 + '		TTCERR.destinationId '
	   			 + '	END, '
	   			 + '	TTOU.destinationId, '
	   			 + '	TTEU.destinationId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblEventLog] EL '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU ON (TTU.sourceId = EL.idObject AND TTU.object = ''users'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC ON (TTC.sourceId = EL.idObject AND TTC.object = ''courses'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTEN on (TTEN.sourceId = EL.idObjectRelated AND TTEN.object = ''enrollment'') '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTL on (TTL.sourceId = EL.idObject AND TTL.object = ''lesson'') '
	   	SET @sql = @sql + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTDL on (TTDL.sourceId = EL.idObjectRelated AND TTDL.object = ''datalesson'') '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTST on (TTST.sourceId = EL.idObject AND TTST.object = ''standuptraining'') '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTSTI on (TTSTI.sourceId = EL.idObject AND TTSTI.object = ''session'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTLP on (TTLP.sourceId = EL.idObject AND TTLP.object = ''learningPaths'') '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTLPEN on (TTLPEN.sourceId = EL.idObjectRelated AND TTLPEN.object = ''learningpathenrollment'') '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCRT on (TTCRT.sourceId = EL.idObject AND TTCRT.object = ''certification'') '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCRTUL on (TTCRTUL.sourceId = EL.idObject AND TTCRTUL.object = ''certificationtouserlink'') '				 
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCER on (TTCER.sourceId = EL.idObject AND TTCER.object = ''certificates'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCERR on (TTCERR.sourceId = EL.idObjectRelated AND TTCERR.object = ''certificaterecord'') '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTOU ON (TTOU.sourceId = EL.idObjectUser AND TTOU.object = ''users'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTEU ON (TTEU.sourceId = EL.idExecutingUser AND TTEU.object = ''users'') '
	   			 + 'WHERE EL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) + ' '
				 + 'AND ((TTU.destinationId IS NOT NULL AND EL.idEventType >= 100 AND EL.idEventType < 200) '
	   			 + 'OR (TTC.destinationId IS NOT NULL AND TTEN.destinationId IS NOT NULL AND EL.idEventType >= 200 AND EL.idEventType < 300) '
	   			 + 'OR (TTL.destinationId IS NOT NULL AND TTDL.destinationId IS NOT NULL AND EL.idEventType >= 300 AND EL.idEventType < 400) '
	   			 + 'OR (TTST.destinationId IS NOT NULL AND TTSTI.destinationId IS NOT NULL AND EL.idEventType >= 400 AND EL.idEventType < 500) '
	   			 + 'OR (TTLP.destinationId IS NOT NULL AND TTLPEN.destinationId IS NOT NULL AND EL.idEventType >= 500 AND EL.idEventType < 600) '
				 + 'OR (TTCRT.destinationId IS NOT NULL AND TTCRTUL.destinationId IS NOT NULL AND EL.idEventType >= 600 AND EL.idEventType < 700) '
	   			 + 'OR (TTCER.destinationId IS NOT NULL AND TTCERR.destinationId IS NOT NULL AND EL.idEventType >= 700 AND EL.idEventType < 800))'
					
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 12.10: Copy DocumentRepositoryEventEmail - Event Log Inserted', 0)
	   
		-- DO NOT CREATE EVENT LOG MAPPINGS

		-- DO NOT COPY EVENT EMAIL QUEUE

		-- copy inbox messages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblInboxMessage] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idParentInboxMessage, '
	   			 + '	idRecipient, '
	   			 + '	idSender, '
	   			 + '	[subject], '
	   			 + '	[message], '
	   			 + '	isDraft, '
	   			 + '	isSent, '
	   			 + '	dtSent, '
	   			 + '	isRead, '
	   			 + '	dtRead, '
	   			 + '	isRecipientDeleted, '
	   			 + '	dtRecipientDeleted, '
	   			 + '	isSenderDeleted, '
	   			 + '	dtSenderDeleted, '
	   			 + '	dtCreated '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	NULL, '			 -- this null value of idParentInboxMessage will be updated from temporary InboxMessageIdMappings table below 
	   			 + '	TTR.destinationId, '
	   			 + '	TTS.destinationId, '
	   			 + '	IM.[subject], '
	   			 + '	IM.[message], '
	   			 + '	IM.isDraft, '
	   			 + '	IM.isSent, '
	   			 + '	IM.dtSent, '
	   			 + '	IM.isRead, '
	   			 + '	IM.dtRead, ' 
	   			 + '	IM.isRecipientDeleted, '
	   			 + '	IM.dtRecipientDeleted, '
	   			 + '	IM.isSenderDeleted, '
	   			 + '	IM.dtSenderDeleted, '
	   			 + '	IM.dtCreated '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblInboxMessage] IM '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTR on (TTR.sourceId = IM.idRecipient AND TTR.object = ''users'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTS on (TTS.sourceId = IM.idSender AND TTS.object = ''users'') '
	   			 + 'WHERE IM.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   							
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 12.11: Copy DocumentRepositoryEventEmail - Inbox Messages Inserted', 0)

		-- insert the mapping for source to destination inbox message ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	SRC.idInboxMessage, '
	   			 + '	DST.idInboxMessage, '
	   			 + '	''inboxmessage'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblInboxMessage] DST '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblInboxMessage] SRC '
	   			 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   			 + '	AND (SRC.message = DST.message) '
	   			 + '	AND (SRC.dtCreated = DST.dtCreated) '
	   			 + '	AND (SRC.dtSent = DST.dtSent OR (SRC.dtSent IS NULL AND DST.dtSent IS NULL)) '
	   			 + ') '
	   			 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 12.12: Copy DocumentRepositoryEventEmail - Inbox Message Mappings Created', 0)
			   			
		-- update idParentInboxMessage mappings
		SET @sql = 'UPDATE DSTIM '
	   			 + '	SET idParentInboxMessage = TEMP.destinationId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblInboxMessage] SRCIM '
	   			 + 'INNER JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblInboxMessage] DSTIM '
	   			 + 'ON (SRCIM.dtCreated = DSTIM.dtCreated AND SRCIM.[message] = DSTIM.[message] AND SRCIM.idSite = ' + CAST(@idSiteSource AS NVARCHAR) + ') '
	   			 + 'INNER JOIN [dbo].[ClonePortal_IDMappings] TEMP '
	   			 + 'ON (TEMP.sourceId = SRCIM.idParentInboxMessage AND TEMP.object = ''inboxmessage'') '
	   			 + 'WHERE DSTIM.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
	   			 + 'AND SRCIM.idParentInboxMessage IS NOT NULL '
	   
		EXEC(@sql)
		
		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 12.13: Copy DocumentRepositoryEventEmail - Inbox Message Parent Ids Updated', 0)
	   
		END

		/*

		RETURN

		*/
	   
		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemClonePortal_DocumentRepositoryEventEmail_InsertedSuccessfully'

	END TRY

	/*

	CATCH AND LOG ERRORS

	*/

	BEGIN CATCH
		
		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 12: Copy DocumentRepositoryEventEmail - ERROR - ' + CAST(ERROR_LINE() AS NVARCHAR) + ' | ' + CAST(ERROR_MESSAGE() AS NVARCHAR(MAX)), 1)	
		SET @Return_Code = 1
		SET @Error_Description_Code	= 'SystemClonePortal_DocumentRepositoryEventEmail_PortalMigrationFailed'
		
	END CATCH
		
	SET XACT_ABORT OFF

END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.ClonePortal.Enrollment]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.ClonePortal.Enrollment]
GO


/*

CLONE PORTAL - ENROLLMENT DATA
OCCURS FOR FULL COPY

*/
CREATE PROCEDURE [System.ClonePortal.Enrollment]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,

	@idSiteSource			INT,
	@idSiteDestination		INT,
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50)
)
AS

BEGIN

	SET NOCOUNT ON
    SET XACT_ABORT ON

	INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 11.0: Copy Enrollment - Initialized', 0)
    
    BEGIN TRY	
    
		DECLARE @sql	NVARCHAR(MAX)

		/*
	  
		replace double slash with single slash. WHY DO WE DO THIS?!? Need to find out!
	 
		*/

		SET @sourceDBServer = REPLACE(@sourceDBServer, '\\', '\')
		SET @destinationDBServer = REPLACE(@destinationDBServer, '\\', '\')

		/*

		LEARNING PATH ENROLLMENTS

		*/

		-- copy learning path enrollments
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblLearningPathEnrollment] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idLearningPath, '
	   			 + '	idUser, '
	   			 + '	idRuleSetLearningPathEnrollment, '
	   			 + '	idTimezone, '
	   			 + '	title, '
	   			 + '	dtStart, '
	   			 + '	dtDue, '
	   			 + '	dtCompleted, '
	   			 + '	dtCreated, '
	   			 + '	dueInterval, '
	   			 + '	dueTimeframe, '
	   			 + '	dtLastSynchronized, '
				 + '	dtExpiresFromStart, '
				 + '	dtExpiresFromFirstLaunch, '
				 + '	dtFirstLaunch, '
				 + '	expiresFromStartInterval, '
				 + '	expiresFromStartTimeframe, '
				 + '	expiresFromFirstLaunchInterval, '
				 + '	expiresFromFirstLaunchTimeframe '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTLP.destinationId, '
	   			 + '	TTU.destinationId, '
	   			 + '	TTRSLPE.destinationId, '
	   			 + '	LPE.idTimezone, '	   			 
				 + '	LPE.title  + '' ##'' + CAST(LPE.idLearningPathEnrollment AS NVARCHAR) + ''##'', '
	   			 + '	LPE.dtStart, '
	   			 + '	LPE.dtDue, '
	   			 + '	LPE.dtCompleted, '
	   			 + '	LPE.dtCreated, '
	   			 + '	LPE.dueInterval, '
	   			 + '	LPE.dueTimeframe, '
	   			 + '	LPE.dtLastSynchronized, '
				 + '	LPE.dtExpiresFromStart, '
				 + '	LPE.dtExpiresFromFirstLaunch, '
				 + '	LPE.dtFirstLaunch, '
				 + '	LPE.expiresFromStartInterval, '
				 + '	LPE.expiresFromStartTimeframe, '
				 + '	LPE.expiresFromFirstLaunchInterval, '
				 + '	LPE.expiresFromFirstLaunchTimeframe '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblLearningPathEnrollment] LPE '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTLP on (TTLP.sourceId = LPE.idLearningPath AND TTLP.object = ''learningPaths'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = LPE.idUser AND TTU.object = ''users'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTRSLPE on (TTRSLPE.sourceId = LPE.idRuleSetLearningPathEnrollment AND TTRSLPE.object = ''rulesetlearningpathenrollment'') '
	   			 + 'WHERE LPE.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 11.1: Copy Enrollment - Learning Path Enrollments Inserted', 0)
	   
		-- insert the mapping for source to destination learning path enrollment ids
		SET @sql = ' INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + ' SELECT '
	   			 + '	SLPE.idLearningPathEnrollment, '
	   			 + '	DLPE.idLearningPathEnrollment, '
	   			 + '	''learningpathenrollment'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblLearningPathEnrollment] DLPE '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblLearningPathEnrollment] SLPE '
	   			 + 'ON (SLPE.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
				 + '	AND SLPE.title + '' ##'' + CAST(SLPE.idLearningPathEnrollment AS NVARCHAR) + ''##'' = DLPE.title) '
	   			 + 'WHERE DLPE.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
	   			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 11.2: Copy Enrollment - Learning Path Enrollment Mappings Created', 0)

		-- clean up the ##idLearningPathEnrollment## additions we made to learning path enrollment titles, we did that to uniquely distinguish courses
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblLearningPathEnrollment] SET '
				 + '	title = REPLACE(DLPE.title, '' ##'' + CAST(SLPE.sourceID AS NVARCHAR) + ''##'', '''') '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblLearningPathEnrollment] DLPE '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] SLPE ON SLPE.destinationID = DLPE.idLearningPathEnrollment AND SLPE.object = ''learningpathenrollment'' '
				 + 'WHERE DLPE.idSite = ' + CAST(@idSiteDestination AS NVARCHAR) + ' AND SLPE.sourceID IS NOT NULL'

		EXEC (@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 11.3: Copy Enrollment - Learning Path Enrollment Titles Cleaned Up', 0)	  

		-- copy learning path enrollment approvers
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblLearningPathEnrollmentApprover] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idLearningPath, '
	   			 + '	idUser '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTLP.destinationId, '
	   			 + '	TTU.destinationId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblLearningPathEnrollmentApprover] LPEA '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTLP on (TTLP.sourceId = LPEA.idLearningPath AND TTLP.object = ''learningPaths'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = LPEA.idUser AND TTU.object = ''users'') '
	   			 + 'WHERE LPEA.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 11.4: Copy Enrollment - Learning Path Enrollment Approvers Inserted', 0)	  

		/*

		ENROLLMENTS

		*/

		-- copy enrollments
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblEnrollment] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idCourse, '
	   			 + '	idUser, '
	   			 + '	idGroupEnrollment, '
	   			 + '	idRuleSetEnrollment, '
	   			 + '	idLearningPathEnrollment, '
	   			 + '	idTimezone, '
	   			 + '	isLockedByPrerequisites, '
	   			 + '	code, '
	   			 + '	revcode, '
	   			 + '	title, '
	   			 + '	dtStart, '
	   			 + '	dtDue, '
	   			 + '	dtExpiresFromStart, '
	   			 + '	dtExpiresFromFirstLaunch, '
	   			 + '	dtFirstLaunch, '
	   			 + '	dtCompleted, '
	   			 + '	dtCreated, '
	   			 + '	dtLastSynchronized, '
	   			 + '	dueInterval, '
	   			 + '	dueTimeframe, '
	   			 + '	expiresFromStartInterval, '
	   			 + '	expiresFromStartTimeframe, '
	   			 + '	expiresFromFirstLaunchInterval, '
	   			 + '	expiresFromFirstLaunchTimeframe, '
	   			 + '	idActivityImport, '
	   			 + '	credits '
	   			 + ') '
	   			 + ' SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTC.destinationId, '
	   			 + '	TTU.destinationId, '
	   			 + '	TTGE.destinationId, '
	   			 + '	TTRSE.destinationId, '
	   			 + '	TTLPE.destinationId, '
	   			 + '	E.idTimezone, '
	   			 + '	E.isLockedByPrerequisites, '
	   			 + '	E.code, '
	   			 + '	E.revcode, '
	   			 + '	E.title + '' ##'' + CAST(E.idEnrollment AS NVARCHAR) + ''##'', '
	   			 + '	E.dtStart, '
	   			 + '	E.dtDue, '
	   			 + '	E.dtExpiresFromStart, '
	   			 + '	E.dtExpiresFromFirstLaunch, '
	   			 + '	E.dtFirstLaunch, '
	   			 + '	E.dtCompleted, '
	   			 + '	E.dtCreated, '
	   			 + '	E.dtLastSynchronized, '
	   			 + '	E.dueInterval, '
	   			 + '	E.dueTimeframe, '
	   			 + '	E.expiresFromStartInterval, '
	   			 + '	E.expiresFromStartTimeframe, '
	   			 + '	E.expiresFromFirstLaunchInterval, '
	   			 + '	E.expiresFromFirstLaunchTimeframe, '
	   			 + '	TTAI.destinationId, '
	   			 + '	E.credits '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblEnrollment] E '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = E.idCourse AND TTC.object = ''courses'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = E.idUser AND TTU.object = ''users'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTGE on (TTGE.sourceId = E.idGroupEnrollment AND TTGE.object = ''groupenrollment'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTRSE on (TTRSE.sourceId = E.idRuleSetEnrollment AND TTRSE.object = ''rulesetenrollment'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTLPE on (TTLPE.sourceId = E.idLearningPathEnrollment AND TTLPE.object = ''learningpathenrollment'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTAI on (TTAI.sourceId = E.idActivityImport AND TTAI.object = ''activityimport'') '
	   			 + 'WHERE E.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 11.5: Copy Enrollment - Enrollments Inserted', 0)
	   		
		-- insert the mapping for source to destination enrollment id
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	SE.idEnrollment, '
	   			 + '	DE.idEnrollment, '
	   			 + '	''enrollment'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblEnrollment] DE '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblEnrollment] SE '
	   			 + 'ON (SE.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
				 + '	AND SE.title + '' ##'' + CAST(SE.idEnrollment AS NVARCHAR) + ''##'' = DE.title)'
	   			 + 'WHERE DE.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
	   			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 11.6: Copy Enrollment - Enrollment Mappings Created', 0)

		-- clean up the ##idEnrollment## additions we made to title, we did that to uniquely distinguish enrollments
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblEnrollment] SET '
				 + '	title = REPLACE(DE.title, '' ##'' + CAST(SE.sourceID AS NVARCHAR) + ''##'', '''') '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblEnrollment] DE '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] SE ON SE.destinationID = DE.idEnrollment AND SE.object = ''enrollment'' '				 
				 + 'WHERE DE.idSite = ' + CAST(@idSiteDestination AS NVARCHAR) + ' AND SE.sourceID IS NOT NULL'

		EXEC (@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 11.7: Copy Enrollment - Enrollment Titles Cleaned Up', 0)

		-- copy enrollment requests
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblEnrollmentRequest] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idCourse, '
	   			 + '	idUser, '
	   			 + '	timestamp, '
	   			 + '	dtApproved, '
	   			 + '	dtDenied, '
	   			 + '	idApprover, '
	   			 + '	rejectionComments '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTC.destinationId, '
	   			 + '	TTU.destinationId, '
	   			 + '	ER.timestamp, '
	   			 + '	ER.dtApproved, '
	   			 + '	ER.dtDenied, '
	   			 + '	TTUA.destinationId, '
	   			 + '	ER.rejectionComments '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblEnrollmentRequest] ER '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = ER.idCourse AND TTC.object = ''courses'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = ER.idUser AND TTU.object = ''users'') '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTUA on (TTU.sourceId = ER.idApprover AND TTU.object = ''users'') '
	   			 + 'WHERE ER.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   
		EXEC(@sql)
		
		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 11.8: Copy Enrollment - Enrollment Requests Inserted', 0)

		/*

		LESSON (MODULE) DATA

		*/

		-- copy lesson (module) data records
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblData-Lesson] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idEnrollment, '
	   			 + '	idLesson, '
	   			 + '	idTimezone, '
	   			 + '	title, '
	   			 + '	revcode, '
	   			 + '	[order], '
	   			 + '	contentTypeCommittedTo, '
	   			 + '	dtCommittedToContentType, '
	   			 + '	dtCompleted, '
	   			 + '	resetForContentChange, '
	   			 + '	preventPostCompletionLaunchForContentChange '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTE.destinationId, '
	   			 + '	TTL.destinationId, '
	   			 + '	DL.idTimezone, '
				 + '	DL.title + '' ##'' + CAST(DL.[idData-Lesson] AS NVARCHAR) + ''##'', '
	   			 + '	DL.revcode, '
	   			 + '	DL.[order], '
	   			 + '	DL.contentTypeCommittedTo, '
	   			 + '	DL.dtCommittedToContentType, '
	   			 + '	DL.dtCompleted, '
	   			 + '	DL.resetForContentChange, '
	   			 + '	DL.preventPostCompletionLaunchForContentChange '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblData-Lesson] DL '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTE on (TTE.sourceId = DL.idEnrollment AND TTE.object = ''enrollment'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTL on (TTL.sourceId = DL.idLesson AND TTL.object = ''lesson'') '
	   			 + 'WHERE DL.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 11.9: Copy Enrollment - Lesson (Module) Data Inserted', 0)
	   		
		-- insert the mapping for source to destination lesson data id
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	SD.[idData-Lesson], '
	   			 + '	DD.[idData-Lesson], '
	   			 + '	''datalesson'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblData-Lesson] DD '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblData-Lesson] SD '
	   			 + 'ON (SD.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
				 + '	AND SD.title + '' ##'' + CAST(SD.[idData-Lesson] AS NVARCHAR) + ''##'' = DD.title)'
	   			 + 'WHERE DD.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
	   			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 11.10: Copy Enrollment - Lesson (Module) Data Mappings Created', 0)	   	   

		-- clean up the ##idData-Lesson## additions we made to title, we did that to uniquely distinguish lesson data
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblData-Lesson] SET '
				 + '	title = REPLACE(DLD.title, '' ##'' + CAST(SLD.sourceID AS NVARCHAR) + ''##'', '''') '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblData-Lesson] DLD '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] SLD ON SLD.destinationID = DLD.[idData-Lesson] AND SLD.object = ''datalesson'' '				 
				 + 'WHERE DLD.idSite = ' + CAST(@idSiteDestination AS NVARCHAR) + ' AND SLD.sourceID IS NOT NULL'

		EXEC (@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 11.11: Copy Enrollment - Lesson (Module) Data Titles Cleaned Up', 0)

		/*

		RETURN

		*/	   
			   
		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemClonePortal_Enrollment_InsertedSuccessfully'

	END TRY

	/*

	CATCH AND LOG ERRORS

	*/

	BEGIN CATCH
			
		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 11: Copy Enrollment - ERROR - ' + CAST(ERROR_LINE() AS NVARCHAR) + ' | ' + CAST(ERROR_MESSAGE() AS NVARCHAR(MAX)), 1)
		SET @Return_Code = 1
		SET @Error_Description_Code	= 'SystemClonePortal_Enrollment_PortalMigrationFailed'
		
	END CATCH
		
	SET XACT_ABORT OFF

END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.ClonePortal.GetIDMappings]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.ClonePortal.GetIDMappings]
GO


/*

Gets all records from the portal clone id mapping table.

*/
CREATE PROCEDURE [System.ClonePortal.GetIDMappings]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,	
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50),
	@forFolder				NVARCHAR(50)	= NULL
)
AS

BEGIN

    SET NOCOUNT ON

	IF (@forFolder IS NULL OR @forFolder = '')
		BEGIN

		SELECT * FROM [dbo].[ClonePortal_IDMappings]

		END
	
	IF (@forFolder = '_config')
		BEGIN

		SELECT * 
		FROM [dbo].[ClonePortal_IDMappings] 
		WHERE [object] IN ('catalog',
						   'certificates',
						   'certificationmodulerequirement',
						   'courses',
						   'datalesson',
						   'emailNotifications',
						   'groups',
						   'learningPaths',
						   'standuptraining',
						   'users')

		END

	IF (@forFolder = '_log')
		BEGIN

		SELECT * 
		FROM [dbo].[ClonePortal_IDMappings] 
		WHERE [object] IN ('datalesson',						   
						   'users')

		END

	IF (@forFolder = 'warehouse')
		BEGIN

		SELECT * 
		FROM [dbo].[ClonePortal_IDMappings] 
		WHERE [object] IN ('courses',
						   'groups',
						   'learningPaths',
						   'lesson',
						   'site',
						   'users')

		END


	SET @Return_Code = 0
	SET @Error_Description_Code	= 'SystemClonePortal_Site_InsertedSuccessfully'

END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.ClonePortal.Group]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.ClonePortal.Group]
GO


/*

CLONE PORTAL - GROUP DATA
OCCURS FOR FULL AND PARTIAL COPY

*/
CREATE PROCEDURE [System.ClonePortal.Group]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,

	@idSiteSource			INT,
	@idSiteDestination		INT,
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50)
)
AS

BEGIN

	SET NOCOUNT ON
	SET XACT_ABORT ON

	INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 3.0: Copy Groups - Initialized', 0)
    
	BEGIN TRY	
    
		DECLARE @sql NVARCHAR(MAX)

		/*
	  
		replace double slash with single slash. WHY DO WE DO THIS?!? Need to find out!
	 
		*/

		SET @sourceDBServer = REPLACE(@sourceDBServer, '\\', '\')
		SET @destinationDBServer = REPLACE(@destinationDBServer, '\\', '\')
	  
		/*

		GROUPS

		*/
                
		-- copy groups
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblGroup] '
				 + '( '
				 + '	idSite, '
				 + '	name, '
				 + '	primaryGroupToken, '
				 + '	objectGUID, '
				 + '	distinguishedName, '
				 + '	avatar, '
				 + '	shortDescription, '
				 + '	longDescription, '
				 + '	isSelfJoinAllowed, '
				 + '	isFeedActive, '
				 + '	isFeedModerated, '
				 + '	membershipIsPublicized, '
				 + '	searchTags '
				 + ') '
				 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	G.name  + '' ##'' + CAST(G.idGroup AS NVARCHAR) + ''##'', '
				 + '	G.primaryGroupToken, '
				 + '	G.objectGUID, '
				 + '	G.distinguishedName, '
				 + '	G.avatar, '
				 + '	G.shortDescription, '
				 + '	G.longDescription, '
				 + '	G.isSelfJoinAllowed, '
				 + '	G.isFeedActive, '
				 + '	G.isFeedModerated, '
				 + '	G.membershipIsPublicized, '
				 + '	G.searchTags '
				 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblGroup] G '
				 + 'WHERE G.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
                										
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 3.1: Copy Groups - Groups Inserted', 0)

		-- insert the mapping for source to destination group id
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	SRC.idGroup, '
	   			 + '	DST.idGroup, '
	   			 + '	''groups'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblGroup] DST '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblGroup] SRC '
	   			 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
				 + '	AND SRC.name + '' ##'' + CAST(SRC.idGroup AS NVARCHAR) + ''##'' = DST.name) '
	   			 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
				 		
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 3.2: Copy Groups - Group Mappings Created', 0)

		-- clean up the ##idGroup## additions we made to group titles, we did that to uniquely distinguish groups
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblGroup] SET '
				 + '	name = REPLACE(DG.name, '' ##'' + CAST(SG.sourceID AS NVARCHAR) + ''##'', '''') '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblGroup] DG '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] SG ON SG.destinationID = DG.idGroup AND SG.object = ''groups'' '
				 + 'WHERE DG.idSite = ' + CAST(@idSiteDestination AS NVARCHAR) + ' AND SG.sourceID IS NOT NULL '

		EXEC (@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 3.3: Copy Groups - Group Titles Cleaned Up', 0)
			   
		-- copy group languages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblGroupLanguage] '
	   				+ '( '
	   				+ '	idSite, '
	   				+ '	idGroup, '
	   				+ '	idLanguage, '
	   				+ '	name, '
	   				+ '	shortDescription, '
	   				+ '	longDescription, '
	   				+ '	searchTags '
	   				+ ') '
	   				+ 'SELECT '
	   				+	CAST(@idSiteDestination AS NVARCHAR) + ', '
	   				+ '	TTG.destinationId, '
	   				+ '	GL.idLanguage, '
	   				+ '	GL.name, '
	   				+ '	GL.shortDescription, '
	   				+ '	GL.longDescription, '
	   				+ '	GL.searchTags '
	   				+ 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblGroupLanguage] GL '
	   				+ 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTG on (TTG.sourceId = GL.idGroup AND TTG.object = ''groups'') '
	   				+ 'WHERE GL.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
	   			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 3.4: Copy Groups - Group Languages Inserted', 0)	 
		
		-- copy catalog access to group links
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCatalogAccessToGroupLink] '
	     		 + '( '
	     		 + '	idSite, '
	     		 + '	idGroup, '
	     		 + '	idCatalog, '
	     		 + '	created '
	     		 + ') '
	     		 + 'SELECT '
	     		 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	     		 + '	TTG.destinationId, '
	     		 + '	TTC.destinationId, '
	     		 + '	CATGL.created '
	     		 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCatalogAccessToGroupLink] CATGL '
	     		 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = CATGL.idCatalog AND TTC.object = ''catalog'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTG on (TTG.sourceId = CATGL.idGroup AND TTG.object = ''groups'') '
				 + 'WHERE CATGL.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
	   	
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 3.5: Copy Groups - Group To Catalog Access Inserted', 0)

		-- copy group enrollments
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblGroupEnrollment] '
	   			 + '( '
				 + '	idSite, '
				 + '	idCourse, '
				 + '	idGroup, '
				 + '	idTimezone, '
				 + '	isLockedByPrerequisites, '
				 + '	dtStart, '
				 + '	dtCreated, '
				 + '	dueInterval, '
				 + '	dueTimeframe, '
				 + '	expiresFromStartInterval, '
				 + '	expiresFromStartTimeframe, '
				 + '	expiresFromFirstLaunchInterval, '
				 + '	expiresFromFirstLaunchTimeframe '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	TTC.destinationId, '
				 + '	TTG.destinationId, '
				 + '	GE.idTimezone, '
				 + '	GE.isLockedByPrerequisites, '
				 + '	GE.dtStart, '
				 + '	GE.dtCreated, '
				 + '	GE.dueInterval, '
				 + '	GE.dueTimeframe, '
				 + '	GE.expiresFromStartInterval, '
				 + '	GE.expiresFromStartTimeframe, '
				 + '	GE.expiresFromFirstLaunchInterval, '
				 + '	GE.expiresFromFirstLaunchTimeframe '
				 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblGroupEnrollment] GE '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTG on (TTG.sourceId = GE.idGroup AND TTG.object = ''groups'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = GE.idCourse AND TTC.object = ''courses'') '
	   			 + 'WHERE GE.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   										
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 3.6: Copy Groups - Group Enrollments Inserted', 0)
	   
		-- insert the mapping for source to destination group enrollment id
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
				 + '( '
				 + '	sourceId, '
				 + '	destinationId, '
				 + '	[object] '
				 + ') '
				 + 'SELECT '
				 + '	SRC.idGroupEnrollment, '
				 + '	DST.idGroupEnrollment, '
				 + '	''groupenrollment'' '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblGroupEnrollment] DST '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTG on (TTG.destinationId = DST.idGroup AND TTG.object = ''groups'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.destinationId = DST.idCourse AND TTC.object = ''courses'') '
				 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblGroupEnrollment] SRC '
				 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
				 + '	AND (SRC.dtStart = DST.dtStart)'
				 + '	AND (SRC.dtCreated = DST.dtCreated)'
				 + '	AND (SRC.idGroup = TTG.sourceId) '
				 + '	AND (SRC.idCourse = TTC.sourceId)) '
				 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 3.7: Copy Groups - Group Enrollment Mappings Created', 0)

		/*

		RETURN

		*/

		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemClonePortal_Group_InsertedSuccessfully'

	END TRY

	/*

	CATCH AND LOG ERRORS

	*/

	BEGIN CATCH
			
		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 3: Copy Groups - ERROR - ' + CAST(ERROR_LINE() AS NVARCHAR) + ' | ' + CAST(ERROR_MESSAGE() AS NVARCHAR(MAX)), 1)
		SET @Return_Code = 1
		SET @Error_Description_Code	= 'SystemClonePortal_Group_PortalMigrationFailed'
		
	END CATCH
		
	SET XACT_ABORT OFF

END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.ClonePortal.InstructorLedTraining]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.ClonePortal.InstructorLedTraining]
GO


/*

CLONE PORTAL - INSTRUCTOR LED TRAINING DATA
OCCURS FOR FULL AND PARTIAL COPY

*/
CREATE PROCEDURE [System.ClonePortal.InstructorLedTraining]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,

	@fullCopyFlag			BIT,			-- needed for ILT Sessions
	@idSiteSource			INT,
	@idSiteDestination		INT,
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50)
)
AS

BEGIN

	SET NOCOUNT ON
	SET XACT_ABORT ON

	INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 6.0: Copy ILT - Initialized', 0)
    
	BEGIN TRY	
    
		DECLARE @sql	NVARCHAR(MAX)
		DECLARE @subSql	NVARCHAR(MAX)

		/*
	  
		replace double slash with single slash. WHY DO WE DO THIS?!? Need to find out!
	 
		*/

		SET @sourceDBServer = REPLACE(@sourceDBServer, '\\', '\')
		SET @destinationDBServer = REPLACE(@destinationDBServer, '\\', '\')

		/*

		ILT MODULES

		*/

		-- copy ilt modules
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblStandUpTraining] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	title, '
	   			 + '	[description], '
	   			 + '	isStandaloneEnroll, '
	   			 + '	isRestrictedEnroll, '
	   			 + '	isRestrictedDrop, '
	   			 + '	searchTags, '
	   			 + '	isDeleted, '
	   			 + '	dtDeleted, '
	   			 + '	avatar, '
	   			 + '	cost, '
	   			 + '	objectives, '
	   			 + '	shortcode, '
				 + '	dtCreated, '
				 + '	dtModified '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	ST.title, '
	   			 + '	ST.[description], '
	   			 + '	ST.isStandaloneEnroll, '
	   			 + '	ST.isRestrictedEnroll, '
	   			 + '	ST.isRestrictedDrop, '
	   			 + '	ST.searchTags, '
	   			 + '	ST.isDeleted, '
	   			 + '	ST.dtDeleted, '
	   			 + '	ST.avatar, '
	   			 + '	ST.cost, '
	   			 + '	ST.objectives, '
	   			 + '	ST.shortcode, '
				 + '	ST.dtCreated, '
				 + '	ST.dtModified '
	   			 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblStandUpTraining] ST '
	   			 + 'WHERE ST.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
	   			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 6.1: Copy ILT - ILT Modules Inserted', 0)
	   
		-- insert the mapping for source to destination ilt module ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	SRC.idStandUpTraining, '
	   			 + '	DST.idStandUpTraining, '
	   			 + '	''standuptraining'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblStandUpTraining] DST '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblStandUpTraining] SRC '
	   			 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
	   			 + '	AND (SRC.title = DST.title) '
	   			 + '	AND (SRC.dtCreated = DST.dtCreated) '
	   			 + '	AND (SRC.dtModified = DST.dtModified)) '
	   			 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)

		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 6.2: Copy ILT - ILT Module Mappings Created', 0)
	   				
		-- copy ilt module languages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblStandUpTrainingLanguage] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idStandUpTraining, '
	   			 + '	idLanguage, '
	   			 + '	title, '
	   			 + '	[description], '
	   			 + '	searchTags, '
	   			 + '	objectives '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTST.destinationId, '
	   			 + '	STL.idLanguage, '
	   			 + '	STL.title, '
	   			 + '	STL.[description], '
	   			 + '	STL.searchTags, '
	   			 + '	STL.objectives '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblStandUpTrainingLanguage] STL '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTST ON (TTST.sourceId = STL.idStandUpTraining AND TTST.object = ''standuptraining'') '
	   			 + 'WHERE STL.idSite = ' + CAST(@idSiteSource AS NVARCHAR)

		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 6.3: Copy ILT - ILT Module Languages Inserted', 0)	      

		/*

		ILT SESSIONS

		*/

		-- copy web meeting organizers - only for full copy
		IF(@fullCopyFlag = 1)
			BEGIN

			SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblWebMeetingOrganizer] '
					 + '( '
					 + '	idSite, '
					 + '	idUser, '
					 + '	organizerType, '
					 + '	username, '
					 + '	[password] '
					 + ') '
					 + 'SELECT '
					 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
					 + '	TTU.destinationId, '
					 + '	WMO.organizerType, '
					 + '	WMO.username, '
					 + '	WMO.[password] '
					 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblWebMeetingOrganizer] WMO '
					 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU ON (TTU.sourceId = WMO.idUser AND TTU.object = ''users'') '
	   				 + 'WHERE WMO.idSite = ' + CAST(@idSiteSource AS NVARCHAR)

			EXEC(@sql)

			INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 6.4: Copy ILT - Web Meeting Organizers Inserted', 0)

			-- insert the mapping for source to destination web meeting organizer ids
			SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   				 + '( '
	   				 + '	sourceId, '
	   				 + '	destinationId, '
	   				 + '	[object] '
	   				 + ') '
	   				 + 'SELECT '
	   				 + '	SRC.idWebMeetingOrganizer, '
	   				 + '	DST.idWebMeetingOrganizer, '
	   				 + '	''webmeetingorganizer'' '
	   				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblWebMeetingOrganizer] DST '
	   				 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblWebMeetingOrganizer] SRC '
	   				 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
	   				 + '	AND (SRC.username = DST.username) '
	   				 + '	AND (SRC.[password] = DST.[password])) '	   				 
	   				 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)

			EXEC(@sql)

			INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 6.5: Copy ILT - Web Meeting Organizer Mappings Created', 0)

			END

		-- copy ilt sessions
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblStandUpTrainingInstance] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idStandupTraining, '
	   			 + '	title, '
	   			 + '	[description], '
	   			 + '	seats, '
	   			 + '	waitingSeats, '
	   			 + '	[type], '
	   			 + '	urlRegistration, '
	   			 + '	urlAttend, '
	   			 + '	city, '
	   			 + '	province, '
	   			 + '	locationDescription, '
	   			 + '	isDeleted, '
	   			 + '	dtDeleted, '
	   			 + '	integratedObjectKey, '
	   			 + '	hostUrl, '
	   			 + '	genericJoinUrl, '
	   			 + '	meetingPassword, '
	   			 + '	idWebMeetingOrganizer ' -- maps to a web meeting organizer, or null if not full copy
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTST.destinationId, '
	   			 + '	STI.title  + '' ##'' + CONVERT(NVARCHAR, STI.idStandUpTrainingInstance) + ''##'', '
	   			 + '	STI.[description], '
	   			 + '	STI.seats, '
	   			 + '	STI.waitingSeats, '
	   			 + '	STI.[type], '
	   			 + '	STI.urlRegistration, '
	   			 + '	STI.urlAttend, '
	   			 + '	STI.city, '
	   			 + '	STI.province, '
	   			 + '	STI.locationDescription, '
	   			 + '	STI.isDeleted, '
	   			 + '	STI.dtDeleted, '
	   			 + '	STI.integratedObjectKey, '
	   			 + '	STI.hostUrl, '
	   			 + '	STI.genericJoinUrl, '
	   			 + '	STI.meetingPassword, '

		IF(@fullCopyFlag = 1)
			BEGIN

			SET @sql = @sql + '	TTU.destinationId '

			END
		
		ELSE
			
			BEGIN

			SET @sql = @sql + '	NULL '

			END

		SET @sql = @sql + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblStandUpTrainingInstance] STI '
	   					+ 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTST ON (TTST.sourceId = STI.idStandUpTraining AND TTST.object = ''standuptraining'') '

		IF(@fullCopyFlag = 1)
			BEGIN

			SET @sql = @sql + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU ON (TTU.sourceId = STI.idWebMeetingOrganizer AND TTU.object = ''users'' ) '

			END

		SET @sql = @sql + 'WHERE STI.idSite = ' + CAST(@idSiteSource AS NVARCHAR)

		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 6.6: Copy ILT - ILT Sessions Inserted', 0)

		-- insert the mapping for source to destination ilt session ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
				 + '( '
				 + '	sourceId, '
				 + '	destinationId, '
				 + '	[object] '
				 + ') '
				 + 'SELECT '
				 + '	SRC.idStandUpTrainingInstance, '
				 + '	DST.idStandUpTrainingInstance, '
				 + '	''session'' '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblStandUpTrainingInstance] DST '
				 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblStandUpTrainingInstance] SRC '
				 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
				 + '	AND SRC.title + '' ##'' + CONVERT(NVARCHAR, SRC.idStandUpTrainingInstance) + ''##'' = DST.title) '
				 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)

		EXEC (@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 6.7: Copy ILT - ILT Session Mappings Created', 0)

		-- clean up the ##idStandUpTrainingInstance## additions we made to titles, we did that to uniquely distinguish sessions
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblStandUpTrainingInstance] SET '
				 + '	title = REPLACE(DSTI.title, '' ##'' + CONVERT(NVARCHAR, SSTI.sourceID) + ''##'', '''') '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblStandUpTrainingInstance] DSTI '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] SSTI ON SSTI.destinationID = DSTI.idStandUpTrainingInstance AND SSTI.object = ''session'' '
				 + 'WHERE SSTI.sourceID IS NOT NULL AND DSTI.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)

		EXEC (@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 6.8: Copy ILT - ILT Session Titles Cleaned Up', 0)		
						
		-- copy ilt session languages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblStandUpTrainingInstanceLanguage] '
				 + '( '
				 + '	idSite, '
				 + '	idStandUpTrainingInstance, '
				 + '	idLanguage, '
				 + '	title, '
				 + '	[description], '
				 + '	locationDescription '
				 + ') '
				 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	TTSTI.destinationId, '
				 + '	STIL.idLanguage, '
				 + '	STIL.title, '
				 + '	STIL.[description], '
				 + '	STIL.locationDescription '
				 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblStandUpTrainingInstanceLanguage] STIL '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTSTI ON (TTSTI.sourceId = STIL.idStandUpTrainingInstance AND TTSTI.object = ''session'') '
				 + 'WHERE STIL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
				
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 6.9: Copy ILT - ILT Session Languages Inserted', 0)

		-- copy resource to object links
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblResourceToObjectLink] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idResource, '
	   			 + '	idObject, '
	   			 + '	objectType, '
	   			 + '	dtStart, '
	   			 + '	dtEnd, '
	   			 + '	isOutsideUse, '
	   			 + '	isMaintenance '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTR.destinationId, '
	   			 + '	CASE RTOL.objectType '
	   			 + '	WHEN ''standuptraining'' THEN '
	   			 + '		TTSTI.destinationId '
	   			 + '	ELSE '
	   			 + '		0 '
	   			 + '	END, '
	   			 + '	RTOL.objectType, '
	   			 + '	RTOL.dtStart, '
	   			 + '	RTOL.dtEnd, '
	   			 + '	RTOL.isOutsideUse, '
	   			 + '	RTOL.isMaintenance '
	   			 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblResourceToObjectLink] RTOL '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTR ON (TTR.sourceId = RTOL.idResource AND TTR.object = ''resource'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTSTI ON (TTSTI.sourceId = RTOL.idObject AND TTSTI.object = ''session'') '
	   			 + 'WHERE RTOL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   				
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 6.10: Copy ILT - ILT Session To Resource Links Inserted', 0)

		-- copy ilt session meeting times
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblStandUpTrainingInstanceMeetingTime] '
	   			 + '( '
	   			 + '	idStandupTrainingInstance, '
	   			 + '	idSite, '
	   			 + '	dtStart, '
	   			 + '	dtEnd, '
	   			 + '	[minutes], '
	   			 + '	idTimezone '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	TTSTI.destinationId, '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	STIMT.dtStart, '
	   			 + '	STIMT.dtEnd, '
	   			 + '	STIMT.[minutes], '
	   			 + '	STIMT.idTimezone '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblStandUpTrainingInstanceMeetingTime] STIMT '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTSTI on (TTSTI.sourceId = STIMT.idStandUpTrainingInstance AND TTSTI.object = ''session'') '
	   			 + 'WHERE STIMT.idSite = ' + CAST(@idSiteSource AS NVARCHAR)

		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 6.11: Copy ILT - ILT Session To Meeting Time Links Inserted', 0)

		-- insert instructor links and roster links if full copy
		IF(@fullCopyFlag = 1)

			BEGIN

			-- copy instructor links
			SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblStandupTrainingInstanceToInstructorLink] '
	   				 + '( '
	   				 + '	idInstructor, '
	   				 + '	idStandupTrainingInstance, '
	   				 + '	idSite '
	   				 + ') '
	   				 + 'SELECT '
	   				 + '	TTU.destinationId, '
	   				 + '	TTSTI.destinationId, '
	   				 +		CAST(@idSiteDestination AS NVARCHAR) + ' '
	   				 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblStandupTrainingInstanceToInstructorLink] STITUL '	   				 
	   				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTSTI on (TTSTI.sourceId = STITUL.idStandUpTrainingInstance AND TTSTI.object = ''session'') '
	   				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = STITUL.idInstructor AND TTU.object = ''users'') '
	   				 + 'WHERE STITUL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 

			EXEC(@sql)

			INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 6.12: Copy ILT - ILT Session To Instructor Links Inserted', 0)

			-- copy session roster
			SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblStandupTrainingInstanceToUserLink] '
	   				 + '( '
	   				 + '	idUser, '
	   				 + '	idStandupTrainingInstance, '
	   				 + '	completionStatus, '
	   				 + '	successStatus, '
	   				 + '	score, '
	   				 + '	isWaitingList, '
	   				 + '	dtCompleted, '
	   				 + '	registrantKey, '
	   				 + '	registrantEmail, '
	   				 + '	joinUrl, '
	   				 + '	waitlistOrder '
	   				 + ') '
	   				 + 'SELECT '
	   				 + '	TTU.destinationId, '
	   				 + '	TTSTI.destinationId, '
	   				 + '	STITUL.completionStatus, '
	   				 + '	STITUL.successStatus, '
	   				 + '	STITUL.score, '
	   				 + '	STITUL.isWaitingList, '
	   				 + '	STITUL.dtCompleted, '
	   				 + '	STITUL.registrantKey, '
	   				 + '	STITUL.registrantEmail, '
	   				 + '	STITUL.joinUrl, '
	   				 + '	STITUL.waitlistOrder '
	   				 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblStandupTrainingInstanceToUserLink] STITUL '
					 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblUser] U ON U.idUser = STITUL.idUser '
	   				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTSTI ON (TTSTI.sourceId = STITUL.idStandUpTrainingInstance AND TTSTI.object = ''session'') '
	   				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU ON (TTU.sourceId = STITUL.idUser AND TTU.object = ''users'') '
	   				 + 'WHERE U.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
					 	   
			EXEC(@sql)

			INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 6.13: Copy ILT - ILT Session Roster Links Inserted', 0)	   

			END	

		/*

		LESSON TO CONTENT LINKS

		*/

		-- copy lesson to content links
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblLessonToContentLink] '
				 + '( '
				 + '	idSite, '
				 + '	idLesson, '
				 + '	idContentType, '
				 + '	idAssignmentDocumentType, '
				 + '	assignmentResourcePath, '
				 + '	allowSupervisorsAsProctor, '
				 + '	allowCourseExpertsAsProctor, '
				 + '	idObject '
				 + ') '
				 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	TTL.destinationId, '
				 + '	LTCL.idContentType, '
				 + '	LTCL.idAssignmentDocumentType, '
				 + '	LTCL.assignmentResourcePath, '
				 + '	LTCL.allowSupervisorsAsProctor, '
				 + '	LTCL.allowCourseExpertsAsProctor, '
				 + '	CASE LTCL.idContentType '
				 + '	WHEN 1 THEN '
				 + '		TTCP.destinationId '
				 + '	WHEN 2 THEN '
				 + '		TTST.destinationId '
				 + '	WHEN 3 THEN '
				 + '		NULL '
				 + '	WHEN 4 THEN '
				 + '		TTCP.destinationId '
				 + '	END '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblLessonToContentLink] LTCL '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTL ON (TTL.sourceId = LTCL.idLesson AND TTL.object = ''lesson'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCP ON (TTCP.sourceId = LTCL.idObject AND TTCP.object = ''contentpackage'') '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTST ON (TTST.sourceId = LTCL.idObject AND TTST.object = ''standuptraining'') '
				 + 'WHERE LTCL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
		  
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 6.14: Copy ILT - Lesson to Content Links Inserted', 0)	   

		/*

		RETURN

		*/
	   
		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemClonePortal_InstructorLedTraining_InsertedSuccessfully'

	END TRY

	/*

	CATCH AND LOG ERRORS

	*/

	BEGIN CATCH
			
		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 6: Copy ILT - ERROR - ' + CAST(ERROR_LINE() AS NVARCHAR) + ' | ' + CAST(ERROR_MESSAGE() AS NVARCHAR(MAX)), 1)
		SET @Return_Code = 1
		SET @Error_Description_Code	= 'SystemClonePortal_InstructorLedTraining_PortalMigrationFailed'
		
	END CATCH
		
	SET XACT_ABORT OFF

END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.ClonePortal.PurchaseTransaction]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.ClonePortal.PurchaseTransaction]
GO


/*

CLONE PORTAL - PURCHASE / TRANSACTION DATA
OCCURS FOR FULL COPY

*/
CREATE PROCEDURE [System.ClonePortal.PurchaseTransaction]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,

	@idSiteSource			INT,
	@idSiteDestination		INT,
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50)
)
AS

BEGIN

	SET NOCOUNT ON
    SET XACT_ABORT ON

	INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 14.0: Copy PurchaseTransaction - Initialized', 0)
    
    BEGIN TRY	
    
		DECLARE @sql NVARCHAR(MAX)

		/*
	  
		replace double slash with single slash. WHY DO WE DO THIS?!? Need to find out!
	 
		*/

		SET @sourceDBServer = REPLACE(@sourceDBServer, '\\', '\')
		SET @destinationDBServer = REPLACE(@destinationDBServer, '\\', '\')	 

		-- copy purchases
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblPurchase] '
	   			 + '( '
	   			 + '	idUser, '
	   			 + '	orderNumber, '
	   			 + '	[timeStamp], '
	   			 + '	ccnum, '
	   			 + '	currency, '
	   			 + '	idSite,	'
	   			 + '	dtPending, '
	   			 + '	failed, '
	   			 + '	transactionId '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	TTU.destinationId, '
	   			 + '	TP.orderNumber, '
	   			 + '	TP.[timeStamp], '
	   			 + '	TP.ccnum, '
	   			 + '	TP.currency, '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TP.dtPending, '
	   			 + '	TP.failed, '
	   			 + '	TP.transactionId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblPurchase] TP '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = TP.idUser AND TTU.object = ''users'') '
	   			 + 'WHERE TP.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   								
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 14.1: Copy PurchaseTransaction - Purchases Inserted', 0)
	   
		-- insert the mapping for source to destination purchase ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	SP.idPurchase, '
	   			 + '	DP.idPurchase, '
	   			 + '	''purchase'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblPurchase] DP '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblPurchase] SP '
	   			 + 'ON (SP.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   			 + '	AND (SP.orderNumber = DP.orderNumber ) '
	   			 + '	AND (SP.timeStamp = DP.timeStamp OR (SP.timeStamp IS NULL AND DP.timeStamp IS NULL)) '
	   			 + '	AND (SP.dtPending = DP.dtPending OR (SP.dtPending IS NULL AND DP.dtPending IS NULL))) '
	   			 + 'WHERE DP.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
	   			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 14.2: Copy PurchaseTransaction - Purchase Mappings Created', 0)

		-- insert records from source site to destination table TransactionItem
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblTransactionItem] '
	   			 + '( '
	   			 + '	idParentTransactionItem, '
	   			 + '	idPurchase, '
	   			 + '	idEnrollment, '
	   			 + '	idUser, '
	   			 + '	userName, '
	   			 + '	idAssigner, '
	   			 + '	assignerName, '
	   			 + '	itemId, '
	   			 + '	itemName, '
	   			 + '	itemType, '
	   			 + '	description, '
	   			 + '	cost, '
	   			 + '	paid, '
	   			 + '	idCouponCode, '
	   			 + '	couponCode, '
	   			 + '	dtAdded, '
	   			 + '	confirmed, '
	   			 + '	idSite, '
				 + '	idIltSession '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	NULL, '     -- this null value of idParentTransactionItem will be updated from temporary TransactionItemIdMappings table below
	   			 + '	TTP.destinationId, '	   			 
	   			 + '	CASE '
	   			 + '	WHEN TI.itemType = 1 THEN '
	   			 + '		TTE.destinationId '
	   			 + '	WHEN TI.itemType = 3 THEN '
	   			 + '		TTLPE.destinationId '
	   			 + '	ELSE '
	   			 + '		NULL '
	   			 + '	END, '
	   			 + '	TTU.destinationId, '
	   			 + '	TI.userName, '
	   			 + '	TTUA.destinationId, '
	   			 + '	TI.assignerName, '	   			 
	   			 + '	CASE '
	   			 + '	WHEN TI.itemType = 1 THEN '
	   			 + '		TTC.destinationId '
	   			 + '	WHEN TI.itemType = 2 THEN '
	   			 + '		TTCA.destinationId '
	   			 + '	WHEN TI.itemType = 3 THEN '
	   			 + '		TTLP.destinationId '
	   			 + '	WHEN TI.itemType = 4 THEN '
	   			 + '		TTST.destinationId '
	   			 + '	WHEN TI.itemType = 5 THEN '
	   			 + '		TTG.destinationId '
	   			 + '	ELSE '
	   			 + '		NULL '
	   			 + '	END, '
				 + '	TI.itemName  + '' ##'' + CAST(TI.idTransactionItem AS NVARCHAR) + ''##'', '
	   			 + '	TI.itemType, '
	   			 + '	TI.description, '
	   			 + '	TI.cost, '
	   			 + '	TI.paid, '
	   			 + '	TTCC.destinationId, '
	   			 + '	TI.couponCode, '
	   			 + '	TI.dtAdded, '
	   			 + '	TI.confirmed, '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	TTSTI.destinationId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblTransactionItem] TI '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTP on (TTP.sourceId = TI.idPurchase AND TTP.object = ''purchase'') '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TI.itemType = 1 AND TTC.sourceId = TI.itemId AND TTC.object = ''courses'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTE on (TI.itemType = 1 AND TTE.sourceId = TI.idEnrollment AND TTE.object = ''enrollment'') '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCA on (TI.itemType = 2 AND TTCA.sourceId = TI.itemId AND TTCA.object = ''catalog'') '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTLP on (TI.itemType = 3 AND TTLP.sourceId = TI.itemId AND TTLP.object = ''learningPaths'') '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTLPE on (TI.itemType = 3 AND TTLPE.sourceId = TI.idEnrollment AND TTLPE.object = ''learningpathenrollment'') '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTST on (TI.itemType = 4 AND TTST.sourceId = TI.itemId AND TTST.object = ''standuptraining'') '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTG on (TI.itemType = 5 AND TTG.sourceId = TI.itemId AND TTG.object = ''groups'') '	   			 
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = TI.idUser AND TTU.object = ''users'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTUA on (TTUA.sourceId = TI.idAssigner AND TTUA.object = ''users'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCC on (TTCC.sourceId = TI.idCouponCode AND TTCC.object = ''couponcode'') '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTSTI on (TTSTI.sourceId = TI.idIltSession AND TTSTI.object = ''session'') '
	   			 + 'WHERE TI.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 14.3: Copy PurchaseTransaction - Transaction Items Inserted', 0)
	   
		-- insert the mapping for source to destination transaction item ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	STI.[idTransactionItem], '
	   			 + '	DTI.[idTransactionItem], '
	   			 + '	''transactionitem'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblTransactionItem] DTI '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblTransactionItem] STI '
	   			 + 'ON (STI.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
				 + '	AND STI.itemName + '' ##'' + CAST(STI.idTransactionItem AS NVARCHAR) + ''##'' = DTI.itemName) '
	   			 + 'WHERE DTI.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
	   			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 14.4: Copy PurchaseTransaction - Transaction Item Mappings Created', 0)

		-- update idParentTransactionItem from Temp Table TransactionItem
		SET @sql = 'UPDATE DTI '
	   			 + '	SET idParentTransactionItem = TEMP.destinationId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblTransactionItem] STI '
	   			 + 'INNER JOIN  [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblTransactionItem] DTI '
	   			 + 'ON (STI.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
				 + '	AND STI.itemName + '' ##'' + CAST(STI.idTransactionItem AS NVARCHAR) + ''##'' = DTI.itemName) '
	   			 + 'INNER JOIN [dbo].[ClonePortal_IDMappings] TEMP '
	   			 + 'ON (TEMP.sourceId = STI.idParentTransactionItem AND TEMP.object = ''transactionitem'') '
	   			 + 'WHERE DTI.idSite = ' + CAST(@idSiteDestination AS NVARCHAR) + ' AND STI.idParentTransactionItem IS NOT NULL'
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 14.5: Copy PurchaseTransaction - Transaction Item Parent Ids Updated', 0)

		-- clean up the ##idTransactionItem## additions we made to transaction item names, we did that to uniquely distinguish transaction items
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblTransactionItem] SET '
				 + '	itemName = REPLACE(DTI.itemName, '' ##'' + CAST(STI.sourceID AS NVARCHAR) + ''##'', '''') '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblTransactionItem] DTI '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] STI ON STI.destinationID = DTI.idTransactionItem AND STI.object = ''transactionitem'' '
				 + 'WHERE DTI.idSite = ' + CAST(@idSiteDestination AS NVARCHAR) + ' AND STI.sourceID IS NOT NULL'

		EXEC (@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 14.6: Copy PurchaseTransaction - Transaction Item Names Cleaned Up', 0)

		/*

		RETURN

		*/
	   
		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemClonePortal_PurchaseTransaction_InsertedSuccessfully'

	END TRY

	/*

	CATCH AND LOG ERRORS

	*/

	BEGIN CATCH
			
		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 14: Copy PurchaseTransaction - ERROR - ' + CAST(ERROR_LINE() AS NVARCHAR) + ' | ' + CAST(ERROR_MESSAGE() AS NVARCHAR(MAX)), 1)
		SET @Return_Code = 1
		SET @Error_Description_Code	= 'SystemClonePortal_PurchaseTransaction_PortalMigrationFailed'
		
	END CATCH
		
	SET XACT_ABORT OFF

END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.ClonePortal.Report]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.ClonePortal.Report]
GO


/*

CLONE PORTAL - REPORT DATA
OCCURS FOR FULL COPY

*/
CREATE PROCEDURE [System.ClonePortal.Report]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,

	@idSiteSource			INT,
	@idSiteDestination		INT,
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50)
)
AS

BEGIN

    SET NOCOUNT ON
    SET XACT_ABORT ON

	INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 15.0: Copy Report - Initialized', 0)
    
	BEGIN TRY	
    
		DECLARE @sql NVARCHAR(MAX)

		/*
	  
		replace double slash with single slash. WHY DO WE DO THIS?!? Need to find out!
	 
		*/

		SET @sourceDBServer = REPLACE(@sourceDBServer, '\\', '\')
		SET @destinationDBServer = REPLACE(@destinationDBServer, '\\', '\')
	  
		-- copy reports
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblReport] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idUser, '
	   			 + '	idDataset, '
	   			 + '	title, '
	   			 + '	fields, '
	   			 + '	filter, '
	   			 + '	[order], '
	   			 + '	isPublic, '
	   			 + '	dtCreated, '
	   			 + '	dtModified, '
	   			 + '	isDeleted, '
	   			 + '	dtDeleted '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTU.destinationId, '
	   			 + '	R.idDataset, '
	   			 + '	R.title, '
	   			 + '	R.fields, '
	   			 + '	R.filter, '
	   			 + '	R.[order], '
	   			 + '	R.isPublic, '
	   			 + '	R.dtCreated, '
	   			 + '	R.dtModified, '
	   			 + '	R.isDeleted, '
	   			 + '	R.dtDeleted '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblReport] R '
	   			 + 'INNER JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = R.idUser AND TTU.object = ''users'') '
	   			 + 'WHERE R.idSite = ' + CAST(@idSiteSource AS NVARCHAR) + ' AND R.idDataset < 1000'
	   	
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 15.1: Copy Report - Reports Inserted', 0)
	   
		-- insert the mapping for source to destination report ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	SR.idReport, '
	   			 + '	DR.idReport, '
	   			 + '	''report'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblReport] DR '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblReport] SR '
	   			 + 'ON (SR.idSite =' + CAST(@idSiteSource AS NVARCHAR) 
	   			 + '	AND (SR.dtCreated = DR.dtCreated OR (SR.dtCreated IS NULL AND DR.dtCreated IS NULL)) '
	   			 + '	AND (SR.dtModified = DR.dtModified OR (SR.dtModified IS NULL AND DR.dtModified IS NULL)) '
	   			 + '	AND (SR.dtDeleted = DR.dtDeleted OR (SR.dtDeleted IS NULL AND DR.dtDeleted IS NULL))) '
	   			 + 'WHERE DR.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
	   				
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 15.2: Copy Report - Report Mappings Created', 0)

		-- copy report languages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblReportLanguage] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idReport, '
	   			 + '	idLanguage, '
	   			 + '	title '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTR.destinationId, '
	   			 + '	RL.idLanguage, '
	   			 + '	RL.title '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblReportLanguage] RL '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTR on (TTR.sourceId = RL.idReport AND TTR.object = ''report'') '
	   			 + 'WHERE RL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) + ' AND TTR.destinationId IS NOT NULL'
	   
		EXEC(@sql)
		
		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 15.3: Copy Report - Report Languages Inserted', 0)
		
		-- copy report files
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblReportFile] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idReport, '
	   			 + '	idUser, '
	   			 + '	[filename], '
	   			 + '	dtCreated, '
	   			 + '	htmlKb, '
	   			 + '	csvKb, '
	   			 + '	pdfKb '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTR.destinationId, '
	   			 + '	TTU.destinationId, '
	   			 + '	RF.[filename], '
	   			 + '	RF.dtCreated, '
	   			 + '	RF.htmlKb, '
	   			 + '	RF.csvKb, '
	   			 + '	RF.pdfKb '	
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblReportFile] RF '
	   			 + 'INNER JOIN [dbo].[ClonePortal_IDMappings] TTR on (TTR.sourceId = RF.idReport AND TTR.object = ''report'') '
	   			 + 'INNER JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = RF.idUser AND TTU.object = ''users'') '
	   			 + 'WHERE RF.idSite = ' + CAST(@idSiteSource AS NVARCHAR) + ' AND TTR.destinationId IS NOT NULL'
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 15.4: Copy Report - Report Files Inserted', 0)
	   
		-- copy report subscriptions
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblReportSubscription] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idUser, '
	   			 + '	idReport, '
	   			 + '	dtStart, '
	   			 + '	dtNextAction, '
	   			 + '	recurInterval, '
	   			 + '	recurTimeframe, '
	   			 + '	token '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTU.destinationId, '
	   			 + '	TTR.destinationId, '
	   			 + '	RS.dtStart, '
	   			 + '	RS.dtNextAction, '
	   			 + '	RS.recurInterval, '
	   			 + '	RS.recurTimeframe, '
	   			 + '	RS.token '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblReportSubscription] RS '
	   			 + 'INNER JOIN [dbo].[ClonePortal_IDMappings] TTR on (TTR.sourceId = RS.idReport AND TTR.object = ''report'') '
	   			 + 'INNER JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = RS.idUser AND TTU.object = ''users'') '
	   			 + 'WHERE RS.idSite = ' + CAST(@idSiteSource AS NVARCHAR) + ' AND TTR.destinationId IS NOT NULL'
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 15.5: Copy Report - Report Subscriptions Inserted', 0)
		
		-- copy report shortcut widget links
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblReportToReportShortcutsWidgetLink] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idReport, '
	   			 + '	idUser '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTR.destinationId, '
	   			 + '	TTU.destinationId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblReportToReportShortcutsWidgetLink] RRSWL '
	   			 + 'INNER JOIN [dbo].[ClonePortal_IDMappings] TTR on (TTR.sourceId = RRSWL.idReport AND TTR.object = ''report'') '
	   			 + 'INNER JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = RRSWL.idUser AND TTU.object = ''users'') '
	   			 + 'WHERE RRSWL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) + ' AND TTR.destinationId IS NOT NULL'
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 15.6: Copy Report - Report Shortcut Widget Links Inserted', 0)

		/*

		FINAL STEP CLEAR OUT RULESET ENGINE QUEUE, AND ENABLE THE DESTINATION DB SQL JOB - THIS MUST ALWAYS BE THE FINAL STEP

		*/

		-- clear out the ruleset engine queue
		SET @sql = 'DELETE FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblRuleSetEngineQueue] '
				 + 'WHERE idSite = ' + CAST(@idSiteDestination AS NVARCHAR)

		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 15.7: FINAL STEP - RULESET ENGINE QUEUE CLEARED', 0)

		-- enable the SQL Agent Job for the destination database
		SET @sql = 'IF EXISTS ('
				 + 'SELECT 1 FROM [' + @destinationDBServer + '].[msdb].[dbo].[sysjobs] '
				 + 'WHERE name = ''Asentia Jobs - ' + @destinationDBName + ''') '
				 + 'BEGIN EXEC [msdb].[dbo].sp_update_job @job_name = ''Asentia Jobs - ' + @destinationDBName + ''', @enabled = 1 END'

		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 15.8: FINAL STEP - SQL AGENT JOB ENABLED', 0)		
				
		EXEC(@sql)

		/*

		RETURN

		*/
	   
		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemClonePortal_PurchaseTransaction_InsertedSuccessfully'

	END TRY

	/*

	CATCH AND LOG ERRORS

	*/

	BEGIN CATCH
			
		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 15: Copy Report - ERROR - ' + CAST(ERROR_LINE() AS NVARCHAR) + ' | ' + CAST(ERROR_MESSAGE() AS NVARCHAR(MAX)), 1)
		SET @Return_Code = 1
		SET @Error_Description_Code	= 'SystemClonePortal_PurchaseTransaction_PortalMigrationFailed'
		
	END CATCH
		
	SET XACT_ABORT OFF

END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.ClonePortal.RoleRule]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.ClonePortal.RoleRule]
GO


/*

CLONE PORTAL - ROLE / RULE / RULESET DATA
OCCURS FOR FULL AND PARTIAL COPY

*/
CREATE PROCEDURE [System.ClonePortal.RoleRule]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,

	@fullCopyFlag			BIT,			-- needed for user links because users are only copied in full copy
	@idSiteSource			INT,
	@idSiteDestination		INT,
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50)
)
AS

BEGIN

	SET NOCOUNT ON
    SET XACT_ABORT ON

	INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 7.0: Copy RoleRule - Initialized', 0)
    
	BEGIN TRY
    
		DECLARE @sql NVARCHAR(MAX)

		/*
	  
		replace double slash with single slash. WHY DO WE DO THIS?!? Need to find out!
	 
		*/

		SET @sourceDBServer = REPLACE(@sourceDBServer, '\\', '\')
		SET @destinationDBServer = REPLACE(@destinationDBServer, '\\', '\')

		/*

		ROLES

		*/	  

		-- copy roles
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblRole] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	name '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	R.name '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblRole] R '
	   			 + 'WHERE R.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
	   			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 7.1: Copy RoleRule - Roles Inserted', 0)	   	

		-- insert the mapping for source to destination role ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	SRC.idRole, '
	   			 + '	DST.idRole, '
	   			 + '	''role'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblRole] DST '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblRole] SRC '
	   			 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   			 + '	AND (SRC.name = DST.name)) '
	   			 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)	   			 
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 7.2: Copy RoleRule - Roles Mappings Created', 0)	   
	   
		-- copy role languages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblRoleLanguage] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idRole, '
	   			 + '	idLanguage, '
	   			 + '	name '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTR.destinationId, '
	   			 + '	RL.idLanguage, '
	   			 + '	RL.name '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblRoleLanguage] RL '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTR on (TTR.sourceId = RL.idRole AND TTR.object = ''role'') '
	   			 + 'WHERE RL.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
	   			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 7.3: Copy RoleRule - Role Languages Inserted', 0)

		-- copy role to permission links -- ONLY NON SCOPED PERMISSIONS
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblRoleToPermissionLink] '
	   			+ '( '
	   			+ '	idSite, '
	   			+ '	idRole, '
	   			+ '	idPermission, '
	   			+ '	scope '
	   			+ ') '
	   			+ 'SELECT '
				+	CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			+ '	TTR.destinationId, '
	   			+ '	RTPL.idPermission, '
	   			+ '	RTPL.scope '
	   			+ 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblRoleToPermissionLink] RTPL '
	   			+ 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTR on (TTR.sourceId = RTPL.idRole AND TTR.object = ''role'' ) '
	   			+ 'WHERE RTPL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) + ' AND RTPL.scope IS NULL'
	   		
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 7.4: Copy RoleRule - Role to Permission Links Inserted', 0)

		/*

		RULESETS

		*/

		-- copy rulesets
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblRuleSet] '
	     		 + '( '
	     		 + '	[idSite], '
	     		 + '	[isAny], '
	     		 + '	[label] '
	     		 + ') '
	     		 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	     		 + '	R.[isAny], '
	     		 + '	R.[label]  + '' ##'' + CAST(R.idRuleSet AS NVARCHAR) + ''##'' '
	     		 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblRuleSet] R '
	     		 + 'WHERE R.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	     
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 7.5: Copy RoleRule - Rulesets Inserted', 0)

		-- insert the mapping for source to destination ruleset ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	SRC.idRuleSet, '
	   			 + '	DST.idRuleSet, '
	   			 + '	''ruleset'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblRuleSet] DST '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblRuleSet] SRC '
	   			 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
				 + '	AND SRC.[label] + '' ##'' + CAST(SRC.idRuleSet AS NVARCHAR) + ''##'' = DST.[label]) ' 
	   			 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 7.6: Copy RoleRule - Ruleset Mappings Created', 0)

		-- clean up the ##idRuleSet## additions we made to labels, we did that to uniquely distinguish rulesets
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRuleSet] SET '
				 + '	label = REPLACE(DRS.label, '' ##'' + CAST(SRS.sourceID AS NVARCHAR) + ''##'', '''') '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRuleSet] DRS '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] SRS ON SRS.destinationID = DRS.idRuleSet AND SRS.object = ''ruleset'' '
				 + 'WHERE DRS.idSite = ' + CAST(@idSiteDestination AS NVARCHAR) + ' AND SRS.sourceID IS NOT NULL'

		EXEC (@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 7.7: Copy RoleRule - Ruleset Titles Cleaned Up', 0)

		-- copy ruleset languages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblRuleSetLanguage] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idRuleSet, '
	   			 + '	idLanguage, '
	   			 + '	label '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTR.destinationId, '
	   			 + '	RL.idLanguage, '
	   			 + '	RL.label '
	   			 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblRuleSetLanguage] RL '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTR on (TTR.sourceId = RL.idRuleset AND TTR.object = ''ruleset'' ) '
	   			 + 'WHERE RL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   		
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 7.8: Copy RoleRule - Ruleset Languages Inserted', 0)

		/*

		RULES

		*/

		-- copy rules
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblRule] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idRuleSet, '
	   			 + '	userField, '
	   			 + '	operator, '
	   			 + '	textValue, '
	   			 + '	dateValue, '
	   			 + '	numValue, '
	   			 + '	bitValue '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTR.destinationId, '
	   			 + '	R.userField, '
	   			 + '	R.operator, '
	   			 + '	R.textValue, '
	   			 + '	R.dateValue, '
	   			 + '	R.numValue, '
	   			 + '	R.bitValue '
	   			 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblRule] R '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTR on (TTR.sourceId = R.idRuleSet AND TTR.object = ''ruleset'') '
	   			 + 'WHERE R.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 7.9: Copy RoleRule - Rules Inserted', 0)

		/*

		RULESET TO OBJECT LINKS

		*/

		-- copy ruleset to group links
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblRuleSetToGroupLink] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idRuleSet, '
	   			 + '	idGroup '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTR.destinationId, '
	   			 + '	TTG.destinationId '
	   			 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblRuleSetToGroupLink] RTGL '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTR on (TTR.sourceId = RTGL.idRuleSet AND TTR.object = ''ruleset'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTG on (TTG.sourceId = RTGL.idGroup AND TTG.object = ''groups'') '
	   			 + 'WHERE RTGL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 7.10: Copy RoleRule - Ruleset to Group Links Inserted', 0)

		-- copy ruleset to role links
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblRuleSetToRoleLink] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idRuleSet, '
	   			 + '	idRole '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTR.destinationId, '
	   			 + '	TTRole.destinationId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblRuleSetToRoleLink] RTRL '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTR on (TTR.sourceId = RTRL.idRuleSet AND TTR.object = ''ruleset'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTRole on (TTRole.sourceId = RTRL.idRole AND TTRole.object = ''role'') '
	   			 + 'WHERE RTRL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 7.11: Copy RoleRule - Ruleset to Role Links Inserted', 0)

		-- copy ruleset to learning path enrollment links
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblRuleSetToRuleSetLearningPathEnrollmentLink] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idRuleSet, ' 
	   			 + '	idRuleSetLearningPathEnrollment '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTRS.destinationId, '
	   			 + '	TTRSLPE.destinationId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblRuleSetToRuleSetLearningPathEnrollmentLink] RSRSLPEL '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTRSLPE on (TTRSLPE.sourceId = RSRSLPEL.idRuleSetLearningPathEnrollment AND TTRSLPE.object = ''rulesetlearningpathenrollment'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTRS on (TTRS.sourceId = RSRSLPEL.idRuleSet AND TTRS.object = ''ruleset'') '
	   			 + 'WHERE RSRSLPEL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 7.12: Copy RoleRule - Ruleset to Ruleset Learning Path Enrollment Links Inserted', 0)

		-- copy ruleset enrollments
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblRuleSetEnrollment] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idCourse, '
	   			 + '	idTimezone, '
	   			 + '	[priority], '
	   			 + '	label, '
	   			 + '	isLockedByPrerequisites, '
	   			 + '	isFixedDate, '
	   			 + '	dtStart, '
	   			 + '	dtEnd, '
	   			 + '	dtCreated, '
	   			 + '	delayInterval, '
	   			 + '	delayTimeframe, '
	   			 + '	dueInterval, '
	   			 + '	dueTimeframe, '
	   			 + '	recurInterval, '
	   			 + '	recurTimeframe, '
	   			 + '	expiresFromStartInterval, '
	   			 + '	expiresFromStartTimeframe, '
	   			 + '	expiresFromFirstLaunchInterval, '
	   			 + '	expiresFromFirstLaunchTimeframe, '
	   			 + '	forceReassignment '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTC.destinationId, '
	   			 + '	RE.idTimezone, '
	   			 + '	RE.[priority], '
	   			 + '	RE.label, '
	   			 + '	RE.isLockedByPrerequisites, '
	   			 + '	RE.isFixedDate, '
	   			 + '	RE.dtStart, '
	   			 + '	RE.dtEnd, '
	   			 + '	RE.dtCreated, '
	   			 + '	RE.delayInterval, '
	   			 + '	RE.delayTimeframe, '
	   			 + '	RE.dueInterval, '
	   			 + '	RE.dueTimeframe, '
	   			 + '	RE.recurInterval, '
	   			 + '	RE.recurTimeframe, '
	   			 + '	RE.expiresFromStartInterval, '
	   			 + '	RE.expiresFromStartTimeframe, '
	   			 + '	RE.expiresFromFirstLaunchInterval, '
	   			 + '	RE.expiresFromFirstLaunchTimeframe, '
	   			 + '	RE.forceReassignment '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblRuleSetEnrollment] RE '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = RE.idCourse AND TTC.object = ''courses'') '
	   			 + 'WHERE RE.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 7.13: Copy RoleRule - Ruleset Enrollments Inserted', 0)
	   
		-- insert the mapping for source to destination ruleset enrollment id
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, ' 
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	SRC.idRuleSetEnrollment, '
	   			 + '	DST.idRuleSetEnrollment, '
	   			 + '	''rulesetenrollment'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblRuleSetEnrollment] DST '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblRuleSetEnrollment] SRC '
	   			 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   			 + '	AND (SRC.label = DST.label) '
	   			 + '	AND (SRC.dtStart = DST.dtStart) '	   			 
	   			 + '	AND (SRC.dtCreated = DST.dtCreated)) '
	   			 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
	   	
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 7.14: Copy RoleRule - Ruleset Enrollment Mappings Created', 0)

		-- copy ruleset enrollment languages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblRuleSetEnrollmentLanguage] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idRuleSetEnrollment, '
	   			 + '	idLanguage, '
	   			 + '	label '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTRE.destinationId, '
	   			 + '	REL.idLanguage, '
	   			 + '	REL.label '
	   			 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblRuleSetEnrollmentLanguage] REL '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTRE on (TTRE.sourceId = REL.idRuleSetEnrollment AND TTRE.object = ''rulesetenrollment'') '
	   			 + 'WHERE REL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	           
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 7.15: Copy RoleRule - Ruleset Enrollment Languages Inserted', 0)

		-- copy ruleset to enrollment links
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblRuleSetToRuleSetEnrollmentLink] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idRuleSet, '
	   			 + '	idRuleSetEnrollment '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTR.destinationId, '
	   			 + '	TTRE.destinationId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblRuleSetToRuleSetEnrollmentLink] RTREL '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTR on (TTR.sourceId = RTREL.idRuleSet AND TTR.object = ''ruleset'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTRE on (TTRE.sourceId = RTREL.idRuleSetEnrollment AND TTRE.object = ''rulesetenrollment'') '
	   			 + 'WHERE RTREL.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
	   			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 7.16: Copy RoleRule - Ruleset to Ruleset Enrollment Links Inserted', 0)

		/*

		GROUP TO ROLE LINKS

		*/

		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblGroupToRoleLink] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idGroup, '
	   			 + '	idRole, '
	   			 + '	idRuleSet, '
	   			 + '	created '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTG.destinationId, '
	   			 + '	TTR.destinationId, '
	   			 + '	TTRS.destinationId, '
	   			 + '	GTRL.created '
	   			 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblGroupToRoleLink] GTRL '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTR on (TTR.sourceId = GTRL.idRole AND TTR.object = ''role'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTG on (TTG.sourceId = GTRL.idGroup AND TTG.object = ''groups'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTRS on (TTRS.sourceId = GTRL.idRuleset AND TTRS.object = ''ruleset'') '
	   			 + 'WHERE GTRL.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 7.17: Copy RoleRule - Group to Role Links Inserted', 0)

		/*

		USER TO OBJECT LINKS - FULL COPY ONLY

		*/
			   
		IF @fullCopyFlag = 1
			BEGIN

			-- copy user to group links
			SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblUserToGroupLink] '
	   				 + '( '
	   				 + '	idSite, '
	   				 + '	idUser, '
	   				 + '	idGroup, '
	   				 + '	idRuleSet, '
	   				 + '	created, '
	   				 + '	selfJoined '
	   				 + ') '
	   				 + 'SELECT '
					 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   				 + '	TTU.destinationId, '
	   				 + '	TTG.destinationId, '
	   				 + '	TTRS.destinationId, '
	   				 + '	UTGL.created, '
	   				 + '	UTGL.selfJoined '
	   				 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblUserToGroupLink] UTGL '
	   				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = UTGL.idUser AND TTU.object = ''users'') '
	   				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTG on (TTG.sourceId = UTGL.idGroup AND TTG.object = ''groups'') '
	   				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTRS on (TTRS.sourceId = UTGL.idRuleset AND TTRS.object = ''ruleset'') '
	   				 + 'WHERE UTGL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   
			EXEC(@sql)

			INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 7.18: Copy RoleRule - User to Group Links Inserted', 0)

			-- copy user to role links
			SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblUserToRoleLink] '
	   				 + '( '
	   				 + '	idSite, '
	   				 + '	idUser, '
	   				 + '	idRole, '
	   				 + '	idRuleSet, '
	   				 + '	created '
	   				 + ') '
	   				 + 'SELECT '
					 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   				 + '	TTU.destinationId, '
	   				 + '	CASE WHEN UTRL.idRole < 100 THEN UTRL.idRole ELSE TTR.destinationId END, '
	   				 + '	TTRS.destinationId, '
	   				 + '	UTRL.created '
	   				 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblUserToRoleLink] UTRL '
	   				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTR on (TTR.sourceId = UTRL.idRole AND TTR.object = ''role'') '
	   				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = UTRL.idUser AND TTU.object = ''users'') '
	   				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTRS on (TTRS.sourceId = UTRL.idRuleset AND TTRS.object = ''ruleset'') '
	   				 + 'WHERE UTRL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   
			EXEC(@sql)

			INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 7.19: Copy RoleRule - User to Role Links Inserted', 0)
	   
			END

		/*

		RETURN

		*/

		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemClonePortal_RoleRule_InsertedSuccessfully'

	END TRY

	/*

	CATCH AND LOG ERRORS

	*/

	BEGIN CATCH
			
		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 7: Copy RoleRule - ERROR - ' + CAST(ERROR_LINE() AS NVARCHAR) + ' | ' + CAST(ERROR_MESSAGE() AS NVARCHAR(MAX)), 1)
		SET @Return_Code = 1
		SET @Error_Description_Code	= 'SystemClonePortal_RoleRule_PortalMigrationFailed'
		
	END CATCH
		
	SET XACT_ABORT OFF

END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.ClonePortal.Site]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.ClonePortal.Site]
GO


/*

CLONE PORTAL - SITE DATA

*/
CREATE PROCEDURE [System.ClonePortal.Site]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,

	@fullCopyFlag			BIT,
	@idSiteSource			INT,
	@idSiteDestination		INT				OUTPUT,
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50),
	@destinationHostName    NVARCHAR(50),
	@idAccount				INT				= NULL
)
AS

BEGIN

	SET NOCOUNT ON
	SET XACT_ABORT ON

	INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 1.0: Copy Site - Initialized', 0)
    
	BEGIN TRY		
    
		DECLARE @sql				NVARCHAR(MAX)				
		DECLARE @doesHostnameExist	BIT

		/*
	  
		replace double slash with single slash. WHY DO WE DO THIS?!? Need to find out!
	 
		*/

		SET @sourceDBServer = REPLACE(@sourceDBServer, '\\', '\')
		SET @destinationDBServer = REPLACE(@destinationDBServer, '\\', '\')

		/*

		first, disable the SQL Agent Job for the destination database

		*/

		SET @sql = 'IF EXISTS ('
				 + 'SELECT 1 FROM [' + @destinationDBServer + '].[msdb].[dbo].[sysjobs] '
				 + 'WHERE name = ''Asentia Jobs - ' + @destinationDBName + ''') '
				 + 'BEGIN EXEC [msdb].[dbo].sp_update_job @job_name = ''Asentia Jobs - ' + @destinationDBName + ''', @enabled = 0 END'

		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 1.1: Copy Site - SQL AGENT JOB DISABLED', 0)

		/*
	  
		create id mappings table or clear out existing id mappings table

		*/
	
		SET @sql = 'IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE name = N''ClonePortal_IDMappings'' AND xtype = ''U'') '
			+ '	BEGIN '
			+ '	CREATE TABLE [dbo].[ClonePortal_IDMappings] '
			+ '	('
			+ '		[sourceID]		INT				NOT NULL, '
			+ '		[destinationID] INT				NOT NULL, '
			+ '		[object]		NVARCHAR(255) '
			+ ')'
			+ '	END '
			+ '	ELSE '
			+ '	BEGIN '
		SET @sql = @sql + '	DELETE FROM [dbo].[ClonePortal_IDMappings] ' 
			+ '	END'
				
		EXEC(@sql)
	
		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 1.2: Copy Site - Mapping Table Created', 0)
	
		/*
	
		validate that the hostname is unique within the destination
	
		*/
		
		SET @sql = 'SET @doesHostnameExist = (SELECT CASE WHEN COUNT(1) > 0 THEN 1 ELSE 0 END FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblSite] '
				 + 'WHERE hostname = ' + '''' + @destinationHostName + '''' + ')'
	
		EXECUTE SP_EXECUTESQL @sql, N'@doesHostnameExist INT OUTPUT', @doesHostnameExist OUTPUT
				
		IF (@doesHostnameExist = 1)
		BEGIN
			SET @Return_Code = 2
			SET @Error_Description_Code = 'SystemClonePortal_HostNameNotUnique'			
			RETURN 1
		END
	   
		/*

		FULL COPY

		*/

		IF @fullCopyFlag = 1
			BEGIN

			-- copy the site
			SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblSite] '
					 + '( '
					 + '	hostname, '
					 + '	password, '
					 + '	isActive, '
					 + '	dtExpires, '
					 + '	title, '
					 + '	company, '
					 + '	contactName, '
					 + '	contactEmail, '
					 + '	userLimit, '
					 + '	kbLimit, '
					 + '	idLanguage, '
					 + '	idTimezone, '
					 + '	dtLicenseAgreementExecuted, '
					 + '	dtUserAgreementModified '
					 + ') '
					 + 'SELECT '
					 + '	''' + @destinationHostName + ''', '
					 + '	CP.password, '
					 + '	CP.isActive, '
					 + '	CP.dtExpires, '
					 + '	CP.title, '
					 + '	CP.company, '
					 + '	CP.contactName, '
					 + '	CP.contactEmail, '
					 + '	CP.userLimit, '
					 + '	CP.kbLimit, '
					 + '	CP.idLanguage, '
					 + '	CP.idTimezone, '
					 + '	CP.dtLicenseAgreementExecuted, '
					 + '	CP.dtUserAgreementModified '
					 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblSite] CP '
					 + 'WHERE CP.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
		
			EXEC (@sql)

			-- get the new site id
			DECLARE @newIdSite INT
			SET @sql = 'SET @newIdSite = (SELECT idSite FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblSite] WHERE hostname = ''' + @destinationHostName + ''')'
			EXECUTE SP_EXECUTESQL @sql, N'@newIdSite INT OUTPUT', @newIdSite OUTPUT

			INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 1.3: Copy Site - Site Record Inserted', 0)
	   
			-- set new created destination site id 
			SET @idSiteDestination = @newIdSite
	  
			-- insert the mapping for source to destination site id
			SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
					 + '( '
					 + '	sourceId, '
					 + '	destinationId, '
					 + '	[object] '
					 + ') '
					 + 'VALUES '
					 + '( '
					 +		CAST(@idSiteSource AS NVARCHAR) + ', '
					 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
					 + '	''site'' ' 
					 + ') '

			EXEC(@sql)	

			INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 1.4: Copy Site - Site Mapping Created', 0)
	   
			-- copy the site language table
			SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblSiteLanguage] '
					 + '( '
					 + '	idSite, '
					 + '	idLanguage, '
					 + '	title, '
					 + '	company '
					 + ') '
					 + 'SELECT '
					 + '	TTS.destinationId, '
					 + '	SL.idLanguage, '
					 + '	SL.title, '
					 + '	SL.company '
					 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblSiteLanguage] SL '
					 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTS ON TTS.sourceId = SL.idSite AND TTS.object = ''site'' '
					 + 'WHERE SL.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
		
			EXEC(@sql)
	   
			INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 1.5: Copy Site - Site Language Record Inserted', 0)
	   
			-- insert the account to domain alias link for the hostname
			SET @sql = 'INSERT INTO [tblAccountToDomainAliasLink] '
					 + '( '
					 + '	idAccount, '
					 + '	hostname, '
					 + '	domain '
					 + ') '
					 + 'SELECT '
					 +		CAST(@idAccount AS NVARCHAR) + ', '
					 + '	''' + @destinationHostName + ''', '
					 + '	NULL ' 
								
			EXEC(@sql)		   
	   
			INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 1.6: Copy Site - Account to Domain Alias Record Inserted', 0)

			-- insert the site to domain alias link for the hostname
			SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblSiteToDomainAliasLink] '
	   				 + '( '
	   				 + '	idSite, '
	   				 + '	domain '
	   				 + ') '
	   				 + 'SELECT '
	   				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   				 + '	''' + @destinationHostName + ''' '
						
			EXEC(@sql)	

			INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 1.7: Copy Site - Site To Domain Alias Record Inserted', 0)

			END
		/*

		PARTIAL COPY - THIS IS FOR CREATING A NEW SITE FROM TEMPLATE
		THE SITE IS ALREADY CREATED FROM THAT PAGE, WE ONLY NEED TO MAP IT

		*/

		ELSE
			BEGIN

			-- insert the mapping for source to destination site id
			SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
					 + '( '
					 + '	sourceId, '
					 + '	destinationId, '
					 + '	[object] '
					 + ') '
					 + 'VALUES '
					 + '( '
					 +		CAST(@idSiteSource AS NVARCHAR) + ', '
					 +		CAST(@idSiteDestination AS NVARCHAR) +', '
					 + '	''site'' '
					 + ') '

			EXEC(@sql)	

			INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 1.4: Copy Site - Site Mapping Created', 0)

			END

		/*

		ITEMS THAT ARE COPIED FOR BOTH FULL AND PARTIAL COPY

		*/
      
		-- copy site params
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblSiteParam] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	[param], '
	   			 + '	value '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	SP.[param], '
	   			 + '	SP.value '
	   			 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblSiteParam] SP '
	   			 + 'WHERE SP.idSite = ' + CAST(@idSiteSource AS NVARCHAR) + ' '
				 + 'AND NOT EXISTS (SELECT 1 FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblSiteParam] SP1 '
				 + '				WHERE SP1.[param] = SP.[param] AND SP1.idSite = ' + CAST(@idSiteDestination AS NVARCHAR) + ')'

		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 1.8: Copy Site - Site Params Inserted', 0)
		
		-- copy site available languages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblSiteAvailableLanguage] '
				 + '( '
				 + '	idSite, '
				 + '	idLanguage '
				 + ') '
				 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	SAL.idLanguage '
				 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblSiteAvailableLanguage] SAL '
				 + 'WHERE SAL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) + ' '
				 + 'AND NOT EXISTS (SELECT 1 FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblSiteAvailableLanguage] SAL1 '
				 + '				WHERE SAL1.[idLanguage] = SAL.[idLanguage] AND SAL1.idSite = ' + CAST(@idSiteDestination AS NVARCHAR) + ')'
			
		EXEC(@sql)		

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 1.9: Copy Site - Site Available Languages Inserted', 0)

		/*

		RETURN

		*/
		
		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemClonePortal_Site_InsertedSuccessfully'

	END TRY

	/*

	CATCH AND LOG ERRORS

	*/

	BEGIN CATCH
		
		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 1: Copy Site - ERROR - ' + CAST(ERROR_LINE() AS NVARCHAR) + ' | ' + CAST(ERROR_MESSAGE() AS NVARCHAR(MAX)), 1)
		SET @Return_Code = 1
		SET @Error_Description_Code	= 'SystemClonePortal_Site_PortalMigrationFailed'

	END CATCH
		
	SET XACT_ABORT OFF

END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.ClonePortal.User]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.ClonePortal.User]
GO


/*

CLONE PORTAL - USER DATA
OCCURS FOR FULL COPY ONLY

*/
CREATE PROCEDURE [System.ClonePortal.User]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,	

	@idSiteSource			INT,
	@idSiteDestination		INT,
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50)
)
AS

BEGIN

	SET NOCOUNT ON
	SET XACT_ABORT ON

	INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 4.0: Copy Users - Initialized', 0)
    
	BEGIN TRY	
    
		DECLARE @sql NVARCHAR(MAX)

		/*
	  
		replace double slash with single slash. WHY DO WE DO THIS?!? Need to find out!
	 
		*/

		SET @sourceDBServer = REPLACE(@sourceDBServer, '\\', '\')
		SET @destinationDBServer = REPLACE(@destinationDBServer, '\\', '\')
	   
		-- insert a row in the mapings table to map user id 1 to user id 1, this is so we don't have to code exceptions for user id 1
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
				 + '( '
				 + '	sourceId, '
				 + '	destinationId, '
				 + '	object '
				 + ') '
				 + 'VALUES '
				 + '( '
				 + '	1, '
				 + '	1, '
				 + '	''users'' '
				 + ')'

		EXEC(@sql)
		
		-- declare two variables for the user insert SQL, the query length exceeds what can be concatenated in one string
		DECLARE @userSql1 NVARCHAR(MAX)
		DECLARE @userSql2 NVARCHAR(MAX)
	   
		-- copy users
		SET @userSql1 = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblUser] '
	   				  + '( '
	   				  + '	idSite, '
	   				  + '	firstName, '
	   				  + '	middleName, '
	   				  + '	lastName, '
	   				  + '	displayName, '
	   				  + '	email, '
	   				  + '	username, '
	   				  + '	[password], '
	   				  + '	isDeleted, '
	   				  + '	dtDeleted, '
	   				  + '	idTimezone, '
	   				  + '	isActive, '
	   				  + '	mustchangePassword, '
	   				  + '	dtCreated, '
	   				  + '	dtExpires, '
	   				  + '	idLanguage, '
	   				  + '	dtModified, '
	   				  + '	employeeID, '
	   				  + '	company, '
	   				  + '	[address], '
	   				  + '	city, '
	   				  + '	province, '
	   				  + '	postalcode, '
	   				  + '	country, '
	   				  + '	phonePrimary, '
	   				  + '	phoneWork, '
	   				  + '	phoneFax, '
	   				  + '	phoneHome, '
	   				  + '	phoneMobile, '
	   				  + '	phonePager, '
	   				  + '	phoneOther, '
	   				  + '	department, '
	   				  + '	division, '
	   				  + '	region, '
	   				  + '	jobTitle, '
	   				  + '	jobClass, '
	   				  + '	gender, '
	   				  + '	race, '
	   				  + '	dtDOB, '
	   				  + '	dtHire, '
	   				  + '	dtTerm, '
	   				  + '	avatar, '
	   				  + '	objectGUID, '
	   				  + '	activationGUID, '
	   				  + '	distinguishedName, '
	   				  + '	dtLastLogin, '
	   				  + '	dtSessionExpires, '
	   				  + '	activeSessionId, '
	   				  + '	dtLastPermissionCheck, '
	   				  + '	field00, '
	   				  + '	field01, '
	   				  + '	field02, '
	   				  + '	field03, '
	   				  + '	field04, '
	   				  + '	field05, '
	   				  + '	field06, '
	   				  + '	field07, '
	   				  + '	field08, '
	   				  + '	field09, '
	   				  + '	field10, '
	   				  + '	field11, '
	   				  + '	field12, '
	   				  + '	field13, '
	   				  + '	field14, '
	   				  + '	field15, '
	   				  + '	field16, '
	   				  + '	field17, '
	   				  + '	field18, '
	   				  + '	field19, '
	   				  + '	isRegistrationApproved, '	   				  
	   				  + '	rejectionComments, '
	   				  + '	dtApproved, '
	   				  + '	dtDenied, '
	   				  + '	idApprover, '
	   				  + '	disableLoginFromLoginForm, '
	   				  + '	dtMarkedDisabled, '
	   				  + '	exemptFromIPLoginRestriction, '
					  + '	optOutOfEmailNotifications, '
					  + '	dtUserAgreementAgreed '
	   				  + ') '
	   				  + 'SELECT '
					  +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   				  + '	U.firstName, '
	   				  + '	U.middleName, '
	   				  + '	U.lastName, '
	   				  + '	U.displayName, '
	   				  + '	U.email, '
	   				  + '	U.username, '
	   				  + '	U.[password], '
	   				  + '	U.isDeleted, '
	   				  + '	U.dtDeleted, '
	   				  + '	U.idTimezone, '
	   				  + '	U.isActive, '
	   				  + '	U.mustchangePassword, '
	   				  + '	U.dtCreated, '
	   				  + '	U.dtExpires, '
	   				  + '	U.idLanguage, '
	   				  + '	U.dtModified, '
	   				  + '	U.employeeID, '
	   				  + '	U.company, '
	   				  + '	U.[address], '
					  + '	U.city, '
	   				  + '	U.province, '
	   				  + '	U.postalcode, '
	   				  + '	U.country, '
	   				  + '	U.phonePrimary, '
	   				  + '	U.phoneWork, '
	   				  + '	U.phoneFax, '
	   				  + '	U.phoneHome, '
	   				  + '	U.phoneMobile, '
	   				  + '	U.phonePager, '
	   				  + '	U.phoneOther, '
	   				  + '	U.department, '
	   				  + '	U.division, '
	   				  + '	U.region, '
	   				  + '	U.jobTitle, '
	   				  + '	U.jobClass, '
	   				  + '	U.gender, '
	   				  + '	U.race, '
	   				  + '	U.dtDOB, '
	   				  + '	U.dtHire, '
	   				  + '	U.dtTerm, '
	   				  + '	U.avatar, '
	   				  + '	U.objectGUID, '
	   				  + '	U.activationGUID, '
	   				  + '	U.distinguishedName, '
	   				  + '	U.dtLastLogin, '
	   				  + '	U.dtSessionExpires, '
	   				  + '	U.activeSessionId, '
	   				  + '	U.dtLastPermissionCheck, '
		SET @userSql2 = '	U.field00, '
		  			  + '	U.field01, '
		  			  + '	U.field02, '
		  			  + '	U.field03, '
		  			  + '	U.field04, '
		  			  + '	U.field05, '
		  			  + '	U.field06, '
		  			  + '	U.field07, '
		  			  + '	U.field08, '
		  			  + '	U.field09, '
		  			  + '	U.field10, '
		  			  + '	U.field11, '
		  			  + '	U.field12, '
		  			  + '	U.field13, '
		  			  + '	U.field14, '
		  			  + '	U.field15, '
		  			  + '	U.field16, '
		  			  + '	U.field17, '
		  			  + '	U.field18, '
		  			  + '	U.field19, '
		  			  + '	U.isRegistrationApproved, '		  			  
		  			  + '	U.rejectionComments, '
		  			  + '	U.dtApproved, '
		  			  + '	U.dtDenied, '
		  			  + '	U.idApprover, '
		  			  + '	U.disableLoginFromLoginForm, '
		  			  + '	U.dtMarkedDisabled, '
		  			  + '	U.exemptFromIPLoginRestriction, '
					  + '	U.optOutOfEmailNotifications, '
					  + '	U.dtUserAgreementAgreed '
		  			  + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblUser] U '
		  			  + 'WHERE U.idSite = ' + CAST(@idSiteSource AS NVARCHAR) + ' '
		  			  + 'AND U.idUser <> 1 '

		SET @userSql1 = @userSql1 + @userSql2
	   
		EXEC(@userSql1)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 4.1: Copy Users - Users Inserted', 0)

		-- insert the mapping for source to destination user ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	SU.idUser, '
	   			 + '	DU.idUser, '
	   			 + '	''users'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblUser] DU '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblUser] SU '
	   			 + 'ON (SU.idSite =' + CAST(@idSiteSource AS NVARCHAR) 
				 + '	AND SU.username = DU.username) '
	   			 + 'WHERE DU.idSite = ' + CAST(@idSiteDestination AS NVARCHAR) + ' '
	   			 + 'AND DU.idUser <> 1'
	
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 4.2: Copy Users - User Mappings Created', 0)
	   
		-- update idApprover column with the appropriate user id
		SET @sql = 'UPDATE DSTU '
				 + '	SET DSTU.idApprover = TEMP.destinationId '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblUser] DSTU '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TEMP '
				 + 'ON TEMP.sourceId = DSTU.idApprover AND TEMP.object = ''users'' '
				 + 'WHERE DSTU.idSite = ' + CAST(@idSiteDestination AS NVARCHAR) + ' '
				 + 'AND DSTU.idApprover IS NOT NULL AND TEMP.destinationId IS NOT NULL'

		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 4.3: Copy Users - User ApprovedBy Column Updated', 0)

		-- copy course experts
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCourseExpert] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idCourse, '
	   			 + '	idUser '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTC.destinationId, '
	   			 + '	TTU.destinationId '
	   			 + ' FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCourseExpert] CE '
	   			 + ' LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU ON (TTU.sourceId = CE.idUser AND TTU.object = ''users'') '
	   			 + ' LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC ON (TTC.sourceId = CE.idCourse AND TTC.object = ''courses'') '
	   			 + ' WHERE CE.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 	
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 4.4: Copy Users - Course Experts Inserted', 0)

		-- copy course enrollment approvers
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCourseEnrollmentApprover] '
	   			 + ' ( '
	   			 + '	idSite, '
	   			 + '	idCourse, '
	   			 + '	idUser '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTC.destinationId, '
	   			 + '	TTU.destinationId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCourseEnrollmentApprover] CEA '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU ON (TTU.sourceId = CEA.idUser AND TTU.object = ''users'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC ON (TTC.sourceId = CEA.idCourse AND TTC.object = ''courses'') '
	   			 + 'WHERE CEA.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 	
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 4.5: Copy Users - Course Enrollment Approvers Inserted', 0)
	   
		-- copy user supervisors
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblUserToSupervisorLink] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idUser, '
	   			 + '	idSupervisor '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTU.destinationId, '
	   			 + '	TTS.destinationId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblUserToSupervisorLink] UTSL '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU ON (TTU.sourceId = UTSL.idUser AND TTU.object = ''users'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTS ON (TTS.sourceId = UTSL.idSupervisor AND TTS.object = ''users'') '
	   			 + 'WHERE UTSL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 4.6: Copy Users - User Supervisors Inserted', 0)

		-- copy activity import records
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblActivityImport] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idUser, '
	   			 + '	[timestamp], '
	   			 + '	idUserImported, '
	   			 + '	importFileName '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTUser.destinationId, '
	   			 + '	AI.[timestamp], '
	   			 + '	TTUserImported.destinationId, '
	   			 + '	AI.importFileName '
	   			 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblActivityImport] AI '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTUser on (TTUser.sourceId = AI.idUser AND TTUser.object = ''users'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTUserImported on (TTUserImported.sourceId = AI.idUserImported AND TTUserImported.object = ''users'') '
	   			 + 'WHERE AI.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 4.7: Copy Users - Activity Import Records Inserted', 0)
	   
		-- insert the mapping for source to destination activity import ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	SRC.idActivityImport, '
	   			 + '	DST.idActivityImport, '
	   			 + '	''activityimport'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblActivityImport] DST '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblActivityImport] SRC '
	   			 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
	   			 + '	AND (SRC.timestamp = DST.timestamp) '
	   			 + '	AND (SRC.importFileName = DST.importFileName OR (SRC.importFileName IS NULL AND DST.importFileName IS NULL))) '
	   			 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 4.8: Copy Users - Activity Import Mappings Created', 0)

		/*

		RETURN

		*/
		
		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemClonePortal_User_InsertedSuccessfully'

	END TRY

	BEGIN CATCH
		
		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 4: Copy Users - ERROR - ' + CAST(ERROR_LINE() AS NVARCHAR) + ' | ' + CAST(ERROR_MESSAGE() AS NVARCHAR(MAX)), 1)
		SET @Return_Code = 1
		SET @Error_Description_Code	= 'SystemClonePortal_User_PortalMigrationFailed'
			
	END CATCH
			
	SET XACT_ABORT OFF

END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.CopyInquisiqToAsentia]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.CopyInquisiqToAsentia]
GO


/*

Copy source site data from Inquisiq to destination site in Asentia

*/
CREATE PROCEDURE [dbo].[System.CopyInquisiqToAsentia]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,	
	@idAccount				INT,
	@idCaller				INT				= 1,
	@idSiteDefaultLang		INT				= 57,		-- English is ID 57	
	@siteHostname			NVARCHAR(255),
	@idSiteDestination		INT				OUTPUT,
	@destinationHostname	NVARCHAR(255),
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@idSiteSource			INT				OUTPUT,
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50)	
)
AS

BEGIN

    SET NOCOUNT ON
    SET XACT_ABORT ON    
    	
	DECLARE @sql NVARCHAR(MAX)
	  
	/*
	  
	create id mappings table or clear out existing id mappings table

	*/

	SET @sql = 'IF NOT EXISTS (SELECT * FROM [' + @destinationDBName + '].dbo.sysobjects WHERE name = N''InquisiqMigration_IDMappings'' AND xtype = ''U'')'
			+ ' BEGIN '
			+ 'CREATE TABLE [' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings]'
			+ '('
			+ '    [sourceID] INT NOT NULL,'
			+ '    [destinationID] INT NOT NULL,'
			+ '    [object] NVARCHAR(255)'
			+ ')'
			+ ' END '
			+ ' ELSE '
	SET @sql = @sql + ' DELETE FROM [' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings]'

	EXEC(@sql)
	  
	/*
	  
	create timezone mappings table, if it doesnt already exist

	*/

	SET @sql = 'IF NOT EXISTS (SELECT * FROM [' + @destinationDBName + '].dbo.sysobjects WHERE name = N''InquisiqMigration_TimezoneIDMappings'' AND xtype = ''U'')'
			+ ' BEGIN '
			+ 'CREATE TABLE [' + @destinationDBName + '].[dbo].[InquisiqMigration_TimezoneIDMappings]'
			+ '('
			+ '    [sourceID] INT NOT NULL,'
			+ '    [destinationID] INT NOT NULL'
			+ ')'
			+ ' '
			+ ' INSERT INTO [' + @destinationDBName + '].[dbo].[InquisiqMigration_TimezoneIDMappings] (sourceID, destinationID) '
			+ ' VALUES '
			+ ' (0, 0), '
			+ ' (1, 1), '
			+ ' (2, 102), '
			+ ' (3, 3), '
			+ ' (4, 4), '
			+ ' (5, 6), '
			+ ' (6, 5), '
			+ ' (7, 7), '
			+ ' (8, 8), '
			+ ' (9, 8), '
			+ ' (10, 9), '
			+ ' (11, 10), '
			+ ' (12, 11), '
			+ ' (13, 12), '
			+ ' (14, 12), '
			+ ' (15, 13), '
			+ ' (16, 14), '
			+ ' (17, 15), '
			+ ' (18, 16), '
			+ ' (19, 17), '
			+ ' (20, 19), '
			+ ' (21, 21), '
			+ ' (22, 21), '
			+ ' (23, 22), '
			+ ' (24, 23), '
			+ ' (25, 24), '
			+ ' (26, 25), '
			+ ' (27, 27), '
			+ ' (28, 28), '
			+ ' (29, 31), '
			+ ' (30, 32), '
			+ ' (31, 33), '
			+ ' (32, 34), '
			+ ' (33, 36), '
			+ ' (34, 38), '
			+ ' (35, 39), '
			+ ' (36, 40), '
			+ ' (37, 41), '
			+ ' (38, 43), '
			+ ' (39, 54), '
			+ ' (40, 45), '
			+ ' (41, 46), '
			+ ' (42, 47), '
			+ ' (43, 50), '
			+ ' (44, 51), '
			+ ' (45, 53), '
			+ ' (46, 56), '
			+ ' (47, 44), '
			+ ' (48, 55), '
			+ ' (49, 57), '
			+ ' (50, 62), '
			+ ' (51, 58), '
			+ ' (52, 64), '
			+ ' (53, 59), '
			+ ' (54, 60), '
			+ ' (55, 61), '
			+ ' (56, 65), '
			+ ' (57, 65), '
			+ ' (58, 66), '
			+ ' (59, 74), '
			+ ' (60, 68), '
			+ ' (61, 69), '
			+ ' (62, 70), '
			+ ' (63, 71), '
			+ ' (64, 77), '
			+ ' (65, 72), '
			+ ' (66, 75), '
			+ ' (67, 76), '
			+ ' (68, 79), '
			+ ' (69, 78), '
			+ ' (70, 83), '
			+ ' (71, 80), '
			+ ' (72, 81), '
			+ ' (73, 82), '
			+ ' (74, 85), '
			+ ' (75, 86), '
			+ ' (76, 93), '
			+ ' (77, 87), '
			+ ' (78, 88), '
			+ ' (79, 89), '
			+ ' (80, 90), '
			+ ' (81, 91), '
			+ ' (82, 92), '
			+ ' (83, 95), '
			+ ' (84, 99), '
			+ ' (85, 96), '
			+ ' (86, 98), '
			+ ' (87, 101) '
			+ ' END '		

	EXEC(@sql)

	/*
	  
	create event type mappings table, if it doesnt already exist

	*/

	SET @sql = 'IF NOT EXISTS (SELECT * FROM [' + @destinationDBName + '].dbo.sysobjects WHERE name = N''InquisiqMigration_EventTypeIDMappings'' AND xtype = ''U'')'
			+ ' BEGIN '
			+ 'CREATE TABLE [' + @destinationDBName + '].[dbo].[InquisiqMigration_EventTypeIDMappings]'
			+ '('
			+ '    [sourceID] INT NOT NULL,'
			+ '    [destinationID] INT NOT NULL'
			+ ')'
			+ ' '
			+ ' INSERT INTO [' + @destinationDBName + '].[dbo].[InquisiqMigration_EventTypeIDMappings] (sourceID, destinationID) '
			+ ' VALUES '
			+ ' (0, 0), '			
			+ ' (1, 702), '		-- Certificate Awarded
			+ ' (2, 701), '		-- Certificate Earned
			+ ' (3, 703), '		-- Certificate Revoked
			+ ' (4, 0), '		-- Purchase Confirmed - NOT IN ASENTIA
			+ ' (5, 206), '		-- Enrollment Start
			+ ' (6, 205), '		-- Enrollment End
			+ ' (7, 203), '		-- Enrollment Revoked
			+ ' (8, 204), '		-- Enrollment Due
			+ ' (9, 301), '		-- Lesson Passed
			+ ' (10, 302), '	-- Lesson Failed
			+ ' (11, 303), '	-- Lesson Completed
			+ ' (14, 101), '	-- User Created
			+ ' (15, 102), '	-- User Deleted
			+ ' (16, 402), '	-- Session Instructor Assigned or Changed
			+ ' (17, 403), '	-- Session Instructor Removed
			+ ' (18, 404), '	-- Learner Joined to Session
			+ ' (19, 405), '	-- Learner Dropped From Session
			+ ' (20, 406), '	-- Session Time or Location Changed
			+ ' (21, 205), '	-- Enrollment Completed
			+ ' (22, 103), '	-- User Account Expires
			+ ' (24, 401), '	-- Session Meets
			+ ' (25, 704) '		-- Certificate Expires
			+ ' END '		

	EXEC(@sql)

	/*
	  
	create event type recipient mappings table, if it doesnt already exist

	*/

	SET @sql = 'IF NOT EXISTS (SELECT * FROM [' + @destinationDBName + '].dbo.sysobjects WHERE name = N''InquisiqMigration_EventTypeRecipientIDMappings'' AND xtype = ''U'')'
			+ ' BEGIN '
			+ 'CREATE TABLE [' + @destinationDBName + '].[dbo].[InquisiqMigration_EventTypeRecipientIDMappings]'
			+ '('
			+ '    [sourceID] INT NOT NULL,'
			+ '    [destinationID] INT NOT NULL'
			+ ')'
			+ ' '
			+ ' INSERT INTO [' + @destinationDBName + '].[dbo].[InquisiqMigration_EventTypeRecipientIDMappings] (sourceID, destinationID) '
			+ ' VALUES '
			+ ' (0, 0), '
			+ ' (1, 1), '
			+ ' (3, 2), '
			+ ' (4, 1), '
			+ ' (5, 2), '
			+ ' (6, 1), '
			+ ' (7, 2), '
			+ ' (8, 1), '
			+ ' (9, 3), '
			+ ' (10, 1), '
			+ ' (11, 2), '
			+ ' (12, 1), '
			+ ' (13, 2), '
			+ ' (14, 1), '
			+ ' (15, 2), '
			+ ' (16, 1), '
			+ ' (17, 2), '
			+ ' (18, 1), '
			+ ' (19, 2), '
			+ ' (20, 1), '
			+ ' (21, 2), '
			+ ' (22, 1), '
			+ ' (23, 2), '
			+ ' (24, 1), '
			+ ' (25, 2), '
			+ ' (28, 1), '
			+ ' (29, 3), '
			+ ' (30, 1), '
			+ ' (32, 1), '
			+ ' (33, 5), '
			+ ' (34, 6), '
			+ ' (35, 1), '
			+ ' (36, 5), '
			+ ' (38, 1), '
			+ ' (39, 5), '
			+ ' (40, 2), '
			+ ' (41, 1), '
			+ ' (42, 5), '
			+ ' (43, 2), '
			+ ' (44, 1), '
			+ ' (45, 5), '
			+ ' (46, 6), '
			+ ' (47, 1), '
			+ ' (48, 2), '
			+ ' (49, 8), '
			+ ' (50, 8), '
			+ ' (51, 8), '
			+ ' (52, 8), '
			+ ' (53, 8), '
			+ ' (54, 8), '
			+ ' (55, 8), '
			+ ' (56, 8), '
			+ ' (57, 8), '
			+ ' (58, 8), '
			+ ' (59, 8), '
			+ ' (60, 8), '
			+ ' (61, 8), '
			+ ' (62, 1), '
			+ ' (63, 3), '
			+ ' (64, 1), '
			+ ' (65, 5), '
			+ ' (66, 6), '
			+ ' (67, 1), '
			+ ' (68, 2), '
			+ ' (69, 8) '
			+ ' END '		

	EXEC(@sql)

	/*
	
	validate unique host name in Account database -- this runs in scope of "Customer Manager" so it can be executed here instead of as dynamic SQL
	
	*/
	
	IF (SELECT COUNT(1) FROM tblAccountToDomainAliasLink WHERE hostname = @destinationHostname) > 0
		BEGIN
		SET @Return_Code = 2
		SET @Error_Description_Code = 'SystemCopyInquisiqToAsentia_HostnameNotUnique'
		RETURN 1
		END

	/*

	get the id of the source (Inquisiq) site

	*/

	SET @sql = 'SELECT @idSiteSource = idSite FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblSite] WHERE hostname = ''' + @siteHostname + ''''

	EXECUTE SP_EXECUTESQL @sql, N'@idSiteSource INT OUTPUT', @idSiteSource OUTPUT
	
    /*

	TRANSACTION BEGIN

	*/

	BEGIN DISTRIBUTED TRANSACTION

	-- =============================================================================
	-- LOGIC START

	/************************************************SITE*************************************************************/
		-- tblSite
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblSite]'
				+ ' ( '
				+ '		hostname, '
				+ '		password, '
				+ '		isActive, '
				+ '		dtExpires, '
				+ '		title, '
				+ '		company, '
				+ '		contactName, '
				+ '		contactEmail, '
				+ '		userLimit, '
				+ '		kbLimit, '
				+ '		idLanguage, '
				+ '		idTimezone' 
				+ ' ) '
				+ ' SELECT ' 
				+ '		''' + @destinationHostname + ''','							-- hostname
				+ '		S.password,'												-- password (this is an MD5 value)
				+ '		S.isActive,'												-- is active?
				+ '		S.dtExpires,'												-- date expires
				+ '		S.name,'													-- title
				+ '		SC.company,'												-- company
				+ '		SC.firstName + '' '' + SC.lastName,'						-- contact name
				+ '		SC.email,'													-- contact email
				+ '		CASE WHEN S.userLimit > 0 THEN S.userLimit ELSE NULL END,'	-- user limit
				+ '		CASE WHEN S.kbLimit > 0 THEN S.kbLimit ELSE NULL END,'		-- kb limit
				+ '		' + CAST(@idSiteDefaultLang AS NVARCHAR) + ','				-- language ID
				+ '		15'															-- timezone ID				
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblSite] S'
				+ ' LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].[tblSiteContact] SC ON SC.idSite = S.idSite'
				+ ' WHERE S.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
		
		EXEC (@sql)

		-- get the newly inserted site id as @idSiteDestination
		SET @sql = 'SELECT @idSiteDestination = idSite FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblSite] WHERE hostname = ''' + @destinationHostname + ''''

		EXECUTE SP_EXECUTESQL @sql, N'@idSiteDestination INT OUTPUT', @idSiteDestination OUTPUT
	  
		-- insert idSite of source and destination site tables inside temporary table for mapping	
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteSource) + ', '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		''site'' ' 				
		
		EXEC(@sql)		   	   

		-- tblSiteLanguage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblSiteLanguage]' 
				+ ' ( '
				+ '		idSite,'
				+ '		idLanguage,'
				+ '		title,'
				+ '		company'
				+ '	) '
				+ ' SELECT '
				+ '		S.idSite, '
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ','
				+ '		S.title, '
				+ '		S.company'
				+ ' FROM  [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblSite] S'
				+ ' WHERE S.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
		
		EXEC(@sql)	

		-- tblSiteToDomainAliasLink
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblSiteToDomainAliasLink]' 
				+ ' ( '
				+ '		idSite,'
				+ '		domain'				
				+ '	) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','
				+ '		''' + @destinationHostname + ''''				
		
		EXEC(@sql)

	/************************************************ USER ******************************************************/
		-- tblUser
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblUser]'
				+ ' ( '
				+ '		firstName, '
				+ '		middleName, '
				+ '		lastName, '
				+ '		displayName, '
				+ '		email, '
				+ '		username, '
				+ '		password, '
				+ '		idSite, '
				+ '		isDeleted, '
				+ '		idTimezone, '
				+ '		objectGUID, '
				+ '		activationGUID, '
				+ '		distinguishedName, '
				+ '		isActive, '
				+ '		mustchangePassword, '  
				+ '		dtCreated, '
				+ '		dtExpires, '
				+ '		idLanguage, '
				+ '		dtModified, '
				+ '		employeeID, '
				+ '		company, '
				+ '		address, '
				+ '		city, '
				+ '		province, '
				+ '		postalcode, '
				+ '		country, '
				+ '		phonePrimary, '
				+ '		phoneWork, '
				+ '		phoneFax, '
				+ '		phoneHome, '
				+ '		phoneMobile, '
				+ '		phonePager, '
				+ '		phoneOther, '
				+ '		department, '
				+ '		division, '
				+ '		region, '
				+ '		jobTitle, '
				+ '		jobClass, '
				+ '		gender, '
				+ '		race, '
				+ '		dtDOB, '
				+ '		dtHire, '
				+ '		dtTerm, '
				+ '		field00, '
				+ '		field01, '
				+ '		field02, '
				+ '		field03, '
				+ '		field04, '
				+ '		field05, '
				+ '		field06, '
				+ '		field07, '
				+ '		field08, '
				+ '		field09, '
				+ '		field10, '
				+ '		field11, '
				+ '		field12, '
				+ '		field13, '
				+ '		field14, '
				+ '		field15, '
				+ '		field16, '
				+ '		field17, '
				+ '		field18, '
				+ '		field19, '
				+ '		avatar, '
				+ '		dtLastLogin, '
				+ '		dtDeleted, '
				+ '		dtSessionExpires, '
				+ '		activeSessionId, '
				+ '		dtLastPermissionCheck, '
				+ '		isRegistrationApproved, '
				+ '		dtApproved, '
				+ '		dtDenied, '				
				+ '		idApprover, '
				+ '		rejectionComments '
				+ ' ) '
				+ ' SELECT ' 
				+ '		U.firstName,'									-- first name
				+ '		U.middleName,'									-- middle name
				+ '		U.lastName,'									-- last name
				+ '		U.lastName + '', '' + U.firstName,'				-- display name
				+ '		U.email,'										-- email
				+ '		U.username,'									-- username
				+ '		U.password,'									-- password (this is an MD5 value)
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- site ID
				+ '		0,'												-- is deleted? 
				+ '		ISNULL(TZ.destinationID, 15) AS idTimezone,'	-- timezone id (default to Eastern timezone if the value is NULL)
				+ '		U.objectGUID,'									-- object GUID
				+ '		NULL,'											-- activation GUID
				+ '		U.distinguishedName,'							-- distinguished name
				+ '		U.isActive,'									-- is active?
				+ '		1,'												-- must change password? (since Asentia uses SHA1, we need to force all users to change their password)
				+ '		U.dtCreated,'									-- date created
				+ '		U.dtExpires,'									-- date expires
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ','	-- language id, use Asentia default
				+ '		U.dtModified,'									-- date modified
				+ '		UP.employeeID,'									-- employeed ID
				+ '		UP.company,'									-- company
				+ '		UP.address,'									-- street address
				+ '		UP.city,'										-- city
				+ '		UP.province,'									-- state/province
				+ '		UP.postalcode,'									-- postal code
				+ '		UP.country,'									-- country
				+ '		UP.phonePrimary,'								-- primary phone
				+ '		UP.phoneWork,'									-- work phone
				+ '		UP.phoneFax,'									-- fax
				+ '		UP.phoneHome,'									-- home phone
				+ '		UP.phoneMobile,'								-- mobile phone
				+ '		UP.phonePager,'									-- pager
				+ '		UP.phoneOther,'									-- other phone
				+ '		UP.department,'									-- department
				+ '		UP.division,'									-- division
				+ '		UP.region,'										-- region
				+ '		UP.jobTitle,'									-- job title
				+ '		UP.jobClass,'									-- job class
				+ '		UP.gender,'										-- gender
				+ '		UP.race,'										-- race
				+ '		UP.dtDOB,'										-- date of birth
				+ '		UP.dtHire,'										-- hire date
				+ '		UP.dtTerm,'										-- term date
				+ '		UDUFV.field00,'									-- user defined field00
				+ '		UDUFV.field01,'									-- user defined field01
				+ '		UDUFV.field02,'									-- user defined field02
				+ '		UDUFV.field03,'									-- user defined field03
				+ '		UDUFV.field04,'									-- user defined field04
				+ '		UDUFV.field05,'									-- user defined field05
				+ '		UDUFV.field06,'									-- user defined field06
				+ '		UDUFV.field07,'									-- user defined field07
				+ '		UDUFV.field08,'									-- user defined field08
				+ '		UDUFV.field09,'									-- user defined field09
				+ '		UDUFV.field10,'									-- user defined field10
				+ '		NULL,'											-- user defined field11
				+ '		NULL,'											-- user defined field12
				+ '		NULL,'											-- user defined field13
				+ '		NULL,'											-- user defined field14
				+ '		NULL,'											-- user defined field15
				+ '		NULL,'											-- user defined field16
				+ '		NULL,'											-- user defined field17
				+ '		NULL,'											-- user defined field18
				+ '		NULL,'											-- user defined field19
				+ '		NULL,'											-- avatar
				+ '		NULL,'											-- date last login (set to null because user has never logged into Asentia)
				+ '		NULL,'											-- date deleted
				+ '		NULL,'											-- date session expires
				+ '		NULL,'											-- active session ID
				+ '		NULL,'											-- date last permission check
				+ '		NULL,'											-- is registration approved?
				+ '		NULL,'											-- date approved
				+ '		NULL,'											-- date denied
				+ '		NULL,'											-- approver id
				+ '		NULL'											-- rejection comments
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].' + '[tblUser] U'
				+ ' LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblUserProperties] UP ON UP.idUser = U.idUser'
				+ ' LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblUserDefinedUserFieldValue] UDUFV ON UDUFV.idUser = U.idUser'
				+ ' LEFT JOIN [' + @destinationDBName + '].[dbo].[InquisiqMigration_TimezoneIDMappings] TZ ON TZ.sourceID = U.idTimezone'
				+ ' WHERE U.idSite = '+ CONVERT(NVARCHAR, @idSiteSource)
		
		EXEC (@sql)
	  
		-- insert mapping for idUser 1 (admin)
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		1, '
				+ '		1, '
				+ '		''user'''
				+ ' ) ' 

		EXEC(@sql)

		-- insert idUser mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SU.idUser, '
				+ '		DU.idUser,'
				+ '		''user''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblUser] DU '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblUser] SU ON SU.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND SU.username = DU.username collate database_default AND SU.dtCreated = DU.dtCreated'
				+ ' WHERE DU.idSite IS NOT NULL AND DU.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SU.idUser IS NOT NULL AND DU.idUser IS NOT NULL'
		
		EXEC(@sql)

	/*****************************************************GROUP********************************************************/
		-- tblGroup
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblGroup]'
				+ ' ( '
				+ '		idSite, '
				+ '		name, '
				+ '		primaryGroupToken, '
				+ '		objectGUID, '
				+ '		distinguishedName, '
				+ '		avatar, '
				+ '		isSelfJoinAllowed, '
				+ '		shortDescription, '
				+ '		longDescription, '
				+ '		isFeedActive, '
				+ '		isFeedModerated, '
				+ '		membershipIsPublicized,'
				+ '		searchTags'
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- idSite
				+ '		G.name,'										-- name
				+ '		G.primaryGroupToken,'							-- primary group token
				+ '		G.objectGUID,'									-- object GUID
				+ '		G.distinguishedName,'							-- distinguished name
				+ '		NULL,'											-- avatar
				+ '		0,'												-- is self joined allowed?
				+ '		NULL,'											-- short description
				+ '		NULL,'											-- long description
				+ '		0,'												-- is feed active?
				+ '		0,'												-- is feed moderated?
				+ '		0,'												-- membership is publicized?
				+ '		NULL'											-- search tags
				+ '		FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblGroup] G'
				+ ' WHERE G.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)

		EXEC (@sql)

		-- insert idGroup mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SG.idGroup, '
				+ '		DG.idGroup,'
				+ '		''group''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblGroup] DG '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblGroup] SG ON SG.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND SG.name = DG.name collate database_default'
				+ ' WHERE DG.idSite IS NOT NULL AND DG.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SG.idGroup IS NOT NULL AND DG.idGroup IS NOT NULL'
						
		EXEC(@sql)

		-- tblGroupLanguage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblGroupLanguage]' 
				+ ' ( '
				+ '		idSite,'
				+ '		idGroup,'
				+ '		idLanguage,'
				+ '		name,'
				+ '		shortDescription,'
				+ '		longDescription,'
				+ '		searchTags'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		G.idGroup, '
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ', '
				+ '		G.name, '
				+ '		G.shortDescription, '
				+ '		G.longDescription, '
				+ '		G.searchTags '
				+ ' FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblGroup] G'
				+ ' WHERE G.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
		
		EXEC(@sql)	

	/**********************************************COURSE************************************************************/
		-- tblCourse
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCourse]'
				+ ' ( '
				+ '		idSite, '
				+ '		title, '
				+ '		coursecode, '
				+ '		revcode, '
				+ '		cost, '
				+ '		credits, '
				+ '		shortDescription, '
				+ '		longDescription, '
				+ '		isPublished, '
				+ '		isClosed, '
				+ '		isLocked,'
				+ '		dtCreated,'
				+ '		dtModified,'
				+ '		isDeleted,'
				+ '		dtDeleted,'
				+ '		minutes,'
				+ '		objectives,'
				+ '		socialMedia,'
				+ '		isPrerequisiteAny,'
				+ '		avatar,'
				+ '		isFeedActive,'
				+ '		isFeedModerated,'
				+ '		isFeedOpenSubscription,'
				+ '		disallowRating,'
				+ '		forceLessonCompletionInOrder,'
				+ '		selfEnrollmentDueInterval,'
				+ '		selfEnrollmentDueTimeframe,'
				+ '		selfEnrollmentExpiresFromStartInterval,'
				+ '		selfEnrollmentExpiresFromStartTimeframe,'
				+ '		selfEnrollmentExpiresFromFirstLaunchInterval,'
				+ '		selfEnrollmentExpiresFromFirstLaunchTimeframe,'
				+ '		searchTags,'
				+ '		requireFirstLessonToBeCompletedBeforeOthers,'
				+ '		lockLastLessonUntilOthersCompleted,'
				+ '		isSelfEnrollmentApprovalRequired,'
				+ '		isApprovalAllowedByCourseExperts,'
				+ '		isApprovalAllowedByUserSupervisor'
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- idSite
				+ '		C.name,'										-- title
				+ '		C.code,'										-- code
				+ '		NULL,'											-- revcode
				+ '		C.cost,'										-- cost
				+ '		C.credits,'										-- credits
				+ '		C.shortDescription,'							-- short description
				+ '		C.description,'									-- description
				+ '		C.isPublished,'									-- is published?
				+ '		C.isClosed,'									-- is closed?	
				+ '		C.isLocked,'									-- is locked?
				+ '		C.dtCreated,'									-- date created
				+ '		C.dtModified,'									-- date modified
				+ '		0,'												-- is deleted?
				+ '		NULL,'											-- date deleted
				+ '		C.minutes,'										-- minutes
				+ '		C.objectives,'									-- objectives
				+ '		REPLACE(REPLACE(C.socialMedia, ''protocol="http://"'', ''protocol="http://" iconType="default"''), ''protocol="https://"'', ''protocol="https://" iconType="default"''),' -- social media
				+ '		C.prerequisiteAny,'								-- any or all prerequisites?
				+ '		NULL,'											-- avatar
				+ '		0,'												-- is feed active?
				+ '		0,'												-- is feed moderated?
				+ '		0,'												-- is feed open subscription?
				+ '		~C.allowRating,'								-- disallow rating? (negate it because Asentia's field is "disallow")
				+ '		C.blnForceLessonCompletionInOrder,'				-- force lesson completion in order?
				+ '		NULL,'											-- self enrollment due interval
				+ '		NULL,'											-- self enrollment due timeframe
				+ '		C.accessInterval,'								-- self enrollment expires from start interval
				+ '		C.accessTimeFrame,'								-- self enrollment expires from start timeframe
				+ '		NULL,'											-- self enrollment expires from first launch interval
				+ '		NULL,'											-- self enrollment expires from first launch timeframe
				+ '		NULL,'											-- search tags
				+ '		C.blnFirstLessonRequired,'						-- require first lesson to be completed before others
				+ '		C.blnLastLessonLocked,'							-- lock last lesson until others completed
				+ '		NULL,'											-- is self enrollment approval required?
				+ '		NULL,'											-- is approval allowed by course experts?
				+ '		NULL'											-- is approval allowed by user supervisor?
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCourse] C'
				+ ' WHERE C.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)
		
		EXEC (@sql)

		-- insert idCourse mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SC.idCourse, '
				+ '		DC.idCourse,'
				+ '		''course''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCourse] DC '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCourse] SC ON SC.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND SC.name = DC.title collate database_default AND SC.dtModified = DC.dtModified'
				+ ' WHERE DC.idSite IS NOT NULL AND DC.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SC.idCourse IS NOT NULL AND DC.idCourse IS NOT NULL'
		
		EXEC(@sql)

		-- tblCourseLanguage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblCourseLanguage]' 
				+ ' ( '
				+ '		idSite,'
				+ '		idCourse,'
				+ '		idLanguage,'
				+ '		title, '
				+ '		shortDescription, '
				+ '		longDescription, '
				+ '		objectives, '
				+ '		searchTags '
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		C.idCourse, '
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ', '
				+ '		C.title, '
				+ '		C.shortDescription, '
				+ '		C.longDescription, '
				+ '		C.objectives, '
				+ '		C.searchTags '
				+ ' FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblCourse] C'
				+ ' WHERE C.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)

		EXEC(@sql)
	   
		-- tblCourseRating
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCourseRating]'
				+ ' ( '
				+ '		idSite, '
				+ '		idCourse, '
				+ '		idUser, '
				+ '		rating, '
				+ '		timestamp '
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- idSite
				+ '		C.destinationID,'								-- course id
				+ '		U.destinationID,'								-- user id
				+ '		CR.rating,'										-- rating
				+ '		CR.timestamp'									-- timestamp
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCourseRating] CR'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] C ON C.sourceID = CR.idCourse AND C.object = ''course'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] U ON U.sourceID = CR.idUser AND U.object = ''user'''
				+ ' WHERE C.destinationID IS NOT NULL AND U.destinationID IS NOT NULL'
		
		EXEC (@sql)

		-- tblCourseExpert	
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCourseExpert]'
				+ ' ( '
				+ '		idSite, '
				+ '		idCourse, '
				+ '		idUser '
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- idSite
				+ '		CM.destinationID,'								-- course id
				+ '		UM.destinationID'								-- user id
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCourse] C'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.sourceID = C.idCourse AND CM.object = ''course'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] UM ON UM.sourceID = C.idExpert AND UM.object = ''user'''
				+ ' WHERE C.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND CM.destinationID IS NOT NULL AND UM.destinationID IS NOT NULL'
		
		EXEC (@sql)

		-- tblDocumentRespositoryItem (tblCourseMaterial in Inquisiq) - file copies are done inside of procedural code
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblDocumentRepositoryItem]'
				+ ' ( '
				+ '		idSite, '
				+ '		idDocumentRepositoryObjectType, '
				+ '		idObject, '
				+ '		idDocumentRepositoryFolder, '
				+ '		idOwner, '
				+ '		filename, '
				+ '		kb, '
				+ '		searchTags, '
				+ '		isPrivate, '
				+ '		idLanguage, '
				+ '		isAllLanguages, '
				+ '		dtCreated, '
				+ '		isDeleted, '
				+ '		dtDeleted, '
				+ '		label, '
				+ '		isVisibleToUser, '
				+ '		isUploadedByUser '
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- idSite
				+ '		2,'												-- document repository object type (course = 2)
				+ '		C.destinationID,'								-- object id (course id)
				+ '		NULL,'											-- document repository folder id
				+ '		1,'												-- owner id
				+ '		CM.filename,'									-- filename
				+ '		CM.kb,'											-- kb
				+ '		NULL,'											-- search tags
				+ '		CM.isPrivate,'									-- is private?
				+ '		NULL,'											-- language id
				+ '		1,'												-- is all languages?
				+ '		GETUTCDATE(),'									-- date created
				+ '		0,'												-- is deleted?
				+ '		NULL,'											-- date deleted
				+ '		CM.name,'										-- label
				+ '		NULL,'											-- is visible to user?
				+ '		NULL'											-- is uploaded by user?
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCourseMaterial] CM'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] C ON C.sourceID = CM.idCourse AND C.object = ''course'''
				+ ' WHERE C.destinationID IS NOT NULL'
		
		EXEC (@sql)

		-- tblDocumentRepositoryItemLanguage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblDocumentRepositoryItemLanguage]' 
				+ ' ( '
				+ '		idSite,'
				+ '		idDocumentRepositoryItem,'
				+ '		idLanguage,'
				+ '		label, '				
				+ '		searchTags '
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		DRI.idDocumentRepositoryItem, '
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ', '
				+ '		DRI.label, '				
				+ '		DRI.searchTags '
				+ ' FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblDocumentRepositoryItem] DRI'
				+ ' WHERE DRI.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND DRI.idDocumentRepositoryObjectType = 2'

		EXEC(@sql)

	/*****************************************************LESSON**********************************************************************/
		-- tblLesson
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblLesson]'
				+ ' ( '
				+ '		idSite, '
				+ '		idCourse, '
				+ '		dtDeleted, '
				+ '		isDeleted, '
				+ '		title, '
				+ '		revcode, '
				+ '		shortDescription, '
				+ '		longDescription, '
				+ '		[order], '
				+ '		dtCreated,'
				+ '		dtModified,'
				+ '		searchTags'
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- idSite
				+ '		CM.destinationID,'								-- course id
				+ '		NULL,'											-- deleted date
				+ '		0,'												-- is deleted?
				+ '		L.name,'										-- title
				+ '		NULL,'											-- revcode
				+ '		L.shortDescription,'							-- short description
				+ '		L.description,'									-- long description
				+ '		L.[order],'										-- lesson order
				+ '		GETUTCDATE(),'									-- date created
				+ '		GETUTCDATE(),'									-- date modified
				+ '		NULL'											-- search tags
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLesson] L'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.sourceID = L.idCourse AND CM.object = ''course'''
				+ ' WHERE CM.destinationID IS NOT NULL'
		
		EXEC (@sql)	  

		-- insert idLesson mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SL.idLesson, '
				+ '		DL.idLesson,'
				+ '		''lesson''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblLesson] DL '
				+ ' LEFT JOIN [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.destinationID = DL.idCourse AND CM.object = ''course'''
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLesson] SL ON SL.idCourse = CM.sourceID AND SL.name = DL.title collate database_default'
				+ ' WHERE DL.idSite IS NOT NULL AND DL.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SL.idLesson IS NOT NULL AND DL.idLesson IS NOT NULL'
		
		EXEC(@sql)

		-- tblLessonLanguage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblLessonLanguage]' 
				+ ' ( '
				+ '		idSite,'
				+ '		idLesson,'
				+ '		idLanguage,'
				+ '		title,'
				+ '		shortDescription,'
				+ '		longDescription,'
				+ '		searchTags'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		L.idLesson, '
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ', '
				+ '		L.title, '
				+ '		L.shortDescription, '
				+ '		L.longDescription, '
				+ '		L.searchTags '
				+ ' FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblLesson] L'
				+ ' WHERE L.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
		
		EXEC(@sql)	

	/*****************************************************INSTRUCTOR LED TRAINING**********************************************************************/
		-- tblStandUpTraining (this needs to be sourced from tblLesson in Inquisiq)
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblStandUpTraining]'
				+ ' ( '
				+ '		idSite, '
				+ '		title, '
				+ '		description, '
				+ '		isStandaloneEnroll, '
				+ '		isRestrictedEnroll, '
				+ '		isRestrictedDrop, '
				+ '		searchTags, '
				+ '		avatar, '
				+ '		isDeleted,'
				+ '		dtDeleted,'
				+ '		cost'
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','					-- idSite
				+ '		L.name + '' ##'' + CONVERT(NVARCHAR, L.idLesson) + ''##'','		-- title
				+ '		NULL,'															-- description
				+ '		0,'																-- isStandaloneEnroll
				+ '		0,'																-- isRestrictedEnroll
				+ '		0,'																-- isRestrictedDrop
				+ '		NULL,'															-- searchTags
				+ '		NULL,'															-- avatar
				+ '		0,'																-- isDeleted
				+ '		NULL,'															-- dtDeleted
				+ '		NULL'															-- cost
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLesson] L'				
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.sourceID = L.idCourse AND CM.object = ''course'''
				+ ' WHERE CM.destinationID IS NOT NULL'
				+ ' AND L.idLessonType IN (1, 2)' -- 1 = classroom, 2 = web meeting
						
		EXEC (@sql)

		-- insert idStandupTraining mappings (idStandupTraining maps to Inquisiq idLesson as they are equivalents)
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SL.idLesson, '
				+ '		DST.idStandupTraining,'
				+ '		''ilt''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblStandupTraining] DST '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLesson] SL ON SL.name + '' ##'' + CONVERT(NVARCHAR, SL.idLesson) + ''##'' = DST.title collate database_default'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCourse] C ON C.idCourse = SL.idCourse AND C.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)				
				+ ' WHERE DST.idSite IS NOT NULL AND DST.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SL.idLesson IS NOT NULL AND DST.idStandupTraining IS NOT NULL'
		
		EXEC(@sql)

		-- clean up the ##idLesson## additions we made to standup training titles, we did that to uniquely distinguish names
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblStandupTraining] SET '
				+ '		title = REPLACE(DST.title, '' ##'' + CONVERT(NVARCHAR, STM.sourceID) + ''##'', '''') '
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblStandupTraining] DST '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] STM ON STM.destinationID = DST.idStandupTraining AND STM.object = ''ilt'''
				+ ' WHERE STM.sourceID IS NOT NULL'

		EXEC (@sql)

		--tblStandUpTrainingLanguage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblStandUpTrainingLanguage]' 
				+ ' ( '
				+ '		idSite,'
				+ '		idStandUpTraining,'
				+ '		idLanguage,'
				+ '		title,'
				+ '		description,'
				+ '		searchTags'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		ST.idStandUpTraining, '
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ', '
				+ '		ST.title, '
				+ '		ST.description, '
				+ '		ST.searchTags '
				+ ' FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblStandUpTraining] ST'
				+ ' WHERE ST.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
		
		EXEC(@sql)

		-- tblStandUpTrainingInstance
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblStandUpTrainingInstance]'
				+ ' ( '
				+ '		idStandUpTraining, '
				+ '		idSite, '
				+ '		title, '
				+ '		description, '
				+ '		seats, '
				+ '		waitingSeats, '
				+ '		urlRegistration, '
				+ '		urlAttend, '
				+ '		city, '
				+ '		province,'
				+ '		locationDescription,'
				+ '		idInstructor,'
				+ '		type,'
				+ '		isDeleted,'
				+ '		dtDeleted,'
				+ '		integratedObjectKey,'
				+ '		hostUrl,'
				+ '		genericJoinUrl,'
				+ '		meetingPassword,'
				+ '		idWebMeetingOrganizer'
				+ ' ) '
				+ ' SELECT ' 
				+ '		STM.destinationID, '																				-- idStandupTraining
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','														-- idSite
				+ '		CONVERT(NVARCHAR, S.[datetime], 101) + '' ('' + CONVERT(NVARCHAR, S.[datetime], 108) + '' UTC)'' + '' ##'' + CONVERT(NVARCHAR, S.idSession) + ''##'','	-- title
				+ '		NULL,'																								-- description
				+ '		S.seats,'																							-- seats
				+ '		S.waitingSeats,'																					-- waitingSeats
				+ '		NULL,'																								-- urlRegistration
				+ '		S.url,'																								-- urlAttend
				+ '		S.city,'																							-- city
				+ '		S.province,'																						-- province
				+ '		S.locationDescription,'																				-- locationDescription
				+ '		UM.destinationID,'																					-- idInstructor
				+ '		CASE WHEN L.idLessonType = 1 THEN 1 ELSE 0 END,'													-- type
				+ '		0,'																									-- is deleted?
				+ '		NULL,'																								-- date deleted
				+ '		NULL,'																								-- integrated object key
				+ '		NULL,'																								-- host url
				+ '		NULL,'																								-- generic join url
				+ '		NULL,'																								-- meeting password
				+ '		NULL'																								-- web meeting organizer id
				+ '	FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblSession] S'
				+ '	LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLesson] L ON L.idLesson = S.idLesson'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCourse] C ON C.idCourse = L.idCourse'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] STM ON STM.sourceID = L.idLesson AND STM.object = ''ilt'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] UM ON UM.sourceID = S.idInstructor AND UM.object = ''user'''				
				+ ' WHERE C.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND STM.destinationID IS NOT NULL'
		
		EXEC (@sql)

		-- insert idStandupTrainingInstance mappings (idStandupTrainingInstance maps to Inquisiq idSession)
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SS.idSession, '
				+ '		DSTI.idStandupTrainingInstance,'
				+ '		''iltsession''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblStandupTrainingInstance] DSTI '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblSession] SS ON CONVERT(NVARCHAR, SS.[datetime], 101) + '' ('' + CONVERT(NVARCHAR, SS.[datetime], 108) + '' UTC)'' + '' ##'' + CONVERT(NVARCHAR, SS.idSession) + ''##'' = DSTI.title collate database_default'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLesson] SL ON SL.idLesson = SS.idLesson'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCourse] SC ON SC.idCourse = SL.idCourse AND SC.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)
				+ ' WHERE DSTI.idSite IS NOT NULL AND DSTI.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SS.idSession IS NOT NULL AND DSTI.idStandupTrainingInstance IS NOT NULL'
		
		EXEC(@sql)

		-- clean up the ##idSession## additions we made to standup training instance titles, we did that to uniquely distinguish names
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblStandupTrainingInstance] SET '
				+ '		title = REPLACE(DSTI.title, '' ##'' + CONVERT(NVARCHAR, STIM.sourceID) + ''##'', '''') '
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblStandupTrainingInstance] DSTI '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] STIM ON STIM.destinationID = DSTI.idStandupTrainingInstance AND STIM.object = ''iltsession'''
				+ ' WHERE STIM.sourceID IS NOT NULL'

		EXEC (@sql)
	   
		-- tblStandUpTrainingInstanceLanguage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblStandUpTrainingInstanceLanguage]' 
				+ ' ( '
				+ '		idSite,'
				+ '		idStandUpTrainingInstance,'
				+ '		idLanguage,'
				+ '		title,'
				+ '		description,'
				+ '		locationDescription'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		STI.idStandUpTrainingInstance, '
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ', '
				+ '		STI.title, '
				+ '		STI.description, '
				+ '		STI.locationDescription '
				+ ' FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblStandUpTrainingInstance] STI'
				+ ' WHERE STI.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
		
		EXEC(@sql)

	/******************************************************CATALOG********************************************************************/	   
		-- tblCatalog
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCatalog]'
				+ ' ( '
				+ '		idSite, '
				+ '		idParent, '
				+ '		title, '
				+ '		shortDescription, '
				+ '		longDescription, '
				+ '		isPrivate, '
				+ '		cost, '
				+ '		isClosed, '
				+ '		dtCreated,'
				+ '		dtModified,'
				+ '		costType,'
				+ '		searchTags'
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- idSite
				+ '		NULL,'											-- parent id (NULL for now)
				+ '		C.name,'										-- title
				+ '		C.shortDescription,'							-- short description
				+ '		C.description,'									-- description
				+ '		C.isPrivate,'									-- is the catalog private
				+ '		CASE WHEN C.cost >= 1 THEN C.cost '				-- cost
				+ '			 WHEN C.cost >= 0.1 AND C.cost <= 0.9 THEN C.cost * 100 '
				+ '		ELSE 0 END, '									
				+ '		C.isClosed,'									-- is catalog closed?
				+ '		GETUTCDATE(),'									-- date created
				+ '		GETUTCDATE(),'									-- date modified
				+ '		CASE WHEN C.cost = 0 THEN 3'					-- cost type
				+ '			 WHEN C.cost >= 1 THEN 2'
				+ '			 WHEN C.cost >= 0.1 AND C.cost <= 0.9 THEN 4'
				+ '		ELSE 1 END,'
				+ '		NULL'											-- search tags
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCatalog] C'
				+ ' WHERE C.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)
		
		EXEC (@sql)

		-- insert idCatalog mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SC.idCatalog, '
				+ '		DC.idCatalog,'
				+ '		''catalog''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCatalog] DC '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCatalog] SC ON SC.idSite = '+ CONVERT(NVARCHAR, @idSiteSource) + ' AND SC.name = DC.title collate database_default'
				+ ' WHERE DC.idSite IS NOT NULL AND DC.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SC.idCatalog IS NOT NULL AND DC.idCatalog IS NOT NULL'
		
		EXEC(@sql)
	   
		-- update idParent for catalogs
		SET @sql = 'UPDATE DESTINATIONCATALOG SET'
				+ '		idParent = CM.destinationID'
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCatalog] SOURCECATALOG'
				+ ' LEFT JOIN  [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCatalog] DESTINATIONCATALOG ON SOURCECATALOG.name = DESTINATIONCATALOG.title collate database_default AND SOURCECATALOG.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)
				+ ' LEFT JOIN  [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.sourceID = SOURCECATALOG.idParent AND CM.object = ''catalog'''
				+ ' WHERE DESTINATIONCATALOG.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
				+ ' AND SOURCECATALOG.idParent IS NOT NULL'
	   
		EXEC(@sql)

		-- tblCatalogLanguage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblCatalogLanguage]' 
				+ ' ( '
				+ '		idSite,'
				+ '		idCatalog,'
				+ '		idLanguage,'
				+ '		title,'
				+ '		shortDescription,'
				+ '		longDescription,'
				+ '		searchTags'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		C.idCatalog, '
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ', '
				+ '		C.title, '
				+ '		C.shortDescription, '
				+ '		C.longDescription, '
				+ '		C.searchTags '
				+ ' FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblCatalog] C'
				+ ' WHERE C.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
		
		EXEC(@sql)	

	/******************************************************CONTENT PACKAGE********************************************************************/	   
		-- tblContentPackage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblContentPackage]'
				+ ' ( '
				+ '		idSite, '
				+ '		idContentPackageType, '
				+ '		idSCORMPackageType, '
				+ '		name, '
				+ '		path, '
				+ '		kb, '
				+ '		manifest, '
				+ '		dtCreated, '
				+ '		dtModified,'
				+ '		isMediaUpload,'
				+ '		idMediaType,'
				+ '		contentTitle,'
				+ '		originalMediaFilename,'
				+ '		isVideoMedia3rdParty,'
				+ '		videoMediaEmbedCode,'
				+ '		enableAutoplay,'
				+ '		allowRewind,'
				+ '		allowFastForward,'
				+ '		allowNavigation,'
				+ '		allowResume,'
				+ '		minProgressForCompletion,'
				+ '		isProcessing,'
				+ '		isProcessed,'
				+ '		dtProcessed,'
				+ '		idQuizSurvey'
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- idSite
				+ '		1,'												-- content package type (1 = SCORM)
				+ '		1,'												-- SCORM package type (1 = content)
				+ '		SP.name,'										-- name
				+ '		STUFF([path], CHARINDEX(''' + CONVERT(NVARCHAR, @idSiteSource) + '-'', [path]), LEN(''' + CONVERT(NVARCHAR, @idSiteSource) + '-''), ''/warehouse/' + CONVERT(NVARCHAR, @idAccount) + '-'' + ''' + CONVERT(NVARCHAR, @idSiteDestination) + '-''),' -- path				
				-- + '		REPLACE(SP.path, ''' + CONVERT(NVARCHAR, @idSiteSource) + '-'', ''/warehouse/' + CONVERT(NVARCHAR, @idAccount) + '-' + CONVERT(NVARCHAR, @idSiteDestination) + '-''),'	-- path
				+ '		SP.kb,'											-- kb
				+ '		SP.imsmanifest,'								-- manifest
				+ '		SP.dateCreated,'								-- created date
				+ '		SP.dateModified,'								-- modified date
				+ '		SP.isMediaUpload,'								-- is media upload?
				+ '		SP.idMediaType,'								-- media type
				+ '		SP.contentTitle,'								-- content title
				+ '		SP.originalMediaFilename,'						-- original media filename
				+ '		SP.isVideoMedia3rdParty,'						-- is video media 3rd party?
				+ '		SP.videoMediaEmbedCode,'						-- video media embed code
				+ '		SP.enableAutoPlay,'								-- enable autoplay
				+ '		SP.allowRewind,'								-- allow rewind
				+ '		SP.allowFastForward,'							-- allow fast forward
				+ '		SP.allowNavigation,'							-- allow navigation
				+ '		SP.allowResume,'								-- allow resume
				+ '		SP.minProgressForCompletion,'					-- minimum progress for completion
				+ '		SP.isProcessing,'								-- is processing?
				+ '		SP.isProcessed,'								-- is processed?
				+ '		SP.dtProcessed,'								-- date processed				
				+ '		NULL'											-- quiz survey id (inserted later, doing so now is a circular reference)
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblSCORMPackage] SP'
				+ ' WHERE SP.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)
		
		EXEC (@sql)

		-- insert idContentPackage mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SCP.idSCORMPackage, '
				+ '		DCP.idContentPackage,'
				+ '		''contentpackage''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblContentPackage] DCP '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblSCORMPackage] SCP ON SCP.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND SCP.name = DCP.name collate database_default AND SCP.dateCreated = DCP.dtCreated'
				+ ' WHERE DCP.idSite IS NOT NULL AND DCP.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SCP.idSCORMPackage IS NOT NULL AND DCP.idContentPackage IS NOT NULL'
		
		EXEC(@sql)		

	/*****************************************************************QUIZ SURVEY*************************************************************/
		-- tblQuizSurvey
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblQuizSurvey]'
				+ ' ( '
				+ '		idSite, '
				+ '		idAuthor, '
				+ '		type, '
				+ '		identifier, '
				+ '		guid, '
				+ '		data, '
				+ '		isDraft, '
				+ '		idContentPackage, '
				+ '		dtCreated,'
				+ '		dtModified'
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','									-- idSite
				+ '		ISNULL(UM.destinationID, 1),'													-- idAuthor
				+ '		QS.type,'																		-- type
				+ '		QS.identifier,'																	-- identifier
				+ '		QS.guid,'																		-- guid
				+ '		QS.data,'																		-- data
				+ '		QS.isDraft,'																	-- isDraft
				+ '		CASE WHEN QS.idSCORMPackage IS NOT NULL THEN CPM.destinationID ELSE NULL END,'	-- idContentPackage
				+ '		QS.dtCreated,'																	-- dtCreated
				+ '		QS.dtModified'																	-- dtModfied
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblQuizSurvey] QS'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] UM ON UM.sourceID = QS.idUser AND UM.object = ''user'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CPM ON CPM.sourceID = QS.idSCORMPackage AND CPM.object = ''contentpackage'''
				+ ' WHERE QS.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)

		EXEC (@sql)

		-- insert idQuizSurvey mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SQS.idQuizSurvey, '
				+ '		DQS.idQuizSurvey,'
				+ '		''quizsurvey''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblQuizSurvey] DQS '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblQuizSurvey] SQS ON SQS.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND SQS.identifier = DQS.identifier collate database_default AND SQS.guid = DQS.guid collate database_default AND SQS.dtCreated = DQS.dtCreated'
				+ ' WHERE DQS.idSite IS NOT NULL AND DQS.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SQS.idQuizSurvey IS NOT NULL AND DQS.idQuizSurvey IS NOT NULL'
		
		EXEC(@sql)

		-- update idQuizSurvey in tblContentPackage
		--SET @sql = 'UPDATE DCP SET'
				--+ '		idQuizSurvey = QSM.destinationID'
				--+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblSCORMPackage] SCP'
				--+ ' LEFT JOIN  [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CPM ON CPM.sourceID = SCP.idSCORMPackage AND CPM.object = ''contentpackage'''
				--+ ' LEFT JOIN  [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] QSM ON QSM.sourceID = SCP.idQuizSurvey AND QSM.object = ''quizsurvey'''
				--+ ' LEFT JOIN  [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblContentPackage] DCP ON SCP.idContentPackage = CPM.destinationID'								
				--+ ' WHERE DCP.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SCP.idQuizSurvey IS NOT NULL AND QSM.destinationID IS NOT NULL'
	   
		--EXEC(@sql)

	/*********************************************************CERTIFICATE**************************************************/
		-- tblCertificate
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCertificate]'
				+ ' ( '
				+ '		idSite, '
				+ '		name, '
				+ '		issuingOrganization, '
				+ '		description, '
				+ '		credits, '
				+ '		filename, '
				+ '		isActive, '
				+ '		activationDate, '
				+ '		expiresInterval,'
				+ '		expiresTimeframe,'
				+ '		objectType,'
				+ '		idObject,'
				+ '		isDeleted,'
				+ '		dtDeleted,'
				+ '		code,'
				+ '		reissueBasedOnMostRecentCompletion'
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','									-- idSite
				+ '		C.name,'																		-- name
				+ '		C.issuingOrganization,'															-- title
				+ '		C.description,'																	-- description
				+ '		C.credits,'																		-- credits
				+ '		REPLACE(C.filename, ''/_config/' + @siteHostname + '/_images/certs/'', ''''),'	-- filename
				+ '		C.isActive,'																	-- is certificate active?
				+ '		C.activationDate,'																-- activation date
				+ '		C.expiresInterval,'																-- expires interval
				+ '		C.expiresTimespan,'																-- expires timeframe
				+ '		1,'																				-- object type (1 = course)
				+ '		CM.destinationID,'																-- object id (course id)
				+ '		0,'																				-- is deleted?
				+ '		NULL,'																			-- date deleted?
				+ '		C.code,'																		-- code
				+ '		NULL'																			-- reissue based on most recent completion
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCertificate] C'
				+ ' LEFT JOIN  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCourseCertificateLink] CCL ON CCL.idCertificate = C.idCertificate'
				+ ' LEFT JOIN  [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.sourceID = CCL.idCourse AND CM.object = ''course'''
				+ ' WHERE C.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND CM.destinationID IS NOT NULL'
		
		EXEC (@sql)		

		-- insert idCertificate mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SC.idCertificate, '
				+ '		DC.idCertificate,'
				+ '		''certificate''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCertificate] DC '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCertificate] SC ON SC.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND  SC.name = DC.name collate database_default AND REPLACE(SC.filename, ''/_config/' + @siteHostname + '/_images/certs/'', '''') = DC.filename collate database_default AND SC.activationDate = DC.activationDate'
				+ ' WHERE DC.idSite IS NOT NULL AND DC.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SC.idCertificate IS NOT NULL AND DC.idCertificate IS NOT NULL'
		
		EXEC(@sql)

		-- tblCertificateLanguage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblCertificateLanguage]' 
				+ ' ( '
				+ '		idSite,'
				+ '		idCertificate,'
				+ '		idLanguage,'
				+ '		name,'
				+ '		description'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		C.idCertificate, '
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ', '
				+ '		C.name, '
				+ '		C.description '
				+ ' FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblCertificate] C'
				+ ' WHERE C.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
		
		EXEC(@sql)		

		-- create a table, and get certificate instance data mappings so certificate config JSON files can be written
		SET @sql = 'IF NOT EXISTS (SELECT * FROM [' + @destinationDBName + '].dbo.sysobjects WHERE name = N''InquisiqMigration_CertificateLayoutDataMappings'' AND xtype = ''U'')'
			+ ' BEGIN '
			+ 'CREATE TABLE [' + @destinationDBName + '].[dbo].[InquisiqMigration_CertificateLayoutDataMappings]'
			+ '('
			+ '    [destinationID] INT NOT NULL,'
			+ '    [data] NVARCHAR(MAX) NOT NULL'
			+ ')'
			+ ' END '
			+ ' ELSE '
		SET @sql = @sql + ' DELETE FROM [' + @destinationDBName + '].[dbo].[InquisiqMigration_CertificateLayoutDataMappings]'

		EXEC(@sql)
		
		-- get the certificate instance data		
		SET @sql = ' INSERT INTO [' + @destinationDBName + '].[dbo].[InquisiqMigration_CertificateLayoutDataMappings] '
				+ ' ( '
				+ '		destinationID,'
				+ '		data'
				+ ' ) '
				+ ' SELECT '
				+ '		DC.idCertificate, '
				+ '		REPLACE(REPLACE(SC.instanceData, ''%20'', '' ''), ''"ImageSource": "'' + SC.[filename] + ''"'', ''"ImageSource": "/_config/' + @destinationHostname + '/certificates/'' + CONVERT(NVARCHAR, DC.idCertificate) + ''/'' + DC.[filename] + ''"'') '
				--+ '		CASE WHEN LEFT(SC.instanceData,14) = ''<instanceData>'' THEN '
				--+ '			  ''{"Certificate" : '''
				--+ '			+ ''{ "Container" : '''
				--+ '			+ ''{ "Width" : "800px", "Height" : "600px", "ImageSource" : "'' + REPLACE(CAST(SC.[instanceData] AS XML).value(''(/instanceData/image)[1]'',''NVARCHAR(MAX)''),''_images/certs'',''certificates/'' + CONVERT(NVARCHAR, DC.idCertificate)) +''" },'''
				--+ '			+ ''"Elements": ['''
				--+ '			+ CASE when CAST(SC.[instanceData] AS XML).value(''(count(/instanceData/fields/field))'',''nvarchar(max)'') >= 1 then ''{"Identifier": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@columnName)[1]'',''nvarchar(max)'') + ''", "Value" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field)[1]'',''nvarchar(max)'') + ''", "IsStaticLabel" : "false", "Top": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@y)[1]'',''nvarchar(max)'') + ''", "Left" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@x)[1]'',''nvarchar(max)'') + ''", "Width" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@width)[1]'',''nvarchar(max)'') + ''", "FontSize" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@size)[1]'',''nvarchar(max)'') + ''", "Alignment" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@align)[1]'',''nvarchar(max)'') + ''"}'' else '''' end'
				--+ '			+ CASE when CAST(SC.[instanceData] AS XML).value(''(count(/instanceData/fields/field))'',''nvarchar(max)'') >= 2 then '', {"Identifier": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@columnName)[2]'',''nvarchar(max)'') + ''", "Value" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field)[2]'',''nvarchar(max)'') + ''", "IsStaticLabel" : "false", "Top": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@y)[2]'',''nvarchar(max)'') + ''", "Left" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@x)[2]'',''nvarchar(max)'') + ''", "Width" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@width)[2]'',''nvarchar(max)'') + ''", "FontSize" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@size)[2]'',''nvarchar(max)'') + ''", "Alignment" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@align)[2]'',''nvarchar(max)'') + ''"}'' else '''' end'
				--+ '			+ CASE when CAST(SC.[instanceData] AS XML).value(''(count(/instanceData/fields/field))'',''nvarchar(max)'') >= 3 then '', {"Identifier": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@columnName)[3]'',''nvarchar(max)'') + ''", "Value" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field)[3]'',''nvarchar(max)'') + ''", "IsStaticLabel" : "false", "Top": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@y)[3]'',''nvarchar(max)'') + ''", "Left" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@x)[3]'',''nvarchar(max)'') + ''", "Width" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@width)[3]'',''nvarchar(max)'') + ''", "FontSize" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@size)[3]'',''nvarchar(max)'') + ''", "Alignment" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@align)[3]'',''nvarchar(max)'') + ''"}'' else '''' end'
		--SET @sql2 = '		+ CASE when CAST(SC.[instanceData] AS XML).value(''(count(/instanceData/fields/field))'',''nvarchar(max)'') >= 4 then '', {"Identifier": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@columnName)[4]'',''nvarchar(max)'') + ''", "Value" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field)[4]'',''nvarchar(max)'') + ''", "IsStaticLabel" : "false", "Top": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@y)[4]'',''nvarchar(max)'') + ''", "Left" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@x)[4]'',''nvarchar(max)'') + ''", "Width" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@width)[4]'',''nvarchar(max)'') + ''", "FontSize" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@size)[4]'',''nvarchar(max)'') + ''", "Alignment" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@align)[4]'',''nvarchar(max)'') + ''"}'' else '''' end'
				--+ '			+ CASE when CAST(SC.[instanceData] AS XML).value(''(count(/instanceData/fields/field))'',''nvarchar(max)'') >= 5 then '', {"Identifier": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@columnName)[5]'',''nvarchar(max)'') + ''", "Value" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field)[5]'',''nvarchar(max)'') + ''", "IsStaticLabel" : "false", "Top": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@y)[5]'',''nvarchar(max)'') + ''", "Left" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@x)[5]'',''nvarchar(max)'') + ''", "Width" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@width)[5]'',''nvarchar(max)'') + ''", "FontSize" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@size)[5]'',''nvarchar(max)'') + ''", "Alignment" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@align)[5]'',''nvarchar(max)'') + ''"}'' else '''' end'
				--+ '			+ CASE when CAST(SC.[instanceData] AS XML).value(''(count(/instanceData/fields/field))'',''nvarchar(max)'') >= 6 then '', {"Identifier": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@columnName)[6]'',''nvarchar(max)'') + ''", "Value" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field)[6]'',''nvarchar(max)'') + ''", "IsStaticLabel" : "false", "Top": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@y)[6]'',''nvarchar(max)'') + ''", "Left" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@x)[6]'',''nvarchar(max)'') + ''", "Width" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@width)[6]'',''nvarchar(max)'') + ''", "FontSize" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@size)[6]'',''nvarchar(max)'') + ''", "Alignment" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@align)[6]'',''nvarchar(max)'') + ''"}'' else '''' end'
				--+ '			+ CASE when CAST(SC.[instanceData] AS XML).value(''(count(/instanceData/fields/field))'',''nvarchar(max)'') >= 7 then '', {"Identifier": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@columnName)[7]'',''nvarchar(max)'') + ''", "Value" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field)[7]'',''nvarchar(max)'') + ''", "IsStaticLabel" : "false", "Top": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@y)[7]'',''nvarchar(max)'') + ''", "Left" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@x)[7]'',''nvarchar(max)'') + ''", "Width" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@width)[7]'',''nvarchar(max)'') + ''", "FontSize" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@size)[7]'',''nvarchar(max)'') + ''", "Alignment" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@align)[7]'',''nvarchar(max)'') + ''"}'' else '''' end'
				--+ '			+'']}}'''    
				--+ '		ELSE SC.instanceData END '
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCertificate] DC '
				+ ' LEFT JOIN  [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.destinationID = DC.idCertificate AND CM.object = ''certificate'''
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCertificate] SC ON SC.idCertificate = CM.sourceID'				
				+ ' WHERE DC.idSite IS NOT NULL AND DC.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
				+ ' AND SC.idSite IS NOT NULL AND SC.idSite = '+ CONVERT(NVARCHAR, @idSiteSource)
	
		EXEC (@sql)
		
		-- tblCertificateRecord - this may need a clean-up after migration as there may be "duplicate" certificate record entries
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCertificateRecord]'
				+ ' ( '
				+ '		idSite, '
				+ '		idCertificate, '
				+ '		idUser, '
				+ '		idTimezone, '
				+ '		idAwardedBy, '
				+ '		timestamp, '
				+ '		expires, '
				+ '		code, '
				+ '		credits,'
				+ '		awardData,'
				+ '		idCertificateImport'
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','								-- idSite
				+ '		CM.destinationID,'															-- certificate id
				+ '		UM.destinationID,'															-- user id
				+ '		CASE WHEN TZ.destinationID > 0 THEN TZ.destinationID ELSE 15 END,'			-- timezone
				+ '		1,'																			-- awarded by id
				+ '		CR.timestamp,'																-- timestamp
				+ '		CR.expires,'																-- expires
				+ '		CR.code,'																	-- code
				+ '		CR.credits,'																-- credits
				+ '		NULL,'																		-- award data
				+ '		NULL'																		-- certificate import
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCertificateRecord] CR '				
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.sourceID = CR.idCertificate AND CM.object = ''certificate'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] UM ON UM.sourceID = CR.idUser AND UM.object = ''user'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_TimezoneIDMappings] TZ ON TZ.sourceID = CR.idTimezone '
				+ ' WHERE CM.destinationID IS NOT NULL AND UM.destinationID IS NOT NULL'
		
		EXEC (@sql)			
	   
	/*************************************************ACTIVITY IMPORT****************************************************/
		-- tblActivityImport
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblActivityImport]'
				+ ' ( '
				+ '		idSite, '
				+ '		idUser, '
				+ '		timestamp, '
				+ '		idUserImported, '
				+ '		importFileName '
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- idSite
				+ '		UM.destinationID,'								-- user id
				+ '		AI.timestamp,'									-- timestamp
				+ '		UIM.destinationID,'								-- user imported id
				+ '		AI.importFileName'								-- import filename
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblActivityImport] AI'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] UM ON UM.sourceID = AI.idUser AND UM.object = ''user'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] UIM ON UIM.sourceID = AI.idUserImported AND UIM.object = ''user'''
				+ ' WHERE idSite = ' + CONVERT(NVARCHAR, @idSiteSource)
		
		EXEC (@sql)

		-- insert idActivityImport mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SAI.idActivityImport, '
				+ '		DAI.idActivityImport,'
				+ '		''activityimport''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblActivityImport] DAI '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblActivityImport] SAI ON SAI.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND SAI.timestamp = DAI.timestamp'
				+ ' WHERE DAI.idSite IS NOT NULL AND DAI.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SAI.idActivityImport IS NOT NULL AND DAI.idActivityImport IS NOT NULL'
		
		EXEC(@sql)

	/**************************************************EVENT EMAIL NOTIFICATION******************************************/
		-- tblEventEmailNotification
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblEventEmailNotification]'
				+ ' ( '
				+ '		idSite, '
				+ '		name, '
				+ '		idEventType, '
				+ '		idEventTypeRecipient, '
				+ '		idObject, '
				+ '		[from], '
				+ '		isActive, '
				+ '		dtActivation, '
				+ '		interval,'
				+ '		timeframe,'
				+ '		priority'
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','										-- idSite
				+ '		EEN.name + '' ##'' + CONVERT(NVARCHAR, EEN.idEventEmailNotification) + ''##'','		-- name
				+ '		ETM.destinationID,'																	-- idEventType
				+ '		ETRM.destinationID,'																-- idEventTypeRecipient
				+ '		NULL,'																				-- idObject
				+ '		EEN.[from],'																		-- from
				+ '		EEN.isActive,'																		-- is active?
				+ '		CASE WHEN EEN.isActive = 1 THEN GETUTCDATE() ELSE NULL END,'						-- activation date
				+ '		EEN.interval,'																		-- interval
				+ '		EEN.timeframe,'																		-- timeframe
				+ '		EEN.priority'																		-- priority
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblEventEmailNotification] EEN'				
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_EventTypeIDMappings] ETM ON ETM.sourceID = EEN.idEventType'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_EventTypeRecipientIDMappings] ETRM ON ETRM.sourceID = EEN.idEventTypeRecipient'
				+ ' WHERE EEN.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND EEN.idEventType <> 4'	-- exclude "Purchase Confirmed"
		
		EXEC (@sql)

		-- insert idEventEmailNotification mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SEEN.idEventEmailNotification, '
				+ '		DEEN.idEventEmailNotification,'
				+ '		''emailnotification''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblEventEmailNotification] DEEN '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblEventEmailNotification] SEEN ON SEEN.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND SEEN.name + '' ##'' + CONVERT(NVARCHAR, SEEN.idEventEmailNotification) + ''##'' = DEEN.name collate database_default'
				+ ' WHERE DEEN.idSite IS NOT NULL AND DEEN.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SEEN.idEventEmailNotification IS NOT NULL AND DEEN.idEventEmailNotification IS NOT NULL'
		
		EXEC(@sql)

		-- clean up the ##idEventEmailNotification## additions we made to email notification titles, we did that to uniquely distinguish names
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblEventEmailNotification] SET '
				+ '		name = REPLACE(DEEN.name, '' ##'' + CONVERT(NVARCHAR, EENM.sourceID) + ''##'', '''') '
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblEventEmailNotification] DEEN '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] EENM ON EENM.destinationID = DEEN.idEventEmailNotification AND EENM.object = ''emailnotification'''
				+ ' WHERE EENM.sourceID IS NOT NULL'

		EXEC (@sql)

		-- tblEventEmailNotificationLanguage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblEventEmailNotificationLanguage]' 
				+ ' ( '
				+ '		idSite,'
				+ '		idEventEmailNotification,'
				+ '		idLanguage,'
				+ '		name'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		EEN.idEventEmailNotification, '
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ', '
				+ '		EEN.name '				
				+ ' FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblEventEmailNotification] EEN'
				+ ' WHERE EEN.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
		
		EXEC(@sql)

		-- create a table, and get email notification data mappings so email notification config XML files can be written		
		SET @sql = 'IF NOT EXISTS (SELECT * FROM [' + @destinationDBName + '].dbo.sysobjects WHERE name = N''InquisiqMigration_EmailNotificationDataMappings'' AND xtype = ''U'')'
			+ ' BEGIN '
			+ 'CREATE TABLE [' + @destinationDBName + '].[dbo].[InquisiqMigration_EmailNotificationDataMappings]'
			+ '('
			+ '    [destinationID] INT NOT NULL,'
			+ '    [data] NVARCHAR(MAX) NOT NULL'
			+ ')'
			+ ' END '
			+ ' ELSE '
		SET @sql = @sql + ' DELETE FROM [' + @destinationDBName + '].[dbo].[InquisiqMigration_EmailNotificationDataMappings]'

		EXEC(@sql)
		
		-- get the email notification data		
		SET @sql = ' INSERT INTO [' + @destinationDBName + '].[dbo].[InquisiqMigration_EmailNotificationDataMappings] '
				+ ' ( '
				+ '		destinationID,'
				+ '		data'
				+ ' ) '
				+ ' SELECT '
				+ '		DEEN.idEventEmailNotification, '
				+ '		  ''<?xml version="1.0" encoding="utf-8"?>'' '
				+ '		+ ''<emailNotification>'' '
				+ '		+ ''	<language code="default">'' '
				+ '		+ ''		<subject><![CDATA['' + SEEN.[subject] + '']]></subject>'' '
				+ '		+ ''		<body><![CDATA['' + SEEN.[bodyTemplate] + '']]></body>'' '
				+ '		+ ''	</language>'' '
				+ '		+ ''	<language code="en-US">'' '
				+ '		+ ''		<subject><![CDATA['' + SEEN.[subject] + '']]></subject>'' '
				+ '		+ ''		<body><![CDATA['' + SEEN.[bodyTemplate] + '']]></body>'' '
				+ '		+ ''	</language>'' '
				+ '		+ ''</emailNotification>'' '				
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblEventEmailNotification] DEEN '
				+ ' LEFT JOIN  [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] EENM ON EENM.destinationID = DEEN.idEventEmailNotification AND EENM.object = ''emailnotification'''
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblEventEmailNotification] SEEN ON SEEN.idEventEmailNotification = EENM.sourceID'				
				+ ' WHERE DEEN.idSite IS NOT NULL AND DEEN.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
				+ ' AND SEEN.idSite IS NOT NULL AND SEEN.idSite = '+ CONVERT(NVARCHAR, @idSiteSource)
	
		EXEC (@sql)

	/*****************************************************COUPON CODE***********************************************************************/		
		-- tblCouponCode
		SET @sql = ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCouponCode]'
				+ ' ( '
				+ '		idSite, '
				+ '		usesAllowed, '
				+ '		discount, '
				+ '		discountType, '
				+ '		code, '
				+ '		isDeleted, '
				+ '		comments, '
				+ '		forCourse, '
				+ '		forCatalog, '
				+ '		dtStart, '
				+ '		dtEnd, '
				+ '		isSingleUsePerUser, '
				+ '		dtDeleted, '
				+ '		forLearningPath, '
				+ '		forStandupTraining ' 
				+ ' ) '
				+ ' SELECT DISTINCT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) +','						-- idSite
				+ '		CC.usesAllowed,'													-- uses allowed
				+ '		CASE WHEN CC.value < 0 THEN CC.value * -1'							-- discount
				+ '			 WHEN CC.value >= 0.1 AND CC.value <= 0.9 THEN CC.value * 100'
				+ '		ELSE CC.value END,'
				+ '		CASE WHEN CC.value = 0 THEN 1'										-- discount type
				+ '			 WHEN CC.value >= 0.1 AND CC.value <= 0.9 THEN 4'
				+ '			 WHEN CC.value < 0 THEN 3'
				+ '		ELSE 2 END,'
				+ '		CC.code,'															-- code
				+ '		CC.isDeleted,'														-- is deleted?
				+ '		CC.comments,'														-- comments
				+ '		CASE WHEN CC.isCourse = 0 THEN 1 '									-- for course
				+ '			 WHEN CC.isCourse = 1 AND courseLink.idCourse IS NULL THEN 2 '
				+ '		ELSE 3 END,'														
				+ '		CASE WHEN CC.isCatalog = 0 THEN 1 '									-- for catalog
				+ '			 WHEN CC.isCatalog = 1 AND catalogLink.idCatalog IS NULL THEN 2 '
				+ '		ELSE 3 END,'														
				+ '		CC.dtStart,'														-- date start
				+ '		CC.dtEnd,'															-- date end
				+ '		CC.isSingleUsePerUser, '											-- is single use per user?
				+ '		CASE WHEN CC.isDeleted = 1 THEN GETUTCDATE() ELSE NULL END, '		-- date deleted
				+ '		1, '																-- for learning path
				+ '		1 '																	-- for standup training	
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCouponCode] CC'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCouponCodeCourseLink] courseLink ON courseLink.idCouponCode = CC.idCouponCode'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCouponCodeCatalogLink] catalogLink ON catalogLink.idCouponCode = CC.idCouponCode'
				+ ' WHERE CC.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)

		EXEC (@sql)

		-- insert idCouponCode mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SCC.idCouponCode, '
				+ '		DCC.idCouponCode,'
				+ '		''couponcode''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCouponCode] DCC '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCouponCode] SCC ON SCC.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND SCC.code = DCC.code AND SCC.usesAllowed = DCC.usesAllowed'
				+ ' WHERE DCC.idSite IS NOT NULL AND DCC.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SCC.idCouponCode IS NOT NULL AND DCC.idCouponCode IS NOT NULL'
		
		EXEC(@sql)
	   
	/*****************************************************LINKING TABLES****************************************************************/
		-- tblCourseToCatalogLink
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCourseToCatalogLink]'
				+ ' ( '
				+ '		idSite, '
				+ '		idCourse, '
				+ '		idCatalog '
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- idSite
				+ '		COM.destinationID,'								-- idCourse
				+ '		CAM.destinationID'								-- idCatalog
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCourseCatalogLink] CCL'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] COM ON COM.sourceID = CCL.idCourse AND COM.object = ''course'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CAM ON CAM.sourceID = CCL.idCatalog AND CAM.object = ''catalog'''
				+ ' WHERE COM.destinationID IS NOT NULL AND CAM.destinationID IS NOT NULL'
		
		EXEC (@sql)
	   
		-- tblCourseToPrerequisiteLink
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCourseToPrerequisiteLink]'
				+ ' ( '
				+ '		idSite, '
				+ '		idCourse, '
				+ '		idPrerequisite '
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- idSite
				+ '		CM.destinationID,'								-- idCourse
				+ '		PM.destinationID'								-- idPrerequisite
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCoursePrerequisiteLink] CPL'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.sourceID = CPL.idCourse AND CM.object = ''course'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] PM ON PM.sourceID = CPL.idPrerequisite AND PM.object = ''course'''
				+ ' WHERE CM.destinationID IS NOT NULL AND PM.destinationID IS NOT NULL'
		
		EXEC (@sql)
		
		-- tblCouponCodeToCourseLink
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCouponCodeToCourseLink]'
				+ ' ( '
				+ '		idCouponCode, '
				+ '		idCourse '
				+ ' ) '
				+ ' SELECT ' 
				+ '		CC.destinationID,'	-- idCouponCode
				+ '		C.destinationID'	-- idCourse
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCouponCodeCourseLink] CCCL'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CC ON CC.sourceID = CCCL.idCouponCode AND CC.object = ''couponcode'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] C ON C.sourceID = CCCL.idCourse AND C.object = ''course'''
				+ ' WHERE CC.destinationID IS NOT NULL AND C.destinationID IS NOT NULL'
		
		EXEC (@sql)

		-- tblCouponCodeToCatalogLink
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCouponCodeToCatalogLink]'
				+ ' ( '
				+ '		idCouponCode, '
				+ '		idCatalog '
				+ ' ) '
				+ ' SELECT ' 
				+ '		CC.destinationID,'	-- idCouponCode
				+ '		C.destinationID'	-- idCatalog
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCouponCodeCatalogLink] CCCL'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CC ON CC.sourceID = CCCL.idCouponCode AND CC.object = ''couponcode'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] C ON C.sourceID = CCCL.idCatalog AND C.object = ''catalog'''
				+ ' WHERE CC.destinationID IS NOT NULL AND C.destinationID IS NOT NULL'
		
		EXEC (@sql)

		-- tblCourseToScreenshotLink
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCourseToScreenshotLink]'
				+ ' ( '
				+ '		idSite, '
				+ '		idCourse, '
				+ '		filename '
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '	-- idSite
				+ '		CM.destinationID, '									-- idCourse
				+ '		SS.s '												-- filename
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCourse] C'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.sourceID = C.idCourse AND CM.object = ''course'''
				+ ' CROSS APPLY (SELECT s FROM [dbo].[DelimitedStringToTable](C.sampleScreens, ''|'') WHERE C.sampleScreens IS NOT NULL AND C.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ') SS'
		
		EXEC (@sql)
	   
		-- tblLessonToContentLink - link SCORM lesson types to content packages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblLessonToContentLink]'
				+ ' ( '
				+ '		idSite, '
				+ '		idLesson, '
				+ '		idObject, '
				+ '		idContentType, '
				+ '		idAssignmentDocumentType, '
				+ '		allowSupervisorsAsProctor, '
				+ '		allowCourseExpertsAsProctor'
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '	-- idSite
				+ '		LM.destinationID, '									-- idLesson
				+ '		CPM.destinationID, '								-- idObject
				+ '		1, '												-- idContentType (1 = content package)
				+ '		NULL, '												-- idAssignmentDocumentType
				+ '		NULL, '												-- allow supervisors as proctor?
				+ '		NULL '												-- allow course experts as proctor?
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLesson] L'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] LM ON LM.sourceID = L.idLesson AND LM.object = ''lesson'''
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblSCORMResource] SR ON SR.idSCORMResource = L.idSCORMResource'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblSCORMPackage] SP ON SP.idSCORMPackage = SR.idSCORMPackage'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CPM ON CPM.sourceID = SP.idSCORMPackage AND CPM.object = ''contentpackage'''
				+ ' WHERE L.idLessonType = 0 AND LM.destinationID IS NOT NULL AND CPM.destinationID IS NOT NULL'
		
		EXEC (@sql)

		-- tblLessonToContentLink - link Classroom/Web Meeting lesson types to ILT modules
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblLessonToContentLink]'
				+ ' ( '
				+ '		idSite, '
				+ '		idLesson, '
				+ '		idObject, '
				+ '		idContentType, '
				+ '		idAssignmentDocumentType, '
				+ '		allowSupervisorsAsProctor, '
				+ '		allowCourseExpertsAsProctor'
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) +', '	-- idSite
				+ '		LM.destinationID, '								-- idLesson
				+ '		ILTM.destinationID, '							-- idObject
				+ '		2, '											-- idContentType (2 = ILT)
				+ '		NULL, '											-- idAssignmentDocumentType
				+ '		NULL, '											-- allow supervisors as proctor?
				+ '		NULL '											-- allow course experts as proctor?
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLesson] L'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] LM ON LM.sourceID = L.idLesson AND LM.object = ''lesson'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] ILTM ON ILTM.sourceID = L.idLesson AND ILTM.object = ''ilt'''
				+ ' WHERE L.idLessonType IN (1, 2) AND LM.destinationID IS NOT NULL AND ILTM.destinationID IS NOT NULL'
		
		EXEC (@sql)

		-- tblUserToGroupLink -- make all user to group links seem as if the user were manually placed into the group,
							  -- post conversion, after group rules have run, we will need to clean up the "manual" entries
							  -- left over where the user gets a group entry by rule
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblUserToGroupLink]'
				+ ' ( '
				+ '		idSite, '
				+ '		idUser, '
				+ '		idGroup, '
				+ '		idRuleSet, '
				+ '		created, '
				+ '		selfJoined '
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- idSite
				+ '		UM.destinationID,'								-- idUser
				+ '		GM.destinationID, '								-- idGroup
				+ '		NULL, '											-- idRuleSet
				+ '		GETUTCDATE(), '									-- created date
				+ '		0 '												-- self joined
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblUserGroupLink] UGL'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] UM ON UM.sourceID = UGL.idUser AND UM.object = ''user'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] GM ON GM.sourceID = UGL.idGroup AND GM.object = ''group'''
				+ ' WHERE UM.destinationID IS NOT NULL AND GM.destinationID IS NOT NULL'
		
		EXEC (@sql)

	/**********************************************ENROLLMENTS (WHERE WE DO NOT HAVE TO ONE-OFF)**********************************************/
		
		/**********ACTIVITY IMPORTED ENROLLMENTS***********/

		-- enrollments (instances only) that belong to an activity import
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblEnrollment]'
				+ ' ( '
				+ '		idSite, '
				+ '		idCourse, '
				+ '		idUser, '
				+ '		idGroupEnrollment, '
				+ '		idRuleSetEnrollment, '				
				+ '		idTimezone, '
				+ '		isLockedByPrerequisites, '
				+ '		code, '
				+ '		title, '
				+ '		dtStart, '
				+ '		dtDue, '
				+ '		dtExpiresFromStart, '				
				+ '		dtFirstLaunch, '
				+ '		dtCompleted, '
				+ '		dtCreated, '
				+ '		dtLastSynchronized, '
				+ '		dueInterval, '
				+ '		dueTimeframe, '
				+ '		expiresFromStartInterval, '
				+ '		expiresFromStartTimeframe, '				
				+ '		idActivityImport, '
				+ '		credits '
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','								-- idSite
				+ '		CM.destinationID,'															-- idCourse				
				+ '		UM.destinationID,'															-- idUser
				+ '		NULL,'																		-- idGroupEnrollment
				+ '		NULL,'																		-- idRulesetEnrollment
				+ '		CASE WHEN TZ.destinationID > 0 THEN TZ.destinationID ELSE 15 END,'			-- idTimezone
				+ '		0,'																			-- isLockedByPrerequisites
				+ '		EI.code,'																	-- code
				+ '		EI.name + '' ##'' + CONVERT(NVARCHAR, EI.idEnrollmentInstance) + ''##'','	-- title
				+ '		EI.dtStart,'																-- dtStart
				+ '		EI.dtDue,'																	-- dtDue
				+ '		EI.dtEnd,'																	-- dtExpiresFromStart
				+ '		EI.dtFirstLaunch,'															-- dtFirstLaunch
				+ '		EI.dtCompleted,'															-- dtCompleted
				+ '		EI.dtStart,'																-- dtCreated
				+ '		EI.dtLastSynchronized,'														-- dtLastSynchronized
				+ '		NULL,'																		-- dueInterval
				+ '		NULL,'																		-- dueTimeframe
				+ '		NULL,'																		-- expiresFromStartInterval
				+ '		NULL,'																		-- expiresFromStartTimeframe
				+ '		AIM.destinationID,'															-- idActivityImport
				+ '		EI.credits '																-- credits				
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblEnrollmentInstance] EI'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblUser] U ON U.idUser = EI.idUser'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.sourceID = EI.idCourse AND CM.object = ''course'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] UM ON UM.sourceID = EI.idUser AND UM.object = ''user'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] AIM ON AIM.sourceID = EI.idActivityImport AND AIM.object = ''activityimport'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_TimezoneIDMappings] TZ ON TZ.sourceID = EI.idTimezone '
				+ ' WHERE U.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)  + ' AND EI.idActivityImport IS NOT NULL AND UM.destinationID IS NOT NULL AND AIM.destinationID IS NOT NULL'
		
		EXEC (@sql)				

		/**********MANUAL SINGLE ENROLLMENTS**********/

		-- manual (self-assigned or admin-assigned) enrollments - recurrence will be taken away 		
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblEnrollment]'
				+ ' ( '
				+ '		idSite, '
				+ '		idCourse, '
				+ '		idUser, '
				+ '		idGroupEnrollment, '
				+ '		idRuleSetEnrollment, '				
				+ '		idTimezone, '
				+ '		isLockedByPrerequisites, '
				+ '		code, '
				+ '		title, '
				+ '		dtStart, '
				+ '		dtDue, '
				+ '		dtExpiresFromStart, '				
				+ '		dtFirstLaunch, '
				+ '		dtCompleted, '
				+ '		dtCreated, '
				+ '		dtLastSynchronized, '
				+ '		dueInterval, '
				+ '		dueTimeframe, '
				+ '		expiresFromStartInterval, '
				+ '		expiresFromStartTimeframe, '				
				+ '		idActivityImport, '
				+ '		credits '
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','													-- idSite
				+ '		CM.destinationID,'																				-- idCourse				
				+ '		UM.destinationID,'																				-- idUser
				+ '		NULL,'																							-- idGroupEnrollment
				+ '		NULL,'																							-- idRulesetEnrollment
				+ '		CASE WHEN TZ.destinationID > 0 THEN TZ.destinationID ELSE 15 END,'								-- idTimezone
				+ '		CASE WHEN E.isLockedForPrerequisites IS NOT NULL THEN E.isLockedForPrerequisites ELSE 0 END,'	-- isLockedByPrerequisites
				+ '		EI.code,'																						-- code
				+ '		EI.name + '' ##'' + CONVERT(NVARCHAR, EI.idEnrollmentInstance) + ''##'','						-- title
				+ '		EI.dtStart,'																					-- dtStart
				+ '		EI.dtDue,'																						-- dtDue
				+ '		EI.dtEnd,'																						-- dtExpiresFromStart
				+ '		EI.dtFirstLaunch,'																				-- dtFirstLaunch
				+ '		EI.dtCompleted,'																				-- dtCompleted
				+ '		EI.dtStart,'																					-- dtCreated
				+ '		EI.dtLastSynchronized,'																			-- dtLastSynchronized
				+ '		E.dueInterval,'																					-- dueInterval
				+ '		E.dueTimeframe,'																				-- dueTimeframe
				+ '		E.accessInterval,'																				-- expiresFromStartInterval
				+ '		E.accessTimeframe,'																				-- expiresFromStartTimeframe
				+ '		NULL,'																							-- idActivityImport
				+ '		EI.credits '																					-- credits				
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblEnrollmentInstance] EI'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblEnrollment] E ON E.idEnrollment = EI.idEnrollment'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblUser] U ON U.idUser = EI.idUser'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.sourceID = EI.idCourse AND CM.object = ''course'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] UM ON UM.sourceID = EI.idUser AND UM.object = ''user'''				
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_TimezoneIDMappings] TZ ON TZ.sourceID = EI.idTimezone '
				+ ' WHERE U.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)  + ' AND EI.idActivityImport IS NULL AND E.idGroupEnrollment IS NULL '
				+ ' AND CM.destinationID IS NOT NULL AND UM.destinationID IS NOT NULL'
		
		EXEC (@sql)

		/**********BUILD RULESET ENROLLMENTS FROM GROUP ENROLLMENTS **********/

		-- add ruleset enrollments using group enrollments from Inquisiq
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblRulesetEnrollment]'
				+ ' ( '
				+ '		idSite, '
				+ '		idCourse, '
				+ '		idTimezone, '
				+ '		priority, '
				+ '		label, '				
				+ '		isLockedByPrerequisites, '
				+ '		isFixedDate, '
				+ '		dtStart, '
				+ '		dtEnd, '
				+ '		dtCreated, '				
				+ '		delayInterval, '
				+ '		delayTimeframe, '
				+ '		dueInterval, '
				+ '		dueTimeframe, '
				+ '		recurInterval, '
				+ '		recurTimeframe, '
				+ '		expiresFromStartInterval, '
				+ '		expiresFromStartTimeframe, '				
				+ '		expiresFromFirstLaunchInterval, '
				+ '		expiresFromFirstLaunchTimeframe, '
				+ '		forceReassignment '
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','																-- idSite
				+ '		CM.destinationID,'																							-- idCourse				
				+ '		CASE WHEN TZ.destinationID > 0 THEN TZ.destinationID ELSE 15 END,'											-- idTimezone
				+ '		ROW_NUMBER() OVER(PARTITION BY CM.destinationID ORDER BY CM.destinationID),'								-- priority
				+ '		''START: '' + CONVERT(NVARCHAR, GE.dtStart, 101)'															-- label
				+ '		+ CASE WHEN GE.dueInterval IS NOT NULL THEN '' DUE: '' + CONVERT(NVARCHAR, GE.dueInterval) + GE.dueTimeframe ELSE '''' END'
				+ '		+ CASE WHEN GE.recurInterval IS NOT NULL THEN '' RECUR: '' + CONVERT(NVARCHAR, GE.recurInterval) + GE.recurTimeframe ELSE '''' END'
				+ '		+ CASE WHEN GE.accessInterval IS NOT NULL THEN '' ACCESS: '' + CONVERT(NVARCHAR, GE.accessInterval) + GE.accessTimeframe ELSE '''' END,'
				+ '		CASE WHEN GE.isLockedForPrerequisites IS NOT NULL THEN GE.isLockedForPrerequisites ELSE 0 END,'				-- isLockedByPrerequisites
				+ '		GE.useFixedDates,'																							-- isFixedDate
				+ '		GE.dtStart,'																								-- dtStart
				+ '		GE.dtEnd,'																									-- dtEnd
				+ '		GE.dtStart,'																								-- dtCreated
				+ '		GE.delayInterval,'																							-- delayInterval
				+ '		GE.delayTimeframe,'																							-- delayTimeframe				
				+ '		GE.dueInterval,'																							-- dueInterval
				+ '		GE.dueTimeframe,'																							-- dueTimeframe
				+ '		GE.recurInterval,'																							-- recurInterval
				+ '		GE.recurTimeframe,'																							-- recurTimeframe
				+ '		GE.accessInterval,'																							-- expiresFromStartInterval
				+ '		GE.accessTimeframe,'																						-- expiresFromStartTimeframe
				+ '		GE.idGroupEnrollment,'																						-- expiresFromFirstLaunchInterval (use as placeholder for mapping, put idGroupEnrollment in it)
				+ '		NULL, '																										-- expiresFromFirstLaunchTimeframe
				+ '		0 '																											-- forceReassignment
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblGroupEnrollment] GE'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCourse] C ON C.idCourse = GE.idCourse'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.sourceID = GE.idCourse AND CM.object = ''course'''						
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_TimezoneIDMappings] TZ ON TZ.sourceID = GE.idTimezone '
				+ ' WHERE C.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)  + ' AND CM.destinationID IS NOT NULL'
		
		EXEC (@sql)

		-- insert idRulesetEnrollment mappings (idRulesetEnrollment maps to Inquisiq idGroupEnrollment)
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SGE.idGroupEnrollment, '
				+ '		DRSE.idRulesetEnrollment,'
				+ '		''rulesetenrollment''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblRulesetEnrollment] DRSE '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblGroupEnrollment] SGE ON SGE.idGroupEnrollment = DRSE.expiresFromFirstLaunchInterval'				
				+ ' WHERE DRSE.idSite IS NOT NULL AND DRSE.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SGE.idGroupEnrollment IS NOT NULL AND DRSE.idRulesetEnrollment IS NOT NULL'
		
		EXEC(@sql)

		-- clean up the idGroupEnrollment additions we made to tblRulesetEnrollment, we did that to allow it to be mapped
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRulesetEnrollment] SET '
				+ '		expiresFromFirstLaunchInterval = NULL '
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblRulesetEnrollment] DRSE '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] SGEM ON SGEM.destinationID = DRSE.[idRulesetEnrollment] AND SGEM.object = ''rulesetenrollment'''
				+ ' WHERE SGEM.sourceID IS NOT NULL'

		EXEC (@sql)

		-- create rulesets for the rulesetenrollment to group mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRuleset] '
				+ ' ( '
				+ '		idSite,'
				+ '		isAny,'
				+ '		label'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','
				+ '		1,'
				+ '		''Member of Group: '' + G.name + '' ##'' + CONVERT(NVARCHAR, GE.idGroupEnrollment) + ''##'''
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblRulesetEnrollment] RSE '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] GEM ON GEM.destinationID = RSE.[idRulesetEnrollment] AND GEM.object = ''rulesetenrollment'''
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblGroupEnrollment] GE ON GE.idGroupEnrollment = GEM.sourceID'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblGroup] G ON G.idGroup = GE.idGroup'
				+ ' WHERE RSE.idSite IS NOT NULL AND RSE.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND GE.idGroupEnrollment IS NOT NULL AND RSE.idRulesetEnrollment IS NOT NULL'
		
		EXEC(@sql)

		-- create rules for the rulesetenrollment to group mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRule] '
				+ ' ( '
				+ '		idSite,'
				+ '		idRuleset,'
				+ '		userField,'
				+ '		operator,'
				+ '		textValue'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','
				+ '		RS.idRuleset,'
				+ '		''group'','
				+ '		''eq'','
				+ '		G.name'				
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblRulesetEnrollment] RSE '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] GEM ON GEM.destinationID = RSE.[idRulesetEnrollment] AND GEM.object = ''rulesetenrollment'''
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblGroupEnrollment] GE ON GE.idGroupEnrollment = GEM.sourceID'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblGroup] G ON G.idGroup = GE.idGroup'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRuleset] RS ON RS.label = ''Member of Group: '' + G.name + '' ##'' + CONVERT(NVARCHAR, GE.idGroupEnrollment) + ''##'' collate database_default'
				+ ' WHERE RSE.idSite IS NOT NULL AND RSE.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND GE.idGroupEnrollment IS NOT NULL AND RSE.idRulesetEnrollment IS NOT NULL AND RS.idRuleset IS NOT NULL'
		
		EXEC(@sql)

		-- link rulesets to rulesetenrollments
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRulesetToRulesetEnrollmentLink] '
				+ ' ( '
				+ '		idSite,'
				+ '		idRuleset,'
				+ '		idRulesetEnrollment'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','
				+ '		RS.idRuleset,'
				+ '		RSE.idRuleSetEnrollment'
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblRulesetEnrollment] RSE '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] GEM ON GEM.destinationID = RSE.[idRulesetEnrollment] AND GEM.object = ''rulesetenrollment'''
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblGroupEnrollment] GE ON GE.idGroupEnrollment = GEM.sourceID'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblGroup] G ON G.idGroup = GE.idGroup'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRuleset] RS ON RS.label = ''Member of Group: '' + G.name + '' ##'' + CONVERT(NVARCHAR, GE.idGroupEnrollment) + ''##'' collate database_default'
				+ ' WHERE RSE.idSite IS NOT NULL AND RSE.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND GE.idGroupEnrollment IS NOT NULL AND RSE.idRulesetEnrollment IS NOT NULL AND RS.idRuleset IS NOT NULL'
		
		EXEC(@sql)

		-- clean up the ##idGroupEnrollment## additions we made to ruleset labels, we did that to uniquely distinguish names
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRuleset] SET '
				+ '		label = REPLACE(DRS.label, '' ##'' + CONVERT(NVARCHAR, RSEM.sourceID) + ''##'', '''') '
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblRuleset] DRS '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRulesetToRulesetEnrollmentLink] RSEL ON RSEL.idRuleset = DRS.idRuleset'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] RSEM ON RSEM.destinationID = RSEL.idRulesetEnrollment AND RSEM.object = ''rulesetenrollment'''
				+ ' WHERE RSEM.sourceID IS NOT NULL'

		EXEC (@sql)

		-- tblRulesetEnrollmentLanguage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRulesetEnrollmentLanguage]' 
				+ ' ( '
				+ '		idSite,'
				+ '		idRulesetEnrollment,'
				+ '		idLanguage,'
				+ '		label'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		RSE.idRulesetEnrollment, '
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ', '
				+ '		RSE.label '				
				+ ' FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRulesetEnrollmentLanguage] RSE'
				+ ' WHERE RSE.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
		
		EXEC(@sql)

		-- tblRulesetLanguage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRulesetLanguage]' 
				+ ' ( '
				+ '		idSite,'
				+ '		idRuleset,'
				+ '		idLanguage,'
				+ '		label'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		RS.idRuleset, '
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ', '
				+ '		RS.label '				
				+ ' FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRuleset] RS'
				+ ' WHERE RS.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
		
		EXEC(@sql)

		/**********RULESET ENROLLMENTS CASCADED**********/

		-- group enrollments from Inquisiq that were cascaded get mapped as cascaded ruleset enrollments in Asentia
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblEnrollment]'
				+ ' ( '
				+ '		idSite, '
				+ '		idCourse, '
				+ '		idUser, '
				+ '		idGroupEnrollment, '
				+ '		idRuleSetEnrollment, '				
				+ '		idTimezone, '
				+ '		isLockedByPrerequisites, '
				+ '		code, '
				+ '		title, '
				+ '		dtStart, '
				+ '		dtDue, '
				+ '		dtExpiresFromStart, '				
				+ '		dtFirstLaunch, '
				+ '		dtCompleted, '
				+ '		dtCreated, '
				+ '		dtLastSynchronized, '
				+ '		dueInterval, '
				+ '		dueTimeframe, '
				+ '		expiresFromStartInterval, '
				+ '		expiresFromStartTimeframe, '				
				+ '		idActivityImport, '
				+ '		credits '
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','													-- idSite
				+ '		CM.destinationID,'																				-- idCourse				
				+ '		UM.destinationID,'																				-- idUser
				+ '		NULL,'																							-- idGroupEnrollment
				+ '		RSEM.destinationID,'																			-- idRulesetEnrollment
				+ '		CASE WHEN TZ.destinationID > 0 THEN TZ.destinationID ELSE 15 END,'								-- idTimezone
				+ '		CASE WHEN E.isLockedForPrerequisites IS NOT NULL THEN E.isLockedForPrerequisites ELSE 0 END,'	-- isLockedByPrerequisites
				+ '		EI.code,'																						-- code
				+ '		EI.name + '' ##'' + CONVERT(NVARCHAR, EI.idEnrollmentInstance) + ''##'','						-- title
				+ '		EI.dtStart,'																					-- dtStart
				+ '		EI.dtDue,'																						-- dtDue
				+ '		EI.dtEnd,'																						-- dtExpiresFromStart
				+ '		EI.dtFirstLaunch,'																				-- dtFirstLaunch
				+ '		EI.dtCompleted,'																				-- dtCompleted
				+ '		EI.dtStart,'																					-- dtCreated
				+ '		EI.dtLastSynchronized,'																			-- dtLastSynchronized
				+ '		E.dueInterval,'																					-- dueInterval
				+ '		E.dueTimeframe,'																				-- dueTimeframe
				+ '		E.accessInterval,'																				-- expiresFromStartInterval
				+ '		E.accessTimeframe,'																				-- expiresFromStartTimeframe
				+ '		NULL,'																							-- idActivityImport
				+ '		EI.credits '																					-- credits				
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblEnrollmentInstance] EI'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblEnrollment] E ON E.idEnrollment = EI.idEnrollment'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblUser] U ON U.idUser = EI.idUser'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.sourceID = EI.idCourse AND CM.object = ''course'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] UM ON UM.sourceID = EI.idUser AND UM.object = ''user'''				
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] RSEM ON RSEM.sourceID = E.idGroupEnrollment AND RSEM.object = ''rulesetenrollment'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_TimezoneIDMappings] TZ ON TZ.sourceID = EI.idTimezone '
				+ ' WHERE U.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)  + ' AND EI.idActivityImport IS NULL AND E.idGroupEnrollment IS NOT NULL '
				+ ' AND CM.destinationID IS NOT NULL AND UM.destinationID IS NOT NULL AND RSEM.destinationID IS NOT NULL'
		
		EXEC (@sql)
		
		/**********LESSON DATA AND SCORM INTERACTIONS ASSOCIATED WITH ENROLLMENTS**********/

		-- insert idEnrollment mappings (idEnrollment maps to Inquisiq idEnrollmentInstance)
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SEI.idEnrollmentInstance, '
				+ '		DE.idEnrollment,'
				+ '		''enrollment''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblEnrollment] DE '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblEnrollmentInstance] SEI ON SEI.name + '' ##'' + CONVERT(NVARCHAR, SEI.idEnrollmentInstance) + ''##'' = DE.title collate database_default'
				+ ' WHERE DE.idSite IS NOT NULL AND DE.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SEI.idEnrollmentInstance IS NOT NULL AND DE.idEnrollment IS NOT NULL'
		
		EXEC(@sql)

		-- clean up the ##idEnrollmentInstance## additions we made to enrollment titles, we did that to uniquely distinguish names
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblEnrollment] SET '
				+ '		title = REPLACE(DE.title, '' ##'' + CONVERT(NVARCHAR, SEIM.sourceID) + ''##'', '''') '
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblEnrollment] DE '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] SEIM ON SEIM.destinationID = DE.idEnrollment AND SEIM.object = ''enrollment'''
				+ ' WHERE SEIM.sourceID IS NOT NULL'

		EXEC (@sql)
		
		-- lesson data
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblData-Lesson]'
				+ ' ( '
				+ '		idSite, '
				+ '		idEnrollment, '
				+ '		idLesson, '
				+ '		title, '
				+ '		revcode, '
				+ '		[order], '
				+ '		dtCompleted, '
				+ '		idTimezone, '
				+ '		contentTypeCommittedTo, '
				+ '		dtCommittedToContentType, '
				+ '		resetForContentChange, '
				+ '		preventPostCompletionLaunchForContentChange '				
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','											-- idSite
				+ '		EM.destinationID,'																		-- idEnrollment				
				+ '		LM.destinationID,'																		-- idLesson
				+ '		LD.lessonName + '' ##'' + CONVERT(NVARCHAR, LD.idLessonData) + ''##'','					-- title
				+ '		NULL,'																					-- revcode
				+ '		LD.[order],'																			-- order
				+ '		CASE WHEN LD.completionStatus = ''completed'' OR LD.successStatus = ''passed'' THEN'	-- dtCompleted
				+ '			LD.[timestamp] '
				+ '		ELSE NULL END,'
				+ '		E.idTimezone,'																			-- idTimezone
				+ '		NULL,'																					-- contentTypeCommittedTo
				+ '		NULL,'																					-- dtCommittedToContentType
				+ '		NULL,'																					-- resetForContentChange
				+ '		NULL'																					-- preventPostCompletionLaunchForContentChange						
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLessonData] LD'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblEnrollmentInstance] EI ON EI.idEnrollmentInstance = LD.idEnrollmentInstance'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblUser] U ON U.idUser = EI.idUser'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] EM ON EM.sourceID = EI.idEnrollmentInstance AND EM.object = ''enrollment'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] LM ON LM.sourceID = LD.idLesson AND LM.object = ''lesson'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblEnrollment] E ON E.idEnrollment = EM.destinationID'				
				+ ' WHERE U.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)  + ' AND EM.destinationID IS NOT NULL AND E.idEnrollment IS NOT NULL'
		
		EXEC (@sql)

		-- insert idData-Lesson mappings (idData-Lesson maps to Inquisiq idLessonData)
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SLD.idLessonData, '
				+ '		DDL.[idData-Lesson],'
				+ '		''lessondata''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblData-Lesson] DDL '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLessonData] SLD ON SLD.lessonName + '' ##'' + CONVERT(NVARCHAR, SLD.idLessonData) + ''##'' = DDL.title collate database_default'				
				+ ' WHERE DDL.idSite IS NOT NULL AND DDL.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SLD.idLessonData IS NOT NULL AND DDL.[idData-Lesson] IS NOT NULL'
		
		EXEC(@sql)

		-- clean up the ##idLessonData## additions we made to lesson data titles, we did that to uniquely distinguish names
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblData-Lesson] SET '
				+ '		title = REPLACE(DDL.title, '' ##'' + CONVERT(NVARCHAR, SLDM.sourceID) + ''##'', '''') '
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblData-Lesson] DDL '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] SLDM ON SLDM.destinationID = DDL.[idData-Lesson] AND SLDM.object = ''lessondata'''
				+ ' WHERE SLDM.sourceID IS NOT NULL'

		EXEC (@sql)
		
		-- lesson data sco
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblData-SCO]'
				+ ' ( '
				+ '		idSite, '
				+ '		[idData-Lesson], '
				+ '		manifestIdentifier, '
				+ '		completionStatus, '
				+ '		successStatus, '
				+ '		scoreScaled, '
				+ '		totalTime, '
				+ '		[timestamp], '
				+ '		idTimezone, '
				+ '		actualAttemptCount, '
				+ '		effectiveAttemptCount, '
				+ '		proctoringUser '
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','									-- idSite
				+ '		LDM.destinationID,'																-- [idData-Lesson]
				+ '		''##'' + CONVERT(NVARCHAR, LD.idLessonData) + ''##'','							-- use manifest identifier as a mapping plceholder
				+ '		CASE WHEN CS.idSCORMVocabulary IS NULL THEN 0 ELSE CS.idSCORMVocabulary END,'	-- completionStatus
				+ '		CASE WHEN SS.idSCORMVocabulary IS NULL THEN 0 ELSE SS.idSCORMVocabulary END,'	-- successStatus
				+ '		LD.scoreScaled,'																-- scoreScaled
				+ '		LD.totalTime,'																	-- totalTime
				+ '		LD.[timestamp],'																-- timestamp
				+ '		DL.idTimezone,'																	-- idTimezone
				+ '		LD.actualAttempts,'																-- actualAttemptCount
				+ '		LD.effectiveAttempts,'															-- effectiveAttemptCount
				+ '		NULL'																			-- proctoringUser
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLessonData] LD'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblEnrollmentInstance] EI ON EI.idEnrollmentInstance = LD.idEnrollmentInstance'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblUser] U ON U.idUser = EI.idUser'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] LDM ON LDM.sourceID = LD.idLessonData AND LDM.object = ''lessondata'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblData-Lesson] DL ON DL.[idData-Lesson] = LDM.destinationID'
				+ ' LEFT JOIN [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblSCORMVocabulary] CS ON CS.value = LD.completionStatus'
				+ ' LEFT JOIN [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblSCORMVocabulary] SS ON SS.value = LD.successStatus'
				+ ' WHERE U.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)  + ' AND LDM.destinationID IS NOT NULL'
		
		EXEC (@sql)

		-- insert idData-SCO mappings (idData-SCO maps to Inquisiq idLessonData)
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SLD.idLessonData, '
				+ '		DDSCO.[idData-SCO],'
				+ '		''lessondatasco''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblData-SCO] DDSCO '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLessonData] SLD ON ''##'' + CONVERT(NVARCHAR, SLD.idLessonData) + ''##'' = DDSCO.manifestIdentifier collate database_default'
				+ ' WHERE DDSCO.idSite IS NOT NULL AND DDSCO.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SLD.idLessonData IS NOT NULL AND DDSCO.[idData-SCO] IS NOT NULL'
		
		EXEC(@sql)

		-- clean up the ##idLessonData## additions we made to data sco manifestIdentifier, we did that to uniquely distinguish names
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblData-SCO] SET '
				+ '		manifestIdentifier = NULL '
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblData-SCO] DDSCO '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] SLDM ON SLDM.destinationID = DDSCO.[idData-Lesson] AND SLDM.object = ''lessondatasco'''
				+ ' WHERE SLDM.sourceID IS NOT NULL'

		EXEC (@sql)
		
		-- scorm interactions
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblData-SCOInt]'
				+ ' ( '
				+ '		idSite, '
				+ '		[idData-SCO], '
				+ '		identifier, '
				+ '		timestamp, '
				+ '		result, '
				+ '		latency, '
				+ '		scoIdentifier '				
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','											-- idSite
				+ '		DSCOM.destinationID,'																	-- [idData-SCO]
				+ '		SI.identifier,'																			-- identifier
				+ '		SI.timestamp,'																			-- timestamp
				+ '		CASE WHEN SV.idSCORMVocabulary IS NOT NULL THEN SV.idSCORMVocabulary ELSE NULL END,'	-- result
				+ '		SI.latency,'																			-- latency
				+ '		NULL'																					-- scoIdentifier
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblScormInt] SI'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLessonData] LD ON LD.idLessonData = SI.idLessonData'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblEnrollmentInstance] EI ON EI.idEnrollmentInstance = LD.idEnrollmentInstance'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblUser] U ON U.idUser = EI.idUser'				
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] DSCOM ON DSCOM.sourceID = SI.idLessonData AND DSCOM.object = ''lessondatasco'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblSCORMVocabulary] SV ON SV.value = SI.result'
				+ ' WHERE U.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)  + ' AND DSCOM.destinationID IS NOT NULL AND DSCOM.destinationID IN (SELECT [idData-SCO] FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblData-SCO])'
		
		EXEC (@sql)
		
	/****************************************************INSERT HOSTNAME INTO ACCOUNT DB******************************************************/

		SET @sql = 'INSERT INTO [tblAccountToDomainAliasLink]'
				+ ' ( '
				+ '		idAccount, '
				+ '		hostname '			
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idAccount) + ','	-- idAccount
				+ '		''' + @destinationHostname + ''''		-- hostname
		
		EXEC (@sql)
		
	/*********************************************************COMMIT THE TRANSACTION**********************************************************/

	BEGIN TRY
		  
		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemCopyInquisiqToAsentia_Success'

		COMMIT TRANSACTION	   
		--TRANSACTION END

	END TRY

	BEGIN CATCH
				
		SET @Return_Code = 1
		SET @Error_Description_Code	= 'SystemCopyInquisiqToAsentia_Failed'
		
		IF @@TRANCOUNT > 0
			BEGIN
			ROLLBACK TRANSACTION
			END

	END CATCH
		
	SET XACT_ABORT OFF

END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.CopyInquisiqToAsentia.ActivityImportEmail]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.CopyInquisiqToAsentia.ActivityImportEmail]
GO


/*

Copy source site data from Inquisiq to destination site in Asentia

*/
CREATE PROCEDURE [dbo].[System.CopyInquisiqToAsentia.ActivityImportEmail]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,	
	@idAccount				INT,
	@idCaller				INT				= 1,
	@idSiteDefaultLang		INT				= 57,		-- English is ID 57	
	@siteHostname			NVARCHAR(255),
	@idSiteDestination		INT,
	@destinationHostname	NVARCHAR(255),
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@idSiteSource			INT,
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50)	
)
AS

BEGIN

    SET NOCOUNT ON
    SET XACT_ABORT ON    
    	
	DECLARE @sql NVARCHAR(MAX)

	/*************************************************ACTIVITY IMPORT****************************************************/
		-- tblActivityImport
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblActivityImport]'
				+ ' ( '
				+ '		idSite, '
				+ '		idUser, '
				+ '		timestamp, '
				+ '		idUserImported, '
				+ '		importFileName '
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- idSite
				+ '		UM.destinationID,'								-- user id
				+ '		AI.timestamp,'									-- timestamp
				+ '		UIM.destinationID,'								-- user imported id
				+ '		AI.importFileName'								-- import filename
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblActivityImport] AI'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] UM ON UM.sourceID = AI.idUser AND UM.object = ''user'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] UIM ON UIM.sourceID = AI.idUserImported AND UIM.object = ''user'''
				+ ' WHERE idSite = ' + CONVERT(NVARCHAR, @idSiteSource)
		
		EXEC (@sql)

		-- insert idActivityImport mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SAI.idActivityImport, '
				+ '		DAI.idActivityImport,'
				+ '		''activityimport''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblActivityImport] DAI '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblActivityImport] SAI ON SAI.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND SAI.timestamp = DAI.timestamp'
				+ ' WHERE DAI.idSite IS NOT NULL AND DAI.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SAI.idActivityImport IS NOT NULL AND DAI.idActivityImport IS NOT NULL'
		
		EXEC(@sql)

	/**************************************************EVENT EMAIL NOTIFICATION******************************************/
		-- tblEventEmailNotification
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblEventEmailNotification]'
				+ ' ( '
				+ '		idSite, '
				+ '		name, '
				+ '		idEventType, '
				+ '		idEventTypeRecipient, '
				+ '		idObject, '
				+ '		[from], '
				+ '		isActive, '
				+ '		dtActivation, '
				+ '		interval,'
				+ '		timeframe,'
				+ '		priority'
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','										-- idSite
				+ '		EEN.name + '' ##'' + CONVERT(NVARCHAR, EEN.idEventEmailNotification) + ''##'','		-- name
				+ '		ETM.destinationID,'																	-- idEventType
				+ '		ETRM.destinationID,'																-- idEventTypeRecipient
				+ '		NULL,'																				-- idObject
				+ '		EEN.[from],'																		-- from
				+ '		EEN.isActive,'																		-- is active?
				+ '		CASE WHEN EEN.isActive = 1 THEN GETUTCDATE() ELSE NULL END,'						-- activation date
				+ '		EEN.interval,'																		-- interval
				+ '		EEN.timeframe,'																		-- timeframe
				+ '		EEN.priority'																		-- priority
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblEventEmailNotification] EEN'				
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_EventTypeIDMappings] ETM ON ETM.sourceID = EEN.idEventType'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_EventTypeRecipientIDMappings] ETRM ON ETRM.sourceID = EEN.idEventTypeRecipient'
				+ ' WHERE EEN.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND EEN.idEventType <> 4'	-- exclude "Purchase Confirmed"
		
		EXEC (@sql)

		-- insert idEventEmailNotification mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SEEN.idEventEmailNotification, '
				+ '		DEEN.idEventEmailNotification,'
				+ '		''emailnotification''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblEventEmailNotification] DEEN '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblEventEmailNotification] SEEN ON SEEN.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND SEEN.name + '' ##'' + CONVERT(NVARCHAR, SEEN.idEventEmailNotification) + ''##'' = DEEN.name collate database_default'
				+ ' WHERE DEEN.idSite IS NOT NULL AND DEEN.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SEEN.idEventEmailNotification IS NOT NULL AND DEEN.idEventEmailNotification IS NOT NULL'
		
		EXEC(@sql)

		-- clean up the ##idEventEmailNotification## additions we made to email notification titles, we did that to uniquely distinguish names
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblEventEmailNotification] SET '
				+ '		name = REPLACE(DEEN.name, '' ##'' + CONVERT(NVARCHAR, EENM.sourceID) + ''##'', '''') '
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblEventEmailNotification] DEEN '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] EENM ON EENM.destinationID = DEEN.idEventEmailNotification AND EENM.object = ''emailnotification'''
				+ ' WHERE EENM.sourceID IS NOT NULL'

		EXEC (@sql)

		-- tblEventEmailNotificationLanguage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblEventEmailNotificationLanguage]' 
				+ ' ( '
				+ '		idSite,'
				+ '		idEventEmailNotification,'
				+ '		idLanguage,'
				+ '		name'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		EEN.idEventEmailNotification, '
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ', '
				+ '		EEN.name '				
				+ ' FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblEventEmailNotification] EEN'
				+ ' WHERE EEN.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
		
		EXEC(@sql)

		-- create a table, and get email notification data mappings so email notification config XML files can be written		
		SET @sql = 'IF NOT EXISTS (SELECT * FROM [' + @destinationDBName + '].dbo.sysobjects WHERE name = N''InquisiqMigration_EmailNotificationDataMappings'' AND xtype = ''U'')'
			+ ' BEGIN '
			+ 'CREATE TABLE [' + @destinationDBName + '].[dbo].[InquisiqMigration_EmailNotificationDataMappings]'
			+ '('
			+ '    [destinationID] INT NOT NULL,'
			+ '    [data] NVARCHAR(MAX) NOT NULL'
			+ ')'
			+ ' END '
			+ ' ELSE '
		SET @sql = @sql + ' DELETE FROM [' + @destinationDBName + '].[dbo].[InquisiqMigration_EmailNotificationDataMappings]'

		EXEC(@sql)
		
		-- get the email notification data		
		SET @sql = ' INSERT INTO [' + @destinationDBName + '].[dbo].[InquisiqMigration_EmailNotificationDataMappings] '
				+ ' ( '
				+ '		destinationID,'
				+ '		data'
				+ ' ) '
				+ ' SELECT '
				+ '		DEEN.idEventEmailNotification, '
				+ '		  ''<?xml version="1.0" encoding="utf-8"?>'' '
				+ '		+ ''<emailNotification>'' '
				+ '		+ ''	<language code="default">'' '
				+ '		+ ''		<subject><![CDATA['' + SEEN.[subject] + '']]></subject>'' '
				+ '		+ ''		<body><![CDATA['' + SEEN.[bodyTemplate] + '']]></body>'' '
				+ '		+ ''	</language>'' '
				+ '		+ ''	<language code="en-US">'' '
				+ '		+ ''		<subject><![CDATA['' + SEEN.[subject] + '']]></subject>'' '
				+ '		+ ''		<body><![CDATA['' + SEEN.[bodyTemplate] + '']]></body>'' '
				+ '		+ ''	</language>'' '
				+ '		+ ''</emailNotification>'' '				
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblEventEmailNotification] DEEN '
				+ ' LEFT JOIN  [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] EENM ON EENM.destinationID = DEEN.idEventEmailNotification AND EENM.object = ''emailnotification'''
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblEventEmailNotification] SEEN ON SEEN.idEventEmailNotification = EENM.sourceID'				
				+ ' WHERE DEEN.idSite IS NOT NULL AND DEEN.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
				+ ' AND SEEN.idSite IS NOT NULL AND SEEN.idSite = '+ CONVERT(NVARCHAR, @idSiteSource)
	
		EXEC (@sql)

		/*********************************************************COMMIT THE TRANSACTION**********************************************************/

	SET @Return_Code = 0
	SET @Error_Description_Code	= 'SystemCopyInquisiqToAsentiaActivityImportEmail_Success'

		
	SET XACT_ABORT OFF

END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.CopyInquisiqToAsentia.Certificate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.CopyInquisiqToAsentia.Certificate]
GO


/*

Copy source site data from Inquisiq to destination site in Asentia

*/
CREATE PROCEDURE [dbo].[System.CopyInquisiqToAsentia.Certificate]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,	
	@idAccount				INT,
	@idCaller				INT				= 1,
	@idSiteDefaultLang		INT				= 57,		-- English is ID 57	
	@siteHostname			NVARCHAR(255),
	@idSiteDestination		INT,
	@destinationHostname	NVARCHAR(255),
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@idSiteSource			INT,
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50)	
)
AS

BEGIN

    SET NOCOUNT ON
    SET XACT_ABORT ON    
    	
	DECLARE @sql NVARCHAR(MAX)

	/*********************************************************CERTIFICATE**************************************************/
		-- tblCertificate
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCertificate]'
				+ ' ( '
				+ '		idSite, '
				+ '		name, '
				+ '		issuingOrganization, '
				+ '		description, '
				+ '		credits, '
				+ '		filename, '
				+ '		isActive, '
				+ '		activationDate, '
				+ '		expiresInterval,'
				+ '		expiresTimeframe,'
				+ '		objectType,'
				+ '		idObject,'
				+ '		isDeleted,'
				+ '		dtDeleted,'
				+ '		code,'
				+ '		reissueBasedOnMostRecentCompletion'
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','									-- idSite
				+ '		C.name,'																		-- name
				+ '		C.issuingOrganization,'															-- title
				+ '		C.description,'																	-- description
				+ '		C.credits,'																		-- credits
				+ '		REPLACE(C.filename, ''/_config/' + @siteHostname + '/_images/certs/'', ''''),'	-- filename
				+ '		C.isActive,'																	-- is certificate active?
				+ '		C.activationDate,'																-- activation date
				+ '		C.expiresInterval,'																-- expires interval
				+ '		C.expiresTimespan,'																-- expires timeframe
				+ '		1,'																				-- object type (1 = course)
				+ '		CM.destinationID,'																-- object id (course id)
				+ '		0,'																				-- is deleted?
				+ '		NULL,'																			-- date deleted?
				+ '		C.code,'																		-- code
				+ '		NULL'																			-- reissue based on most recent completion
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCertificate] C'
				+ ' LEFT JOIN  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCourseCertificateLink] CCL ON CCL.idCertificate = C.idCertificate'
				+ ' LEFT JOIN  [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.sourceID = CCL.idCourse AND CM.object = ''course'''
				+ ' WHERE C.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND CM.destinationID IS NOT NULL'
		
		EXEC (@sql)		

		-- insert idCertificate mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SC.idCertificate, '
				+ '		DC.idCertificate,'
				+ '		''certificate''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCertificate] DC '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCertificate] SC ON SC.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND  SC.name = DC.name collate database_default AND REPLACE(SC.filename, ''/_config/' + @siteHostname + '/_images/certs/'', '''') = DC.filename collate database_default AND SC.activationDate = DC.activationDate'
				+ ' WHERE DC.idSite IS NOT NULL AND DC.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SC.idCertificate IS NOT NULL AND DC.idCertificate IS NOT NULL'
		
		EXEC(@sql)

		-- tblCertificateLanguage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblCertificateLanguage]' 
				+ ' ( '
				+ '		idSite,'
				+ '		idCertificate,'
				+ '		idLanguage,'
				+ '		name,'
				+ '		description'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		C.idCertificate, '
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ', '
				+ '		C.name, '
				+ '		C.description '
				+ ' FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblCertificate] C'
				+ ' WHERE C.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
		
		EXEC(@sql)		

		-- create a table, and get certificate instance data mappings so certificate config JSON files can be written
		SET @sql = 'IF NOT EXISTS (SELECT * FROM [' + @destinationDBName + '].dbo.sysobjects WHERE name = N''InquisiqMigration_CertificateLayoutDataMappings'' AND xtype = ''U'')'
			+ ' BEGIN '
			+ 'CREATE TABLE [' + @destinationDBName + '].[dbo].[InquisiqMigration_CertificateLayoutDataMappings]'
			+ '('
			+ '    [destinationID] INT NOT NULL,'
			+ '    [data] NVARCHAR(MAX) NOT NULL'
			+ ')'
			+ ' END '
			+ ' ELSE '
		SET @sql = @sql + ' DELETE FROM [' + @destinationDBName + '].[dbo].[InquisiqMigration_CertificateLayoutDataMappings]'

		EXEC(@sql)
		
		-- get the certificate instance data		
		SET @sql = ' INSERT INTO [' + @destinationDBName + '].[dbo].[InquisiqMigration_CertificateLayoutDataMappings] '
				+ ' ( '
				+ '		destinationID,'
				+ '		data'
				+ ' ) '
				+ ' SELECT '
				+ '		DC.idCertificate, '
				+ '		REPLACE(REPLACE(SC.instanceData, ''%20'', '' ''), ''"ImageSource": "'' + SC.[filename] + ''"'', ''"ImageSource": "/_config/' + @destinationHostname + '/certificates/'' + CONVERT(NVARCHAR, DC.idCertificate) + ''/'' + DC.[filename] + ''"'') '
				--+ '		CASE WHEN LEFT(SC.instanceData,14) = ''<instanceData>'' THEN '
				--+ '			  ''{"Certificate" : '''
				--+ '			+ ''{ "Container" : '''
				--+ '			+ ''{ "Width" : "800px", "Height" : "600px", "ImageSource" : "'' + REPLACE(CAST(SC.[instanceData] AS XML).value(''(/instanceData/image)[1]'',''NVARCHAR(MAX)''),''_images/certs'',''certificates/'' + CONVERT(NVARCHAR, DC.idCertificate)) +''" },'''
				--+ '			+ ''"Elements": ['''
				--+ '			+ CASE when CAST(SC.[instanceData] AS XML).value(''(count(/instanceData/fields/field))'',''nvarchar(max)'') >= 1 then ''{"Identifier": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@columnName)[1]'',''nvarchar(max)'') + ''", "Value" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field)[1]'',''nvarchar(max)'') + ''", "IsStaticLabel" : "false", "Top": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@y)[1]'',''nvarchar(max)'') + ''", "Left" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@x)[1]'',''nvarchar(max)'') + ''", "Width" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@width)[1]'',''nvarchar(max)'') + ''", "FontSize" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@size)[1]'',''nvarchar(max)'') + ''", "Alignment" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@align)[1]'',''nvarchar(max)'') + ''"}'' else '''' end'
				--+ '			+ CASE when CAST(SC.[instanceData] AS XML).value(''(count(/instanceData/fields/field))'',''nvarchar(max)'') >= 2 then '', {"Identifier": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@columnName)[2]'',''nvarchar(max)'') + ''", "Value" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field)[2]'',''nvarchar(max)'') + ''", "IsStaticLabel" : "false", "Top": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@y)[2]'',''nvarchar(max)'') + ''", "Left" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@x)[2]'',''nvarchar(max)'') + ''", "Width" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@width)[2]'',''nvarchar(max)'') + ''", "FontSize" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@size)[2]'',''nvarchar(max)'') + ''", "Alignment" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@align)[2]'',''nvarchar(max)'') + ''"}'' else '''' end'
				--+ '			+ CASE when CAST(SC.[instanceData] AS XML).value(''(count(/instanceData/fields/field))'',''nvarchar(max)'') >= 3 then '', {"Identifier": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@columnName)[3]'',''nvarchar(max)'') + ''", "Value" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field)[3]'',''nvarchar(max)'') + ''", "IsStaticLabel" : "false", "Top": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@y)[3]'',''nvarchar(max)'') + ''", "Left" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@x)[3]'',''nvarchar(max)'') + ''", "Width" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@width)[3]'',''nvarchar(max)'') + ''", "FontSize" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@size)[3]'',''nvarchar(max)'') + ''", "Alignment" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@align)[3]'',''nvarchar(max)'') + ''"}'' else '''' end'
		--SET @sql2 = '		+ CASE when CAST(SC.[instanceData] AS XML).value(''(count(/instanceData/fields/field))'',''nvarchar(max)'') >= 4 then '', {"Identifier": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@columnName)[4]'',''nvarchar(max)'') + ''", "Value" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field)[4]'',''nvarchar(max)'') + ''", "IsStaticLabel" : "false", "Top": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@y)[4]'',''nvarchar(max)'') + ''", "Left" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@x)[4]'',''nvarchar(max)'') + ''", "Width" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@width)[4]'',''nvarchar(max)'') + ''", "FontSize" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@size)[4]'',''nvarchar(max)'') + ''", "Alignment" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@align)[4]'',''nvarchar(max)'') + ''"}'' else '''' end'
				--+ '			+ CASE when CAST(SC.[instanceData] AS XML).value(''(count(/instanceData/fields/field))'',''nvarchar(max)'') >= 5 then '', {"Identifier": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@columnName)[5]'',''nvarchar(max)'') + ''", "Value" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field)[5]'',''nvarchar(max)'') + ''", "IsStaticLabel" : "false", "Top": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@y)[5]'',''nvarchar(max)'') + ''", "Left" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@x)[5]'',''nvarchar(max)'') + ''", "Width" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@width)[5]'',''nvarchar(max)'') + ''", "FontSize" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@size)[5]'',''nvarchar(max)'') + ''", "Alignment" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@align)[5]'',''nvarchar(max)'') + ''"}'' else '''' end'
				--+ '			+ CASE when CAST(SC.[instanceData] AS XML).value(''(count(/instanceData/fields/field))'',''nvarchar(max)'') >= 6 then '', {"Identifier": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@columnName)[6]'',''nvarchar(max)'') + ''", "Value" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field)[6]'',''nvarchar(max)'') + ''", "IsStaticLabel" : "false", "Top": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@y)[6]'',''nvarchar(max)'') + ''", "Left" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@x)[6]'',''nvarchar(max)'') + ''", "Width" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@width)[6]'',''nvarchar(max)'') + ''", "FontSize" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@size)[6]'',''nvarchar(max)'') + ''", "Alignment" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@align)[6]'',''nvarchar(max)'') + ''"}'' else '''' end'
				--+ '			+ CASE when CAST(SC.[instanceData] AS XML).value(''(count(/instanceData/fields/field))'',''nvarchar(max)'') >= 7 then '', {"Identifier": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@columnName)[7]'',''nvarchar(max)'') + ''", "Value" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field)[7]'',''nvarchar(max)'') + ''", "IsStaticLabel" : "false", "Top": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@y)[7]'',''nvarchar(max)'') + ''", "Left" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@x)[7]'',''nvarchar(max)'') + ''", "Width" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@width)[7]'',''nvarchar(max)'') + ''", "FontSize" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@size)[7]'',''nvarchar(max)'') + ''", "Alignment" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@align)[7]'',''nvarchar(max)'') + ''"}'' else '''' end'
				--+ '			+'']}}'''    
				--+ '		ELSE SC.instanceData END '
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCertificate] DC '
				+ ' LEFT JOIN  [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.destinationID = DC.idCertificate AND CM.object = ''certificate'''
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCertificate] SC ON SC.idCertificate = CM.sourceID'				
				+ ' WHERE DC.idSite IS NOT NULL AND DC.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
				+ ' AND SC.idSite IS NOT NULL AND SC.idSite = '+ CONVERT(NVARCHAR, @idSiteSource)
	
		EXEC (@sql)
		
		-- tblCertificateRecord - this may need a clean-up after migration as there may be "duplicate" certificate record entries
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCertificateRecord]'
				+ ' ( '
				+ '		idSite, '
				+ '		idCertificate, '
				+ '		idUser, '
				+ '		idTimezone, '
				+ '		idAwardedBy, '
				+ '		timestamp, '
				+ '		expires, '
				+ '		code, '
				+ '		credits,'
				+ '		awardData,'
				+ '		idCertificateImport'
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','								-- idSite
				+ '		CM.destinationID,'															-- certificate id
				+ '		UM.destinationID,'															-- user id
				+ '		CASE WHEN TZ.destinationID > 0 THEN TZ.destinationID ELSE 15 END,'			-- timezone
				+ '		1,'																			-- awarded by id
				+ '		CR.timestamp,'																-- timestamp
				+ '		CR.expires,'																-- expires
				+ '		CR.code,'																	-- code
				+ '		CR.credits,'																-- credits
				+ '		NULL,'																		-- award data
				+ '		NULL'																		-- certificate import
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCertificateRecord] CR '				
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.sourceID = CR.idCertificate AND CM.object = ''certificate'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] UM ON UM.sourceID = CR.idUser AND UM.object = ''user'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_TimezoneIDMappings] TZ ON TZ.sourceID = CR.idTimezone '
				+ ' WHERE CM.destinationID IS NOT NULL AND UM.destinationID IS NOT NULL'
		
		EXEC (@sql)	

	SET @Return_Code = 0
	SET @Error_Description_Code	= 'SystemCopyInquisiqToAsentiaCertificate_Success'
		
	SET XACT_ABORT OFF

END

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.CopyInquisiqToAsentia.Content]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.CopyInquisiqToAsentia.Content]
GO


/*

Copy source site data from Inquisiq to destination site in Asentia

*/
CREATE PROCEDURE [dbo].[System.CopyInquisiqToAsentia.Content]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,	
	@idAccount				INT,
	@idCaller				INT				= 1,
	@idSiteDefaultLang		INT				= 57,		-- English is ID 57	
	@siteHostname			NVARCHAR(255),
	@idSiteDestination		INT,
	@destinationHostname	NVARCHAR(255),
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@idSiteSource			INT,
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50),
	@useNewContentPath		BIT
)
AS

BEGIN

    SET NOCOUNT ON
    SET XACT_ABORT ON    
    	
	DECLARE @sql NVARCHAR(MAX)
	
	/******************************************************CONTENT PACKAGE********************************************************************/	   
		-- tblContentPackage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblContentPackage]'
				+ ' ( '
				+ '		idSite, '
				+ '		idContentPackageType, '
				+ '		idSCORMPackageType, '
				+ '		name, '
				+ '		path, '
				+ '		kb, '
				+ '		manifest, '
				+ '		dtCreated, '
				+ '		dtModified,'
				+ '		isMediaUpload,'
				+ '		idMediaType,'
				+ '		contentTitle,'
				+ '		originalMediaFilename,'
				+ '		isVideoMedia3rdParty,'
				+ '		videoMediaEmbedCode,'
				+ '		enableAutoplay,'
				+ '		allowRewind,'
				+ '		allowFastForward,'
				+ '		allowNavigation,'
				+ '		allowResume,'
				+ '		minProgressForCompletion,'
				+ '		isProcessing,'
				+ '		isProcessed,'
				+ '		dtProcessed,'
				+ '		idQuizSurvey'
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- idSite
				+ '		1,'												-- content package type (1 = SCORM)
				+ '		1,'												-- SCORM package type (1 = content)
				+ '		SP.name,'										-- name
				+ '		CASE WHEN ' + CONVERT(NVARCHAR, @useNewContentPath) + ' = 1 THEN'
				+ '			''/warehouse/' + CONVERT(NVARCHAR, @idAccount) + '-'' + ''' + CONVERT(NVARCHAR, @idSiteDestination) + '-'' + CONVERT(NVARCHAR, FORMAT(SP.dateCreated, ''yyyy-MM-dd-hh-mm-ss'')) + ''-'' + SP.path' -- new content path
				+ '		ELSE'
				+ '			STUFF([path], CHARINDEX(''' + CONVERT(NVARCHAR, @idSiteSource) + '-'', [path]), LEN(''' + CONVERT(NVARCHAR, @idSiteSource) + '-''), ''/warehouse/' + CONVERT(NVARCHAR, @idAccount) + '-'' + ''' + CONVERT(NVARCHAR, @idSiteDestination) + '-'')' -- old content path
				+ '		END,'				
				+ '		SP.kb,'											-- kb
				+ '		SP.imsmanifest,'								-- manifest
				+ '		SP.dateCreated,'								-- created date
				+ '		SP.dateModified,'								-- modified date
				+ '		SP.isMediaUpload,'								-- is media upload?
				+ '		SP.idMediaType,'								-- media type
				+ '		SP.contentTitle,'								-- content title
				+ '		SP.originalMediaFilename,'						-- original media filename
				+ '		SP.isVideoMedia3rdParty,'						-- is video media 3rd party?
				+ '		SP.videoMediaEmbedCode,'						-- video media embed code
				+ '		SP.enableAutoPlay,'								-- enable autoplay
				+ '		SP.allowRewind,'								-- allow rewind
				+ '		SP.allowFastForward,'							-- allow fast forward
				+ '		SP.allowNavigation,'							-- allow navigation
				+ '		SP.allowResume,'								-- allow resume
				+ '		SP.minProgressForCompletion,'					-- minimum progress for completion
				+ '		SP.isProcessing,'								-- is processing?
				+ '		SP.isProcessed,'								-- is processed?
				+ '		SP.dtProcessed,'								-- date processed				
				+ '		NULL'											-- quiz survey id (inserted later, doing so now is a circular reference)
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblSCORMPackage] SP'
				+ ' WHERE SP.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)
		
		EXEC (@sql)

		-- insert idContentPackage mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SCP.idSCORMPackage, '
				+ '		DCP.idContentPackage,'
				+ '		''contentpackage''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblContentPackage] DCP '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblSCORMPackage] SCP ON SCP.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND SCP.name = DCP.name collate database_default AND SCP.dateCreated = DCP.dtCreated'
				+ ' WHERE DCP.idSite IS NOT NULL AND DCP.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SCP.idSCORMPackage IS NOT NULL AND DCP.idContentPackage IS NOT NULL'
		
		EXEC(@sql)		

	/*****************************************************************QUIZ SURVEY*************************************************************/
		-- tblQuizSurvey
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblQuizSurvey]'
				+ ' ( '
				+ '		idSite, '
				+ '		idAuthor, '
				+ '		type, '
				+ '		identifier, '
				+ '		guid, '
				+ '		data, '
				+ '		isDraft, '
				+ '		idContentPackage, '
				+ '		dtCreated,'
				+ '		dtModified'
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','									-- idSite
				+ '		ISNULL(UM.destinationID, 1),'													-- idAuthor
				+ '		QS.type,'																		-- type
				+ '		QS.identifier,'																	-- identifier
				+ '		QS.guid,'																		-- guid
				+ '		QS.data,'																		-- data
				+ '		QS.isDraft,'																	-- isDraft
				+ '		CASE WHEN QS.idSCORMPackage IS NOT NULL THEN CPM.destinationID ELSE NULL END,'	-- idContentPackage
				+ '		QS.dtCreated,'																	-- dtCreated
				+ '		QS.dtModified'																	-- dtModfied
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblQuizSurvey] QS'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] UM ON UM.sourceID = QS.idUser AND UM.object = ''user'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CPM ON CPM.sourceID = QS.idSCORMPackage AND CPM.object = ''contentpackage'''
				+ ' WHERE QS.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)

		EXEC (@sql)

		-- insert idQuizSurvey mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SQS.idQuizSurvey, '
				+ '		DQS.idQuizSurvey,'
				+ '		''quizsurvey''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblQuizSurvey] DQS '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblQuizSurvey] SQS ON SQS.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND SQS.identifier = DQS.identifier collate database_default AND SQS.guid = DQS.guid collate database_default AND SQS.dtCreated = DQS.dtCreated'
				+ ' WHERE DQS.idSite IS NOT NULL AND DQS.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SQS.idQuizSurvey IS NOT NULL AND DQS.idQuizSurvey IS NOT NULL'
		
		EXEC(@sql)

		
		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemCopyInquisiqToAsentia_Success'

		
		
	SET XACT_ABORT OFF

END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.CopyInquisiqToAsentia.CouponCode]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.CopyInquisiqToAsentia.CouponCode]
GO


/*

Copy source site data from Inquisiq to destination site in Asentia

*/
CREATE PROCEDURE [dbo].[System.CopyInquisiqToAsentia.CouponCode]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,	
	@idAccount				INT,
	@idCaller				INT				= 1,
	@idSiteDefaultLang		INT				= 57,		-- English is ID 57	
	@siteHostname			NVARCHAR(255),
	@idSiteDestination		INT,
	@destinationHostname	NVARCHAR(255),
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@idSiteSource			INT,
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50)	
)
AS

BEGIN

    SET NOCOUNT ON
    SET XACT_ABORT ON    
    	
	DECLARE @sql NVARCHAR(MAX)

	

	/*****************************************************COUPON CODE***********************************************************************/		
		-- tblCouponCode
		SET @sql = ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCouponCode]'
				+ ' ( '
				+ '		idSite, '
				+ '		usesAllowed, '
				+ '		discount, '
				+ '		discountType, '
				+ '		code, '
				+ '		isDeleted, '
				+ '		comments, '
				+ '		forCourse, '
				+ '		forCatalog, '
				+ '		dtStart, '
				+ '		dtEnd, '
				+ '		isSingleUsePerUser, '
				+ '		dtDeleted, '
				+ '		forLearningPath, '
				+ '		forStandupTraining ' 
				+ ' ) '
				+ ' SELECT DISTINCT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) +','						-- idSite
				+ '		CC.usesAllowed,'													-- uses allowed
				+ '		CASE WHEN CC.value < 0 THEN CC.value * -1'							-- discount
				+ '			 WHEN CC.value >= 0.1 AND CC.value <= 0.9 THEN CC.value * 100'
				+ '		ELSE CC.value END,'
				+ '		CASE WHEN CC.value = 0 THEN 1'										-- discount type
				+ '			 WHEN CC.value >= 0.1 AND CC.value <= 0.9 THEN 4'
				+ '			 WHEN CC.value < 0 THEN 3'
				+ '		ELSE 2 END,'
				+ '		CC.code,'															-- code
				+ '		CC.isDeleted,'														-- is deleted?
				+ '		CC.comments,'														-- comments
				+ '		CASE WHEN CC.isCourse = 0 THEN 1 '									-- for course
				+ '			 WHEN CC.isCourse = 1 AND courseLink.idCourse IS NULL THEN 2 '
				+ '		ELSE 3 END,'														
				+ '		CASE WHEN CC.isCatalog = 0 THEN 1 '									-- for catalog
				+ '			 WHEN CC.isCatalog = 1 AND catalogLink.idCatalog IS NULL THEN 2 '
				+ '		ELSE 3 END,'														
				+ '		CC.dtStart,'														-- date start
				+ '		CC.dtEnd,'															-- date end
				+ '		CC.isSingleUsePerUser, '											-- is single use per user?
				+ '		CASE WHEN CC.isDeleted = 1 THEN GETUTCDATE() ELSE NULL END, '		-- date deleted
				+ '		1, '																-- for learning path
				+ '		1 '																	-- for standup training	
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCouponCode] CC'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCouponCodeCourseLink] courseLink ON courseLink.idCouponCode = CC.idCouponCode'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCouponCodeCatalogLink] catalogLink ON catalogLink.idCouponCode = CC.idCouponCode'
				+ ' WHERE CC.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)

		EXEC (@sql)

		-- insert idCouponCode mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SCC.idCouponCode, '
				+ '		DCC.idCouponCode,'
				+ '		''couponcode''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCouponCode] DCC '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCouponCode] SCC ON SCC.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND SCC.code = DCC.code AND SCC.usesAllowed = DCC.usesAllowed'
				+ ' WHERE DCC.idSite IS NOT NULL AND DCC.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SCC.idCouponCode IS NOT NULL AND DCC.idCouponCode IS NOT NULL'
		
		EXEC(@sql)

		
		  
		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemCopyInquisiqToAsentiaCouponCode_Success'

		
		
	SET XACT_ABORT OFF

END

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.CopyInquisiqToAsentia.Course]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.CopyInquisiqToAsentia.Course]
GO


/*

Copy source site data from Inquisiq to destination site in Asentia

*/
CREATE PROCEDURE [dbo].[System.CopyInquisiqToAsentia.Course]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,	
	@idAccount				INT,
	@idCaller				INT				= 1,
	@idSiteDefaultLang		INT				= 57,		-- English is ID 57	
	@siteHostname			NVARCHAR(255),
	@idSiteDestination		INT,
	@destinationHostname	NVARCHAR(255),
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@idSiteSource			INT,
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50)	
)
AS

BEGIN

    SET NOCOUNT ON
    SET XACT_ABORT ON    
    	
	DECLARE @sql NVARCHAR(MAX)

	/**********************************************COURSE************************************************************/
		-- tblCourse
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCourse]'
				+ ' ( '
				+ '		idSite, '
				+ '		title, '
				+ '		coursecode, '
				+ '		revcode, '
				+ '		cost, '
				+ '		credits, '
				+ '		shortDescription, '
				+ '		longDescription, '
				+ '		isPublished, '
				+ '		isClosed, '
				+ '		isLocked,'
				+ '		dtCreated,'
				+ '		dtModified,'
				+ '		isDeleted,'
				+ '		dtDeleted,'
				+ '		minutes,'
				+ '		objectives,'
				+ '		socialMedia,'
				+ '		isPrerequisiteAny,'
				+ '		avatar,'
				+ '		isFeedActive,'
				+ '		isFeedModerated,'
				+ '		isFeedOpenSubscription,'
				+ '		disallowRating,'
				+ '		forceLessonCompletionInOrder,'
				+ '		selfEnrollmentDueInterval,'
				+ '		selfEnrollmentDueTimeframe,'
				+ '		selfEnrollmentExpiresFromStartInterval,'
				+ '		selfEnrollmentExpiresFromStartTimeframe,'
				+ '		selfEnrollmentExpiresFromFirstLaunchInterval,'
				+ '		selfEnrollmentExpiresFromFirstLaunchTimeframe,'
				+ '		searchTags,'
				+ '		requireFirstLessonToBeCompletedBeforeOthers,'
				+ '		lockLastLessonUntilOthersCompleted,'
				+ '		isSelfEnrollmentApprovalRequired,'
				+ '		isApprovalAllowedByCourseExperts,'
				+ '		isApprovalAllowedByUserSupervisor'
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- idSite				
				+ '		C.name + '' ##'' + CONVERT(NVARCHAR, C.idCourse) + ''##'','	-- title
				+ '		C.code,'										-- code
				+ '		NULL,'											-- revcode
				+ '		C.cost,'										-- cost
				+ '		C.credits,'										-- credits
				+ '		C.shortDescription,'							-- short description
				+ '		C.description,'									-- description
				+ '		C.isPublished,'									-- is published?
				+ '		C.isClosed,'									-- is closed?	
				+ '		C.isLocked,'									-- is locked?
				+ '		C.dtCreated,'									-- date created
				+ '		C.dtModified,'									-- date modified
				+ '		0,'												-- is deleted?
				+ '		NULL,'											-- date deleted
				+ '		C.minutes,'										-- minutes
				+ '		C.objectives,'									-- objectives
				+ '		REPLACE(REPLACE(C.socialMedia, ''protocol="http://"'', ''protocol="http://" iconType="default"''), ''protocol="https://"'', ''protocol="https://" iconType="default"''),' -- social media
				+ '		C.prerequisiteAny,'								-- any or all prerequisites?
				+ '		NULL,'											-- avatar
				+ '		0,'												-- is feed active?
				+ '		0,'												-- is feed moderated?
				+ '		0,'												-- is feed open subscription?
				+ '		~C.allowRating,'								-- disallow rating? (negate it because Asentia's field is "disallow")
				+ '		C.blnForceLessonCompletionInOrder,'				-- force lesson completion in order?
				+ '		NULL,'											-- self enrollment due interval
				+ '		NULL,'											-- self enrollment due timeframe
				+ '		C.accessInterval,'								-- self enrollment expires from start interval
				+ '		C.accessTimeFrame,'								-- self enrollment expires from start timeframe
				+ '		NULL,'											-- self enrollment expires from first launch interval
				+ '		NULL,'											-- self enrollment expires from first launch timeframe
				+ '		NULL,'											-- search tags
				+ '		C.blnFirstLessonRequired,'						-- require first lesson to be completed before others
				+ '		C.blnLastLessonLocked,'							-- lock last lesson until others completed
				+ '		NULL,'											-- is self enrollment approval required?
				+ '		NULL,'											-- is approval allowed by course experts?
				+ '		NULL'											-- is approval allowed by user supervisor?
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCourse] C'
				+ ' WHERE C.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)
		
		EXEC (@sql)

		-- insert idCourse mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SC.idCourse, '
				+ '		DC.idCourse,'
				+ '		''course''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCourse] DC '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCourse] SC ON SC.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND SC.name + '' ##'' + CONVERT(NVARCHAR, SC.idCourse) + ''##'' = DC.title collate database_default AND SC.dtModified = DC.dtModified'
				+ ' WHERE DC.idSite IS NOT NULL AND DC.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SC.idCourse IS NOT NULL AND DC.idCourse IS NOT NULL'
		
		EXEC(@sql)		

		-- clean up the ##idCourse## additions we made to course titles, we did that to uniquely distinguish names
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblCourse] SET '
				+ '		title = REPLACE(DST.title, '' ##'' + CONVERT(NVARCHAR, SRC.sourceID) + ''##'', '''') '
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCourse] DST '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] SRC ON SRC.destinationID = DST.idCourse AND SRC.object = ''course'''
				+ ' WHERE SRC.sourceID IS NOT NULL'

		EXEC (@sql)

		-- tblCourseLanguage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblCourseLanguage]' 
				+ ' ( '
				+ '		idSite,'
				+ '		idCourse,'
				+ '		idLanguage,'
				+ '		title, '
				+ '		shortDescription, '
				+ '		longDescription, '
				+ '		objectives, '
				+ '		searchTags '
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		C.idCourse, '
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ', '
				+ '		C.title, '
				+ '		C.shortDescription, '
				+ '		C.longDescription, '
				+ '		C.objectives, '
				+ '		C.searchTags '
				+ ' FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblCourse] C'
				+ ' WHERE C.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)

		EXEC(@sql)
	   
		-- tblCourseRating
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCourseRating]'
				+ ' ( '
				+ '		idSite, '
				+ '		idCourse, '
				+ '		idUser, '
				+ '		rating, '
				+ '		timestamp '
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- idSite
				+ '		C.destinationID,'								-- course id
				+ '		U.destinationID,'								-- user id
				+ '		CR.rating,'										-- rating
				+ '		CR.timestamp'									-- timestamp
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCourseRating] CR'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] C ON C.sourceID = CR.idCourse AND C.object = ''course'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] U ON U.sourceID = CR.idUser AND U.object = ''user'''
				+ ' WHERE C.destinationID IS NOT NULL AND U.destinationID IS NOT NULL'
		
		EXEC (@sql)

		-- tblCourseExpert	
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCourseExpert]'
				+ ' ( '
				+ '		idSite, '
				+ '		idCourse, '
				+ '		idUser '
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- idSite
				+ '		CM.destinationID,'								-- course id
				+ '		UM.destinationID'								-- user id
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCourse] C'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.sourceID = C.idCourse AND CM.object = ''course'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] UM ON UM.sourceID = C.idExpert AND UM.object = ''user'''
				+ ' WHERE C.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND CM.destinationID IS NOT NULL AND UM.destinationID IS NOT NULL'
		
		EXEC (@sql)

		-- tblDocumentRespositoryItem (tblCourseMaterial in Inquisiq) - file copies are done inside of procedural code
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblDocumentRepositoryItem]'
				+ ' ( '
				+ '		idSite, '
				+ '		idDocumentRepositoryObjectType, '
				+ '		idObject, '
				+ '		idDocumentRepositoryFolder, '
				+ '		idOwner, '
				+ '		filename, '
				+ '		kb, '
				+ '		searchTags, '
				+ '		isPrivate, '
				+ '		idLanguage, '
				+ '		isAllLanguages, '
				+ '		dtCreated, '
				+ '		isDeleted, '
				+ '		dtDeleted, '
				+ '		label, '
				+ '		isVisibleToUser, '
				+ '		isUploadedByUser '
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- idSite
				+ '		2,'												-- document repository object type (course = 2)
				+ '		C.destinationID,'								-- object id (course id)
				+ '		NULL,'											-- document repository folder id
				+ '		1,'												-- owner id
				+ '		CM.filename,'									-- filename
				+ '		CM.kb,'											-- kb
				+ '		NULL,'											-- search tags
				+ '		CM.isPrivate,'									-- is private?
				+ '		NULL,'											-- language id
				+ '		1,'												-- is all languages?
				+ '		GETUTCDATE(),'									-- date created
				+ '		0,'												-- is deleted?
				+ '		NULL,'											-- date deleted
				+ '		CM.name,'										-- label
				+ '		NULL,'											-- is visible to user?
				+ '		NULL'											-- is uploaded by user?
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCourseMaterial] CM'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] C ON C.sourceID = CM.idCourse AND C.object = ''course'''
				+ ' WHERE C.destinationID IS NOT NULL'
		
		EXEC (@sql)

		-- tblDocumentRepositoryItemLanguage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblDocumentRepositoryItemLanguage]' 
				+ ' ( '
				+ '		idSite,'
				+ '		idDocumentRepositoryItem,'
				+ '		idLanguage,'
				+ '		label, '				
				+ '		searchTags '
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		DRI.idDocumentRepositoryItem, '
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ', '
				+ '		DRI.label, '				
				+ '		DRI.searchTags '
				+ ' FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblDocumentRepositoryItem] DRI'
				+ ' WHERE DRI.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND DRI.idDocumentRepositoryObjectType = 2'

		EXEC(@sql)

		/*****************************************************LESSON**********************************************************************/
		-- tblLesson
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblLesson]'
				+ ' ( '
				+ '		idSite, '
				+ '		idCourse, '
				+ '		dtDeleted, '
				+ '		isDeleted, '
				+ '		title, '
				+ '		revcode, '
				+ '		shortDescription, '
				+ '		longDescription, '
				+ '		[order], '
				+ '		dtCreated,'
				+ '		dtModified,'
				+ '		searchTags'
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- idSite
				+ '		CM.destinationID,'								-- course id
				+ '		NULL,'											-- deleted date
				+ '		0,'												-- is deleted?
				+ '		L.name,'										-- title
				+ '		NULL,'											-- revcode
				+ '		L.shortDescription,'							-- short description
				+ '		L.description,'									-- long description
				+ '		L.[order],'										-- lesson order
				+ '		GETUTCDATE(),'									-- date created
				+ '		GETUTCDATE(),'									-- date modified
				+ '		NULL'											-- search tags
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLesson] L'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.sourceID = L.idCourse AND CM.object = ''course'''
				+ ' WHERE CM.destinationID IS NOT NULL'
		
		EXEC (@sql)	  

		-- insert idLesson mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SL.idLesson, '
				+ '		DL.idLesson,'
				+ '		''lesson''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblLesson] DL '
				+ ' LEFT JOIN [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.destinationID = DL.idCourse AND CM.object = ''course'''
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLesson] SL ON SL.idCourse = CM.sourceID AND SL.name = DL.title collate database_default'
				+ ' WHERE DL.idSite IS NOT NULL AND DL.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SL.idLesson IS NOT NULL AND DL.idLesson IS NOT NULL'
		
		EXEC(@sql)

		-- tblLessonLanguage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblLessonLanguage]' 
				+ ' ( '
				+ '		idSite,'
				+ '		idLesson,'
				+ '		idLanguage,'
				+ '		title,'
				+ '		shortDescription,'
				+ '		longDescription,'
				+ '		searchTags'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		L.idLesson, '
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ', '
				+ '		L.title, '
				+ '		L.shortDescription, '
				+ '		L.longDescription, '
				+ '		L.searchTags '
				+ ' FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblLesson] L'
				+ ' WHERE L.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
		
		EXEC(@sql)	

	/*****************************************************INSTRUCTOR LED TRAINING**********************************************************************/
		-- tblStandUpTraining (this needs to be sourced from tblLesson in Inquisiq)
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblStandUpTraining]'
				+ ' ( '
				+ '		idSite, '
				+ '		title, '
				+ '		description, '
				+ '		isStandaloneEnroll, '
				+ '		isRestrictedEnroll, '
				+ '		isRestrictedDrop, '
				+ '		searchTags, '
				+ '		avatar, '
				+ '		isDeleted,'
				+ '		dtDeleted,'
				+ '		cost'
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','					-- idSite
				+ '		L.name + '' ##'' + CONVERT(NVARCHAR, L.idLesson) + ''##'','		-- title
				+ '		NULL,'															-- description
				+ '		0,'																-- isStandaloneEnroll
				+ '		0,'																-- isRestrictedEnroll
				+ '		0,'																-- isRestrictedDrop
				+ '		NULL,'															-- searchTags
				+ '		NULL,'															-- avatar
				+ '		0,'																-- isDeleted
				+ '		NULL,'															-- dtDeleted
				+ '		NULL'															-- cost
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLesson] L'				
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.sourceID = L.idCourse AND CM.object = ''course'''
				+ ' WHERE CM.destinationID IS NOT NULL'
				+ ' AND L.idLessonType IN (1, 2)' -- 1 = classroom, 2 = web meeting
						
		EXEC (@sql)

		-- insert idStandupTraining mappings (idStandupTraining maps to Inquisiq idLesson as they are equivalents)
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SL.idLesson, '
				+ '		DST.idStandupTraining,'
				+ '		''ilt''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblStandupTraining] DST '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLesson] SL ON SL.name + '' ##'' + CONVERT(NVARCHAR, SL.idLesson) + ''##'' = DST.title collate database_default'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCourse] C ON C.idCourse = SL.idCourse AND C.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)				
				+ ' WHERE DST.idSite IS NOT NULL AND DST.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SL.idLesson IS NOT NULL AND DST.idStandupTraining IS NOT NULL'
		
		EXEC(@sql)

		-- clean up the ##idLesson## additions we made to standup training titles, we did that to uniquely distinguish names
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblStandupTraining] SET '
				+ '		title = REPLACE(DST.title, '' ##'' + CONVERT(NVARCHAR, STM.sourceID) + ''##'', '''') '
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblStandupTraining] DST '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] STM ON STM.destinationID = DST.idStandupTraining AND STM.object = ''ilt'''
				+ ' WHERE STM.sourceID IS NOT NULL'

		EXEC (@sql)

		--tblStandUpTrainingLanguage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblStandUpTrainingLanguage]' 
				+ ' ( '
				+ '		idSite,'
				+ '		idStandUpTraining,'
				+ '		idLanguage,'
				+ '		title,'
				+ '		description,'
				+ '		searchTags'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		ST.idStandUpTraining, '
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ', '
				+ '		ST.title, '
				+ '		ST.description, '
				+ '		ST.searchTags '
				+ ' FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblStandUpTraining] ST'
				+ ' WHERE ST.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
		
		EXEC(@sql)

		-- tblStandUpTrainingInstance
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblStandUpTrainingInstance]'
				+ ' ( '
				+ '		idStandUpTraining, '
				+ '		idSite, '
				+ '		title, '
				+ '		description, '
				+ '		seats, '
				+ '		waitingSeats, '
				+ '		urlRegistration, '
				+ '		urlAttend, '
				+ '		city, '
				+ '		province,'
				+ '		locationDescription,'
				+ '		type,'
				+ '		isDeleted,'
				+ '		dtDeleted,'
				+ '		integratedObjectKey,'
				+ '		hostUrl,'
				+ '		genericJoinUrl,'
				+ '		meetingPassword,'
				+ '		idWebMeetingOrganizer'
				+ ' ) '
				+ ' SELECT ' 
				+ '		STM.destinationID, '																				-- idStandupTraining
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','														-- idSite
				+ '		CONVERT(NVARCHAR, S.[datetime], 101) + '' ('' + CONVERT(NVARCHAR, S.[datetime], 108) + '' UTC)'' + '' ##'' + CONVERT(NVARCHAR, S.idSession) + ''##'','	-- title
				+ '		NULL,'																								-- description
				+ '		S.seats,'																							-- seats
				+ '		S.waitingSeats,'																					-- waitingSeats
				+ '		NULL,'																								-- urlRegistration
				+ '		S.url,'																								-- urlAttend
				+ '		S.city,'																							-- city
				+ '		S.province,'																						-- province
				+ '		S.locationDescription,'																				-- locationDescription
				+ '		CASE WHEN L.idLessonType = 1 THEN 1 ELSE 0 END,'													-- type
				+ '		0,'																									-- is deleted?
				+ '		NULL,'																								-- date deleted
				+ '		NULL,'																								-- integrated object key
				+ '		NULL,'																								-- host url
				+ '		NULL,'																								-- generic join url
				+ '		NULL,'																								-- meeting password
				+ '		NULL'																								-- web meeting organizer id
				+ '	FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblSession] S'
				+ '	LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLesson] L ON L.idLesson = S.idLesson'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCourse] C ON C.idCourse = L.idCourse'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] STM ON STM.sourceID = L.idLesson AND STM.object = ''ilt'''			
				+ ' WHERE C.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND STM.destinationID IS NOT NULL'
		
		EXEC (@sql)

		-- insert idStandupTrainingInstance mappings (idStandupTrainingInstance maps to Inquisiq idSession)
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SS.idSession, '
				+ '		DSTI.idStandupTrainingInstance,'
				+ '		''iltsession''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblStandupTrainingInstance] DSTI '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblSession] SS ON CONVERT(NVARCHAR, SS.[datetime], 101) + '' ('' + CONVERT(NVARCHAR, SS.[datetime], 108) + '' UTC)'' + '' ##'' + CONVERT(NVARCHAR, SS.idSession) + ''##'' = DSTI.title collate database_default'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLesson] SL ON SL.idLesson = SS.idLesson'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCourse] SC ON SC.idCourse = SL.idCourse AND SC.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)
				+ ' WHERE DSTI.idSite IS NOT NULL AND DSTI.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SS.idSession IS NOT NULL AND DSTI.idStandupTrainingInstance IS NOT NULL'
		
		EXEC(@sql)

		-- clean up the ##idSession## additions we made to standup training instance titles, we did that to uniquely distinguish names
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblStandupTrainingInstance] SET '
				+ '		title = REPLACE(DSTI.title, '' ##'' + CONVERT(NVARCHAR, STIM.sourceID) + ''##'', '''') '
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblStandupTrainingInstance] DSTI '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] STIM ON STIM.destinationID = DSTI.idStandupTrainingInstance AND STIM.object = ''iltsession'''
				+ ' WHERE STIM.sourceID IS NOT NULL'

		EXEC (@sql)

		-- tblStandupTrainingInstanceToInstructorLink
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblStandupTrainingInstanceToInstructorLink] '
				+ ' ( '
				+ '		idSite,'
				+ '		idStandupTrainingInstance,'
				+ '		idInstructor'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		STI.destinationID, '
				+ '		U.destinationID '
				+ ' FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].[tblSession] S '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] STI ON STI.sourceID = S.idSession AND STI.object = ''iltsession'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] U ON U.sourceID = S.idInstructor AND U.object = ''user'''
				+ ' WHERE S.idInstructor IS NOT NULL AND STI.destinationID IS NOT NULL AND U.destinationID IS NOT NULL'

		EXEC (@sql)

		-- tblStandUpTrainingInstanceMeetingTime
		SET @sql = 'INSERT INTO[' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblStandUpTrainingInstanceMeetingTime] '
			+ ' ( '
			+ '		idStandUpTrainingInstance,'
			+ '		idSite,'
			+ '		dtStart,'
			+ '		dtEnd,'
			+ '		minutes,'
			+ '		idTimezone'
			+ ' ) '
			+ ' SELECT '
			+ '		STI.destinationID,'
			+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
			+ '		S.datetime,'
			+ '		DATEADD(minute, S.minutes, S.datetime),'
			+ '		S.minutes,'
			+ '		TZ.destinationID'
			+ '	FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].[tblSession] S '
			+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] STI ON STI.sourceID = S.idSession AND STI.object = ''iltsession'''
			+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_TimezoneIDMappings] TZ ON TZ.sourceID = S.idTimezone '
			+ ' WHERE STI.destinationID IS NOT NULL'

			EXEC (@sql)
	   
		-- tblStandUpTrainingInstanceLanguage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblStandUpTrainingInstanceLanguage]' 
				+ ' ( '
				+ '		idSite,'
				+ '		idStandUpTrainingInstance,'
				+ '		idLanguage,'
				+ '		title,'
				+ '		description,'
				+ '		locationDescription'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		STI.idStandUpTrainingInstance, '
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ', '
				+ '		STI.title, '
				+ '		STI.description, '
				+ '		STI.locationDescription '
				+ ' FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblStandUpTrainingInstance] STI'
				+ ' WHERE STI.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
		
		EXEC(@sql)

	/******************************************************CATALOG********************************************************************/	   
		-- tblCatalog
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCatalog]'
				+ ' ( '
				+ '		idSite, '
				+ '		idParent, '
				+ '		title, '
				+ '		shortDescription, '
				+ '		longDescription, '
				+ '		isPrivate, '
				+ '		cost, '
				+ '		isClosed, '
				+ '		dtCreated,'
				+ '		dtModified,'
				+ '		costType,'
				+ '		searchTags'
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- idSite
				+ '		NULL,'											-- parent id (NULL for now)				
				+ '		C.name + '' ##'' + CONVERT(NVARCHAR, C.idCatalog) + ''##'','	-- title
				+ '		C.shortDescription,'							-- short description
				+ '		C.description,'									-- description
				+ '		C.isPrivate,'									-- is the catalog private
				+ '		CASE WHEN C.cost >= 1 THEN C.cost '				-- cost
				+ '			 WHEN C.cost >= 0.1 AND C.cost <= 0.9 THEN C.cost * 100 '
				+ '		ELSE 0 END, '									
				+ '		C.isClosed,'									-- is catalog closed?
				+ '		GETUTCDATE(),'									-- date created
				+ '		GETUTCDATE(),'									-- date modified
				+ '		CASE WHEN C.cost = 0 THEN 3'					-- cost type
				+ '			 WHEN C.cost >= 1 THEN 2'
				+ '			 WHEN C.cost >= 0.1 AND C.cost <= 0.9 THEN 4'
				+ '		ELSE 1 END,'
				+ '		NULL'											-- search tags
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCatalog] C'
				+ ' WHERE C.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' '
		
		EXEC (@sql)

		-- insert idCatalog mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SC.idCatalog, '
				+ '		DC.idCatalog,'
				+ '		''catalog''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCatalog] DC '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCatalog] SC ON SC.idSite = '+ CONVERT(NVARCHAR, @idSiteSource) + ' AND SC.name + '' ##'' + CONVERT(NVARCHAR, SC.idCatalog) + ''##'' = DC.title collate database_default'
				+ ' WHERE DC.idSite IS NOT NULL AND DC.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SC.idCatalog IS NOT NULL AND DC.idCatalog IS NOT NULL'
		
		EXEC(@sql)	

		-- update idParent for catalogs
		SET @sql = 'UPDATE DESTINATIONCATALOG SET'
				+ '		idParent = CM.destinationID'
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCatalog] SOURCECATALOG'
				+ ' LEFT JOIN  [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCatalog] DESTINATIONCATALOG ON SOURCECATALOG.name + '' ##'' + CONVERT(NVARCHAR, SOURCECATALOG.idCatalog) + ''##'' = DESTINATIONCATALOG.title collate database_default AND SOURCECATALOG.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)
				+ ' LEFT JOIN  [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.sourceID = SOURCECATALOG.idParent AND CM.object = ''catalog'''
				+ ' WHERE DESTINATIONCATALOG.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
				+ ' AND SOURCECATALOG.idParent IS NOT NULL'
	   
		EXEC(@sql)

		-- clean up the ##idCatalog## additions we made to catalog titles, we did that to uniquely distinguish names
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblCatalog] SET '
				+ '		title = REPLACE(DST.title, '' ##'' + CONVERT(NVARCHAR, SRC.sourceID) + ''##'', '''') '
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCatalog] DST '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] SRC ON SRC.destinationID = DST.idCatalog AND SRC.object = ''catalog'''
				+ ' WHERE SRC.sourceID IS NOT NULL'

		EXEC (@sql)

		-- tblCatalogLanguage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblCatalogLanguage]' 
				+ ' ( '
				+ '		idSite,'
				+ '		idCatalog,'
				+ '		idLanguage,'
				+ '		title,'
				+ '		shortDescription,'
				+ '		longDescription,'
				+ '		searchTags'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		C.idCatalog, '
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ', '
				+ '		C.title, '
				+ '		C.shortDescription, '
				+ '		C.longDescription, '
				+ '		C.searchTags '
				+ ' FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblCatalog] C'
				+ ' WHERE C.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
		
		EXEC(@sql)
		
		
		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemCopyInquisiqToAsentiaCourse_Success'

	SET XACT_ABORT OFF

END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.CopyInquisiqToAsentia.Enrollment]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.CopyInquisiqToAsentia.Enrollment]
GO


/*

Copy source site data from Inquisiq to destination site in Asentia

*/
CREATE PROCEDURE [dbo].[System.CopyInquisiqToAsentia.Enrollment]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,	
	@idAccount				INT,
	@idCaller				INT				= 1,
	@idSiteDefaultLang		INT				= 57,		-- English is ID 57	
	@siteHostname			NVARCHAR(255),
	@idSiteDestination		INT,
	@destinationHostname	NVARCHAR(255),
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@idSiteSource			INT,
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50)	
)
AS

BEGIN

    SET NOCOUNT ON
    SET XACT_ABORT ON    
    	
	DECLARE @sql NVARCHAR(MAX)


	/**********************************************ENROLLMENTS (WHERE WE DO NOT HAVE TO ONE-OFF)**********************************************/
		
		/**********ACTIVITY IMPORTED ENROLLMENTS***********/

		-- enrollments (instances only) that belong to an activity import
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblEnrollment]'
				+ ' ( '
				+ '		idSite, '
				+ '		idCourse, '
				+ '		idUser, '
				+ '		idGroupEnrollment, '
				+ '		idRuleSetEnrollment, '				
				+ '		idTimezone, '
				+ '		isLockedByPrerequisites, '
				+ '		code, '
				+ '		title, '
				+ '		dtStart, '
				+ '		dtDue, '
				+ '		dtExpiresFromStart, '				
				+ '		dtFirstLaunch, '
				+ '		dtCompleted, '
				+ '		dtCreated, '
				+ '		dtLastSynchronized, '
				+ '		dueInterval, '
				+ '		dueTimeframe, '
				+ '		expiresFromStartInterval, '
				+ '		expiresFromStartTimeframe, '				
				+ '		idActivityImport, '
				+ '		credits '
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','								-- idSite
				+ '		CM.destinationID,'															-- idCourse				
				+ '		UM.destinationID,'															-- idUser
				+ '		NULL,'																		-- idGroupEnrollment
				+ '		NULL,'																		-- idRulesetEnrollment
				+ '		CASE WHEN TZ.destinationID > 0 THEN TZ.destinationID ELSE 15 END,'			-- idTimezone
				+ '		0,'																			-- isLockedByPrerequisites
				+ '		EI.code,'																	-- code
				+ '		EI.name + '' ##'' + CONVERT(NVARCHAR, EI.idEnrollmentInstance) + ''##'','	-- title
				+ '		EI.dtStart,'																-- dtStart
				+ '		EI.dtDue,'																	-- dtDue
				+ '		EI.dtEnd,'																	-- dtExpiresFromStart
				+ '		EI.dtFirstLaunch,'															-- dtFirstLaunch
				+ '		EI.dtCompleted,'															-- dtCompleted
				+ '		EI.dtStart,'																-- dtCreated
				+ '		EI.dtLastSynchronized,'														-- dtLastSynchronized
				+ '		NULL,'																		-- dueInterval
				+ '		NULL,'																		-- dueTimeframe
				+ '		NULL,'																		-- expiresFromStartInterval
				+ '		NULL,'																		-- expiresFromStartTimeframe
				+ '		AIM.destinationID,'															-- idActivityImport
				+ '		EI.credits '																-- credits				
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblEnrollmentInstance] EI'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblUser] U ON U.idUser = EI.idUser'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.sourceID = EI.idCourse AND CM.object = ''course'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] UM ON UM.sourceID = EI.idUser AND UM.object = ''user'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] AIM ON AIM.sourceID = EI.idActivityImport AND AIM.object = ''activityimport'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_TimezoneIDMappings] TZ ON TZ.sourceID = EI.idTimezone '
				+ ' WHERE U.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)  + ' AND EI.idActivityImport IS NOT NULL AND UM.destinationID IS NOT NULL AND AIM.destinationID IS NOT NULL'
		
		EXEC (@sql)				

		/**********MANUAL SINGLE ENROLLMENTS**********/

		-- manual (self-assigned or admin-assigned) enrollments - recurrence will be taken away 		
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblEnrollment]'
				+ ' ( '
				+ '		idSite, '
				+ '		idCourse, '
				+ '		idUser, '
				+ '		idGroupEnrollment, '
				+ '		idRuleSetEnrollment, '				
				+ '		idTimezone, '
				+ '		isLockedByPrerequisites, '
				+ '		code, '
				+ '		title, '
				+ '		dtStart, '
				+ '		dtDue, '
				+ '		dtExpiresFromStart, '				
				+ '		dtFirstLaunch, '
				+ '		dtCompleted, '
				+ '		dtCreated, '
				+ '		dtLastSynchronized, '
				+ '		dueInterval, '
				+ '		dueTimeframe, '
				+ '		expiresFromStartInterval, '
				+ '		expiresFromStartTimeframe, '				
				+ '		idActivityImport, '
				+ '		credits '
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','													-- idSite
				+ '		CM.destinationID,'																				-- idCourse				
				+ '		UM.destinationID,'																				-- idUser
				+ '		NULL,'																							-- idGroupEnrollment
				+ '		NULL,'																							-- idRulesetEnrollment
				+ '		CASE WHEN TZ.destinationID > 0 THEN TZ.destinationID ELSE 15 END,'								-- idTimezone
				+ '		CASE WHEN E.isLockedForPrerequisites IS NOT NULL THEN E.isLockedForPrerequisites ELSE 0 END,'	-- isLockedByPrerequisites
				+ '		EI.code,'																						-- code
				+ '		EI.name + '' ##'' + CONVERT(NVARCHAR, EI.idEnrollmentInstance) + ''##'','						-- title
				+ '		EI.dtStart,'																					-- dtStart
				+ '		EI.dtDue,'																						-- dtDue
				+ '		EI.dtEnd,'																						-- dtExpiresFromStart
				+ '		EI.dtFirstLaunch,'																				-- dtFirstLaunch
				+ '		EI.dtCompleted,'																				-- dtCompleted
				+ '		EI.dtStart,'																					-- dtCreated
				+ '		EI.dtLastSynchronized,'																			-- dtLastSynchronized
				+ '		E.dueInterval,'																					-- dueInterval
				+ '		E.dueTimeframe,'																				-- dueTimeframe
				+ '		E.accessInterval,'																				-- expiresFromStartInterval
				+ '		E.accessTimeframe,'																				-- expiresFromStartTimeframe
				+ '		NULL,'																							-- idActivityImport
				+ '		EI.credits '																					-- credits				
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblEnrollmentInstance] EI'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblEnrollment] E ON E.idEnrollment = EI.idEnrollment'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblUser] U ON U.idUser = EI.idUser'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.sourceID = EI.idCourse AND CM.object = ''course'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] UM ON UM.sourceID = EI.idUser AND UM.object = ''user'''				
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_TimezoneIDMappings] TZ ON TZ.sourceID = EI.idTimezone '
				+ ' WHERE U.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)  + ' AND EI.idActivityImport IS NULL AND E.idGroupEnrollment IS NULL '
				+ ' AND CM.destinationID IS NOT NULL AND UM.destinationID IS NOT NULL'
		
		EXEC (@sql)

		/**********BUILD RULESET ENROLLMENTS FROM GROUP ENROLLMENTS **********/

		-- add ruleset enrollments using group enrollments from Inquisiq
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblRulesetEnrollment]'
				+ ' ( '
				+ '		idSite, '
				+ '		idCourse, '
				+ '		idTimezone, '
				+ '		priority, '
				+ '		label, '				
				+ '		isLockedByPrerequisites, '
				+ '		isFixedDate, '
				+ '		dtStart, '
				+ '		dtEnd, '
				+ '		dtCreated, '				
				+ '		delayInterval, '
				+ '		delayTimeframe, '
				+ '		dueInterval, '
				+ '		dueTimeframe, '
				+ '		recurInterval, '
				+ '		recurTimeframe, '
				+ '		expiresFromStartInterval, '
				+ '		expiresFromStartTimeframe, '				
				+ '		expiresFromFirstLaunchInterval, '
				+ '		expiresFromFirstLaunchTimeframe, '
				+ '		forceReassignment '
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','																-- idSite
				+ '		CM.destinationID,'																							-- idCourse				
				+ '		CASE WHEN TZ.destinationID > 0 THEN TZ.destinationID ELSE 15 END,'											-- idTimezone
				+ '		ROW_NUMBER() OVER(PARTITION BY CM.destinationID ORDER BY CM.destinationID),'								-- priority
				+ '		''START: '' + CONVERT(NVARCHAR, GE.dtStart, 101)'															-- label
				+ '		+ CASE WHEN GE.dueInterval IS NOT NULL THEN '' DUE: '' + CONVERT(NVARCHAR, GE.dueInterval) + GE.dueTimeframe ELSE '''' END'
				+ '		+ CASE WHEN GE.recurInterval IS NOT NULL THEN '' RECUR: '' + CONVERT(NVARCHAR, GE.recurInterval) + GE.recurTimeframe ELSE '''' END'
				+ '		+ CASE WHEN GE.accessInterval IS NOT NULL THEN '' ACCESS: '' + CONVERT(NVARCHAR, GE.accessInterval) + GE.accessTimeframe ELSE '''' END,'
				+ '		CASE WHEN GE.isLockedForPrerequisites IS NOT NULL THEN GE.isLockedForPrerequisites ELSE 0 END,'				-- isLockedByPrerequisites
				+ '		GE.useFixedDates,'																							-- isFixedDate
				+ '		GE.dtStart,'																								-- dtStart
				+ '		GE.dtEnd,'																									-- dtEnd
				+ '		GE.dtStart,'																								-- dtCreated
				+ '		GE.delayInterval,'																							-- delayInterval
				+ '		GE.delayTimeframe,'																							-- delayTimeframe				
				+ '		GE.dueInterval,'																							-- dueInterval
				+ '		GE.dueTimeframe,'																							-- dueTimeframe
				+ '		GE.recurInterval,'																							-- recurInterval
				+ '		GE.recurTimeframe,'																							-- recurTimeframe
				+ '		GE.accessInterval,'																							-- expiresFromStartInterval
				+ '		GE.accessTimeframe,'																						-- expiresFromStartTimeframe
				+ '		GE.idGroupEnrollment,'																						-- expiresFromFirstLaunchInterval (use as placeholder for mapping, put idGroupEnrollment in it)
				+ '		NULL, '																										-- expiresFromFirstLaunchTimeframe
				+ '		0 '																											-- forceReassignment
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblGroupEnrollment] GE'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCourse] C ON C.idCourse = GE.idCourse'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.sourceID = GE.idCourse AND CM.object = ''course'''						
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_TimezoneIDMappings] TZ ON TZ.sourceID = GE.idTimezone '
				+ ' WHERE C.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)  + ' AND CM.destinationID IS NOT NULL'
		
		EXEC (@sql)

		-- insert idRulesetEnrollment mappings (idRulesetEnrollment maps to Inquisiq idGroupEnrollment)
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SGE.idGroupEnrollment, '
				+ '		DRSE.idRulesetEnrollment,'
				+ '		''rulesetenrollment''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblRulesetEnrollment] DRSE '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblGroupEnrollment] SGE ON SGE.idGroupEnrollment = DRSE.expiresFromFirstLaunchInterval'				
				+ ' WHERE DRSE.idSite IS NOT NULL AND DRSE.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SGE.idGroupEnrollment IS NOT NULL AND DRSE.idRulesetEnrollment IS NOT NULL'
		
		EXEC(@sql)

		-- clean up the idGroupEnrollment additions we made to tblRulesetEnrollment, we did that to allow it to be mapped
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRulesetEnrollment] SET '
				+ '		expiresFromFirstLaunchInterval = NULL '
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblRulesetEnrollment] DRSE '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] SGEM ON SGEM.destinationID = DRSE.[idRulesetEnrollment] AND SGEM.object = ''rulesetenrollment'''
				+ ' WHERE SGEM.sourceID IS NOT NULL'

		EXEC (@sql)

		-- create rulesets for the rulesetenrollment to group mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRuleset] '
				+ ' ( '
				+ '		idSite,'
				+ '		isAny,'
				+ '		label'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','
				+ '		1,'
				+ '		''Member of Group: '' + G.name + '' ##'' + CONVERT(NVARCHAR, GE.idGroupEnrollment) + ''##'''
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblRulesetEnrollment] RSE '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] GEM ON GEM.destinationID = RSE.[idRulesetEnrollment] AND GEM.object = ''rulesetenrollment'''
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblGroupEnrollment] GE ON GE.idGroupEnrollment = GEM.sourceID'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblGroup] G ON G.idGroup = GE.idGroup'
				+ ' WHERE RSE.idSite IS NOT NULL AND RSE.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND GE.idGroupEnrollment IS NOT NULL AND RSE.idRulesetEnrollment IS NOT NULL'
		
		EXEC(@sql)

		-- create rules for the rulesetenrollment to group mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRule] '
				+ ' ( '
				+ '		idSite,'
				+ '		idRuleset,'
				+ '		userField,'
				+ '		operator,'
				+ '		textValue'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','
				+ '		RS.idRuleset,'
				+ '		''group'','
				+ '		''eq'','
				+ '		G.name'				
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblRulesetEnrollment] RSE '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] GEM ON GEM.destinationID = RSE.[idRulesetEnrollment] AND GEM.object = ''rulesetenrollment'''
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblGroupEnrollment] GE ON GE.idGroupEnrollment = GEM.sourceID'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblGroup] G ON G.idGroup = GE.idGroup'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRuleset] RS ON RS.label = ''Member of Group: '' + G.name + '' ##'' + CONVERT(NVARCHAR, GE.idGroupEnrollment) + ''##'' collate database_default'
				+ ' WHERE RSE.idSite IS NOT NULL AND RSE.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND GE.idGroupEnrollment IS NOT NULL AND RSE.idRulesetEnrollment IS NOT NULL AND RS.idRuleset IS NOT NULL'
		
		EXEC(@sql)

		-- link rulesets to rulesetenrollments
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRulesetToRulesetEnrollmentLink] '
				+ ' ( '
				+ '		idSite,'
				+ '		idRuleset,'
				+ '		idRulesetEnrollment'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','
				+ '		RS.idRuleset,'
				+ '		RSE.idRuleSetEnrollment'
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblRulesetEnrollment] RSE '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] GEM ON GEM.destinationID = RSE.[idRulesetEnrollment] AND GEM.object = ''rulesetenrollment'''
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblGroupEnrollment] GE ON GE.idGroupEnrollment = GEM.sourceID'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblGroup] G ON G.idGroup = GE.idGroup'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRuleset] RS ON RS.label = ''Member of Group: '' + G.name + '' ##'' + CONVERT(NVARCHAR, GE.idGroupEnrollment) + ''##'' collate database_default'
				+ ' WHERE RSE.idSite IS NOT NULL AND RSE.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND GE.idGroupEnrollment IS NOT NULL AND RSE.idRulesetEnrollment IS NOT NULL AND RS.idRuleset IS NOT NULL'
		
		EXEC(@sql)

		-- clean up the ##idGroupEnrollment## additions we made to ruleset labels, we did that to uniquely distinguish names
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRuleset] SET '
				+ '		label = REPLACE(DRS.label, '' ##'' + CONVERT(NVARCHAR, RSEM.sourceID) + ''##'', '''') '
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblRuleset] DRS '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRulesetToRulesetEnrollmentLink] RSEL ON RSEL.idRuleset = DRS.idRuleset'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] RSEM ON RSEM.destinationID = RSEL.idRulesetEnrollment AND RSEM.object = ''rulesetenrollment'''
				+ ' WHERE RSEM.sourceID IS NOT NULL'

		EXEC (@sql)

		-- tblRulesetEnrollmentLanguage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRulesetEnrollmentLanguage]' 
				+ ' ( '
				+ '		idSite,'
				+ '		idRulesetEnrollment,'
				+ '		idLanguage,'
				+ '		label'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		RSE.idRulesetEnrollment, '
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ', '
				+ '		RSE.label '				
				+ ' FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRulesetEnrollmentLanguage] RSE'
				+ ' WHERE RSE.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
		
		EXEC(@sql)

		-- tblRulesetLanguage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRulesetLanguage]' 
				+ ' ( '
				+ '		idSite,'
				+ '		idRuleset,'
				+ '		idLanguage,'
				+ '		label'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		RS.idRuleset, '
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ', '
				+ '		RS.label '				
				+ ' FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRuleset] RS'
				+ ' WHERE RS.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
		
		EXEC(@sql)

		/**********RULESET ENROLLMENTS CASCADED**********/

		-- group enrollments from Inquisiq that were cascaded get mapped as cascaded ruleset enrollments in Asentia
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblEnrollment]'
				+ ' ( '
				+ '		idSite, '
				+ '		idCourse, '
				+ '		idUser, '
				+ '		idGroupEnrollment, '
				+ '		idRuleSetEnrollment, '				
				+ '		idTimezone, '
				+ '		isLockedByPrerequisites, '
				+ '		code, '
				+ '		title, '
				+ '		dtStart, '
				+ '		dtDue, '
				+ '		dtExpiresFromStart, '				
				+ '		dtFirstLaunch, '
				+ '		dtCompleted, '
				+ '		dtCreated, '
				+ '		dtLastSynchronized, '
				+ '		dueInterval, '
				+ '		dueTimeframe, '
				+ '		expiresFromStartInterval, '
				+ '		expiresFromStartTimeframe, '				
				+ '		idActivityImport, '
				+ '		credits '
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','													-- idSite
				+ '		CM.destinationID,'																				-- idCourse				
				+ '		UM.destinationID,'																				-- idUser
				+ '		NULL,'																							-- idGroupEnrollment
				+ '		RSEM.destinationID,'																			-- idRulesetEnrollment
				+ '		CASE WHEN TZ.destinationID > 0 THEN TZ.destinationID ELSE 15 END,'								-- idTimezone
				+ '		CASE WHEN E.isLockedForPrerequisites IS NOT NULL THEN E.isLockedForPrerequisites ELSE 0 END,'	-- isLockedByPrerequisites
				+ '		EI.code,'																						-- code
				+ '		EI.name + '' ##'' + CONVERT(NVARCHAR, EI.idEnrollmentInstance) + ''##'','						-- title
				+ '		EI.dtStart,'																					-- dtStart
				+ '		EI.dtDue,'																						-- dtDue
				+ '		EI.dtEnd,'																						-- dtExpiresFromStart
				+ '		EI.dtFirstLaunch,'																				-- dtFirstLaunch
				+ '		EI.dtCompleted,'																				-- dtCompleted
				+ '		EI.dtStart,'																					-- dtCreated
				+ '		EI.dtLastSynchronized,'																			-- dtLastSynchronized
				+ '		E.dueInterval,'																					-- dueInterval
				+ '		E.dueTimeframe,'																				-- dueTimeframe
				+ '		E.accessInterval,'																				-- expiresFromStartInterval
				+ '		E.accessTimeframe,'																				-- expiresFromStartTimeframe
				+ '		NULL,'																							-- idActivityImport
				+ '		EI.credits '																					-- credits				
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblEnrollmentInstance] EI'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblEnrollment] E ON E.idEnrollment = EI.idEnrollment'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblUser] U ON U.idUser = EI.idUser'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.sourceID = EI.idCourse AND CM.object = ''course'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] UM ON UM.sourceID = EI.idUser AND UM.object = ''user'''				
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] RSEM ON RSEM.sourceID = E.idGroupEnrollment AND RSEM.object = ''rulesetenrollment'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_TimezoneIDMappings] TZ ON TZ.sourceID = EI.idTimezone '
				+ ' WHERE U.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)  + ' AND EI.idActivityImport IS NULL AND E.idGroupEnrollment IS NOT NULL '
				+ ' AND CM.destinationID IS NOT NULL AND UM.destinationID IS NOT NULL AND RSEM.destinationID IS NOT NULL'
		
		EXEC (@sql)
		
		/**********LESSON DATA AND SCORM INTERACTIONS ASSOCIATED WITH ENROLLMENTS**********/

		-- insert idEnrollment mappings (idEnrollment maps to Inquisiq idEnrollmentInstance)
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SEI.idEnrollmentInstance, '
				+ '		DE.idEnrollment,'
				+ '		''enrollment''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblEnrollment] DE '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblEnrollmentInstance] SEI ON SEI.name + '' ##'' + CONVERT(NVARCHAR, SEI.idEnrollmentInstance) + ''##'' = DE.title collate database_default'
				+ ' WHERE DE.idSite IS NOT NULL AND DE.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SEI.idEnrollmentInstance IS NOT NULL AND DE.idEnrollment IS NOT NULL'
		
		EXEC(@sql)

		-- clean up the ##idEnrollmentInstance## additions we made to enrollment titles, we did that to uniquely distinguish names
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblEnrollment] SET '
				+ '		title = REPLACE(DE.title, '' ##'' + CONVERT(NVARCHAR, SEIM.sourceID) + ''##'', '''') '
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblEnrollment] DE '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] SEIM ON SEIM.destinationID = DE.idEnrollment AND SEIM.object = ''enrollment'''
				+ ' WHERE SEIM.sourceID IS NOT NULL'

		EXEC (@sql)
		
		-- lesson data
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblData-Lesson]'
				+ ' ( '
				+ '		idSite, '
				+ '		idEnrollment, '
				+ '		idLesson, '
				+ '		title, '
				+ '		revcode, '
				+ '		[order], '
				+ '		dtCompleted, '
				+ '		idTimezone, '
				+ '		contentTypeCommittedTo, '
				+ '		dtCommittedToContentType, '
				+ '		resetForContentChange, '
				+ '		preventPostCompletionLaunchForContentChange '				
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','											-- idSite
				+ '		EM.destinationID,'																		-- idEnrollment				
				+ '		LM.destinationID,'																		-- idLesson
				+ '		LD.lessonName + '' ##'' + CONVERT(NVARCHAR, LD.idLessonData) + ''##'','					-- title
				+ '		NULL,'																					-- revcode
				+ '		LD.[order],'																			-- order
				+ '		CASE WHEN LD.completionStatus = ''completed'' OR LD.successStatus = ''passed'' THEN'	-- dtCompleted
				+ '			LD.[timestamp] '
				+ '		ELSE NULL END,'
				+ '		E.idTimezone,'																			-- idTimezone
				+ '		NULL,'																					-- contentTypeCommittedTo
				+ '		NULL,'																					-- dtCommittedToContentType
				+ '		NULL,'																					-- resetForContentChange
				+ '		NULL'																					-- preventPostCompletionLaunchForContentChange						
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLessonData] LD'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblEnrollmentInstance] EI ON EI.idEnrollmentInstance = LD.idEnrollmentInstance'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblUser] U ON U.idUser = EI.idUser'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] EM ON EM.sourceID = EI.idEnrollmentInstance AND EM.object = ''enrollment'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] LM ON LM.sourceID = LD.idLesson AND LM.object = ''lesson'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblEnrollment] E ON E.idEnrollment = EM.destinationID'				
				+ ' WHERE U.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)  + ' AND EM.destinationID IS NOT NULL AND E.idEnrollment IS NOT NULL'
		
		EXEC (@sql)

		-- insert idData-Lesson mappings (idData-Lesson maps to Inquisiq idLessonData)
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SLD.idLessonData, '
				+ '		DDL.[idData-Lesson],'
				+ '		''lessondata''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblData-Lesson] DDL '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLessonData] SLD ON SLD.lessonName + '' ##'' + CONVERT(NVARCHAR, SLD.idLessonData) + ''##'' = DDL.title collate database_default'				
				+ ' WHERE DDL.idSite IS NOT NULL AND DDL.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SLD.idLessonData IS NOT NULL AND DDL.[idData-Lesson] IS NOT NULL'
		
		EXEC(@sql)

		-- clean up the ##idLessonData## additions we made to lesson data titles, we did that to uniquely distinguish names
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblData-Lesson] SET '
				+ '		title = REPLACE(DDL.title, '' ##'' + CONVERT(NVARCHAR, SLDM.sourceID) + ''##'', '''') '
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblData-Lesson] DDL '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] SLDM ON SLDM.destinationID = DDL.[idData-Lesson] AND SLDM.object = ''lessondata'''
				+ ' WHERE SLDM.sourceID IS NOT NULL'

		EXEC (@sql)
		
		-- lesson data sco
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblData-SCO]'
				+ ' ( '
				+ '		idSite, '
				+ '		[idData-Lesson], '
				+ '		manifestIdentifier, '
				+ '		completionStatus, '
				+ '		successStatus, '
				+ '		scoreScaled, '
				+ '		totalTime, '
				+ '		[timestamp], '
				+ '		idTimezone, '
				+ '		actualAttemptCount, '
				+ '		effectiveAttemptCount, '
				+ '		proctoringUser '
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','									-- idSite
				+ '		LDM.destinationID,'																-- [idData-Lesson]
				+ '		''##'' + CONVERT(NVARCHAR, LD.idLessonData) + ''##'','							-- use manifest identifier as a mapping plceholder
				+ '		CASE WHEN CS.idSCORMVocabulary IS NULL THEN 0 ELSE CS.idSCORMVocabulary END,'	-- completionStatus
				+ '		CASE WHEN SS.idSCORMVocabulary IS NULL THEN 0 ELSE SS.idSCORMVocabulary END,'	-- successStatus
				+ '		LD.scoreScaled,'																-- scoreScaled
				+ '		LD.totalTime,'																	-- totalTime
				+ '		LD.[timestamp],'																-- timestamp
				+ '		DL.idTimezone,'																	-- idTimezone
				+ '		LD.actualAttempts,'																-- actualAttemptCount
				+ '		LD.effectiveAttempts,'															-- effectiveAttemptCount
				+ '		NULL'																			-- proctoringUser
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLessonData] LD'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblEnrollmentInstance] EI ON EI.idEnrollmentInstance = LD.idEnrollmentInstance'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblUser] U ON U.idUser = EI.idUser'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] LDM ON LDM.sourceID = LD.idLessonData AND LDM.object = ''lessondata'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblData-Lesson] DL ON DL.[idData-Lesson] = LDM.destinationID'
				+ ' LEFT JOIN [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblSCORMVocabulary] CS ON CS.value = LD.completionStatus'
				+ ' LEFT JOIN [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblSCORMVocabulary] SS ON SS.value = LD.successStatus'
				+ ' WHERE U.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)  + ' AND LDM.destinationID IS NOT NULL'
		
		EXEC (@sql)

		-- insert idData-SCO mappings (idData-SCO maps to Inquisiq idLessonData)
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SLD.idLessonData, '
				+ '		DDSCO.[idData-SCO],'
				+ '		''lessondatasco''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblData-SCO] DDSCO '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLessonData] SLD ON ''##'' + CONVERT(NVARCHAR, SLD.idLessonData) + ''##'' = DDSCO.manifestIdentifier collate database_default'
				+ ' WHERE DDSCO.idSite IS NOT NULL AND DDSCO.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SLD.idLessonData IS NOT NULL AND DDSCO.[idData-SCO] IS NOT NULL'
		
		EXEC(@sql)

		-- clean up the ##idLessonData## additions we made to data sco manifestIdentifier, we did that to uniquely distinguish names
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblData-SCO] SET '
				+ '		manifestIdentifier = NULL '
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblData-SCO] DDSCO '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] SLDM ON SLDM.destinationID = DDSCO.[idData-Lesson] AND SLDM.object = ''lessondatasco'''
				+ ' WHERE SLDM.sourceID IS NOT NULL'

		EXEC (@sql)
		
		-- scorm interactions
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblData-SCOInt]'
				+ ' ( '
				+ '		idSite, '
				+ '		[idData-SCO], '
				+ '		identifier, '
				+ '		timestamp, '
				+ '		result, '
				+ '		latency, '
				+ '		scoIdentifier '				
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','											-- idSite
				+ '		DSCOM.destinationID,'																	-- [idData-SCO]
				+ '		SI.identifier,'																			-- identifier
				+ '		SI.timestamp,'																			-- timestamp
				+ '		CASE WHEN SV.idSCORMVocabulary IS NOT NULL THEN SV.idSCORMVocabulary ELSE NULL END,'	-- result
				+ '		SI.latency,'																			-- latency
				+ '		NULL'																					-- scoIdentifier
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblScormInt] SI'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLessonData] LD ON LD.idLessonData = SI.idLessonData'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblEnrollmentInstance] EI ON EI.idEnrollmentInstance = LD.idEnrollmentInstance'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblUser] U ON U.idUser = EI.idUser'				
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] DSCOM ON DSCOM.sourceID = SI.idLessonData AND DSCOM.object = ''lessondatasco'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblSCORMVocabulary] SV ON SV.value = SI.result'
				+ ' WHERE U.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)  + ' AND DSCOM.destinationID IS NOT NULL AND DSCOM.destinationID IN (SELECT [idData-SCO] FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblData-SCO])'
		
		EXEC (@sql)

	/***************************************************ARCHIVED ACTIVITY DATA*****************************************************************/

	-- archived enrollments
	SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblEnrollment]'
				+ ' ( '
				+ '		idSite, '
				+ '		idCourse, '
				+ '		idUser, '
				+ '		idGroupEnrollment, '
				+ '		idRuleSetEnrollment, '				
				+ '		idTimezone, '
				+ '		isLockedByPrerequisites, '
				+ '		code, '
				+ '		title, '
				+ '		dtStart, '
				+ '		dtDue, '
				+ '		dtExpiresFromStart, '				
				+ '		dtFirstLaunch, '
				+ '		dtCompleted, '
				+ '		dtCreated, '
				+ '		dtLastSynchronized, '
				+ '		dueInterval, '
				+ '		dueTimeframe, '
				+ '		expiresFromStartInterval, '
				+ '		expiresFromStartTimeframe, '				
				+ '		idActivityImport, '
				+ '		credits '
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','													-- idSite
				+ '		CM.destinationID,'																				-- idCourse				
				+ '		UM.destinationID,'																				-- idUser
				+ '		NULL,'																							-- idGroupEnrollment
				+ '		NULL,'																							-- idRulesetEnrollment
				+ '		CASE WHEN TZ.destinationID > 0 THEN TZ.destinationID ELSE 15 END,'								-- idTimezone
				+ '		CASE WHEN E.isLockedForPrerequisites IS NOT NULL THEN E.isLockedForPrerequisites ELSE 0 END,'	-- isLockedByPrerequisites
				+ '		EI.code,'																						-- code
				+ '		EI.name + '' ##'' + CONVERT(NVARCHAR, EI.idEnrollmentInstance) + ''##'','						-- title
				+ '		EI.dtStart,'																					-- dtStart
				+ '		EI.dtDue,'																						-- dtDue
				+ '		EI.dtEnd,'																						-- dtExpiresFromStart
				+ '		EI.dtFirstLaunch,'																				-- dtFirstLaunch
				+ '		EI.dtCompleted,'																				-- dtCompleted
				+ '		EI.dtStart,'																					-- dtCreated
				+ '		EI.dtLastSynchronized,'																			-- dtLastSynchronized
				+ '		E.dueInterval,'																					-- dueInterval
				+ '		E.dueTimeframe,'																				-- dueTimeframe
				+ '		E.accessInterval,'																				-- expiresFromStartInterval
				+ '		E.accessTimeframe,'																				-- expiresFromStartTimeframe
				+ '		NULL,'																							-- idActivityImport
				+ '		EI.credits '																					-- credits				
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblARCHIVEEnrollmentInstance] EI'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblEnrollment] E ON E.idEnrollment = EI.idEnrollment'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblUser] U ON U.idUser = EI.idUser'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.sourceID = EI.idCourse AND CM.object = ''course'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] UM ON UM.sourceID = EI.idUser AND UM.object = ''user'''				
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_TimezoneIDMappings] TZ ON TZ.sourceID = EI.idTimezone '
				+ ' WHERE U.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)  + ' AND EI.idActivityImport IS NULL AND E.idGroupEnrollment IS NULL '
				+ ' AND CM.destinationID IS NOT NULL AND UM.destinationID IS NOT NULL'
		
		EXEC (@sql)

		-- insert idEnrollment mappings (idEnrollment maps to Inquisiq idEnrollmentInstance)
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SEI.idEnrollmentInstance, '
				+ '		DE.idEnrollment,'
				+ '		''enrollment''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblEnrollment] DE '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblARCHIVEEnrollmentInstance] SEI ON SEI.name + '' ##'' + CONVERT(NVARCHAR, SEI.idEnrollmentInstance) + ''##'' = DE.title collate database_default'
				+ ' WHERE DE.idSite IS NOT NULL AND DE.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SEI.idEnrollmentInstance IS NOT NULL AND DE.idEnrollment IS NOT NULL'
		
		EXEC(@sql)

		-- clean up the ##idEnrollmentInstance## additions we made to enrollment titles, we did that to uniquely distinguish names
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblEnrollment] SET '
				+ '		title = REPLACE(DE.title, '' ##'' + CONVERT(NVARCHAR, SEIM.sourceID) + ''##'', '''') '
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblEnrollment] DE '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] SEIM ON SEIM.destinationID = DE.idEnrollment AND SEIM.object = ''enrollment'''
				+ ' WHERE SEIM.sourceID IS NOT NULL'

		EXEC (@sql)
		
		-- lesson data
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblData-Lesson]'
				+ ' ( '
				+ '		idSite, '
				+ '		idEnrollment, '
				+ '		idLesson, '
				+ '		title, '
				+ '		revcode, '
				+ '		[order], '
				+ '		dtCompleted, '
				+ '		idTimezone, '
				+ '		contentTypeCommittedTo, '
				+ '		dtCommittedToContentType, '
				+ '		resetForContentChange, '
				+ '		preventPostCompletionLaunchForContentChange '				
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','											-- idSite
				+ '		EM.destinationID,'																		-- idEnrollment				
				+ '		LM.destinationID,'																		-- idLesson
				+ '		LD.lessonName + '' ##'' + CONVERT(NVARCHAR, LD.idARCHIVELessonData) + ''##'','					-- title
				+ '		NULL,'																					-- revcode
				+ '		LD.[order],'																			-- order
				+ '		CASE WHEN LD.completionStatus = ''completed'' OR LD.successStatus = ''passed'' THEN'	-- dtCompleted
				+ '			LD.[timestamp] '
				+ '		ELSE NULL END,'
				+ '		E.idTimezone,'																			-- idTimezone
				+ '		NULL,'																					-- contentTypeCommittedTo
				+ '		NULL,'																					-- dtCommittedToContentType
				+ '		NULL,'																					-- resetForContentChange
				+ '		NULL'																					-- preventPostCompletionLaunchForContentChange						
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblARCHIVELessonData] LD'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblARCHIVEEnrollmentInstance] EI ON EI.idEnrollmentInstance = LD.idEnrollmentInstance'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblUser] U ON U.idUser = EI.idUser'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] EM ON EM.sourceID = EI.idEnrollmentInstance AND EM.object = ''enrollment'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] LM ON LM.sourceID = LD.idLesson AND LM.object = ''lesson'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblEnrollment] E ON E.idEnrollment = EM.destinationID'				
				+ ' WHERE U.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)  + ' AND EM.destinationID IS NOT NULL AND E.idEnrollment IS NOT NULL'
		
		EXEC (@sql)

		-- insert idData-Lesson mappings (idData-Lesson maps to Inquisiq idLessonData)
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SLD.idLessonData, '
				+ '		DDL.[idData-Lesson],'
				+ '		''lessondata''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblData-Lesson] DDL '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblARCHIVELessonData] SLD ON SLD.lessonName + '' ##'' + CONVERT(NVARCHAR, SLD.idARCHIVELessonData) + ''##'' = DDL.title collate database_default'				
				+ ' WHERE DDL.idSite IS NOT NULL AND DDL.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SLD.idLessonData IS NOT NULL AND DDL.[idData-Lesson] IS NOT NULL'
		
		EXEC(@sql)

		-- clean up the ##idLessonData## additions we made to lesson data titles, we did that to uniquely distinguish names
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblData-Lesson] SET '
				+ '		title = REPLACE(DDL.title, '' ##'' + CONVERT(NVARCHAR, SLDM.sourceID) + ''##'', '''') '
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblData-Lesson] DDL '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] SLDM ON SLDM.destinationID = DDL.[idData-Lesson] AND SLDM.object = ''lessondata'''
				+ ' WHERE SLDM.sourceID IS NOT NULL'

		EXEC (@sql)
		
		-- lesson data sco
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblData-SCO]'
				+ ' ( '
				+ '		idSite, '
				+ '		[idData-Lesson], '
				+ '		manifestIdentifier, '
				+ '		completionStatus, '
				+ '		successStatus, '
				+ '		scoreScaled, '
				+ '		totalTime, '
				+ '		[timestamp], '
				+ '		idTimezone, '
				+ '		actualAttemptCount, '
				+ '		effectiveAttemptCount, '
				+ '		proctoringUser '
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','									-- idSite
				+ '		LDM.destinationID,'																-- [idData-Lesson]
				+ '		''##'' + CONVERT(NVARCHAR, LD.idLessonData) + ''##'','							-- use manifest identifier as a mapping plceholder
				+ '		CASE WHEN CS.idSCORMVocabulary IS NULL THEN 0 ELSE CS.idSCORMVocabulary END,'	-- completionStatus
				+ '		CASE WHEN SS.idSCORMVocabulary IS NULL THEN 0 ELSE SS.idSCORMVocabulary END,'	-- successStatus
				+ '		LD.scoreScaled,'																-- scoreScaled
				+ '		LD.totalTime,'																	-- totalTime
				+ '		LD.[timestamp],'																-- timestamp
				+ '		DL.idTimezone,'																	-- idTimezone
				+ '		LD.actualAttempts,'																-- actualAttemptCount
				+ '		LD.effectiveAttempts,'															-- effectiveAttemptCount
				+ '		NULL'																			-- proctoringUser
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblARCHIVELessonData] LD'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblARCHIVEEnrollmentInstance] EI ON EI.idEnrollmentInstance = LD.idEnrollmentInstance'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblUser] U ON U.idUser = EI.idUser'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] LDM ON LDM.sourceID = LD.idLessonData AND LDM.object = ''lessondata'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblData-Lesson] DL ON DL.[idData-Lesson] = LDM.destinationID'
				+ ' LEFT JOIN [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblSCORMVocabulary] CS ON CS.value = LD.completionStatus'
				+ ' LEFT JOIN [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblSCORMVocabulary] SS ON SS.value = LD.successStatus'
				+ ' WHERE U.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)  + ' AND LDM.destinationID IS NOT NULL'
		
		EXEC (@sql)

		-- insert idData-SCO mappings (idData-SCO maps to Inquisiq idLessonData)
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SLD.idLessonData, '
				+ '		DDSCO.[idData-SCO],'
				+ '		''lessondatasco''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblData-SCO] DDSCO '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblARCHIVELessonData] SLD ON ''##'' + CONVERT(NVARCHAR, SLD.idLessonData) + ''##'' = DDSCO.manifestIdentifier collate database_default'
				+ ' WHERE DDSCO.idSite IS NOT NULL AND DDSCO.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SLD.idLessonData IS NOT NULL AND DDSCO.[idData-SCO] IS NOT NULL'
		
		EXEC(@sql)

		-- clean up the ##idLessonData## additions we made to data sco manifestIdentifier, we did that to uniquely distinguish names
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblData-SCO] SET '
				+ '		manifestIdentifier = NULL '
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblData-SCO] DDSCO '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] SLDM ON SLDM.destinationID = DDSCO.[idData-Lesson] AND SLDM.object = ''lessondatasco'''
				+ ' WHERE SLDM.sourceID IS NOT NULL'

		EXEC (@sql)
		
		-- scorm interactions
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblData-SCOInt]'
				+ ' ( '
				+ '		idSite, '
				+ '		[idData-SCO], '
				+ '		identifier, '
				+ '		timestamp, '
				+ '		result, '
				+ '		latency, '
				+ '		scoIdentifier '				
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','											-- idSite
				+ '		DSCOM.destinationID,'																	-- [idData-SCO]
				+ '		SI.identifier,'																			-- identifier
				+ '		SI.timestamp,'																			-- timestamp
				+ '		CASE WHEN SV.idSCORMVocabulary IS NOT NULL THEN SV.idSCORMVocabulary ELSE NULL END,'	-- result
				+ '		SI.latency,'																			-- latency
				+ '		NULL'																					-- scoIdentifier
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblARCHIVEScormInt] SI'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblARCHIVELessonData] LD ON LD.idLessonData = SI.idLessonData'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblARCHIVEEnrollmentInstance] EI ON EI.idEnrollmentInstance = LD.idEnrollmentInstance'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblUser] U ON U.idUser = EI.idUser'				
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] DSCOM ON DSCOM.sourceID = SI.idLessonData AND DSCOM.object = ''lessondatasco'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblSCORMVocabulary] SV ON SV.value = SI.result'
				+ ' WHERE U.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)  + ' AND DSCOM.destinationID IS NOT NULL AND DSCOM.destinationID IN (SELECT [idData-SCO] FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblData-SCO])'
		
		EXEC (@sql)
		
	/****************************************************INSERT HOSTNAME INTO ACCOUNT DB******************************************************/

		SET @sql = 'INSERT INTO [tblAccountToDomainAliasLink]'
				+ ' ( '
				+ '		idAccount, '
				+ '		hostname '			
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idAccount) + ','	-- idAccount
				+ '		''' + @destinationHostname + ''''		-- hostname
		
		EXEC (@sql)

		
		  
		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemCopyInquisiqToAsentiaEnrollment_Success'

		
		
	SET XACT_ABORT OFF

END

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.CopyInquisiqToAsentia.LinkingTables]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.CopyInquisiqToAsentia.LinkingTables]
GO


/*

Copy source site data from Inquisiq to destination site in Asentia

*/
CREATE PROCEDURE [dbo].[System.CopyInquisiqToAsentia.LinkingTables]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,	
	@idAccount				INT,
	@idCaller				INT				= 1,
	@idSiteDefaultLang		INT				= 57,		-- English is ID 57	
	@siteHostname			NVARCHAR(255),
	@idSiteDestination		INT,
	@destinationHostname	NVARCHAR(255),
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@idSiteSource			INT,
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50)	
)
AS

BEGIN

    SET NOCOUNT ON
    SET XACT_ABORT ON    
    	
	DECLARE @sql NVARCHAR(MAX)

	/*****************************************************LINKING TABLES****************************************************************/
		-- tblCourseToCatalogLink
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCourseToCatalogLink]'
				+ ' ( '
				+ '		idSite, '
				+ '		idCourse, '
				+ '		idCatalog '
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- idSite
				+ '		COM.destinationID,'								-- idCourse
				+ '		CAM.destinationID'								-- idCatalog
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCourseCatalogLink] CCL'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] COM ON COM.sourceID = CCL.idCourse AND COM.object = ''course'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CAM ON CAM.sourceID = CCL.idCatalog AND CAM.object = ''catalog'''
				+ ' WHERE COM.destinationID IS NOT NULL AND CAM.destinationID IS NOT NULL'
		
		EXEC (@sql)

		-- tblCatalogAccessToGroupLink
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCatalogAccessToGroupLink]'
				+ ' ( '
				+ '		idSite, '
				+ '		idGroup, '
				+ '		idCatalog, '
				+ '		created '
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- idSite
				+ '		GRM.destinationID,'								-- idGroup
				+ '		CAM.destinationID,'								-- idCatalog
				+ '		GETUTCDATE()'									-- created
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblGroupRight] GR'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] GRM ON GRM.sourceID = GR.idGroup AND GRM.object = ''group'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CAM ON CAM.sourceID = GR.idScope AND CAM.object = ''catalog'''
				+ ' WHERE CAM.destinationID IS NOT NULL AND CAM.destinationID IS NOT NULL'
		
		EXEC (@sql)
	   
		-- tblCourseToPrerequisiteLink
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCourseToPrerequisiteLink]'
				+ ' ( '
				+ '		idSite, '
				+ '		idCourse, '
				+ '		idPrerequisite '
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- idSite
				+ '		CM.destinationID,'								-- idCourse
				+ '		PM.destinationID'								-- idPrerequisite
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCoursePrerequisiteLink] CPL'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.sourceID = CPL.idCourse AND CM.object = ''course'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] PM ON PM.sourceID = CPL.idPrerequisite AND PM.object = ''course'''
				+ ' WHERE CM.destinationID IS NOT NULL AND PM.destinationID IS NOT NULL'
		
		EXEC (@sql)
		
		-- tblCouponCodeToCourseLink
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCouponCodeToCourseLink]'
				+ ' ( '
				+ '		idCouponCode, '
				+ '		idCourse '
				+ ' ) '
				+ ' SELECT ' 
				+ '		CC.destinationID,'	-- idCouponCode
				+ '		C.destinationID'	-- idCourse
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCouponCodeCourseLink] CCCL'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CC ON CC.sourceID = CCCL.idCouponCode AND CC.object = ''couponcode'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] C ON C.sourceID = CCCL.idCourse AND C.object = ''course'''
				+ ' WHERE CC.destinationID IS NOT NULL AND C.destinationID IS NOT NULL'
		
		EXEC (@sql)

		-- tblCouponCodeToCatalogLink
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCouponCodeToCatalogLink]'
				+ ' ( '
				+ '		idCouponCode, '
				+ '		idCatalog '
				+ ' ) '
				+ ' SELECT ' 
				+ '		CC.destinationID,'	-- idCouponCode
				+ '		C.destinationID'	-- idCatalog
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCouponCodeCatalogLink] CCCL'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CC ON CC.sourceID = CCCL.idCouponCode AND CC.object = ''couponcode'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] C ON C.sourceID = CCCL.idCatalog AND C.object = ''catalog'''
				+ ' WHERE CC.destinationID IS NOT NULL AND C.destinationID IS NOT NULL'
		
		EXEC (@sql)

		-- tblCourseToScreenshotLink
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCourseToScreenshotLink]'
				+ ' ( '
				+ '		idSite, '
				+ '		idCourse, '
				+ '		filename '
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '	-- idSite
				+ '		CM.destinationID, '									-- idCourse
				+ '		SS.s '												-- filename
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCourse] C'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.sourceID = C.idCourse AND CM.object = ''course'''
				+ ' CROSS APPLY (SELECT s FROM [dbo].[DelimitedStringToTable](C.sampleScreens, ''|'') WHERE C.sampleScreens IS NOT NULL AND C.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ') SS'
		
		EXEC (@sql)
	   
		-- tblLessonToContentLink - link SCORM lesson types to content packages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblLessonToContentLink]'
				+ ' ( '
				+ '		idSite, '
				+ '		idLesson, '
				+ '		idObject, '
				+ '		idContentType, '
				+ '		idAssignmentDocumentType, '
				+ '		allowSupervisorsAsProctor, '
				+ '		allowCourseExpertsAsProctor'
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '	-- idSite
				+ '		LM.destinationID, '									-- idLesson
				+ '		CPM.destinationID, '								-- idObject
				+ '		1, '												-- idContentType (1 = content package)
				+ '		NULL, '												-- idAssignmentDocumentType
				+ '		NULL, '												-- allow supervisors as proctor?
				+ '		NULL '												-- allow course experts as proctor?
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLesson] L'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] LM ON LM.sourceID = L.idLesson AND LM.object = ''lesson'''
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblSCORMResource] SR ON SR.idSCORMResource = L.idSCORMResource'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblSCORMPackage] SP ON SP.idSCORMPackage = SR.idSCORMPackage'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CPM ON CPM.sourceID = SP.idSCORMPackage AND CPM.object = ''contentpackage'''
				+ ' WHERE L.idLessonType = 0 AND LM.destinationID IS NOT NULL AND CPM.destinationID IS NOT NULL'
		
		EXEC (@sql)

		-- tblLessonToContentLink - link Classroom/Web Meeting lesson types to ILT modules
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblLessonToContentLink]'
				+ ' ( '
				+ '		idSite, '
				+ '		idLesson, '
				+ '		idObject, '
				+ '		idContentType, '
				+ '		idAssignmentDocumentType, '
				+ '		allowSupervisorsAsProctor, '
				+ '		allowCourseExpertsAsProctor'
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) +', '	-- idSite
				+ '		LM.destinationID, '								-- idLesson
				+ '		ILTM.destinationID, '							-- idObject
				+ '		2, '											-- idContentType (2 = ILT)
				+ '		NULL, '											-- idAssignmentDocumentType
				+ '		NULL, '											-- allow supervisors as proctor?
				+ '		NULL '											-- allow course experts as proctor?
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLesson] L'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] LM ON LM.sourceID = L.idLesson AND LM.object = ''lesson'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] ILTM ON ILTM.sourceID = L.idLesson AND ILTM.object = ''ilt'''
				+ ' WHERE L.idLessonType IN (1, 2) AND LM.destinationID IS NOT NULL AND ILTM.destinationID IS NOT NULL'
		
		EXEC (@sql)

		-- tblUserToGroupLink -- make all user to group links seem as if the user were manually placed into the group,
							  -- post conversion, after group rules have run, we will need to clean up the "manual" entries
							  -- left over where the user gets a group entry by rule
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblUserToGroupLink]'
				+ ' ( '
				+ '		idSite, '
				+ '		idUser, '
				+ '		idGroup, '
				+ '		idRuleSet, '
				+ '		created, '
				+ '		selfJoined '
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- idSite
				+ '		UM.destinationID,'								-- idUser
				+ '		GM.destinationID, '								-- idGroup
				+ '		NULL, '											-- idRuleSet
				+ '		GETUTCDATE(), '									-- created date
				+ '		0 '												-- self joined
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblUserGroupLink] UGL'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] UM ON UM.sourceID = UGL.idUser AND UM.object = ''user'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] GM ON GM.sourceID = UGL.idGroup AND GM.object = ''group'''
				+ ' WHERE UM.destinationID IS NOT NULL AND GM.destinationID IS NOT NULL'
		
		EXEC (@sql)
		  
		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemCopyInquisiqToAsentiaLinkingTables_Success'

		
	SET XACT_ABORT OFF

END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.CopyInquisiqToAsentia.Report]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.CopyInquisiqToAsentia.Report]
GO


/*

Copy report data from Inquisiq to destination site in Asentia

*/
CREATE PROCEDURE [dbo].[System.CopyInquisiqToAsentia.Report]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,	
	@idAccount				INT,
	@idCaller				INT				= 1,
	@idSiteDefaultLang		INT				= 57,		-- English is ID 57	
	@siteHostname			NVARCHAR(255),
	@idSiteDestination		INT,
	@destinationHostname	NVARCHAR(255),
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@idSiteSource			INT,
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50)	
)
AS

BEGIN

SET NOCOUNT ON
SET XACT_ABORT ON    
    	
/*

Filter Data Structure Transform Mapping

*/
DECLARE @sql NVARCHAR(MAX), @filterTransform xml, @intFilterCondition int
SET @filterTransform = 
'<conditions>
  <condition identifier="is"        operator="=&apos;data&apos;"                                   dataType="String"          input="true"  />
  <condition identifier="nis"       operator="&lt;&gt;&apos;data&apos;"                            dataType="String"          input="true"  />
  <condition identifier="is_date"   operator="BETWEEN DATEADD(MINUTE, DATEDIFF(MINUTE, GETDATE(), GETUTCDATE()), DATEADD(d, 0, DATEDIFF(d, 0, &apos;bDate&apos;))) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()) - 1, DATEADD(d, 1, DATEDIFF(d, 0, &apos;bDate&apos;)))" dataType="DateTime"        input="true"  />
  <condition identifier="nis_date"  operator="NOT BETWEEN DATEADD(MINUTE, DATEDIFF(MINUTE, GETDATE(), GETUTCDATE()), DATEADD(d, 0, DATEDIFF(d, 0, &apos;bDate&apos;))) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()) - 1, DATEADD(d, 1, DATEDIFF(d, 0, &apos;bDate&apos;)))" dataType="DateTime"    input="true"  />
  <condition identifier="ct"        operator="LIKE &apos;%data%&apos;"                             dataType="StringContain"   input="true"  />
  <condition identifier="nct"       operator="NOT LIKE &apos;%data%&apos;"                         dataType="StringContain"   input="true"  />
  <condition identifier="sw"        operator="LIKE &apos;data%&apos;"                              dataType="String"          input="true"  />
  <condition identifier="ew"        operator="LIKE &apos;%data&apos;"                              dataType="String"          input="true"  />
  <condition identifier="bf"        operator="&lt;&apos;bDate&apos;"                               dataType="DateTime"        input="true"  />
  <condition identifier="af"        operator="&gt;&apos;bDate&apos;"                               dataType="DateTime"        input="true"  />
  <condition identifier="bw"        operator="BETWEEN DATEADD(MINUTE, DATEDIFF(MINUTE, GETDATE(), GETUTCDATE()), DATEADD(d, 0, DATEDIFF(d, 0, &apos;bDate&apos;))) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())- 1, DATEADD(d, 1, DATEDIFF(d, 0, &apos;eDate&apos;)))"  dataType="DateTime"        input="true"  />
  <condition identifier="nbw"       operator="NOT BETWEEN DATEADD(MINUTE, DATEDIFF(MINUTE, GETDATE(), GETUTCDATE()), DATEADD(d, 0, DATEDIFF(d, 0, &apos;bDate&apos;))) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())- 1, DATEADD(d, 1, DATEDIFF(d, 0, &apos;eDate&apos;)))" dataType="DateTime"        input="true"  />
  <condition identifier="r_d0"      operator="BETWEEN DATEADD(MINUTE, DATEDIFF(MINUTE, GETDATE(), GETUTCDATE()), DATEADD(d, 0, DATEDIFF(d, 0, GETDATE()))) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())- 1, DATEADD(d, 1, DATEDIFF(d, 0, GETDATE())))" dataType="DateTime"     input="false" />
  <condition identifier="r_d1"      operator="BETWEEN DATEADD(MINUTE, DATEDIFF(MINUTE, GETDATE(), GETUTCDATE()), DATEADD(d,-1, DATEDIFF(d, 0, GETDATE()))) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())- 1, DATEADD(d, 0, DATEDIFF(d, 0, GETDATE())))" dataType="DateTime"    input="false" />
  <condition identifier="r_d7"      operator="BETWEEN DATEADD(MINUTE, DATEDIFF(MINUTE, GETDATE(), GETUTCDATE()), DATEADD(d,-7, DATEDIFF(d, 0, GETDATE()))) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())- 1, DATEADD(d, 0, DATEDIFF(d, 0, GETDATE())))" dataType="DateTime"    input="false" />
  <condition identifier="r_w0"      operator="BETWEEN DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DATEADD(wk,DATEDIFF(wk,0,GETDATE()),-1)) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())-1, DATEADD(d, 1, DATEDIFF(d, 0, GETDATE())))" dataType="DateTime" input="false" />
  <condition identifier="r_w1"      operator="BETWEEN DATEADD(MINUTE, DATEDIFF(MINUTE, GETDATE(), GETUTCDATE()), DATEADD(wk,DATEDIFF(wk,0,GETDATE())-1,-1)) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())-1, DATEADD(wk,DATEDIFF(wk,0,GETDATE()), -1))"   dataType="DateTime" input="false" />
  <condition identifier="r_w2"      operator="BETWEEN DATEADD(MINUTE, DATEDIFF(MINUTE, GETDATE(), GETUTCDATE()), DATEADD(wk,DATEDIFF(wk,0,GETDATE())-2,-1)) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())-1, DATEADD(wk,DATEDIFF(wk,0,GETDATE()), -1))"   dataType="DateTime" input="false" />
  <condition identifier="r_m0"      operator="BETWEEN DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DATEADD(mm,DATEDIFF(mm,0,GETDATE()), 0)) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())-1, DATEADD(d, 1, DATEDIFF(d,0,GETDATE())))"     dataType="DateTime" input="false" />
  <condition identifier="r_m1"      operator="BETWEEN DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DATEADD(mm,DATEDIFF(mm,0,GETDATE())-1,0)) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())-1, DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0))"    dataType="DateTime" input="false" />
  <condition identifier="r_m3"      operator="BETWEEN DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DATEADD(mm,DATEDIFF(mm,0,GETDATE())-3,0)) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())-1, DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0))"    dataType="DateTime" input="false" />
  <condition identifier="r_m6"      operator="BETWEEN DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DATEADD(mm,DATEDIFF(mm,0,GETDATE())-6,0)) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())-1, DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0))"    dataType="DateTime" input="false" />
  <condition identifier="r_q0"      operator="BETWEEN DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DATEADD(qq,DATEDIFF(qq,0,GETDATE()),0))  AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())-1, DATEADD(d, 1, DATEDIFF(d, 0, GETDATE())))"   dataType="DateTime" input="false" />
  <condition identifier="r_q1"      operator="BETWEEN DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DATEADD(qq,DATEDIFF(qq,0,GETDATE())-1,0)) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())-1, DATEADD(qq,DATEDIFF(qq,0,GETDATE()),0))"    dataType="DateTime" input="false" />
  <condition identifier="r_q2"      operator="BETWEEN DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DATEADD(qq,DATEDIFF(qq,0,GETDATE())-2,0)) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())-1, DATEADD(qq,DATEDIFF(qq,0,GETDATE()),0))"    dataType="DateTime" input="false" />
  <condition identifier="r_q3"      operator="BETWEEN DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DATEADD(qq,DATEDIFF(qq,0,GETDATE())-3,0)) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())-1, DATEADD(qq,DATEDIFF(qq,0,GETDATE()),0))"    dataType="DateTime" input="false" />
  <condition identifier="r_q4"      operator="BETWEEN DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DATEADD(qq,DATEDIFF(qq,0,GETDATE())-4,0)) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())-1, DATEADD(qq,DATEDIFF(qq,0,GETDATE()),0))"    dataType="DateTime" input="false" />
  <condition identifier="r_y1"      operator="BETWEEN DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DATEADD(yy,DATEDIFF(yy,0,GETDATE())-1,0)) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())-1, DATEADD(yy,DATEDIFF(yy,0,GETDATE()),0))"    dataType="DateTime" input="false" />
  <condition identifier="r_y0"      operator="BETWEEN DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DATEADD(yy,DATEDIFF(yy,0,GETDATE()),0)) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())- 1, DATEADD(d, 1, DATEDIFF(d, 0, GETDATE())))"  dataType="DateTime" input="false" />
  <condition identifier="rf_d1"     operator="BETWEEN DATEADD(MINUTE, DATEDIFF(MINUTE, GETDATE(), GETUTCDATE()), DATEADD(d, 1, DATEDIFF(d, 0, GETDATE()))) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())- 1, DATEADD(d, 2, DATEDIFF(d, 0, GETDATE())))" dataType="DateTime" input="false" />
  <condition identifier="rf_d7"     operator="BETWEEN DATEADD(MINUTE, DATEDIFF(MINUTE, GETDATE(), GETUTCDATE()), DATEADD(d, 1, DATEDIFF(d, 0, GETDATE()))) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())- 1, DATEADD(d, 8, DATEDIFF(d, 0, GETDATE())))" dataType="DateTime" input="false" />
  <condition identifier="rf_w1"     operator="BETWEEN DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DATEADD(wk, DATEDIFF(wk,0,GETDATE()),6)) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())-1, DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 13))"  dataType="DateTime" input="false" />
  <condition identifier="rf_w2"     operator="BETWEEN DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DATEADD(wk, DATEDIFF(wk,0,GETDATE()),6)) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())-1, DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 20))"  dataType="DateTime" input="false" />
  <condition identifier="rf_m1"     operator="BETWEEN DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DATEADD(mm,DATEDIFF(mm,-1,GETDATE()),0)) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())-1, DATEADD(mm,DATEDIFF(mm,0,DateAdd(mm,2,GETDATE())),0))" dataType="DateTime" input="false" />
  <condition identifier="rf_m3"     operator="BETWEEN DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DATEADD(mm,DATEDIFF(mm,-1,GETDATE()),0)) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())-1, DATEADD(mm,DATEDIFF(mm,0,DateAdd(mm,4,GETDATE())),0))" dataType="DateTime" input="false" />
  <condition identifier="rf_m6"     operator="BETWEEN DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DATEADD(mm,DATEDIFF(mm,-1,GETDATE()),0)) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())-1, DATEADD(mm,DATEDIFF(mm,0,DateAdd(mm,7,GETDATE())),0))" dataType="DateTime" input="false" />
  <condition identifier="rf_q1"     operator="BETWEEN DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DATEADD(qq,DATEDIFF(qq,-1,GETDATE()),0)) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())-1, DATEADD(qq,DATEDIFF(qq,0,DateAdd(qq,2,GETDATE())),0))" dataType="DateTime" input="false" />
  <condition identifier="rf_q2"     operator="BETWEEN DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DATEADD(qq,DATEDIFF(qq,-1,GETDATE()),0)) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())-1, DATEADD(qq,DATEDIFF(qq,0,DateAdd(qq,3,GETDATE())),0))" dataType="DateTime" input="false" />
  <condition identifier="rf_q3"     operator="BETWEEN DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DATEADD(qq,DATEDIFF(qq,-1,GETDATE()),0)) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())-1, DATEADD(qq,DATEDIFF(qq,0,DateAdd(qq,4,GETDATE())),0))" dataType="DateTime" input="false" />
  <condition identifier="rf_q4"     operator="BETWEEN DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DATEADD(qq,DATEDIFF(qq,-1,GETDATE()),0)) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())-1, DATEADD(qq,DATEDIFF(qq,0,DateAdd(qq,5,GETDATE())),0))" dataType="DateTime" input="false" />
  <condition identifier="rf_y1"     operator="BETWEEN DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DATEADD(yy,DATEDIFF(yy,-1,GETDATE()),0)) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())-1, DATEADD(yy,DATEDIFF(yy,0,DateAdd(yy,2,GETDATE())),0))" dataType="DateTime" input="false" />
  <condition identifier="eq"        operator="=data"                                               dataType="IntegerEnum"         input="true"  />
  <condition identifier="neq"       operator="&lt;&gt;data"                                        dataType="IntegerEnum"         input="true"  />
  <condition identifier="lt"        operator="&lt;data"                                            dataType="Integer"         input="true"  />
  <condition identifier="leq"       operator="&lt;=data"                                           dataType="Integer"         input="true"  />
  <condition identifier="gt"        operator="&gt;data"                                            dataType="Integer"         input="true"  />
  <condition identifier="geq"       operator="&gt;=data"                                           dataType="Integer"         input="true"  />
  <condition identifier="yes"       operator="=1"                                                  dataType="Bit"             input="false" />
  <condition identifier="no"        operator="=0"                                                  dataType="Bit"             input="false" />
  <condition identifier="Active"    operator="=1"                                                  dataType="Status"          input="false" />
  <condition identifier="Disabled"  operator="=0"                                                  dataType="Status"          input="false" />
  <condition identifier="null"      operator="IS NULL"                                             dataType="StringIntegerDateTimeEnum"   input="false"  />
  <condition identifier="nnull"     operator="IS NOT NULL"                                         dataType="StringIntegerDateTimeEnum"   input="false"  />
</conditions>'

EXEC sp_xml_preparedocument @intFilterCondition OUTPUT, @filterTransform

CREATE TABLE #tblFilterTransform (
	identifier				VARCHAR(50),
	operator				VARCHAR(MAX),
	input					BIT
)

INSERT INTO #tblFilterTransform
SELECT identifier, operator, input
	FROM OPENXML(@intFilterCondition, 'conditions/condition', 1) 
	WITH 
	(	
		identifier [varchar](50) '@identifier',
		operator [varchar](max) '@operator',
		input [varchar](50) '@input' 
	)
EXEC sp_xml_removedocument @intFilterCondition

/*

Column Name Transform Mapping

*/
DECLARE @columnNameTransform xml, @intColumnName int
SET @columnNameTransform = 
'<columns>
  <column inquisiq="_idSite" asentia="_idSite" />
  <column inquisiq="Full Name" asentia="Full Name" />
  <column inquisiq="##name.firstname##" asentia="##firstName##" />
  <column inquisiq="##name.lastname##" asentia="##lastName##" />
  <column inquisiq="##email##" asentia="##email##" />
  <column inquisiq="##username##" asentia="##username##" />
  <column inquisiq="##jobtitle##" asentia="##jobTitle##" />
  <column inquisiq="##jobclass##" asentia="##jobClass##" />
  <column inquisiq="##company##" asentia="##company##" />
  <column inquisiq="##address.street##" asentia="##address##" />
  <column inquisiq="##address.city##" asentia="##city##" />
  <column inquisiq="##address.province##" asentia="##province##" />
  <column inquisiq="##address.postalcode##" asentia="##postalcode##" />
  <column inquisiq="##address.country##" asentia="##country##" />
  <column inquisiq="##phone.primary##" asentia="##phonePrimary##" />
  <column inquisiq="##phone.work##" asentia="##phoneWork##" />
  <column inquisiq="##phone.home##" asentia="##phoneHome##" />
  <column inquisiq="##phone.fax##" asentia="##phoneFax##" />
  <column inquisiq="##phone.mobile##" asentia="##phoneMobile##" />
  <column inquisiq="##phone.pager##" asentia="##phonePager##" />
  <column inquisiq="##phone.other##" asentia="##phoneOther##" />
  <column inquisiq="##division##" asentia="##division##" />
  <column inquisiq="##department##" asentia="##department##" />
  <column inquisiq="##region##" asentia="##region##" />
  <column inquisiq="##employeeid##" asentia="##employeeID##" />
  <column inquisiq="##hiredate##" asentia="##dtHire##" />
  <column inquisiq="##termdate##" asentia="##dtTerm##" />
  <column inquisiq="##gender##" asentia="##gender##" />
  <column inquisiq="##race##" asentia="##race##" />
  <column inquisiq="##dob##" asentia="##dtDOB##" />
  <column inquisiq="##isactive##" asentia="##isActive##" />
  <column inquisiq="##created##" asentia="##dtCreated##" />
  <column inquisiq="##expires##" asentia="##dtExpires##" />
  <column inquisiq="##dtLastLogin##" asentia="##dtLastLogin##" />
  <column inquisiq="##language##" asentia="##idLanguage##" />
  <column inquisiq="##idtimezone##" asentia="##idTimezone##" />
  <column inquisiq="Group" asentia="Group" />
  <column inquisiq="_sbGroup" asentia="_sbGroup"  optionGroup="" />
  <column inquisiq="##supervisor##" asentia="Supervisor" />
  <column inquisiq="##field00##" asentia="##field00##" />
  <column inquisiq="##field01##" asentia="##field01##" />
  <column inquisiq="##field02##" asentia="##field02##" />
  <column inquisiq="##field03##" asentia="##field03##" />
  <column inquisiq="##field04##" asentia="##field04##" />
  <column inquisiq="##field05##" asentia="##field05##" />
  <column inquisiq="##field06##" asentia="##field06##" />
  <column inquisiq="##field07##" asentia="##field07##" />
  <column inquisiq="##field08##" asentia="##field08##" />
  <column inquisiq="##field09##" asentia="##field09##" />
  <column inquisiq="Code" asentia="Code" />
  <column inquisiq="Course" asentia="Course" />
  <column inquisiq="_order_Course" asentia="_order_Course" />
  <column inquisiq="Credits" asentia="Credits" />
  <column inquisiq="Enroll Date" asentia="Enroll Date" />
  <column inquisiq="Course Status" asentia="Course Status" />
  <column inquisiq="_sbCourse Status" asentia="_sbCourse Status" />
  <column inquisiq="Due Date" asentia="Date Due" />
  <column inquisiq="Date Completed" asentia="Date Completed" />
  <column inquisiq="Lesson" asentia="Lesson" />
  <column inquisiq="Session Date/Time" asentia="Session Date/Time" />
  <column inquisiq="Content Resource" asentia="Content Resource" />
  <column inquisiq="Lesson Completion" asentia="Lesson Completion" />
  <column inquisiq="Lesson Success" asentia="Lesson Success" />
  <column inquisiq="Score" asentia="Score" />
  <column inquisiq="Lesson Timestamp" asentia="Lesson Timestamp" />
  <column inquisiq="Lesson Time" asentia="Lesson Time" />
  <column inquisiq="Attempts" asentia="Attempts" />  
  <column inquisiq="Interaction" asentia="Interaction" />
  <column inquisiq="Interaction Description" asentia="Interaction Description" />
  <column inquisiq="Type" asentia="Interaction Type" />
  <column inquisiq="Interaction Timestamp" asentia="Interaction Timestamp" />
  <column inquisiq="Interaction Time" asentia="Interaction Time" />
  <column inquisiq="Learner Response" asentia="Learner Response" />
  <column inquisiq="Result" asentia="Result" />
  <column inquisiq="Objective" asentia="Objective" />
  <column inquisiq="Objective Score" asentia="Objective Score" />
  <column inquisiq="Objective Completion Status" asentia="Objective Completion Status" />
  <column inquisiq="Objective Success Status" asentia="Objective Success Status" />
  <column inquisiq="Catalog Name" asentia="Catalog Name" />
  <column inquisiq="Catalog Cost" asentia="Catalog Cost" />
  <column inquisiq="Course Name" asentia="Course Name" />
  <column inquisiq="_order_Course" asentia="_order_Course" />
  <column inquisiq="Course Code" asentia="Course Code" />
  <column inquisiq="Course Cost" asentia="Course Cost" />
  <column inquisiq="Course Rating" asentia="Course Rating" />
  <column inquisiq="Votes Cast" asentia="Votes Cast" />
  <column inquisiq="Course Credits" asentia="Course Credits" />
  <column inquisiq="Course Published" asentia="Course Published" />
  <column inquisiq="Course Closed" asentia="Course Closed" dataType="Bit" />
  <column inquisiq="Course Locked" asentia="Course Locked" dataType="Bit" />
  <column inquisiq="Lesson Name" asentia="Lesson Name" />
  <column inquisiq="Lesson Type" asentia="Content Type_s" dataType="Contain" />
  <column inquisiq="Content Resource" asentia="Resource Name" />
  <column inquisiq="Session Date/Time" asentia="Session Date/Time" />
  <column inquisiq="Session Seats" asentia="Session Seats" />
  <column inquisiq="Session Waiting Seats" asentia="Session Waiting Seats" />
  <column inquisiq="Session Seats Available" asentia="Session Seats Available" />
  <column inquisiq="Session Instructor" asentia="Session Instructor" />
  <column inquisiq="Session Instructor Email" asentia="Session Instructor Email" />
  <column inquisiq="Session City" asentia="Session City" />
  <column inquisiq="Session State/Province" asentia="Session Province" />
  <column inquisiq="Session Student Name" asentia="Session Student Name" />
  <column inquisiq="Session Student Email" asentia="Session Student Email" />
  <column inquisiq="Session Student Username" asentia="Session Student Username" />
  <column inquisiq="Certificate Name" asentia="Certificate Name" />
  <column inquisiq="Issuing Organization" asentia="Issuing Organization" />
  <column inquisiq="Awarded By" asentia="Awarded By" />
  <column inquisiq="Award Date" asentia="Award Date" />
  <column inquisiq="Expiration Date" asentia="Expiration Date" />
  <column inquisiq="Credits" asentia="Credits" />
  <column inquisiq="Certificate Code" asentia="Code" />
  <column inquisiq="Order Number" asentia="Order Number" />
  <column inquisiq="Order Date" asentia="Order Date" />
  <column inquisiq="Credit Card Digits" asentia="Credit Card Digits" />
  <column inquisiq="Item Type" asentia="Item Type" />
  <column inquisiq="Item Name" asentia="Item Name" />
  <column inquisiq="Item Description" asentia="Item Description" />
  <column inquisiq="Cost" asentia="Cost" />
  <column inquisiq="Paid" asentia="Paid" />
  <column inquisiq="Coupon Code" asentia="Coupon Code" />
</columns>'

EXEC sp_xml_preparedocument @intColumnName OUTPUT, @columnNameTransform

CREATE TABLE #tblColumnMapping (
	inquisiq			VARCHAR(50),
	asentia				VARCHAR(50)
)

INSERT INTO #tblColumnMapping
SELECT inquisiq, asentia
	FROM OPENXML(@intColumnName, 'columns/column', 1) 
	WITH 
	(	
		inquisiq [varchar](50) '@inquisiq',
		asentia [varchar](50) '@asentia'
	)
EXEC sp_xml_removedocument @intColumnName

/*

Start Transfer Process, loop through each report and transform the data structure

*/

DECLARE @idReport int, @instanceData xml, @hDoc int, @fields varchar(max), @filters varchar(max)

CREATE TABLE #TempTable(
	idReport				INT,
	instanceData			VARCHAR(MAX)
)

SET @sql = ' INSERT INTO #TempTable'
		 + ' SELECT idReport, instanceData'
		 + ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblReport] R'
		 + ' WHERE R.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)
EXEC (@sql)

CREATE TABLE #AsentiaFormatReportTable (
	idReport			INT,
	idSite				INT,
	idUser				INT,
	idDataset			INT,
	isPublic			BIT,
	fields				VARCHAR(MAX),
	filters				VARCHAR(MAX)
)

WHILE EXISTS(SELECT * FROM #TempTable)
BEGIN
	SELECT TOP 1 @idReport = idReport, @instanceData = instanceData From #TempTable

	EXEC sp_xml_preparedocument @hDoc OUTPUT, @instanceData

	CREATE TABLE #ColumnTable (
	columnName			VARCHAR(MAX)
	)

	INSERT INTO #ColumnTable 
	SELECT DISTINCT '[' + CM.asentia + '],'
	FROM OPENXML(@hDoc, 'instanceData/field') 
	WITH 
	(	ColumnName [varchar](50) '@columnName'
	) FN
	LEFT JOIN #tblColumnMapping CM ON CM.inquisiq = FN.ColumnName

	SELECT @fields = STUFF(
		(
			SELECT columnName FROM #ColumnTable FOR XML PATH('')
		)
	, 1, 0, '')

	DROP TABLE #ColumnTable

	CREATE TABLE #FilterTable (
	orAND				VARCHAR(10),
	columnName			VARCHAR(MAX),
	filterValue			VARCHAR(MAX),
	comment				VARCHAR(MAX))

	INSERT INTO #FilterTable
	SELECT
		 CASE WHEN ROW_NUMBER() OVER(ORDER BY ColumnName ASC) = 1
			THEN ''
		 ELSE
			 CASE WHEN matchType= 'any' THEN ' or ' ELSE ' and ' END
		 END,
		'([' + CM.asentia + '] ' + FT.operator + ' ' ,  val, ')'
	FROM OPENXML(@hDoc, 'instanceData/field/criterion', 3) 
	WITH 
	( 
		ColumnName [varchar](50) '../@columnName',
		operator [varchar](20) '@operator',
		val ntext 'text()',
		matchType [varchar](50) '../@matchType' 
	) FR
	LEFT JOIN #tblFilterTransform FT ON FT.identifier = FR.operator
	LEFT JOIN #tblColumnMapping CM ON CM.inquisiq = FR.ColumnName
	WHERE matchType is not null
	
	UPDATE #FilterTable
	SET columnName = REPLACE(columnName, 'data', filterValue) 
	WHERE filterValue IS NOT NULL

	SELECT @filters = STUFF(
		(
			SELECT orAND + columnName + comment FROM #FilterTable FOR XML PATH('')
		)
	, 1, 0, '')

	DROP TABLE #FilterTable
	EXEC sp_xml_removedocument @hDoc

	SET @sql = ' INSERT INTO #AsentiaFormatReportTable'
		 + ' ('
		 + '	idReport,'
		 + '	idSite,'
		 + '	idUser,'
		 + '	idDataset,'
		 + '	isPublic,'
		 + '	fields,'
		 + '	filters'
		 + ' )'
		 + '  SELECT'
		 + '	R.idReport,'
		 + '	R.idSite,'
		 + '	R.idUser,'
		 + '	R.idDataset,'
		 + '	R.isPublic,'
		 + '	Left(''' + @fields + ''',LEN(''' + @fields + ''')-1),'
		 + '	''' + REPLACE(@filters, '''', '''''')  +''''
		 + '	FROM   [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblReport] R'
		 + '	WHERE R.idReport = ' + CONVERT(varchar(10), @idReport)
	EXEC (@sql)
	DELETE #TempTable Where idReport = @idReport
END

	DROP TABLE #tblFilterTransform
	DROP TABLE #TempTable

	CREATE TABLE #TempDatasetTranformTable (
		idOldDataset		INT,
		idNewDataset		INT
	)

	INSERT INTO #TempDatasetTranformTable VALUES(1,1)
	INSERT INTO #TempDatasetTranformTable VALUES(2,2)
	INSERT INTO #TempDatasetTranformTable VALUES(3,4)
	INSERT INTO #TempDatasetTranformTable VALUES(4,8)
	INSERT INTO #TempDatasetTranformTable VALUES(5,3)

	-- reports
	SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblReport]'
			+ ' ( '
			+ '		idSite, '
			+ '		idUser, '
			+ '		idDataset, '
			+ '		title, '
			+ '		fields, '
			+ '		filter, '
			+ '		[order], '				
			+ '		isPublic, '
			+ '		dtCreated, '
			+ '		dtModified, '
			+ '		isDeleted, '
			+ '		dtDeleted '
			+ ' ) '
			+ ' SELECT '
			+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','								-- idSite	
			+ '		UM.destinationID,'															-- idUser
			+ '		DST.idNewDataset,'															-- idDataset
			+ '		R.name + '' ##'' + CONVERT(NVARCHAR, R.idReport) + ''##'','					-- title
			+ '		AFR.fields,'																-- fields
			+ '		AFR.filters,'																-- filter
			+ '		NULL,'																		-- order
			+ '		R.isPublic,'																-- isPublic
			+ '		GETDATE(),'																	-- dtCreated
			+ '		GETDATE(),'																	-- dtModified
			+ '		NULL,'																		-- isDeleted
			+ '		NULL'																		-- dtDeleted			
			+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblReport] R'
			+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblUser] U ON U.idUser = R.idUser'
			+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] UM ON UM.sourceID = R.idUser AND UM.object = ''user'''
			+ ' LEFT JOIN #TempDatasetTranformTable DST ON DST.idOldDataset = R.idDataset'
			+ ' LEFT JOIN #AsentiaFormatReportTable AFR ON AFR.idReport = R.idReport'
			+ ' WHERE U.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) 
		
	 EXEC (@sql)
	 DROP TABLE #TempDatasetTranformTable

		-- insert idReport mappings (idReport maps to Inquisiq idReport)
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SR.idReport, '
				+ '		DR.idReport,'
				+ '		''report''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblReport] DR '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblReport] SR ON SR.name + '' ##'' + CONVERT(NVARCHAR, SR.idReport) + ''##'' = DR.title collate database_default'
				+ ' WHERE DR.idSite IS NOT NULL AND DR.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SR.idReport IS NOT NULL AND DR.idReport IS NOT NULL'		
		EXEC(@sql)

		-- clean up the ##idReport## additions we made to report titles, we did that to uniquely distinguish names
		SET @sql = ' UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblReport] '
				+ '	SET	title = REPLACE(DR.title, '' ##'' + CONVERT(NVARCHAR, SEIM.sourceID) + ''##'', ''''),'
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblReport] DR '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] SEIM ON SEIM.destinationID = DR.idReport AND SEIM.object = ''report'''
				+ ' WHERE SEIM.sourceID IS NOT NULL'

		EXEC (@sql)

		-- insert user report subscription
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblReportSubscription]'
			+ ' ( '
			+ '		idSite, '
			+ '		idUser, '
			+ '		idReport, '
			+ '		dtStart, '
			+ '		dtNextAction, '
			+ '		recurInterval, '
			+ '		recurTimeframe, '				
			+ '		token '
			+ ' ) '
			+ ' SELECT '
			+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','								-- idSite	
			+ '		UM.destinationID,'															-- idUser
			+ '		RM.destinationID,'															-- idReport
			+ '		RS.dtStart,'																-- dtStart
			+ '		RS.dtNextAction,'															-- dtNextAction
			+ '		RS.recurInterval,'															-- recurInterval
			+ '		RS.recurTimeframe,'															-- recurTimeframe
			+ '		RS.token'																	-- token		
			+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblReportSubscription] RS'
			+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] UM ON UM.sourceID = RS.idUser AND UM.object = ''user'''
			+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] RM ON RM.sourceID = RS.idReport AND RM.object = ''report'''
			+ ' WHERE UM.destinationID IS NOT NULL AND RM.destinationID IS NOT NULL'
		
		EXEC (@sql)

		DROP TABLE #AsentiaFormatReportTable
		  
		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemCopyInquisiqToAsentiaReport_Success'		
		
	SET XACT_ABORT OFF

END

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.CopyInquisiqToAsentia.Site]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.CopyInquisiqToAsentia.Site]
GO


/*

Copy source site data from Inquisiq to destination site in Asentia

*/
CREATE PROCEDURE [dbo].[System.CopyInquisiqToAsentia.Site]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,	
	@idAccount				INT,
	@idCaller				INT				= 1,
	@idSiteDefaultLang		INT				= 57,		-- English is ID 57	
	@siteHostname			NVARCHAR(255),
	@idSiteDestination		INT				OUTPUT,
	@destinationHostname	NVARCHAR(255),
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@idSiteSource			INT				OUTPUT,
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50)	
)
AS

BEGIN

    SET NOCOUNT ON
    SET XACT_ABORT ON    
    	
	DECLARE @sql NVARCHAR(MAX)
	  
	/*
	  
	create id mappings table or clear out existing id mappings table

	*/

	SET @sql = 'IF NOT EXISTS (SELECT * FROM [' + @destinationDBName + '].dbo.sysobjects WHERE name = N''InquisiqMigration_IDMappings'' AND xtype = ''U'')'
			+ ' BEGIN '
			+ 'CREATE TABLE [' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings]'
			+ '('
			+ '    [sourceID] INT NOT NULL,'
			+ '    [destinationID] INT NOT NULL,'
			+ '    [object] NVARCHAR(255)'
			+ ')'
			+ ' END '
			+ ' ELSE '
	SET @sql = @sql + ' DELETE FROM [' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings]'

	EXEC(@sql)
	  
	/*
	  
	create timezone mappings table, if it doesnt already exist

	*/

	SET @sql = 'IF NOT EXISTS (SELECT * FROM [' + @destinationDBName + '].dbo.sysobjects WHERE name = N''InquisiqMigration_TimezoneIDMappings'' AND xtype = ''U'')'
			+ ' BEGIN '
			+ 'CREATE TABLE [' + @destinationDBName + '].[dbo].[InquisiqMigration_TimezoneIDMappings]'
			+ '('
			+ '    [sourceID] INT NOT NULL,'
			+ '    [destinationID] INT NOT NULL'
			+ ')'
			+ ' '
			+ ' INSERT INTO [' + @destinationDBName + '].[dbo].[InquisiqMigration_TimezoneIDMappings] (sourceID, destinationID) '
			+ ' VALUES '
			+ ' (0, 0), '
			+ ' (1, 1), '
			+ ' (2, 102), '
			+ ' (3, 3), '
			+ ' (4, 4), '
			+ ' (5, 6), '
			+ ' (6, 5), '
			+ ' (7, 7), '
			+ ' (8, 8), '
			+ ' (9, 8), '
			+ ' (10, 9), '
			+ ' (11, 10), '
			+ ' (12, 11), '
			+ ' (13, 12), '
			+ ' (14, 12), '
			+ ' (15, 13), '
			+ ' (16, 14), '
			+ ' (17, 15), '
			+ ' (18, 16), '
			+ ' (19, 17), '
			+ ' (20, 19), '
			+ ' (21, 21), '
			+ ' (22, 21), '
			+ ' (23, 22), '
			+ ' (24, 23), '
			+ ' (25, 24), '
			+ ' (26, 25), '
			+ ' (27, 27), '
			+ ' (28, 28), '
			+ ' (29, 31), '
			+ ' (30, 32), '
			+ ' (31, 33), '
			+ ' (32, 34), '
			+ ' (33, 36), '
			+ ' (34, 38), '
			+ ' (35, 39), '
			+ ' (36, 40), '
			+ ' (37, 41), '
			+ ' (38, 43), '
			+ ' (39, 54), '
			+ ' (40, 45), '
			+ ' (41, 46), '
			+ ' (42, 47), '
			+ ' (43, 50), '
			+ ' (44, 51), '
			+ ' (45, 53), '
			+ ' (46, 56), '
			+ ' (47, 44), '
			+ ' (48, 55), '
			+ ' (49, 57), '
			+ ' (50, 62), '
			+ ' (51, 58), '
			+ ' (52, 64), '
			+ ' (53, 59), '
			+ ' (54, 60), '
			+ ' (55, 61), '
			+ ' (56, 65), '
			+ ' (57, 65), '
			+ ' (58, 66), '
			+ ' (59, 74), '
			+ ' (60, 68), '
			+ ' (61, 69), '
			+ ' (62, 70), '
			+ ' (63, 71), '
			+ ' (64, 77), '
			+ ' (65, 72), '
			+ ' (66, 75), '
			+ ' (67, 76), '
			+ ' (68, 79), '
			+ ' (69, 78), '
			+ ' (70, 83), '
			+ ' (71, 80), '
			+ ' (72, 81), '
			+ ' (73, 82), '
			+ ' (74, 85), '
			+ ' (75, 86), '
			+ ' (76, 93), '
			+ ' (77, 87), '
			+ ' (78, 88), '
			+ ' (79, 89), '
			+ ' (80, 90), '
			+ ' (81, 91), '
			+ ' (82, 92), '
			+ ' (83, 95), '
			+ ' (84, 99), '
			+ ' (85, 96), '
			+ ' (86, 98), '
			+ ' (87, 101) '
			+ ' END '		

	EXEC(@sql)

	/*
	  
	create event type mappings table, if it doesnt already exist

	*/

	SET @sql = 'IF NOT EXISTS (SELECT * FROM [' + @destinationDBName + '].dbo.sysobjects WHERE name = N''InquisiqMigration_EventTypeIDMappings'' AND xtype = ''U'')'
			+ ' BEGIN '
			+ 'CREATE TABLE [' + @destinationDBName + '].[dbo].[InquisiqMigration_EventTypeIDMappings]'
			+ '('
			+ '    [sourceID] INT NOT NULL,'
			+ '    [destinationID] INT NOT NULL'
			+ ')'
			+ ' '
			+ ' INSERT INTO [' + @destinationDBName + '].[dbo].[InquisiqMigration_EventTypeIDMappings] (sourceID, destinationID) '
			+ ' VALUES '
			+ ' (0, 0), '			
			+ ' (1, 702), '		-- Certificate Awarded
			+ ' (2, 701), '		-- Certificate Earned
			+ ' (3, 703), '		-- Certificate Revoked
			+ ' (4, 0), '		-- Purchase Confirmed - NOT IN ASENTIA
			+ ' (5, 206), '		-- Enrollment Start
			+ ' (6, 205), '		-- Enrollment End
			+ ' (7, 203), '		-- Enrollment Revoked
			+ ' (8, 204), '		-- Enrollment Due
			+ ' (9, 301), '		-- Lesson Passed
			+ ' (10, 302), '	-- Lesson Failed
			+ ' (11, 303), '	-- Lesson Completed
			+ ' (14, 101), '	-- User Created
			+ ' (15, 102), '	-- User Deleted
			+ ' (16, 402), '	-- Session Instructor Assigned or Changed
			+ ' (17, 403), '	-- Session Instructor Removed
			+ ' (18, 404), '	-- Learner Joined to Session
			+ ' (19, 405), '	-- Learner Dropped From Session
			+ ' (20, 406), '	-- Session Time or Location Changed
			+ ' (21, 205), '	-- Enrollment Completed
			+ ' (22, 103), '	-- User Account Expires
			+ ' (24, 401), '	-- Session Meets
			+ ' (25, 704) '		-- Certificate Expires
			+ ' END '		

	EXEC(@sql)

	/*
	  
	create event type recipient mappings table, if it doesnt already exist

	*/

	SET @sql = 'IF NOT EXISTS (SELECT * FROM [' + @destinationDBName + '].dbo.sysobjects WHERE name = N''InquisiqMigration_EventTypeRecipientIDMappings'' AND xtype = ''U'')'
			+ ' BEGIN '
			+ 'CREATE TABLE [' + @destinationDBName + '].[dbo].[InquisiqMigration_EventTypeRecipientIDMappings]'
			+ '('
			+ '    [sourceID] INT NOT NULL,'
			+ '    [destinationID] INT NOT NULL'
			+ ')'
			+ ' '
			+ ' INSERT INTO [' + @destinationDBName + '].[dbo].[InquisiqMigration_EventTypeRecipientIDMappings] (sourceID, destinationID) '
			+ ' VALUES '
			+ ' (0, 0), '
			+ ' (1, 1), '
			+ ' (3, 2), '
			+ ' (4, 1), '
			+ ' (5, 2), '
			+ ' (6, 1), '
			+ ' (7, 2), '
			+ ' (8, 1), '
			+ ' (9, 3), '
			+ ' (10, 1), '
			+ ' (11, 2), '
			+ ' (12, 1), '
			+ ' (13, 2), '
			+ ' (14, 1), '
			+ ' (15, 2), '
			+ ' (16, 1), '
			+ ' (17, 2), '
			+ ' (18, 1), '
			+ ' (19, 2), '
			+ ' (20, 1), '
			+ ' (21, 2), '
			+ ' (22, 1), '
			+ ' (23, 2), '
			+ ' (24, 1), '
			+ ' (25, 2), '
			+ ' (28, 1), '
			+ ' (29, 3), '
			+ ' (30, 1), '
			+ ' (32, 1), '
			+ ' (33, 5), '
			+ ' (34, 6), '
			+ ' (35, 1), '
			+ ' (36, 5), '
			+ ' (38, 1), '
			+ ' (39, 5), '
			+ ' (40, 2), '
			+ ' (41, 1), '
			+ ' (42, 5), '
			+ ' (43, 2), '
			+ ' (44, 1), '
			+ ' (45, 5), '
			+ ' (46, 6), '
			+ ' (47, 1), '
			+ ' (48, 2), '
			+ ' (49, 8), '
			+ ' (50, 8), '
			+ ' (51, 8), '
			+ ' (52, 8), '
			+ ' (53, 8), '
			+ ' (54, 8), '
			+ ' (55, 8), '
			+ ' (56, 8), '
			+ ' (57, 8), '
			+ ' (58, 8), '
			+ ' (59, 8), '
			+ ' (60, 8), '
			+ ' (61, 8), '
			+ ' (62, 1), '
			+ ' (63, 3), '
			+ ' (64, 1), '
			+ ' (65, 5), '
			+ ' (66, 6), '
			+ ' (67, 1), '
			+ ' (68, 2), '
			+ ' (69, 8) '
			+ ' END '		

	EXEC(@sql)

	/*
	
	validate unique host name in Account database -- this runs in scope of "Customer Manager" so it can be executed here instead of as dynamic SQL
	
	*/
	
	IF (SELECT COUNT(1) FROM tblAccountToDomainAliasLink WHERE hostname = @destinationHostname) > 0
		BEGIN
		SET @Return_Code = 2
		SET @Error_Description_Code = 'SystemCopyInquisiqToAsentia_HostnameNotUnique'
		RETURN 1
		END

	/*

	get the id of the source (Inquisiq) site

	*/

	SET @sql = 'SELECT @idSiteSource = idSite FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblSite] WHERE hostname = ''' + @siteHostname + ''''

	EXECUTE SP_EXECUTESQL @sql, N'@idSiteSource INT OUTPUT', @idSiteSource OUTPUT
	
    /*

	TRANSACTION BEGIN

	*/


	-- =============================================================================
	-- LOGIC START

	/************************************************SITE*************************************************************/
		-- tblSite
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblSite]'
				+ ' ( '
				+ '		hostname, '
				+ '		password, '
				+ '		isActive, '
				+ '		dtExpires, '
				+ '		title, '
				+ '		company, '
				+ '		contactName, '
				+ '		contactEmail, '
				+ '		userLimit, '
				+ '		kbLimit, '
				+ '		idLanguage, '
				+ '		idTimezone' 
				+ ' ) '
				+ ' SELECT ' 
				+ '		''' + @destinationHostname + ''','							-- hostname
				+ '		S.password,'												-- password (this is an MD5 value)
				+ '		S.isActive,'												-- is active?
				+ '		S.dtExpires,'												-- date expires
				+ '		S.name,'													-- title
				+ '		SC.company,'												-- company
				+ '		SC.firstName + '' '' + SC.lastName,'						-- contact name
				+ '		SC.email,'													-- contact email
				+ '		CASE WHEN S.userLimit > 0 THEN S.userLimit ELSE NULL END,'	-- user limit
				+ '		CASE WHEN S.kbLimit > 0 THEN S.kbLimit ELSE NULL END,'		-- kb limit
				+ '		' + CAST(@idSiteDefaultLang AS NVARCHAR) + ','				-- language ID
				+ '		15'															-- timezone ID				
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblSite] S'
				+ ' LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].[tblSiteContact] SC ON SC.idSite = S.idSite'
				+ ' WHERE S.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
		
		EXEC (@sql)

		-- get the newly inserted site id as @idSiteDestination
		SET @sql = 'SELECT @idSiteDestination = idSite FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblSite] WHERE hostname = ''' + @destinationHostname + ''''

		EXECUTE SP_EXECUTESQL @sql, N'@idSiteDestination INT OUTPUT', @idSiteDestination OUTPUT
	  
		-- insert idSite of source and destination site tables inside temporary table for mapping	
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteSource) + ', '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		''site'' ' 				
		
		EXEC(@sql)		   	   

		-- tblSiteLanguage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblSiteLanguage]' 
				+ ' ( '
				+ '		idSite,'
				+ '		idLanguage,'
				+ '		title,'
				+ '		company'
				+ '	) '
				+ ' SELECT '
				+ '		S.idSite, '
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ','
				+ '		S.title, '
				+ '		S.company'
				+ ' FROM  [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblSite] S'
				+ ' WHERE S.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
		
		EXEC(@sql)	

		-- tblSiteToDomainAliasLink
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblSiteToDomainAliasLink]' 
				+ ' ( '
				+ '		idSite,'
				+ '		domain'				
				+ '	) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','
				+ '		''' + @destinationHostname + ''''				
		
		EXEC(@sql)

		  
		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemCopyInquisiqToAsentiaSite_Success'
		
	SET XACT_ABORT OFF

END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.CopyInquisiqToAsentia.User]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.CopyInquisiqToAsentia.User]
GO


/*

Copy source site data from Inquisiq to destination site in Asentia

*/
CREATE PROCEDURE [dbo].[System.CopyInquisiqToAsentia.User]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,	
	@idAccount				INT,
	@idCaller				INT				= 1,
	@idSiteDefaultLang		INT				= 57,		-- English is ID 57	
	@siteHostname			NVARCHAR(255),
	@idSiteDestination		INT,
	@destinationHostname	NVARCHAR(255),
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@idSiteSource			INT,
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50)	
)
AS

BEGIN

    SET NOCOUNT ON
    SET XACT_ABORT ON    
    	
	DECLARE @sql NVARCHAR(MAX)
	  
	/************************************************ USER ******************************************************/
		-- tblUser
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblUser]'
				+ ' ( '
				+ '		firstName, '
				+ '		middleName, '
				+ '		lastName, '
				+ '		displayName, '
				+ '		email, '
				+ '		username, '
				+ '		password, '
				+ '		idSite, '
				+ '		isDeleted, '
				+ '		idTimezone, '
				+ '		objectGUID, '
				+ '		activationGUID, '
				+ '		distinguishedName, '
				+ '		isActive, '
				+ '		mustchangePassword, '  
				+ '		dtCreated, '
				+ '		dtExpires, '
				+ '		idLanguage, '
				+ '		dtModified, '
				+ '		employeeID, '
				+ '		company, '
				+ '		address, '
				+ '		city, '
				+ '		province, '
				+ '		postalcode, '
				+ '		country, '
				+ '		phonePrimary, '
				+ '		phoneWork, '
				+ '		phoneFax, '
				+ '		phoneHome, '
				+ '		phoneMobile, '
				+ '		phonePager, '
				+ '		phoneOther, '
				+ '		department, '
				+ '		division, '
				+ '		region, '
				+ '		jobTitle, '
				+ '		jobClass, '
				+ '		gender, '
				+ '		race, '
				+ '		dtDOB, '
				+ '		dtHire, '
				+ '		dtTerm, '
				+ '		field00, '
				+ '		field01, '
				+ '		field02, '
				+ '		field03, '
				+ '		field04, '
				+ '		field05, '
				+ '		field06, '
				+ '		field07, '
				+ '		field08, '
				+ '		field09, '
				+ '		field10, '
				+ '		field11, '
				+ '		field12, '
				+ '		field13, '
				+ '		field14, '
				+ '		field15, '
				+ '		field16, '
				+ '		field17, '
				+ '		field18, '
				+ '		field19, '
				+ '		avatar, '
				+ '		dtLastLogin, '
				+ '		dtDeleted, '
				+ '		dtSessionExpires, '
				+ '		activeSessionId, '
				+ '		dtLastPermissionCheck, '
				+ '		isRegistrationApproved, '
				+ '		dtApproved, '
				+ '		dtDenied, '				
				+ '		idApprover, '
				+ '		rejectionComments, '
				+ '		excludeFromLeaderboards '
				+ ' ) '
				+ ' SELECT ' 
				+ '		U.firstName,'									-- first name
				+ '		U.middleName,'									-- middle name
				+ '		U.lastName,'									-- last name
				+ '		U.lastName + '', '' + U.firstName,'				-- display name
				+ '		U.email,'										-- email
				+ '		U.username,'									-- username
				+ '		U.password,'									-- password (this is an MD5 value)
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- site ID
				+ '		0,'												-- is deleted? 
				+ '		ISNULL(TZ.destinationID, 15) AS idTimezone,'	-- timezone id (default to Eastern timezone if the value is NULL)
				+ '		U.objectGUID,'									-- object GUID
				+ '		NULL,'											-- activation GUID
				+ '		U.distinguishedName,'							-- distinguished name
				+ '		U.isActive,'									-- is active?
				+ '		1,'												-- must change password? (since Asentia uses SHA1, we need to force all users to change their password)
				+ '		U.dtCreated,'									-- date created
				+ '		U.dtExpires,'									-- date expires
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ','	-- language id, use Asentia default
				+ '		U.dtModified,'									-- date modified
				+ '		UP.employeeID,'									-- employeed ID
				+ '		UP.company,'									-- company
				+ '		UP.address,'									-- street address
				+ '		UP.city,'										-- city
				+ '		UP.province,'									-- state/province
				+ '		UP.postalcode,'									-- postal code
				+ '		UP.country,'									-- country
				+ '		UP.phonePrimary,'								-- primary phone
				+ '		UP.phoneWork,'									-- work phone
				+ '		UP.phoneFax,'									-- fax
				+ '		UP.phoneHome,'									-- home phone
				+ '		UP.phoneMobile,'								-- mobile phone
				+ '		UP.phonePager,'									-- pager
				+ '		UP.phoneOther,'									-- other phone
				+ '		UP.department,'									-- department
				+ '		UP.division,'									-- division
				+ '		UP.region,'										-- region
				+ '		UP.jobTitle,'									-- job title
				+ '		UP.jobClass,'									-- job class
				+ '		UP.gender,'										-- gender
				+ '		UP.race,'										-- race
				+ '		UP.dtDOB,'										-- date of birth
				+ '		UP.dtHire,'										-- hire date
				+ '		UP.dtTerm,'										-- term date
				+ '		UDUFV.field00,'									-- user defined field00
				+ '		UDUFV.field01,'									-- user defined field01
				+ '		UDUFV.field02,'									-- user defined field02
				+ '		UDUFV.field03,'									-- user defined field03
				+ '		UDUFV.field04,'									-- user defined field04
				+ '		UDUFV.field05,'									-- user defined field05
				+ '		UDUFV.field06,'									-- user defined field06
				+ '		UDUFV.field07,'									-- user defined field07
				+ '		UDUFV.field08,'									-- user defined field08
				+ '		UDUFV.field09,'									-- user defined field09
				+ '		UDUFV.field10,'									-- user defined field10
				+ '		NULL,'											-- user defined field11
				+ '		NULL,'											-- user defined field12
				+ '		NULL,'											-- user defined field13
				+ '		NULL,'											-- user defined field14
				+ '		NULL,'											-- user defined field15
				+ '		NULL,'											-- user defined field16
				+ '		NULL,'											-- user defined field17
				+ '		NULL,'											-- user defined field18
				+ '		NULL,'											-- user defined field19
				+ '		NULL,'											-- avatar
				+ '		NULL,'											-- date last login (set to null because user has never logged into Asentia)
				+ '		NULL,'											-- date deleted
				+ '		NULL,'											-- date session expires
				+ '		NULL,'											-- active session ID
				+ '		NULL,'											-- date last permission check
				+ '		NULL,'											-- is registration approved?
				+ '		NULL,'											-- date approved
				+ '		NULL,'											-- date denied
				+ '		NULL,'											-- approver id
				+ '		NULL,'											-- rejection comments
				+ '		U.excludeFromLeaderboards'						-- exclude from leaderboards?
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].' + '[tblUser] U'
				+ ' LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblUserProperties] UP ON UP.idUser = U.idUser'
				+ ' LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblUserDefinedUserFieldValue] UDUFV ON UDUFV.idUser = U.idUser'
				+ ' LEFT JOIN [' + @destinationDBName + '].[dbo].[InquisiqMigration_TimezoneIDMappings] TZ ON TZ.sourceID = U.idTimezone'
				+ ' WHERE U.idSite = '+ CONVERT(NVARCHAR, @idSiteSource)
		
		EXEC (@sql)
	  
		-- insert mapping for idUser 1 (admin)
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		1, '
				+ '		1, '
				+ '		''user'''
				+ ' ) ' 

		EXEC(@sql)

		-- insert idUser mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SU.idUser, '
				+ '		DU.idUser,'
				+ '		''user''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblUser] DU '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblUser] SU ON SU.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND SU.username = DU.username collate database_default AND SU.dtCreated = DU.dtCreated'
				+ ' WHERE DU.idSite IS NOT NULL AND DU.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SU.idUser IS NOT NULL AND DU.idUser IS NOT NULL'
		
		EXEC(@sql)

	/*****************************************************GROUP********************************************************/
		-- tblGroup
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblGroup]'
				+ ' ( '
				+ '		idSite, '
				+ '		name, '
				+ '		primaryGroupToken, '
				+ '		objectGUID, '
				+ '		distinguishedName, '
				+ '		avatar, '
				+ '		isSelfJoinAllowed, '
				+ '		shortDescription, '
				+ '		longDescription, '
				+ '		isFeedActive, '
				+ '		isFeedModerated, '
				+ '		membershipIsPublicized,'
				+ '		searchTags'
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- idSite
				+ '		G.name,'										-- name
				+ '		G.primaryGroupToken,'							-- primary group token
				+ '		G.objectGUID,'									-- object GUID
				+ '		G.distinguishedName,'							-- distinguished name
				+ '		NULL,'											-- avatar
				+ '		0,'												-- is self joined allowed?
				+ '		NULL,'											-- short description
				+ '		NULL,'											-- long description
				+ '		0,'												-- is feed active?
				+ '		0,'												-- is feed moderated?
				+ '		0,'												-- membership is publicized?
				+ '		NULL'											-- search tags
				+ '		FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblGroup] G'
				+ ' WHERE G.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)

		EXEC (@sql)

		-- insert idGroup mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SG.idGroup, '
				+ '		DG.idGroup,'
				+ '		''group''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblGroup] DG '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblGroup] SG ON SG.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND SG.name = DG.name collate database_default'
				+ ' WHERE DG.idSite IS NOT NULL AND DG.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SG.idGroup IS NOT NULL AND DG.idGroup IS NOT NULL'
						
		EXEC(@sql)

		-- tblGroupLanguage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblGroupLanguage]' 
				+ ' ( '
				+ '		idSite,'
				+ '		idGroup,'
				+ '		idLanguage,'
				+ '		name,'
				+ '		shortDescription,'
				+ '		longDescription,'
				+ '		searchTags'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		G.idGroup, '
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ', '
				+ '		G.name, '
				+ '		G.shortDescription, '
				+ '		G.longDescription, '
				+ '		G.searchTags '
				+ ' FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblGroup] G'
				+ ' WHERE G.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
		
		EXEC(@sql)

		/***************************************************ROLES / PERMISSIONS********************************************************/
		
		-- create Asentia roles based on Inquisiq Permissions and put them in mapping table

		/***************************************************** 
			Inquisiq Permission: System Manager (6)
				Asentia Permissions:
					System_AccountSettings (1)
					System_Configuration (2)
					System_Ecommerce (3)
					System_CouponCodes (4)
					System_TrackingCodes (5)
					System_EmailNotifications (6)
					System_API (7)
					System_xAPIEndpoints (8)
					System_Logs (9)
					System_WebMeetingIntegration (10)
		********************************************************/

		DECLARE @idRole int

		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRole]'
				+ ' ( '
				+ '		idSite,'
				+ '		name'
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		''System Manager'' '
				+ ' ) '
		EXEC(@sql)

		SET @idRole = @@IDENTITY

		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRoleToPermissionLink]'
				+ ' ( '
				+ '		idSite,'
				+ '		idRole,'
				+ '		idPermission'
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		1'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		2'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		3'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		4'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		5'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		6'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		7'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		8'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		9'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		10'
				+ ' ) '
		EXEC(@sql)

		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings]'
				+ ' ( '
				+ '		sourceID, '
				+ '		destinationID, '
				+ '		object '
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		6, '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		''role'' '
				+ ' ) '
		EXEC(@sql)

		/***************************************************** 
			Inquisiq Permission: Designer (7)
				Asentia Permissions:
					InterfaceAndLayout_HomePage (201)
					InterfaceAndLayout_Masthead (202)
					InterfaceAndLayout_Footer (203)
					InterfaceAndLayout_CSSEditor (204)
					InterfaceAndLayout_ImageEditor (205)
		********************************************************/
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRole]'
				+ ' ( '
				+ '		idSite,'
				+ '		name'
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		''Designer'' '
				+ ' ) '
		EXEC(@sql)

		SET @idRole = @@IDENTITY

		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRoleToPermissionLink]'
				+ ' ( '
				+ '		idSite,'
				+ '		idRole,'
				+ '		idPermission'
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		201'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		202'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		203'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		204'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		205'
				+ ' ) '
				
		EXEC(@sql)

		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings]'
				+ ' ( '
				+ '		sourceID, '
				+ '		destinationID, '
				+ '		object '
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		7, '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		''role'' '
				+ ' ) '
		EXEC(@sql)

		/***************************************************** 
			Inquisiq Permission: User Creator (1)
				Asentia Permissions:
					UsersAndGroups_UserCreator (102)
					UsersAndGroups_UserEditor (104)
		********************************************************/
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRole]'
				+ ' ( '
				+ '		idSite,'
				+ '		name'
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		''User Creator'' '
				+ ' ) '
		EXEC(@sql)

		SET @idRole = @@IDENTITY

		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRoleToPermissionLink]'
				+ ' ( '
				+ '		idSite,'
				+ '		idRole,'
				+ '		idPermission'
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		102'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		104'
				+ ' ) '
		EXEC(@sql)

		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings]'
				+ ' ( '
				+ '		sourceID, '
				+ '		destinationID, '
				+ '		object '
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		1, '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		''role'' '
				+ ' ) '
		EXEC(@sql)

		/***************************************************** 
			Inquisiq Permission: User Deleter (11)
				Asentia Permissions:
					UsersAndGroups_UserDeleter (103)
		********************************************************/
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRole]'
				+ ' ( '
				+ '		idSite,'
				+ '		name'
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		''User Deleter'' '
				+ ' ) '
		EXEC(@sql)

		SET @idRole = @@IDENTITY

		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRoleToPermissionLink]'
				+ ' ( '
				+ '		idSite,'
				+ '		idRole,'
				+ '		idPermission'
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		103'
				+ ' ) '
		EXEC(@sql)

		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings]'
				+ ' ( '
				+ '		sourceID, '
				+ '		destinationID, '
				+ '		object '
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		11, '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		''role'' '
				+ ' ) '
		EXEC(@sql)

		/***************************************************** 
			Inquisiq Permission: User Editor (10)
				Asentia Permissions:
					UsersAndGroups_UserEditor (104)
		********************************************************/
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRole]'
				+ ' ( '
				+ '		idSite,'
				+ '		name'
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		''User Editor'' '
				+ ' ) '
		EXEC(@sql)

		SET @idRole = @@IDENTITY

		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRoleToPermissionLink]'
				+ ' ( '
				+ '		idSite,'
				+ '		idRole,'
				+ '		idPermission'
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		104'
				+ ' ) '
		EXEC(@sql)

		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings]'
				+ ' ( '
				+ '		sourceID, '
				+ '		destinationID, '
				+ '		object '
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		10, '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		''role'' '
				+ ' ) '
		EXEC(@sql)

		/***************************************************** 
			Inquisiq Permission: User Manager (2)
				Asentia Permissions:
					UsersAndGroups_UserManager (105)
		********************************************************/
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRole]'
				+ ' ( '
				+ '		idSite,'
				+ '		name'
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		''User Manager'' '
				+ ' ) '
		EXEC(@sql)

		SET @idRole = @@IDENTITY

		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRoleToPermissionLink]'
				+ ' ( '
				+ '		idSite,'
				+ '		idRole,'
				+ '		idPermission'
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		105'
				+ ' ) '
		EXEC(@sql)

		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings]'
				+ ' ( '
				+ '		sourceID, '
				+ '		destinationID, '
				+ '		object '
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		2, '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		''role'' '
				+ ' ) '
		EXEC(@sql)

		/***************************************************** 
			Inquisiq Permission: User Impersonator (13)
				Asentia Permissions:
					UsersAndGroups_UserImpersonator (106)
		********************************************************/
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRole]'
				+ ' ( '
				+ '		idSite,'
				+ '		name'
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		''User Impersonator'' '
				+ ' ) '
		EXEC(@sql)

		SET @idRole = @@IDENTITY

		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRoleToPermissionLink]'
				+ ' ( '
				+ '		idSite,'
				+ '		idRole,'
				+ '		idPermission'
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		106'
				+ ' ) '
		EXEC(@sql)

		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings]'
				+ ' ( '
				+ '		sourceID, '
				+ '		destinationID, '
				+ '		object '
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		13, '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		''role'' '
				+ ' ) '
		EXEC(@sql)

		/***************************************************** 
			Inquisiq Permission: Group Creator (9)
				Asentia Permissions:
					UsersAndGroups_GroupManager (107)
					UsersAndGroups_GroupCreator (112)
					UsersAndGroups_GroupDeleter (113)
					UsersAndGroups_GroupEditor (114)
		********************************************************/
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRole]'
				+ ' ( '
				+ '		idSite,'
				+ '		name'
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		''Group Creator'' '
				+ ' ) '
		EXEC(@sql)

		SET @idRole = @@IDENTITY

		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRoleToPermissionLink]'
				+ ' ( '
				+ '		idSite,'
				+ '		idRole,'
				+ '		idPermission'
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		107'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		112'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		113'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		114'
				+ ' ) '
		EXEC(@sql)

		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings]'
				+ ' ( '
				+ '		sourceID, '
				+ '		destinationID, '
				+ '		object '
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		9, '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		''role'' '
				+ ' ) '
		EXEC(@sql)

		/***************************************************** 
			Inquisiq Permission: Content Manager (8)
				Asentia Permissions:
					LearningAssets_CourseCatalog (301)
					LearningAssets_CourseContentManager (302)
					LearningAssets_LearningPathContentManager (303)
					LearningAssets_ContentPackageManager (304)
					LearningAssets_CertificateTemplateManager (305)
					LearningAssets_QuizAndSurveyManager (309)
		********************************************************/
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRole]'
				+ ' ( '
				+ '		idSite,'
				+ '		name'
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		''Content Manager'' '
				+ ' ) '
		EXEC(@sql)

		SET @idRole = @@IDENTITY

		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRoleToPermissionLink]'
				+ ' ( '
				+ '		idSite,'
				+ '		idRole,'
				+ '		idPermission'
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		301'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		302'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		303'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		304'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		305'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		309'
				+ ' ) '
		EXEC(@sql)

		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings]'
				+ ' ( '
				+ '		sourceID, '
				+ '		destinationID, '
				+ '		object '
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		8, '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		''role'' '
				+ ' ) '
		EXEC(@sql)

		/***************************************************** 
			Inquisiq Permission: Reporter (3, 12)
				Asentia Permissions:
					Reporting_Reporter_UserDemographics (401)
					Reporting_Reporter_UserCourseTranscripts (402)
					Reporting_Reporter_CatalogAndCourseInformation (403)
					Reporting_Reporter_Certificates (404)
					Reporting_Reporter_XAPI (405)
					Reporting_Reporter_UserLearningPathTranscripts (406)
					Reporting_Reporter_UserInstructorLedTrainingTranscripts (407)
					Reporting_Reporter_Purchases (408)
					Reporting_Reporter_UserCertificateTranscripts (409)
		********************************************************/
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRole]'
				+ ' ( '
				+ '		idSite,'
				+ '		name'
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		''Reporter'' '
				+ ' ) '
		EXEC(@sql)

		SET @idRole = @@IDENTITY

		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRoleToPermissionLink]'
				+ ' ( '
				+ '		idSite,'
				+ '		idRole,'
				+ '		idPermission'
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		401'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		402'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		403'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		404'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		405'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		406'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		407'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		408'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		409'
				+ ' ) '
		EXEC(@sql)

		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings]'
				+ ' ( '
				+ '		sourceID, '
				+ '		destinationID, '
				+ '		object '
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		3, '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		''role'' '
				+ ' ) '
		EXEC(@sql)

		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings]'
				+ ' ( '
				+ '		sourceID, '
				+ '		destinationID, '
				+ '		object '
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		12, '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		''role'' '
				+ ' ) '
		EXEC(@sql)

		-- map user rights to roles
	
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblUserToRoleLink]'
				+ ' ( '
				+ '		idSite,'
				+ '		idUser,'
				+ '		idRole,'
				+ '		created'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		UM.destinationID, '		-- user id					
				+ '		RM.destinationID, '		-- role id
				+ '		GETDATE() '
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblUserRight] UR'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] UM ON UM.sourceID = UR.idUser AND UM.object = ''user'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] RM ON RM.sourceID = UR.idUserRight AND RM.object = ''role'''
				+ ' WHERE UM.destinationID IS NOT NULL AND RM.destinationID IS NOT NULL'
				
		EXEC(@sql)

		-- map group rights to roles (assign the roles to individual users in the groups)

		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblUserToRoleLink]'
				+ ' ( '
				+ '		idSite,'
				+ '		idUser,'
				+ '		idRole,'
				+ '		created'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		UM.destinationID, '		-- user id					
				+ '		RM.destinationID, '		-- role id
				+ '		GETDATE() '
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblGroupRight] GR'
				+ ' LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].[tblUserGroupLink] UGL ON UGL.idGroup = GR.idGroup'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] UM ON UM.sourceID = UGL.idUser AND UM.object = ''user'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] RM ON RM.sourceID = GR.idGroupRight AND RM.object = ''role'''
				+ ' WHERE UM.destinationID IS NOT NULL AND RM.destinationID IS NOT NULL'
				
		EXEC(@sql)
		  
		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemCopyInquisiqToAsentiaUser_Success'
		
	SET XACT_ABORT OFF

END
GO
-- =====================================================================
-- PROCEDURE: [System.GetBillingTotalsAndInformation]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.GetBillingTotalsAndInformation]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.GetBillingTotalsAndInformation]
GO

/*

Returns the information and totals needed for billing Asentia clients. 

*/

CREATE PROCEDURE [System.GetBillingTotalsAndInformation]
AS
	BEGIN
	SET NOCOUNT ON
	
	/*

	Get all account contracts and place them into a temporary table.

	*/

	DECLARE @AccountContractInfoAndTotals TABLE (
		idAccount						INT				NOT NULL,
		idAccountContract				INT				NOT NULL,
		serverName						NVARCHAR(255)	NOT NULL,
		databaseName					NVARCHAR(255)	NOT NULL,
		company							NVARCHAR(255)	NOT NULL,
		isReseller						BIT				NULL,
		allowedUsers					INT				NULL,
		numUsersLoggedIn				INT				NULL,
		disabledUserRatio				FLOAT			NULL,
		disabledUserThresholdInterval	INT				NULL,
		disabledUserThresholdTimeframe	NVARCHAR(4)		NULL,
		diskSpaceLimitKB				INT				NULL,
		portalIdList					NVARCHAR(MAX)	NOT NULL,
		portalNameList					NVARCHAR(MAX)	NOT NULL,
		contractNotes					NVARCHAR(MAX)	NULL,
		activeUsers						INT				NULL,
		disabledUsers					INT				NULL,
		totalUsers						INT				NULL,
		totalDiskSpaceKB				INT				NULL
	)

	INSERT INTO @AccountContractInfoAndTotals (
		idAccount,
		idAccountContract,
		serverName,
		databaseName,
		company,
		--isReseller, -- not yet implemented
		allowedUsers,		
		disabledUserRatio,
		disabledUserThresholdInterval,
		disabledUserThresholdTimeframe,
		diskSpaceLimitKB,
		portalIdList,
		portalNameList,
		contractNotes	
	)
	SELECT
		A.idAccount,
		AC.idAccountContract,
		DS.serverName,
		A.databaseName,
		A.company,
		--A.isReseller -- not yet implemented
		AC.allowedUsers,
		AC.disabledUserRatio,
		AC.disabledUserThresholdInterval,
		AC.disabledUserThresholdTimeframe,
		AC.diskSpaceLimitKB,
		STUFF((
			SELECT ',' + CONVERT(NVARCHAR, ACPLS.idPortal)
			FROM tblAccountContractToPortalLink ACPLS
			WHERE ACPLS.idAccountContract = AC.idAccountContract
			ORDER BY ACPLS.idPortal
			FOR XML PATH('')
		), 1, 1, '') AS portalIdList,
		STUFF((
			SELECT ', ' + ACPLS.portalName
			FROM tblAccountContractToPortalLink ACPLS
			WHERE ACPLS.idAccountContract = AC.idAccountContract
			ORDER BY ACPLS.portalName
			FOR XML PATH('')
		), 1, 2, '') AS portalNameList,
		AC.contractNotes	
	FROM tblAccountContract AC
	LEFT JOIN tblAccount A ON A.idAccount = AC.idAccount
	LEFT JOIN tblDatabaseServer DS ON DS.idDatabaseServer = A.idDatabaseServer
	WHERE A.isActive = 1 AND A.isDeleted = 0

	/*

	Cursor through all account contracts, and get the user counts and disk space used for each.

	*/

	-- gets the following counts across all portals in the contract
	--		- active users: defined as currently active users or disabled users who have been disabled 
	--		  for less than the contract's disabled threshold
	--
	--		- disabled users: defined as disabled users who have been disabled for longer than the
	--		  contract's disabled threshold, or active users who have been expired for longer than
	--		  the contract's disabled threshold
	--
	--		- total users: the total number of users in the site, used as a reference for accuracy
	--		  of the other counts, i.e. active + disabled = total
	--
	--		- total disk space in kb: the total of all disk space used by objects in the system

	DECLARE @idAccountContract				INT
	DECLARE @databaseServer					NVARCHAR(255)
	DECLARE @databaseName					NVARCHAR(255)
	DECLARE @idSiteList						NVARCHAR(MAX)
	DECLARE @disabledUserRatio				FLOAT
	DECLARE @disabledUserThresholdInterval	INT
	DECLARE @disabledUserThresholdTimeframe	NVARCHAR(4)

	DECLARE @params							NVARCHAR(MAX)
	DECLARE @sql							NVARCHAR(MAX)
	DECLARE @activeUsers					INT
	DECLARE @disabledUsers					INT
	DECLARE @totalUsers						INT
	DECLARE @totalDiskSpaceKB				INT
	DECLARE @numUsersLoggedIn				INT

	SET @params = '@activeUsers INT OUTPUT, '
				+ '@disabledUsers INT OUTPUT, '
				+ '@totalUsers INT OUTPUT, '
				+ '@totalDiskSpaceKB INT OUTPUT, '
				+ '@numUsersLoggedIn INT OUTPUT '
	
	-- declare cursor
	DECLARE accountContractsCursor CURSOR LOCAL FOR
		SELECT 
			idAccountContract,
			serverName,
			databaseName,
			portalIdList,
			disabledUserThresholdInterval,
			disabledUserThresholdTimeframe
		FROM @AccountContractInfoAndTotals

	OPEN accountContractsCursor

	FETCH NEXT FROM accountContractsCursor INTO 
		@idAccountContract,
		@databaseServer,
		@databaseName,
		@idSiteList,
		@disabledUserThresholdInterval,
		@disabledUserThresholdTimeframe

	-- loop through cursor, getting the counts from the account's Asentia database, and updating in the temp table
	WHILE @@FETCH_STATUS = 0
		BEGIN

		-- get the counts
		SET @sql = 'SELECT '
				 + ' '
				 + '	@activeUsers = (SELECT COUNT(1) '
				 + '	 FROM [' + @databaseServer + '].[' + @databaseName + '].dbo.tblUser U '
				 + '	 WHERE U.idSite IN (' + CONVERT(NVARCHAR, @idSiteList) + ') '
				 + '	 AND (U.isDeleted IS NULL OR U.isDeleted = 0) '
				 + '	 AND ( '
				 + '		  (U.isActive = 1 AND (dtExpires IS NULL OR dtExpires > DATEADD(' + CONVERT(NVARCHAR, ISNULL(@disabledUserThresholdTimeframe, 'yyyy')) + ', -' + CONVERT(NVARCHAR, ISNULL(@disabledUserThresholdInterval, 0)) + ', GETUTCDATE()))) '
				 + '		  OR '
				 + '		  (U.isActive = 0 AND  U.dtMarkedDisabled > DATEADD(' + CONVERT(NVARCHAR, ISNULL(@disabledUserThresholdTimeframe, 'yyyy')) + ', -' + CONVERT(NVARCHAR, ISNULL(@disabledUserThresholdInterval, 0)) + ', GETUTCDATE())) '
				 + '		 ) '
				 + '	 ), '
				 + ' '
				 + '	@disabledUsers = (SELECT COUNT(1) '
				 + '	 FROM [' + @databaseServer + '].[' + @databaseName + '].dbo.tblUser U '
				 + '	 WHERE U.idSite IN (' + CONVERT(NVARCHAR, @idSiteList) + ') '
				 + '	 AND (U.isDeleted IS NULL OR U.isDeleted = 0) '
				 + '	 AND ( '
				 + '		  (U.isActive = 0 AND U.dtMarkedDisabled <= DATEADD(' + CONVERT(NVARCHAR, ISNULL(@disabledUserThresholdTimeframe, 'yyyy')) + ', -' + CONVERT(NVARCHAR, ISNULL(@disabledUserThresholdInterval, 0)) + ', GETUTCDATE())) '
				 + '		  OR '
				 + '		  (U.isActive = 1 AND dtExpires IS NOT NULL AND dtExpires <= DATEADD(' + CONVERT(NVARCHAR, ISNULL(@disabledUserThresholdTimeframe, 'yyyy')) + ', -' + CONVERT(NVARCHAR, ISNULL(@disabledUserThresholdInterval, 0)) + ', GETUTCDATE())) '
				 + '		 ) '
				 + '	), '
				 + ' '
				 + '	@totalUsers = (SELECT COUNT(1) '
				 + '	 FROM [' + @databaseServer + '].[' + @databaseName + '].dbo.tblUser U '
				 + '	 WHERE U.idSite IN (' + CONVERT(NVARCHAR, @idSiteList) + ') '
				 + '	 AND (U.isDeleted IS NULL OR U.isDeleted = 0) '
				 + '	), '
				 + ' '
				 + '	@totalDiskSpaceKB = (SELECT '
				 + '		(SELECT ISNULL(SUM(kb), 0) FROM [' + @databaseServer + '].[' + @databaseName + '].dbo.tblContentPackage CP WHERE CP.idSite IN (' + CONVERT(NVARCHAR, @idSiteList) + ')) '
				 + '		+ '
				 + '		(SELECT ISNULL(SUM(kb), 0) FROM [' + @databaseServer + '].[' + @databaseName + '].dbo.tblDocumentRepositoryItem DRI WHERE DRI.idSite IN (' + CONVERT(NVARCHAR, @idSiteList) + ') AND (DRI.isDeleted IS NULL OR DRI.isDeleted = 0)) '
				 + '		+ '
				 + '		(SELECT ISNULL(SUM(htmlKb), 0) + ISNULL(SUM(csvKb), 0) + ISNULL(SUM(pdfKb), 0) FROM [' + @databaseServer + '].[' + @databaseName + '].dbo.tblReportFile RF WHERE RF.idSite IN (' + CONVERT(NVARCHAR, @idSiteList) + ')) '
				 + '	), '
				 + ' '
				 + '	@numUsersLoggedIn = (SELECT COUNT(1) '
				 + '	 FROM [' + @databaseServer + '].[' + @databaseName + '].dbo.tblUser U '
				 + '	 WHERE U.idSite IN (' + CONVERT(NVARCHAR, @idSiteList) + ') '
				 + '	 AND (U.isDeleted IS NULL OR U.isDeleted = 0) AND U.dtLastLogin IS NOT NULL AND U.dtLastLogin >= DATEADD(MONTH, -1, GETUTCDATE()) '
				 + '	) '

		EXECUTE sp_executesql @sql, @params, @activeUsers OUTPUT, @disabledUsers OUTPUT, @totalUsers OUTPUT, @totalDiskSpaceKB OUTPUT, @numUsersLoggedIn OUTPUT

		-- update temp table
		UPDATE @AccountContractInfoAndTotals SET
			activeUsers = @activeUsers,
			disabledUsers = @disabledUsers,
			totalUsers = @totalUsers,
			totalDiskSpaceKB = @totalDiskSpaceKB,
			numUsersLoggedIn = @numUsersLoggedIn
		WHERE idAccountContract = @idAccountContract

		-- get the next row in the cursor
		FETCH NEXT FROM accountContractsCursor INTO 
			@idAccountContract,
			@databaseServer,
			@databaseName,
			@idSiteList,
			@disabledUserThresholdInterval,
			@disabledUserThresholdTimeframe

		END

	-- kill the cursor
	CLOSE accountContractsCursor
	DEALLOCATE accountContractsCursor

	-- return the data
	SELECT
		company AS [Client/Contract],
		--CASE WHEN isReseller = 1 THEN 'YES' ELSE 'NO' END AS [Is Reseller], -- not yet implemented
		activeUsers + CEILING(disabledUsers * ISNULL(disabledUserRatio, 1)) AS [Total Users],
		CASE WHEN allowedUsers IS NOT NULL THEN CONVERT(NVARCHAR, allowedUsers) ELSE 'NO LIMIT' END AS [Allowed Users],
		CASE WHEN numUsersLoggedIn IS NOT NULL THEN numUsersLoggedIn ELSE 0 END AS [# Users Logged In],
		CASE WHEN allowedUsers IS NOT NULL THEN CONVERT(NVARCHAR, ROUND(((activeUsers + CEILING(disabledUsers * ISNULL(disabledUserRatio, 1))) / allowedUsers) * 100, 2)) ELSE 'N/A' END AS [% of User Limit Used],		
		activeUsers AS [Active Users],
		disabledUsers AS [Disabled Users],
		CASE WHEN disabledUserRatio IS NOT NULL THEN disabledUserRatio ELSE 1 END AS [Disabled User Ratio],
		CASE WHEN totalDiskSpaceKB > 0 THEN dbo.[RawKBToFriendlyFormat](totalDiskSpaceKB) ELSE 'NONE' END AS [Total Disk Used],
		CASE WHEN diskSpaceLimitKB IS NOT NULL THEN dbo.[RawKBToFriendlyFormat](diskSpaceLimitKB) ELSE 'NO LIMIT' END AS [Total Disk Allowed],
		CASE WHEN diskSpaceLimitKB IS NOT NULL THEN CONVERT(NVARCHAR, ROUND((CONVERT(FLOAT, totalDiskSpaceKB) / CONVERT(FLOAT, diskSpaceLimitKB)) * 100, 2)) ELSE 'N/A' END AS [% of Disk Limit Used],
		portalNameList AS [Included Portals],
		contractNotes AS [Notes]
	FROM @AccountContractInfoAndTotals

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.GetContentPackagePathsForFileSystemCleanup]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.GetContentPackagePathsForFileSystemCleanup]
GO


/*

Get Object IDs of the objects in the database

*/
CREATE PROCEDURE [dbo].[System.GetContentPackagePathsForFileSystemCleanup]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,	
	@DBServerName			NVARCHAR(50),
	@DatabaseName			NVARCHAR(50)	
)
AS

BEGIN

    SET NOCOUNT ON
    SET XACT_ABORT ON    
    	
	DECLARE @sql NVARCHAR(MAX)
	  
		SET @sql = 'SELECT '
				 + '	[path] '
				 + 'FROM [' + @DBServerName + '].[' + @DatabaseName + '].[dbo].[tblContentPackage]'
						
		
		EXEC(@sql)
		
		
		
		  
		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemGetContentPackagePathsForFileSystemCleanup_Success'
		
	SET XACT_ABORT OFF

END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.GetDocumentRepositoryItemObjectIdsForFileSystemCleanup]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.GetDocumentRepositoryItemObjectIdsForFileSystemCleanup]
GO


/*

Get Object IDs of the objects in the database

*/
CREATE PROCEDURE [dbo].[System.GetDocumentRepositoryItemObjectIdsForFileSystemCleanup]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,	
	@DBServerName			NVARCHAR(50),
	@DatabaseName			NVARCHAR(50)	
)
AS

BEGIN

    SET NOCOUNT ON
    SET XACT_ABORT ON    
    	
	DECLARE @sql NVARCHAR(MAX)

	-- Create temp table to hold object IDs
	CREATE TABLE #ObjectIDs
	   (
		  [objectId] INT NOT NULL,
		  [objectName] NVARCHAR(100) NOT NULL,
		  [siteHostname] NVARCHAR(100) NOT NULL
	   )
	  

		-- add object ids to the temp table
				
		SET @sql = 'INSERT INTO [#ObjectIDs]'
				+ ' ( '
				+ '		objectId, '
				+ '		objectName, '
				+ '		siteHostname '
				+ ' ) '
				+ ' SELECT ' 
				+ '		DRI.idObject,'
				+ '		CASE WHEN DRI.idDocumentRepositoryObjectType = 1 THEN ''group'''
				+ '		     WHEN DRI.idDocumentRepositoryObjectType = 2 THEN ''course'''
				+ '			 WHEN DRI.idDocumentRepositoryObjectType = 3 THEN ''learningpath'''
				+ '			 ELSE ''user'' END,'
				+ '		S.hostname'
				+ ' FROM  [' + @DBServerName + '].[' + @DatabaseName + '].[dbo].' + '[tblDocumentRepositoryItem] DRI'
				+ ' LEFT JOIN [' + @DBServerName + '].['+ @DatabaseName + '].[dbo].' + '[tblSite] S'
				+ ' ON S.idSite = DRI.idSite'
		
		EXEC (@sql)
	  
		
		SELECT objectId, objectName, siteHostname FROM [#ObjectIDs]
		
		
		-- Drop temporary object IDs table
		
		DROP TABLE #ObjectIDs
		  
		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemGetDocumentRepositoryItemObjectIdsForFileSystemCleanup_Success'
		
	SET XACT_ABORT OFF

END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.GetObjectIdsForFileSystemCleanup]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.GetObjectIdsForFileSystemCleanup]
GO


/*

Get Object IDs of the objects in the database

*/
CREATE PROCEDURE [dbo].[System.GetObjectIdsForFileSystemCleanup]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,	
	@DBServerName			NVARCHAR(50),
	@DatabaseName			NVARCHAR(50)	
)
AS

BEGIN

    SET NOCOUNT ON
    SET XACT_ABORT ON    
    	
	DECLARE @sql NVARCHAR(MAX)

	-- Create temp table to hold object IDs
	CREATE TABLE #ObjectIDs
	   (
		  [objectId] INT NOT NULL,
		  [objectName] NVARCHAR(100) NOT NULL,
		  [siteHostname] NVARCHAR(100) NOT NULL
	   )
	  
	/************************************************ CERTIFICATES ******************************************************/
		
		SET @sql = 'INSERT INTO [#ObjectIDs]'
				+ ' ( '
				+ '		objectId, '
				+ '		objectName, '
				+ '		siteHostname '
				+ ' ) '
				+ ' SELECT ' 
				+ '		C.idCertificate,'
				+ '		''certificates'','
				+ '		S.hostname'
				+ ' FROM  [' + @DBServerName + '].['+ @DatabaseName + '].[dbo].' + '[tblCertificate] C'
				+ ' LEFT JOIN [' + @DBServerName + '].['+ @DatabaseName + '].[dbo].' + '[tblSite] S'
				+ ' ON S.idSite = C.idSite'
		
		EXEC (@sql)
	  
	  /************************************************ COURSES ******************************************************/
		
		SET @sql = 'INSERT INTO [#ObjectIDs]'
				+ ' ( '
				+ '		objectId, '
				+ '		objectName, '
				+ '		siteHostname '
				+ ' ) '
				+ ' SELECT ' 
				+ '		C.idCourse,'
				+ '		''courses'','
				+ '		S.hostname'
				+ ' FROM  [' + @DBServerName + '].['+ @DatabaseName + '].[dbo].' + '[tblCourse] C'
				+ ' LEFT JOIN [' + @DBServerName + '].['+ @DatabaseName + '].[dbo].' + '[tblSite] S'
				+ ' ON S.idSite = C.idSite'
		
		EXEC (@sql)

		/************************************************ EMAIL NOTIFICATIONS ******************************************************/
		
		SET @sql = 'INSERT INTO [#ObjectIDs]'
				+ ' ( '
				+ '		objectId, '
				+ '		objectName, '
				+ '		siteHostname '
				+ ' ) '
				+ ' SELECT ' 
				+ '		EN.idEventEmailNotification,'
				+ '		''emailNotifications'','
				+ '		S.hostname'
				+ ' FROM  [' + @DBServerName + '].['+ @DatabaseName + '].[dbo].' + '[tblEventEmailNotification] EN'
				+ ' LEFT JOIN [' + @DBServerName + '].['+ @DatabaseName + '].[dbo].' + '[tblSite] S'
				+ ' ON S.idSite = EN.idSite'
		
		EXEC (@sql)

		/************************************************ GROUPS ******************************************************/
		
		SET @sql = 'INSERT INTO [#ObjectIDs]'
				+ ' ( '
				+ '		objectId, '
				+ '		objectName, '
				+ '		siteHostname '
				+ ' ) '
				+ ' SELECT ' 
				+ '		G.idGroup,'
				+ '		''groups'','
				+ '		S.hostname'
				+ ' FROM  [' + @DBServerName + '].['+ @DatabaseName + '].[dbo].' + '[tblGroup] G'
				+ ' LEFT JOIN [' + @DBServerName + '].['+ @DatabaseName + '].[dbo].' + '[tblSite] S'
				+ ' ON S.idSite = G.idSite'
		
		EXEC (@sql)

		/************************************************ LEARNING PATHS ******************************************************/
		
		SET @sql = 'INSERT INTO [#ObjectIDs]'
				+ ' ( '
				+ '		objectId, '
				+ '		objectName, '
				+ '		siteHostname '
				+ ' ) '
				+ ' SELECT ' 
				+ '		LP.idLearningPath,'
				+ '		''learningPaths'','
				+ '		S.hostname'
				+ ' FROM  [' + @DBServerName + '].['+ @DatabaseName + '].[dbo].' + '[tblLearningPath] LP'
				+ ' LEFT JOIN [' + @DBServerName + '].['+ @DatabaseName + '].[dbo].' + '[tblSite] S'
				+ ' ON S.idSite = LP.idSite'
		
		EXEC (@sql)

		/************************************************ ILT ******************************************************/
		
		SET @sql = 'INSERT INTO [#ObjectIDs]'
				+ ' ( '
				+ '		objectId, '
				+ '		objectName, '
				+ '		siteHostname '
				+ ' ) '
				+ ' SELECT ' 
				+ '		ILT.idStandUpTraining,'
				+ '		''standuptraining'','
				+ '		S.hostname'
				+ ' FROM  [' + @DBServerName + '].['+ @DatabaseName + '].[dbo].' + '[tblStandUpTraining] ILT'
				+ ' LEFT JOIN [' + @DBServerName + '].['+ @DatabaseName + '].[dbo].' + '[tblSite] S'
				+ ' ON S.idSite = ILT.idSite'
		
		EXEC (@sql)

		/************************************************ USERS ******************************************************/
		
		SET @sql = 'INSERT INTO [#ObjectIDs]'
				+ ' ( '
				+ '		objectId, '
				+ '		objectName, '
				+ '		siteHostname '
				+ ' ) '
				+ ' SELECT ' 
				+ '		U.idUser,'
				+ '		''users'','
				+ '		S.hostname'
				+ ' FROM  [' + @DBServerName + '].['+ @DatabaseName + '].[dbo].' + '[tblUser] U'
				+ ' LEFT JOIN [' + @DBServerName + '].['+ @DatabaseName + '].[dbo].' + '[tblSite] S'
				+ ' ON S.idSite = U.idSite'
		
		EXEC (@sql)
	  
		
		SELECT objectId, objectName, siteHostname FROM [#ObjectIDs]
		
		
		-- Drop temporary object IDs table
		
		DROP TABLE #ObjectIDs
		  
		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemGetObjectIdsForFileSystemCleanup_Success'
		
	SET XACT_ABORT OFF

END
GO
-- =====================================================================
-- PROCEDURE: [System.MatchRequestedUrlToAccount]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF exists (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.MatchRequestedUrlToAccount]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.MatchRequestedUrlToAccount]
GO

/*

Matches a url to an account id.

*/
CREATE PROCEDURE [System.MatchRequestedUrlToAccount]
(
	@Return_Code				INT					OUTPUT,
	@Error_Description_Code		NVARCHAR(50)		OUTPUT,

	@idAccount					INT					OUTPUT,
	@url						NVARCHAR(255)
)
AS
	BEGIN
	SET NOCOUNT ON
		SELECT DISTINCT @idAccount = idAccount
		FROM tblAccountToDomainAliasLink
		WHERE @url = hostname
		OR @url = domain
		
		IF (@idAccount is null)
			BEGIN
			SELECT @idAccount = 1
			END
			
		SELECT @Return_Code = 0 --(0 is 'success')
		RETURN
	END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


-- =====================================================================
-- PROCEDURE: [Concurrency.GetConcurrentCountsForTimespan]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Concurrency.GetConcurrentCountsForTimespan]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Concurrency.GetConcurrentCountsForTimespan]
GO

/*

Returns concurrency history for a specified timespan, or the most current concurrency 
count when the "current" parameter is set.

Note, NULL timespans will return everything before, everything after, or just everything.

*/

CREATE PROCEDURE [Concurrency.GetConcurrentCountsForTimespan]
(
	@dtTimespanStart	DATETIME	= NULL,
	@dtTimeSpanEnd		DATETIME	= NULL,
	@getLatestCount		BIT			= 0
)
AS
	
	BEGIN
	SET NOCOUNT ON
		
	IF (@getLatestCount = 1) -- this will get the latest (current) count
		BEGIN

		SELECT 
			SUM(concurrentUsers), 
			[timestamp]
		FROM tblConcurrency
		WHERE [timestamp] = (SELECT MAX([timestamp]) FROM tblConcurrency)
		GROUP BY [timestamp]
		ORDER BY [timestamp]

		END

	ELSE -- this will get all counts within a timespan
		BEGIN

		/* 
		
		if the timespan is not logical, i.e. start not before end, 
		just null out the timespan start and end

		*/

		IF (@dtTimespanStart IS NOT NULL AND @dtTimeSpanEnd IS NOT NULL AND @dtTimeSpanEnd < @dtTimespanStart)
			BEGIN
			SET @dtTimespanStart = NULL
			SET @dtTimeSpanEnd = NULL
			END

		/*

		get the counts within timespan
		 - if both start and end are null, the entire history is returned
		 - if start specified but no end, everything after start is returned
		 - if end but no start, everything before end is returned
		 - if both specified, everything in between is returned
		 - all counts are inclusive (___ than or equal to)

		*/

		IF (@dtTimespanStart IS NULL AND @dtTimeSpanEnd IS NULL) -- both null
			BEGIN

			SELECT 
				SUM(concurrentUsers), 
				[timestamp] 
			FROM tblConcurrency 
			GROUP BY [timestamp]
			ORDER BY [timestamp]

			END

		IF (@dtTimespanStart IS NOT NULL AND @dtTimeSpanEnd IS NULL) -- only start
			BEGIN

			SELECT 
				SUM(concurrentUsers), 
				[timestamp] 
			FROM tblConcurrency 
			WHERE [timestamp] >= @dtTimespanStart
			GROUP BY [timestamp]
			ORDER BY [timestamp]

			END

		IF (@dtTimespanEnd IS NOT NULL AND @dtTimeSpanStart IS NULL) -- only end
			BEGIN

			SELECT 
				SUM(concurrentUsers), 
				[timestamp] 
			FROM tblConcurrency 
			WHERE [timestamp] <= @dtTimespanEnd
			GROUP BY [timestamp]
			ORDER BY [timestamp]

			END

		IF (@dtTimespanEnd IS NOT NULL AND @dtTimeSpanStart IS NOT NULL) -- both
			BEGIN

			SELECT 
				SUM(concurrentUsers), 
				[timestamp] 
			FROM tblConcurrency 
			WHERE [timestamp] >= @dtTimespanStart
			AND [timestamp] <= @dtTimespanEnd
			GROUP BY [timestamp]
			ORDER BY [timestamp]

			END

		END
		
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

-- =====================================================================
-- PROCEDURE: [Concurrency.LogConcurrency]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Concurrency.LogConcurrency]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Concurrency.LogConcurrency]
GO

/*

Logs concurrent user counts for all account databases found in customer manager
that are active and not deleted.

*/

CREATE PROCEDURE [Concurrency.LogConcurrency]
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idAccount		INT
	DECLARE @serverName		NVARCHAR(255)
	DECLARE @databaseName	NVARCHAR(255)
	DECLARE @SQL			NVARCHAR(MAX)
	DECLARE @utcNow			DATETIME
	DECLARE @utcNowString	NVARCHAR(50)

	SET @utcNow = GETUTCDATE()
	SET @utcNowString = CONVERT(NVARCHAR(10), DATEPART(year, @utcNow)) + '-' +
					RIGHT('0' + CONVERT(NVARCHAR(10), DATEPART(month, @utcNow)), 2) + '-' +
					RIGHT('0' + CONVERT(NVARCHAR(10), DATEPART(day, @utcNow)), 2) + ' ' +
					RIGHT('0' + CONVERT(NVARCHAR(10), DATEPART(hour, @utcNow)), 2) + ':' +
					RIGHT('0' + CONVERT(NVARCHAR(10), DATEPART(minute, @utcNow)), 2) + ':' +
					RIGHT('0' + CONVERT(NVARCHAR(10), DATEPART(second, @utcNow)), 2)

	DECLARE C CURSOR FOR 
	
		SELECT
			A.idAccount,
			LOWER(DBS.networkName),
			A.databaseName
		FROM tblAccount A
		LEFT JOIN tblDatabaseServer DBS ON DBS.idDatabaseServer = A.idDatabaseServer
		WHERE A.idAccount > 1
		AND A.isActive = 1
		AND (A.isDeleted IS NULL OR A.isDeleted = 0)

	OPEN C

	FETCH NEXT FROM C INTO @idAccount, @serverName, @databaseName

	WHILE @@FETCH_STATUS = 0
	BEGIN
	
		IF (@serverName = 'asentia-sql01')	-- this is the database that the customer manager resides on, and therefore local
											-- so we do not need (and cannot have) a database server reference in the query
			BEGIN

			SET @SQL = '
				INSERT INTO tblConcurrency (
					idAccount,
					concurrentUsers,
					[timestamp]
				)
				SELECT 
					' + CONVERT(NVARCHAR, @idAccount) + ',
					COUNT(1),
					''' + @utcNowString + '''
				FROM 
				[' + @databaseName + '].[dbo].[tblUser] WHERE dtSessionExpires > GETUTCDATE()'

			END

		ELSE
			BEGIN

			SET @SQL = '
				INSERT INTO tblConcurrency (
					idAccount,
					concurrentUsers,
					[timestamp]
				)
				SELECT 
					' + CONVERT(NVARCHAR, @idAccount) + ',
					COUNT(1),
					''' + @utcNowString + '''
				FROM 
				[' + @serverName + '].[' + @databaseName + '].[dbo].[tblUser] WHERE dtSessionExpires > GETUTCDATE()'

			END
	
		EXECUTE sp_executesql @SQL

		FETCH NEXT FROM C INTO @idAccount, @serverName, @databaseName

	END

	CLOSE C
	DEALLOCATE C
		
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

