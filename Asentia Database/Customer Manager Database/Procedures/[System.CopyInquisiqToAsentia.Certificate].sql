
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.CopyInquisiqToAsentia.Certificate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.CopyInquisiqToAsentia.Certificate]
GO


/*

Copy source site data from Inquisiq to destination site in Asentia

*/
CREATE PROCEDURE [dbo].[System.CopyInquisiqToAsentia.Certificate]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,	
	@idAccount				INT,
	@idCaller				INT				= 1,
	@idSiteDefaultLang		INT				= 57,		-- English is ID 57	
	@siteHostname			NVARCHAR(255),
	@idSiteDestination		INT,
	@destinationHostname	NVARCHAR(255),
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@idSiteSource			INT,
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50)	
)
AS

BEGIN

    SET NOCOUNT ON
    SET XACT_ABORT ON    
    	
	DECLARE @sql NVARCHAR(MAX)

	/*********************************************************CERTIFICATE**************************************************/
		-- tblCertificate
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCertificate]'
				+ ' ( '
				+ '		idSite, '
				+ '		name, '
				+ '		issuingOrganization, '
				+ '		description, '
				+ '		credits, '
				+ '		filename, '
				+ '		isActive, '
				+ '		activationDate, '
				+ '		expiresInterval,'
				+ '		expiresTimeframe,'
				+ '		objectType,'
				+ '		idObject,'
				+ '		isDeleted,'
				+ '		dtDeleted,'
				+ '		code,'
				+ '		reissueBasedOnMostRecentCompletion'
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','									-- idSite
				+ '		C.name,'																		-- name
				+ '		C.issuingOrganization,'															-- title
				+ '		C.description,'																	-- description
				+ '		C.credits,'																		-- credits
				+ '		REPLACE(C.filename, ''/_config/' + @siteHostname + '/_images/certs/'', ''''),'	-- filename
				+ '		C.isActive,'																	-- is certificate active?
				+ '		C.activationDate,'																-- activation date
				+ '		C.expiresInterval,'																-- expires interval
				+ '		C.expiresTimespan,'																-- expires timeframe
				+ '		1,'																				-- object type (1 = course)
				+ '		CM.destinationID,'																-- object id (course id)
				+ '		0,'																				-- is deleted?
				+ '		NULL,'																			-- date deleted?
				+ '		C.code,'																		-- code
				+ '		NULL'																			-- reissue based on most recent completion
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCertificate] C'
				+ ' LEFT JOIN  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCourseCertificateLink] CCL ON CCL.idCertificate = C.idCertificate'
				+ ' LEFT JOIN  [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.sourceID = CCL.idCourse AND CM.object = ''course'''
				+ ' WHERE C.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND CM.destinationID IS NOT NULL'
		
		EXEC (@sql)		

		-- insert idCertificate mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SC.idCertificate, '
				+ '		DC.idCertificate,'
				+ '		''certificate''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCertificate] DC '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCertificate] SC ON SC.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND  SC.name = DC.name collate database_default AND REPLACE(SC.filename, ''/_config/' + @siteHostname + '/_images/certs/'', '''') = DC.filename collate database_default AND SC.activationDate = DC.activationDate'
				+ ' WHERE DC.idSite IS NOT NULL AND DC.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SC.idCertificate IS NOT NULL AND DC.idCertificate IS NOT NULL'
		
		EXEC(@sql)

		-- tblCertificateLanguage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblCertificateLanguage]' 
				+ ' ( '
				+ '		idSite,'
				+ '		idCertificate,'
				+ '		idLanguage,'
				+ '		name,'
				+ '		description'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		C.idCertificate, '
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ', '
				+ '		C.name, '
				+ '		C.description '
				+ ' FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblCertificate] C'
				+ ' WHERE C.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
		
		EXEC(@sql)		

		-- create a table, and get certificate instance data mappings so certificate config JSON files can be written
		SET @sql = 'IF NOT EXISTS (SELECT * FROM [' + @destinationDBName + '].dbo.sysobjects WHERE name = N''InquisiqMigration_CertificateLayoutDataMappings'' AND xtype = ''U'')'
			+ ' BEGIN '
			+ 'CREATE TABLE [' + @destinationDBName + '].[dbo].[InquisiqMigration_CertificateLayoutDataMappings]'
			+ '('
			+ '    [destinationID] INT NOT NULL,'
			+ '    [data] NVARCHAR(MAX) NOT NULL'
			+ ')'
			+ ' END '
			+ ' ELSE '
		SET @sql = @sql + ' DELETE FROM [' + @destinationDBName + '].[dbo].[InquisiqMigration_CertificateLayoutDataMappings]'

		EXEC(@sql)
		
		-- get the certificate instance data		
		SET @sql = ' INSERT INTO [' + @destinationDBName + '].[dbo].[InquisiqMigration_CertificateLayoutDataMappings] '
				+ ' ( '
				+ '		destinationID,'
				+ '		data'
				+ ' ) '
				+ ' SELECT '
				+ '		DC.idCertificate, '
				+ '		REPLACE(REPLACE(SC.instanceData, ''%20'', '' ''), ''"ImageSource": "'' + SC.[filename] + ''"'', ''"ImageSource": "/_config/' + @destinationHostname + '/certificates/'' + CONVERT(NVARCHAR, DC.idCertificate) + ''/'' + DC.[filename] + ''"'') '
				--+ '		CASE WHEN LEFT(SC.instanceData,14) = ''<instanceData>'' THEN '
				--+ '			  ''{"Certificate" : '''
				--+ '			+ ''{ "Container" : '''
				--+ '			+ ''{ "Width" : "800px", "Height" : "600px", "ImageSource" : "'' + REPLACE(CAST(SC.[instanceData] AS XML).value(''(/instanceData/image)[1]'',''NVARCHAR(MAX)''),''_images/certs'',''certificates/'' + CONVERT(NVARCHAR, DC.idCertificate)) +''" },'''
				--+ '			+ ''"Elements": ['''
				--+ '			+ CASE when CAST(SC.[instanceData] AS XML).value(''(count(/instanceData/fields/field))'',''nvarchar(max)'') >= 1 then ''{"Identifier": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@columnName)[1]'',''nvarchar(max)'') + ''", "Value" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field)[1]'',''nvarchar(max)'') + ''", "IsStaticLabel" : "false", "Top": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@y)[1]'',''nvarchar(max)'') + ''", "Left" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@x)[1]'',''nvarchar(max)'') + ''", "Width" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@width)[1]'',''nvarchar(max)'') + ''", "FontSize" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@size)[1]'',''nvarchar(max)'') + ''", "Alignment" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@align)[1]'',''nvarchar(max)'') + ''"}'' else '''' end'
				--+ '			+ CASE when CAST(SC.[instanceData] AS XML).value(''(count(/instanceData/fields/field))'',''nvarchar(max)'') >= 2 then '', {"Identifier": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@columnName)[2]'',''nvarchar(max)'') + ''", "Value" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field)[2]'',''nvarchar(max)'') + ''", "IsStaticLabel" : "false", "Top": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@y)[2]'',''nvarchar(max)'') + ''", "Left" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@x)[2]'',''nvarchar(max)'') + ''", "Width" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@width)[2]'',''nvarchar(max)'') + ''", "FontSize" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@size)[2]'',''nvarchar(max)'') + ''", "Alignment" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@align)[2]'',''nvarchar(max)'') + ''"}'' else '''' end'
				--+ '			+ CASE when CAST(SC.[instanceData] AS XML).value(''(count(/instanceData/fields/field))'',''nvarchar(max)'') >= 3 then '', {"Identifier": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@columnName)[3]'',''nvarchar(max)'') + ''", "Value" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field)[3]'',''nvarchar(max)'') + ''", "IsStaticLabel" : "false", "Top": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@y)[3]'',''nvarchar(max)'') + ''", "Left" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@x)[3]'',''nvarchar(max)'') + ''", "Width" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@width)[3]'',''nvarchar(max)'') + ''", "FontSize" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@size)[3]'',''nvarchar(max)'') + ''", "Alignment" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@align)[3]'',''nvarchar(max)'') + ''"}'' else '''' end'
		--SET @sql2 = '		+ CASE when CAST(SC.[instanceData] AS XML).value(''(count(/instanceData/fields/field))'',''nvarchar(max)'') >= 4 then '', {"Identifier": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@columnName)[4]'',''nvarchar(max)'') + ''", "Value" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field)[4]'',''nvarchar(max)'') + ''", "IsStaticLabel" : "false", "Top": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@y)[4]'',''nvarchar(max)'') + ''", "Left" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@x)[4]'',''nvarchar(max)'') + ''", "Width" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@width)[4]'',''nvarchar(max)'') + ''", "FontSize" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@size)[4]'',''nvarchar(max)'') + ''", "Alignment" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@align)[4]'',''nvarchar(max)'') + ''"}'' else '''' end'
				--+ '			+ CASE when CAST(SC.[instanceData] AS XML).value(''(count(/instanceData/fields/field))'',''nvarchar(max)'') >= 5 then '', {"Identifier": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@columnName)[5]'',''nvarchar(max)'') + ''", "Value" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field)[5]'',''nvarchar(max)'') + ''", "IsStaticLabel" : "false", "Top": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@y)[5]'',''nvarchar(max)'') + ''", "Left" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@x)[5]'',''nvarchar(max)'') + ''", "Width" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@width)[5]'',''nvarchar(max)'') + ''", "FontSize" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@size)[5]'',''nvarchar(max)'') + ''", "Alignment" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@align)[5]'',''nvarchar(max)'') + ''"}'' else '''' end'
				--+ '			+ CASE when CAST(SC.[instanceData] AS XML).value(''(count(/instanceData/fields/field))'',''nvarchar(max)'') >= 6 then '', {"Identifier": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@columnName)[6]'',''nvarchar(max)'') + ''", "Value" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field)[6]'',''nvarchar(max)'') + ''", "IsStaticLabel" : "false", "Top": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@y)[6]'',''nvarchar(max)'') + ''", "Left" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@x)[6]'',''nvarchar(max)'') + ''", "Width" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@width)[6]'',''nvarchar(max)'') + ''", "FontSize" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@size)[6]'',''nvarchar(max)'') + ''", "Alignment" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@align)[6]'',''nvarchar(max)'') + ''"}'' else '''' end'
				--+ '			+ CASE when CAST(SC.[instanceData] AS XML).value(''(count(/instanceData/fields/field))'',''nvarchar(max)'') >= 7 then '', {"Identifier": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@columnName)[7]'',''nvarchar(max)'') + ''", "Value" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field)[7]'',''nvarchar(max)'') + ''", "IsStaticLabel" : "false", "Top": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@y)[7]'',''nvarchar(max)'') + ''", "Left" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@x)[7]'',''nvarchar(max)'') + ''", "Width" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@width)[7]'',''nvarchar(max)'') + ''", "FontSize" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@size)[7]'',''nvarchar(max)'') + ''", "Alignment" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@align)[7]'',''nvarchar(max)'') + ''"}'' else '''' end'
				--+ '			+'']}}'''    
				--+ '		ELSE SC.instanceData END '
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCertificate] DC '
				+ ' LEFT JOIN  [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.destinationID = DC.idCertificate AND CM.object = ''certificate'''
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCertificate] SC ON SC.idCertificate = CM.sourceID'				
				+ ' WHERE DC.idSite IS NOT NULL AND DC.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
				+ ' AND SC.idSite IS NOT NULL AND SC.idSite = '+ CONVERT(NVARCHAR, @idSiteSource)
	
		EXEC (@sql)
		
		-- tblCertificateRecord - this may need a clean-up after migration as there may be "duplicate" certificate record entries
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCertificateRecord]'
				+ ' ( '
				+ '		idSite, '
				+ '		idCertificate, '
				+ '		idUser, '
				+ '		idTimezone, '
				+ '		idAwardedBy, '
				+ '		timestamp, '
				+ '		expires, '
				+ '		code, '
				+ '		credits,'
				+ '		awardData,'
				+ '		idCertificateImport'
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','								-- idSite
				+ '		CM.destinationID,'															-- certificate id
				+ '		UM.destinationID,'															-- user id
				+ '		CASE WHEN TZ.destinationID > 0 THEN TZ.destinationID ELSE 15 END,'			-- timezone
				+ '		1,'																			-- awarded by id
				+ '		CR.timestamp,'																-- timestamp
				+ '		CR.expires,'																-- expires
				+ '		CR.code,'																	-- code
				+ '		CR.credits,'																-- credits
				+ '		NULL,'																		-- award data
				+ '		NULL'																		-- certificate import
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCertificateRecord] CR '				
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.sourceID = CR.idCertificate AND CM.object = ''certificate'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] UM ON UM.sourceID = CR.idUser AND UM.object = ''user'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_TimezoneIDMappings] TZ ON TZ.sourceID = CR.idTimezone '
				+ ' WHERE CM.destinationID IS NOT NULL AND UM.destinationID IS NOT NULL'
		
		EXEC (@sql)	

	SET @Return_Code = 0
	SET @Error_Description_Code	= 'SystemCopyInquisiqToAsentiaCertificate_Success'
		
	SET XACT_ABORT OFF

END

GO