﻿SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[GetHashedString]') AND xtype IN (N'FN', N'IF', N'TF'))
	DROP FUNCTION [GetHashedString]
GO

/*
  Hashes a string using a specified hashing algorithm
  and returns the hashed string value.
*/
CREATE FUNCTION [GetHashedString]
(
	@hashingAlgorithm		NVARCHAR(8),
	@stringToHash			NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	-- validate hash algorithm variable, valid values are
	-- MD2, MD4, MD5, SHA, SHA1, SHA2_256, and SHA2_512
	IF (@hashingAlgorithm <> 'MD2' AND
		@hashingAlgorithm <> 'MD4' AND
		@hashingAlgorithm <> 'MD5' AND
		@hashingAlgorithm <> 'SHA' AND
		@hashingAlgorithm <> 'SHA1' AND
		@hashingAlgorithm <> 'SHA2_256' AND
		@hashingAlgorithm <> 'SHA2_512')
		BEGIN
			-- default hashing algorithm to SHA1
			SET @hashingAlgorithm = 'SHA1'
		END
		
	-- hash the string
	DECLARE @passwordHash VARBINARY(MAX)
	SELECT @passwordHash = HASHBYTES(@hashingAlgorithm, @stringToHash)
 
	--return the hashed string as an nvarchar
    --RETURN CONVERT(NVARCHAR(MAX), @passwordHash, 2) -- SQL SERVER 2008+ ONLY
    RETURN CAST('' AS XML).value('xs:hexBinary(sql:variable("@passwordHash"))', 'NVARCHAR(MAX)') -- COMPATIBLE WITH SQL SERVER 2005
END
GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[DelimitedStringToTable]') AND xtype IN (N'FN', N'IF', N'TF'))
	DROP FUNCTION [DelimitedStringToTable]
GO

/*

Takes in a delimited string and converts it to a table.

*/

CREATE FUNCTION [DelimitedStringToTable]
(
	@string			NVARCHAR(MAX),
	@delimiter		NVARCHAR(10)
)
RETURNS @table TABLE (s NVARCHAR(MAX))
AS
BEGIN

	DECLARE @i INT, @j INT

	SELECT @i = 1

	WHILE @i <= DATALENGTH(@string)
		BEGIN

		SELECT @j = CHARINDEX(@delimiter, @string, @i)
		
		IF @j = 0
			BEGIN
			SELECT @j = DATALENGTH(@string) + 1
			END
		
		INSERT @table SELECT LTRIM(RTRIM(SUBSTRING(@string, @i, @j - @i)))
		
		SELECT @i = @j + LEN(@delimiter)
		
		END

	RETURN

END
GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[RawKBToFriendlyFormat]') AND xtype IN (N'FN', N'IF', N'TF'))
	DROP FUNCTION [RawKBToFriendlyFormat]
GO

/*
  Takes in a raw number representing KBs, and converts to a 
  string that reads in a friendly KB, MB, GB format.
*/
CREATE FUNCTION [RawKBToFriendlyFormat]
(
	@kb		INT
)
RETURNS NVARCHAR(20)
AS
BEGIN
	DECLARE @div		INT
	DECLARE @counter	INT
	DECLARE @unit		NVARCHAR(3)

	DECLARE @sizes		TABLE (id INT, unit NVARCHAR(3))

	INSERT INTO @sizes 
		(id, unit)
		  SELECT 1, 'KB'
	UNION SELECT 2, 'MB'
	UNION SELECT 3, 'GB'
	UNION SELECT 4, 'TB'

	SET @div = 1024
	SET @counter = 1

	WHILE @kb >= 1 AND @counter <= 4
		BEGIN

		IF @kb / @div < 1
			BREAK
		ELSE
			SET @kb = @kb / @div
			SET @counter += 1

		END

	SELECT @unit = unit
	FROM @sizes
	WHERE id = @counter

	RETURN CAST(ROUND(@kb, 2) AS NVARCHAR(17)) + ' ' + @unit

END
GO
