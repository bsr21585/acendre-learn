SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[GetHashedString]') AND xtype IN (N'FN', N'IF', N'TF'))
	DROP FUNCTION [GetHashedString]
GO

/*
  Hashes a string using a specified hashing algorithm
  and returns the hashed string value.
*/
CREATE FUNCTION [GetHashedString]
(
	@hashingAlgorithm		NVARCHAR(8),
	@stringToHash			NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	-- validate hash algorithm variable, valid values are
	-- MD2, MD4, MD5, SHA, SHA1, SHA2_256, and SHA2_512
	IF (@hashingAlgorithm <> 'MD2' AND
		@hashingAlgorithm <> 'MD4' AND
		@hashingAlgorithm <> 'MD5' AND
		@hashingAlgorithm <> 'SHA' AND
		@hashingAlgorithm <> 'SHA1' AND
		@hashingAlgorithm <> 'SHA2_256' AND
		@hashingAlgorithm <> 'SHA2_512')
		BEGIN
			-- default hashing algorithm to SHA1
			SET @hashingAlgorithm = 'SHA1'
		END
		
	-- hash the string
	DECLARE @passwordHash VARBINARY(MAX)
	SELECT @passwordHash = HASHBYTES(@hashingAlgorithm, @stringToHash)
 
	--return the hashed string as an nvarchar
    --RETURN CONVERT(NVARCHAR(MAX), @passwordHash, 2) -- SQL SERVER 2008+ ONLY
    RETURN CAST('' AS XML).value('xs:hexBinary(sql:variable("@passwordHash"))', 'NVARCHAR(MAX)') -- COMPATIBLE WITH SQL SERVER 2005
END
GO