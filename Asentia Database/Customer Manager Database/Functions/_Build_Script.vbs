dim fso			: set fso 		= createObject("scripting.filesystemobject")
dim script_list_file	: script_list_file 	= "_execution order.txt"
dim script_list		: set script_list 	= createObject("scripting.dictionary")

dim script_list_file_string	: script_list_file_string = readTextFile(script_list_file)
dim script_list_file_array	: script_list_file_array = split(script_list_file_string, vbcrlf)

dim output_stream	: set output_stream = createObject("ADODB.Stream")
	output_stream.charset = "utf-8"
	output_stream.type = 2
	output_stream.lineSeparator = -1
	output_stream.open

dim s	: s = 1

for s = 1 to ubound(script_list_file_array)

	if instr(script_list_file_array(s), ".sql") > 0 then 

		output_stream.writeText(readTextFile(script_list_file_array(s))), 1

	end if

next

output_stream.saveToFile "_FUNCTIONS.sql", 2

output_stream.close()

function readTextFile(pPath)

	dim stream	: set stream = createObject("ADODB.Stream")
	
	stream.charSet = "utf-8"
	stream.open
	stream.loadFromFile(pPath)
	stream.position = 0
	
	readTextFile = stream.readText(stream.size)
	
	set stream = nothing
    
end function


