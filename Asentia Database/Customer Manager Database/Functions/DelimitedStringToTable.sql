SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[DelimitedStringToTable]') AND xtype IN (N'FN', N'IF', N'TF'))
	DROP FUNCTION [DelimitedStringToTable]
GO

/*

Takes in a delimited string and converts it to a table.

*/

CREATE FUNCTION [DelimitedStringToTable]
(
	@string			NVARCHAR(MAX),
	@delimiter		NVARCHAR(10)
)
RETURNS @table TABLE (s NVARCHAR(MAX))
AS
BEGIN

	DECLARE @i INT, @j INT

	SELECT @i = 1

	WHILE @i <= DATALENGTH(@string)
		BEGIN

		SELECT @j = CHARINDEX(@delimiter, @string, @i)
		
		IF @j = 0
			BEGIN
			SELECT @j = DATALENGTH(@string) + 1
			END
		
		INSERT @table SELECT LTRIM(RTRIM(SUBSTRING(@string, @i, @j - @i)))
		
		SELECT @i = @j + LEN(@delimiter)
		
		END

	RETURN

END
GO