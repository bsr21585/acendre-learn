SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[RawKBToFriendlyFormat]') AND xtype IN (N'FN', N'IF', N'TF'))
	DROP FUNCTION [RawKBToFriendlyFormat]
GO

/*
  Takes in a raw number representing KBs, and converts to a 
  string that reads in a friendly KB, MB, GB format.
*/
CREATE FUNCTION [RawKBToFriendlyFormat]
(
	@kb		INT
)
RETURNS NVARCHAR(20)
AS
BEGIN
	DECLARE @div		INT
	DECLARE @counter	INT
	DECLARE @unit		NVARCHAR(3)

	DECLARE @sizes		TABLE (id INT, unit NVARCHAR(3))

	INSERT INTO @sizes 
		(id, unit)
		  SELECT 1, 'KB'
	UNION SELECT 2, 'MB'
	UNION SELECT 3, 'GB'
	UNION SELECT 4, 'TB'

	SET @div = 1024
	SET @counter = 1

	WHILE @kb >= 1 AND @counter <= 4
		BEGIN

		IF @kb / @div < 1
			BREAK
		ELSE
			SET @kb = @kb / @div
			SET @counter += 1

		END

	SELECT @unit = unit
	FROM @sizes
	WHERE id = @counter

	RETURN CAST(ROUND(@kb, 2) AS NVARCHAR(17)) + ' ' + @unit

END
GO