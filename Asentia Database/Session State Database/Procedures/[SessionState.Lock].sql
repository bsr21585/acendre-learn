-- =====================================================================
-- PROCEDURE: [SessionState.Lock]

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[SessionState.Lock]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [SessionState.Lock]
GO

/*
Locks the session state record.
*/
CREATE PROCEDURE [SessionState.Lock]
(
	@Return_Code		INT				OUTPUT	,

	@sessionId			NVARCHAR(80)
)
AS
	BEGIN
		SET NOCOUNT ON
		
		DECLARE @utcNow DATETIME
		SET @utcNow = GETUTCDATE()
		
		-- check that the session state record exists
		IF (SELECT COUNT(1) FROM tblSessionState
			WHERE sessionId = @sessionId
			AND dtExpires > @utcNow) < 1
			BEGIN
			
			SELECT @Return_Code = 1 --(1 is 'details not found')
			RETURN
			
			END

		-- lock the session state record
		UPDATE tblSessionState SET
			isLocked = 1,
			dtLocked = @utcNow
		WHERE sessionId = @sessionId
		AND dtExpires > @utcNow
		
		SELECT @Return_Code = 0 --(0 is 'success')
	END
GO