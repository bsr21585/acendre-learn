-- =====================================================================
-- PROCEDURE: [SessionState.ResetTimeout]

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[SessionState.ResetTimeout]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [SessionState.ResetTimeout]
GO

/*
Resets the timeout for a session state record.
*/
CREATE PROCEDURE [SessionState.ResetTimeout]
(
	@Return_Code		INT				OUTPUT	,

	@sessionId			NVARCHAR(80)			,
	@timeout			INT
)
AS
	BEGIN
		SET NOCOUNT ON
		
		DECLARE @utcNow DATETIME
		SET @utcNow = GETUTCDATE()
		
		-- check that the session state record exists
		IF (SELECT COUNT(1) FROM tblSessionState
			WHERE sessionId = @sessionId) < 1
			BEGIN
			
			SELECT @Return_Code = 1 --(1 is 'details not found')
			RETURN
			
			END

		-- reset the timeout for the session state record
		UPDATE tblSessionState SET
			dtExpires = DATEADD(MINUTE, @timeout, @utcNow),
			[timeout] = @timeout
		WHERE sessionId = @sessionId
		
		SELECT @Return_Code = 0 --(0 is 'success')
	END
GO