-- =====================================================================
-- PROCEDURE: [SessionState.SetLockId]

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[SessionState.SetLockId]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [SessionState.SetLockId]
GO

/*
Sets the lock id on a session state record.
*/
CREATE PROCEDURE [SessionState.SetLockId]
(
	@Return_Code		INT				OUTPUT	,

	@sessionId			NVARCHAR(80)			,
	@lockId				INT
)
AS
	BEGIN
		SET NOCOUNT ON
		
		-- check that the session state record exists
		IF (SELECT COUNT(1) FROM tblSessionState
			WHERE sessionId = @sessionId) < 1
			BEGIN
			
			SELECT @Return_Code = 1 --(1 is 'details not found')
			RETURN
			
			END

		-- set the lock id
		UPDATE tblSessionState SET
			lockId = @lockId,
			flags = 0
		WHERE sessionId = @sessionId
		
		SELECT @Return_Code = 0 --(0 is 'success')
	END
GO