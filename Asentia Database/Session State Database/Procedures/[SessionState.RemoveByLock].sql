-- =====================================================================
-- PROCEDURE: [SessionState.RemoveByLock]

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[SessionState.RemoveByLock]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [SessionState.RemoveByLock]
GO

/*
Removes a session state record by lock id.
*/
CREATE PROCEDURE [SessionState.RemoveByLock]
(
	@Return_Code		INT				OUTPUT	,

	@sessionId			NVARCHAR(80)			,
	@lockId				INT
)
AS
	BEGIN
		SET NOCOUNT ON
		
		-- check that the session state record exists
		IF (SELECT COUNT(1) FROM tblSessionState
			WHERE sessionId = @sessionId
			AND lockId = @lockId) < 1
			BEGIN
			
			SELECT @Return_Code = 1 --(1 is 'details not found')
			RETURN
			
			END

		-- remove session
		DELETE FROM tblSessionState
		WHERE sessionId = @sessionId
		AND lockId = @lockId
		
		SELECT @Return_Code = 0 --(0 is 'success')
	END
GO