-- =====================================================================
-- PROCEDURE: [SessionState.Remove]

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[SessionState.Remove]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [SessionState.Remove]
GO

/*
Removes a session state record.
*/
CREATE PROCEDURE [SessionState.Remove]
(
	@Return_Code		INT				OUTPUT,	
	@sessionId			NVARCHAR(80)
)
AS
	BEGIN
	SET NOCOUNT ON				

		-- remove session
		DELETE FROM tblSessionState WHERE sessionId = @sessionId
		
		-- return
		SET @Return_Code = 0 --(0 is 'success')
	END
GO