-- =====================================================================
-- PROCEDURE: [SessionState.RemoveExpired]

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[SessionState.RemoveExpired]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [SessionState.RemoveExpired]
GO

/*
Removes expired session state records.
*/
CREATE PROCEDURE [SessionState.RemoveExpired]
(
	@Return_Code		INT				OUTPUT
)
AS
	BEGIN
		SET NOCOUNT ON
		
		DECLARE @utcNow DATETIME
		SET @utcNow = GETUTCDATE()
			
		-- globally clear expired sessions
		DELETE FROM tblSessionState 
		WHERE dtExpires < @utcNow
		
		SELECT @Return_Code = 0 --(0 is 'success')
	END
GO


