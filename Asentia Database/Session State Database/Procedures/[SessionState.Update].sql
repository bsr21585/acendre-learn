-- =====================================================================
-- PROCEDURE: [SessionState.Update]

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[SessionState.Update]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [SessionState.Update]
GO

/*
Updates a session state record.
*/
CREATE PROCEDURE [SessionState.Update]
(
	@Return_Code		INT				OUTPUT	,

	@sessionId			NVARCHAR(80)			,
	@lockId				INT						,
	@timeout			INT						,
	@isLocked			BIT						,
	@sessionItems		NVARCHAR(MAX)
)
AS
	BEGIN
		SET NOCOUNT ON
		
		DECLARE @utcNow DATETIME
		SET @utcNow = GETUTCDATE()
		
		-- check that the session state record exists
		IF (SELECT COUNT(1) FROM tblSessionState
			WHERE sessionId = @sessionId
			AND lockId = @lockId) < 1
			BEGIN
			
			SELECT @Return_Code = 1 --(1 is 'details not found')
			RETURN
			
			END

		-- update the session item
		UPDATE tblSessionState SET
			dtExpires = DATEADD(MINUTE, @timeout, @utcNow),
			isLocked = @isLocked,
			[timeout] = @timeout,
			sessionItems = @sessionItems
		WHERE sessionId = @sessionId
		AND lockId = @lockId
		
		SELECT @Return_Code = 0 --(0 is 'success')
	END
GO