-- =====================================================================
-- PROCEDURE: [SessionState.Get]

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[SessionState.Get]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [SessionState.Get]
GO

/*
Returns session state properties.
*/
CREATE PROCEDURE [SessionState.Get]
(
	@Return_Code		INT				OUTPUT	,
	
	@sessionId			NVARCHAR(80)			,
	@isExpired			BIT				OUTPUT	,
	@sessionItems		NVARCHAR(MAX)	OUTPUT	,
	@isLocked			BIT				OUTPUT	,
	@lockId				INT				OUTPUT	,
	@dtLocked			DATETIME		OUTPUT	,
	@utcNow				DATETIME		OUTPUT	,
	@flags				INT				OUTPUT	,
	@timeout			INT				OUTPUT	,
	@application		NVARCHAR(255)	OUTPUT	,
	@dtExpires			DATETIME		OUTPUT
)
AS
	BEGIN
		SET NOCOUNT ON
		
		SET @utcNow = GETUTCDATE()
		
		SELECT
			@isExpired = CASE WHEN dtExpires < @utcNow THEN 1 ELSE 0 END,
			@sessionItems = SS.sessionItems,
			@isLocked = SS.isLocked,
			@lockId = SS.lockId,
			@dtLocked = SS.dtLocked,
			@flags = SS.flags,
			@timeout = SS.[timeout],
			@application = SS.[application],
			@dtExpires	= SS.dtExpires
		FROM tblSessionState SS
		WHERE
			SS.sessionId = @sessionId
		IF @@ROWCOUNT = 0
			SELECT @Return_Code = 1
		ELSE
			SELECT @Return_Code = 0
	END
GO