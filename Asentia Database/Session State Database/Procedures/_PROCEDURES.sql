﻿-- =====================================================================
-- PROCEDURE: [SessionState.Get]

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[SessionState.Get]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [SessionState.Get]
GO

/*
Returns session state properties.
*/
CREATE PROCEDURE [SessionState.Get]
(
	@Return_Code		INT				OUTPUT	,
	
	@sessionId			NVARCHAR(80)			,
	@isExpired			BIT				OUTPUT	,
	@sessionItems		NVARCHAR(MAX)	OUTPUT	,
	@isLocked			BIT				OUTPUT	,
	@lockId				INT				OUTPUT	,
	@dtLocked			DATETIME		OUTPUT	,
	@utcNow				DATETIME		OUTPUT	,
	@flags				INT				OUTPUT	,
	@timeout			INT				OUTPUT	,
	@application		NVARCHAR(255)	OUTPUT	,
	@dtExpires			DATETIME		OUTPUT
)
AS
	BEGIN
		SET NOCOUNT ON
		
		SET @utcNow = GETUTCDATE()
		
		SELECT
			@isExpired = CASE WHEN dtExpires < @utcNow THEN 1 ELSE 0 END,
			@sessionItems = SS.sessionItems,
			@isLocked = SS.isLocked,
			@lockId = SS.lockId,
			@dtLocked = SS.dtLocked,
			@flags = SS.flags,
			@timeout = SS.[timeout],
			@application = SS.[application],
			@dtExpires	= SS.dtExpires
		FROM tblSessionState SS
		WHERE
			SS.sessionId = @sessionId
		IF @@ROWCOUNT = 0
			SELECT @Return_Code = 1
		ELSE
			SELECT @Return_Code = 0
	END
GO
-- =====================================================================
-- PROCEDURE: [SessionState.Initialize]

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[SessionState.Initialize]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [SessionState.Initialize]
GO

/*
Adds a new session state record.
*/
CREATE PROCEDURE [SessionState.Initialize]
(
	@Return_Code		INT				OUTPUT	,

	@sessionId			NVARCHAR(80)			,
	@timeout			INT						,
	@sessionItems		NVARCHAR(MAX)			,
	@flags				INT						,
	@application		NVARCHAR(255)
)
AS
	BEGIN
		SET NOCOUNT ON
		
		DECLARE @utcNow DATETIME
		SET @utcNow = GETUTCDATE()
		
		-- insert the new session state record
		INSERT INTO tblSessionState
		(
			sessionId,
			dtCreated,
			dtExpires,
			dtLocked,
			lockId,
			[timeout],
			isLocked,
			sessionItems,
			flags,
			[application]
		)
		VALUES
		(
			@sessionId,
			@utcNow,
			DATEADD(MINUTE, @timeout, @utcNow),
			@utcNow,
			0,
			@timeout,
			0,
			@sessionItems,
			@flags,
			@application
		)
		
		SELECT @Return_Code = 0 --(0 is 'success')
	END
GO



-- =====================================================================
-- PROCEDURE: [SessionState.Lock]

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[SessionState.Lock]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [SessionState.Lock]
GO

/*
Locks the session state record.
*/
CREATE PROCEDURE [SessionState.Lock]
(
	@Return_Code		INT				OUTPUT	,

	@sessionId			NVARCHAR(80)
)
AS
	BEGIN
		SET NOCOUNT ON
		
		DECLARE @utcNow DATETIME
		SET @utcNow = GETUTCDATE()
		
		-- check that the session state record exists
		IF (SELECT COUNT(1) FROM tblSessionState
			WHERE sessionId = @sessionId
			AND dtExpires > @utcNow) < 1
			BEGIN
			
			SELECT @Return_Code = 1 --(1 is 'details not found')
			RETURN
			
			END

		-- lock the session state record
		UPDATE tblSessionState SET
			isLocked = 1,
			dtLocked = @utcNow
		WHERE sessionId = @sessionId
		AND dtExpires > @utcNow
		
		SELECT @Return_Code = 0 --(0 is 'success')
	END
GO
-- =====================================================================
-- PROCEDURE: [SessionState.ReleaseLock]

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[SessionState.ReleaseLock]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [SessionState.ReleaseLock]
GO

/*
Releases the lock on the session state record.
*/
CREATE PROCEDURE [SessionState.ReleaseLock]
(
	@Return_Code		INT				OUTPUT	,

	@sessionId			NVARCHAR(80)			,
	@lockId				INT
)
AS
	BEGIN
		SET NOCOUNT ON
		
		DECLARE @utcNow DATETIME
		SET @utcNow = GETUTCDATE()
		
		-- check that the session state record exists
		IF (SELECT COUNT(1) FROM tblSessionState
			WHERE sessionId = @sessionId
			AND lockId = @lockId) < 1
			BEGIN
			
			SELECT @Return_Code = 1 --(1 is 'details not found')
			RETURN
			
			END

		-- release the lock on the session state record
		UPDATE tblSessionState SET
			isLocked = 0,
			dtExpires = DATEADD(MINUTE, [timeout], @utcNow)
		FROM tblSessionState SS
		WHERE sessionId = @sessionId
		AND lockId = @lockId
		
		SELECT @Return_Code = 0 --(0 is 'success')
	END
GO
-- =====================================================================
-- PROCEDURE: [SessionState.RemoveByLock]

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[SessionState.RemoveByLock]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [SessionState.RemoveByLock]
GO

/*
Removes a session state record by lock id.
*/
CREATE PROCEDURE [SessionState.RemoveByLock]
(
	@Return_Code		INT				OUTPUT	,

	@sessionId			NVARCHAR(80)			,
	@lockId				INT
)
AS
	BEGIN
		SET NOCOUNT ON
		
		-- check that the session state record exists
		IF (SELECT COUNT(1) FROM tblSessionState
			WHERE sessionId = @sessionId
			AND lockId = @lockId) < 1
			BEGIN
			
			SELECT @Return_Code = 1 --(1 is 'details not found')
			RETURN
			
			END

		-- remove session
		DELETE FROM tblSessionState
		WHERE sessionId = @sessionId
		AND lockId = @lockId
		
		SELECT @Return_Code = 0 --(0 is 'success')
	END
GO
-- =====================================================================
-- PROCEDURE: [SessionState.RemoveExpired]

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[SessionState.RemoveExpired]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [SessionState.RemoveExpired]
GO

/*
Removes expired session state records.
*/
CREATE PROCEDURE [SessionState.RemoveExpired]
(
	@Return_Code		INT				OUTPUT
)
AS
	BEGIN
		SET NOCOUNT ON
		
		DECLARE @utcNow DATETIME
		SET @utcNow = GETUTCDATE()
			
		-- globally clear expired sessions
		DELETE FROM tblSessionState 
		WHERE dtExpires < @utcNow
		
		SELECT @Return_Code = 0 --(0 is 'success')
	END
GO



-- =====================================================================
-- PROCEDURE: [SessionState.Remove]

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[SessionState.Remove]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [SessionState.Remove]
GO

/*
Removes a session state record.
*/
CREATE PROCEDURE [SessionState.Remove]
(
	@Return_Code		INT				OUTPUT,	
	@sessionId			NVARCHAR(80)
)
AS
	BEGIN
	SET NOCOUNT ON				

		-- remove session
		DELETE FROM tblSessionState WHERE sessionId = @sessionId
		
		-- return
		SET @Return_Code = 0 --(0 is 'success')
	END
GO
-- =====================================================================
-- PROCEDURE: [SessionState.ResetTimeout]

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[SessionState.ResetTimeout]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [SessionState.ResetTimeout]
GO

/*
Resets the timeout for a session state record.
*/
CREATE PROCEDURE [SessionState.ResetTimeout]
(
	@Return_Code		INT				OUTPUT	,

	@sessionId			NVARCHAR(80)			,
	@timeout			INT
)
AS
	BEGIN
		SET NOCOUNT ON
		
		DECLARE @utcNow DATETIME
		SET @utcNow = GETUTCDATE()
		
		-- check that the session state record exists
		IF (SELECT COUNT(1) FROM tblSessionState
			WHERE sessionId = @sessionId) < 1
			BEGIN
			
			SELECT @Return_Code = 1 --(1 is 'details not found')
			RETURN
			
			END

		-- reset the timeout for the session state record
		UPDATE tblSessionState SET
			dtExpires = DATEADD(MINUTE, @timeout, @utcNow),
			[timeout] = @timeout
		WHERE sessionId = @sessionId
		
		SELECT @Return_Code = 0 --(0 is 'success')
	END
GO
-- =====================================================================
-- PROCEDURE: [SessionState.SetLockId]

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[SessionState.SetLockId]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [SessionState.SetLockId]
GO

/*
Sets the lock id on a session state record.
*/
CREATE PROCEDURE [SessionState.SetLockId]
(
	@Return_Code		INT				OUTPUT	,

	@sessionId			NVARCHAR(80)			,
	@lockId				INT
)
AS
	BEGIN
		SET NOCOUNT ON
		
		-- check that the session state record exists
		IF (SELECT COUNT(1) FROM tblSessionState
			WHERE sessionId = @sessionId) < 1
			BEGIN
			
			SELECT @Return_Code = 1 --(1 is 'details not found')
			RETURN
			
			END

		-- set the lock id
		UPDATE tblSessionState SET
			lockId = @lockId,
			flags = 0
		WHERE sessionId = @sessionId
		
		SELECT @Return_Code = 0 --(0 is 'success')
	END
GO
-- =====================================================================
-- PROCEDURE: [SessionState.Update]

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[SessionState.Update]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [SessionState.Update]
GO

/*
Updates a session state record.
*/
CREATE PROCEDURE [SessionState.Update]
(
	@Return_Code		INT				OUTPUT	,

	@sessionId			NVARCHAR(80)			,
	@lockId				INT						,
	@timeout			INT						,
	@isLocked			BIT						,
	@sessionItems		NVARCHAR(MAX)
)
AS
	BEGIN
		SET NOCOUNT ON
		
		DECLARE @utcNow DATETIME
		SET @utcNow = GETUTCDATE()
		
		-- check that the session state record exists
		IF (SELECT COUNT(1) FROM tblSessionState
			WHERE sessionId = @sessionId
			AND lockId = @lockId) < 1
			BEGIN
			
			SELECT @Return_Code = 1 --(1 is 'details not found')
			RETURN
			
			END

		-- update the session item
		UPDATE tblSessionState SET
			dtExpires = DATEADD(MINUTE, @timeout, @utcNow),
			isLocked = @isLocked,
			[timeout] = @timeout,
			sessionItems = @sessionItems
		WHERE sessionId = @sessionId
		AND lockId = @lockId
		
		SELECT @Return_Code = 0 --(0 is 'success')
	END
GO
