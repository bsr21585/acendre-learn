-- =====================================================================
-- PROCEDURE: [SessionState.Initialize]

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[SessionState.Initialize]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [SessionState.Initialize]
GO

/*
Adds a new session state record.
*/
CREATE PROCEDURE [SessionState.Initialize]
(
	@Return_Code		INT				OUTPUT	,

	@sessionId			NVARCHAR(80)			,
	@timeout			INT						,
	@sessionItems		NVARCHAR(MAX)			,
	@flags				INT						,
	@application		NVARCHAR(255)
)
AS
	BEGIN
		SET NOCOUNT ON
		
		DECLARE @utcNow DATETIME
		SET @utcNow = GETUTCDATE()
		
		-- insert the new session state record
		INSERT INTO tblSessionState
		(
			sessionId,
			dtCreated,
			dtExpires,
			dtLocked,
			lockId,
			[timeout],
			isLocked,
			sessionItems,
			flags,
			[application]
		)
		VALUES
		(
			@sessionId,
			@utcNow,
			DATEADD(MINUTE, @timeout, @utcNow),
			@utcNow,
			0,
			@timeout,
			0,
			@sessionItems,
			@flags,
			@application
		)
		
		SELECT @Return_Code = 0 --(0 is 'success')
	END
GO


