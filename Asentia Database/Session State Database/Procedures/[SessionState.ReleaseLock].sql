-- =====================================================================
-- PROCEDURE: [SessionState.ReleaseLock]

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[SessionState.ReleaseLock]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [SessionState.ReleaseLock]
GO

/*
Releases the lock on the session state record.
*/
CREATE PROCEDURE [SessionState.ReleaseLock]
(
	@Return_Code		INT				OUTPUT	,

	@sessionId			NVARCHAR(80)			,
	@lockId				INT
)
AS
	BEGIN
		SET NOCOUNT ON
		
		DECLARE @utcNow DATETIME
		SET @utcNow = GETUTCDATE()
		
		-- check that the session state record exists
		IF (SELECT COUNT(1) FROM tblSessionState
			WHERE sessionId = @sessionId
			AND lockId = @lockId) < 1
			BEGIN
			
			SELECT @Return_Code = 1 --(1 is 'details not found')
			RETURN
			
			END

		-- release the lock on the session state record
		UPDATE tblSessionState SET
			isLocked = 0,
			dtExpires = DATEADD(MINUTE, [timeout], @utcNow)
		FROM tblSessionState SS
		WHERE sessionId = @sessionId
		AND lockId = @lockId
		
		SELECT @Return_Code = 0 --(0 is 'success')
	END
GO