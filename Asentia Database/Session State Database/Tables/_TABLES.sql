﻿IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblSessionState]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblSessionState] (
	[sessionId]			[nvarchar](80)	NOT NULL,
	[dtCreated]			[datetime]		NOT NULL,
	[dtExpires]			[datetime]		NOT NULL,
	[isLocked]			[bit]			NOT NULL,
	[dtLocked]			[datetime]		NOT NULL,
	[lockId]			[int]			NOT NULL,
	[timeout]			[int]			NOT NULL,
	[sessionItems]		[nvarchar](MAX) NULL	,
	[flags]				[int]			NOT NULL,
	[application]		[nvarchar](255) NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_SessionState]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblSessionState ADD CONSTRAINT [PK_SessionState] PRIMARY KEY CLUSTERED (sessionId)
